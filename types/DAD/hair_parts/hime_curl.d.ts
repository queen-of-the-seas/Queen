import {HairPart} from "./hair_part";

export class HimeCurlMedium extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}


export class HimeCurlSide extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}
