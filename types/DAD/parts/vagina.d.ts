import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {BodyPart} from "./part";

declare class Vagina extends BodyPart {
	constructor(...data: object[]);
}


export class VaginaHuman extends Vagina {
	constructor(...data: object[]);

	stroke(): string;
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
