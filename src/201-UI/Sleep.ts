namespace App.UI.Passages {
	class Sleep extends DOMPassage {
		constructor() {
			super("Sleep");
			this._intro = [
				"Night finally comes and you drag yourself to bed.",
				"The late evening relieves you of further responsibilities, so you collapse into your rickety bed.",
				"The day is done and over, for better or worse. You fall into bed.",
			];
		}

		override render(): DocumentFragment {
			const res = new DocumentFragment();
			const today = setup.world.day;
			$(res).wiki(this._intro.randomElement(), '\n\n',
				Sleep._renderSection(Notifications.MessageCategory.Dreams, today,
					"Dreaming", "white", "steelblue", "No dreams tonight."), '\n',
				Sleep._renderSection(Notifications.MessageCategory.StatChange, today,
					"Physical and Mental Changes", "white", "maroon", "No body changes tonight."), '\n',
				Sleep._renderSection(Notifications.MessageCategory.StatusChange, today,
					"Status Changes", "white", "indigo", "Everything seems normal."), '\n',
				Sleep._renderSection(Notifications.MessageCategory.Knowledge, today,
					"Knowledge gains", "white", "seagreen", "No knowledge gains tonight."),
				"\n\nYou wake up and are ready to start the day.\n\n",
				UI.pInteractLinkStrip([["Wake up", "Cabin"]], null));
			return res;
		}

		private static _renderSection(category: Notifications.MessageCategory, day: number,
			header: string, color: string, bgColor: string, placeholder?: string) {
			const out = setup.notifications.strPrint(category, day, header, color, bgColor);
			if (out.length > 0) {
				return out + '<br/>';
			}
			return (placeholder === undefined) ? '' : Notifications.Engine.header(header, color, bgColor) + placeholder + '<br/>';
		}

		private readonly _intro: string[];
	}

	new Sleep()
}
