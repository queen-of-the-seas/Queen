namespace App.Data.Combat {
	moves[App.Combat.Style.Siren] = {
		moves: {
			[App.Combat.Moves.Siren.Touch]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 5,
				combo: 0, // Costs no combo points
				speed: 10,
				damage: 1.0,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME reaches for you, but misses!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME lands an icy touch on you, so cold it burns!",
					],
				],
			},
			[App.Combat.Moves.Siren.Toss]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0, // Costs no combo points
				speed: 10,
				damage: 1.2,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tries to grab you, but you break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME grabs you and tosses you to the ground!",
					],
				],
			},
			[App.Combat.Moves.Siren.Scream]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 4,
				speed: 20,
				damage: 1.5,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME lets loose an ear splitting scream, but it has no effect!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME lets loosean ear splitting scream, causing immense pain!",
					],
				],
			},
			[App.Combat.Moves.Siren.Drown]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 20,
				combo: 6,
				speed: 20,
				damage: 2.5,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME summons a large tidal wave, but you narrowly avoid it!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME summons a large tidal wave that smothers and drowns you!",
					],
				],
			},
		},
	}
}
