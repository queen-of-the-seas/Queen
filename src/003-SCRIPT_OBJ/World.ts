namespace App.Entity {
	export class World {
		get state(): GameState.IGame { /* eslint-disable-line class-methods-use-this */
			return State.variables;
		}

		/**
		 * Previously we stored a dictionary of NPC objects.
		 *  Now we just store some vital information and make new ones as needed.
		 */
		npc(npTag: Entity.NpcId): Entity.NPC;
		npc(npcTag: string): Entity.NPC;
		npc(npcTag: string, allowUndefined: false): Entity.NPC;
		npc(npcTag: string, allowUndefined: true): Entity.NPC | undefined;
		npc(npcTag: string, allowUndefined?: boolean): Entity.NPC | undefined {
			const data = Data.npcs[npcTag];
			if (!data) {
				if (allowUndefined) {
					return undefined;
				} else {
					throw new Error(`Could not find npc '${npcTag}'`);
				}
			}

			if (!this.state.npc.hasOwnProperty(npcTag)) {
				this.state.npc[npcTag] = {questFlags: {}, mood: data.mood, lust: data.lust};
			}
			return new Entity.NPC(npcTag, data, this.state.npc[npcTag]);
		}

		npcGroup(name: string): Entity.NPC[] {
			const gr = Data.npcGroups[name];
			if (!gr) {
				throw new Error(`Could not find NPC group '${name}'`);
			}
			return gr.map(n => this.npc(n));
		}

		/**
		 * With Icon = true then return string time with Icon.
		 * With Icon = false return numerical time of day.
		 */
		phaseName(addIcon = false): string {
			const phase = this.state.phase;
			return addIcon ? `${PR.phaseName(phase)} ${PR.phaseIcon(phase)}` : PR.phaseName(phase);
		}

		// Game/Environment Variables
		get day(): number {return this.state.day;}
		set day(n: number) {this.state.day = n;}
		get phase(): DayPhase {return this.state.phase;} // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night
		set phase(n: DayPhase) {this.state.phase = n;}

		get now(): DateTime {
			return {day: this.day, phase: this.phase};
		}

		character(): Entity.Player;
		character(id: Entity.CharacterId.Player): Entity.Player;
		character(id?: string): Entity.Human;
		character(id: string = Entity.CharacterId.Player): Entity.Human {
			let res = this._characters.get(id);
			if (!res) {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-enum-comparison
				if (id === Entity.CharacterId.Player) {
					throw new Error('Invalid attempt to create character for PC');
				}
				res = new Entity.GameCharacter(id);
				console.log(`Creating character for '${id}'`);
				this._characters.set(id, res);
			}
			return res;
		}

		get pc(): Entity.Player {return this._characters.get(Entity.CharacterId.Player) as Entity.Player; }
		get storeInventory(): Record<string, GameState.StoreInventory> {return this.state.storeInventory;}
		get npcs(): Record<string, GameState.NPC> {return this.state.npc;}

		get debugMode(): boolean {return this.state.debugMode === true;}
		set debugMode(v: boolean) {this.state.debugMode = v;}
		get difficultySetting(): number {return this.state.difficultySetting;}
		get whoreTutorial(): boolean {return this.state.tutorial.whoring;}
		set whoreTutorial(v: boolean) {this.state.tutorial.whoring = v;}

		npcNextDay(): void {
			for (const npcId in this.npcs) { // NPC mood/desire/quest flags.
				const npcState = this.npcs[npcId];
				if (!npcState) continue;
				const npc = this.npc(npcId);
				npc.adjustFeelings();
				npc.resetFlags();
			}
		}

		nextDay(): void {
			this.pc.nextDay();
			this.npcNextDay();

			this.state.day++;
			this.state.phase = DayPhase.Morning;
			Quest.nextDay(this.pc);
			setup.avatar.queuePortraitDraw();
		}

		/**
		 * Move time counter to next phase of day.
		 * @param phases - Number of phases to increment.
		 */
		nextPhase(phases = 1): void {
			// Can't advance to next day, only do that when sleeping.
			for (let i = 0; i < phases && this.state.phase <= DayPhase.LateNight; ++i) {
				setup.world.state.phase++;
				this.pc.nextPhase();
			}
		}

		advancePhaseTo(phase: DayPhase): number {
			if (this.phase > phase) {
				return 0;
			}
			const diff = phase - this.phase;
			this.nextPhase(diff);
			return diff;
		}

		saveLoaded(): void {
			this._initCharacters();
			setup.notifications.clear();
		}

		initNewGame(): void {
			GameState.setNewGameVariables();
			this.saveLoaded();
			Quest.byId("Q001").accept(this);
		}

		constructor() {
			this._initCharacters();
		}

		private _initCharacters() {
			this._characters.clear();
			this._characters.set(Entity.CharacterId.Player, new Entity.Player());
		}

		private readonly _characters: Map<string, Entity.Human> = new Map();
	}
}
