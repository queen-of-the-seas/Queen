// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange
namespace App.Data {
	App.Data.lootBoxes["common courtesan loot box"] = {
		name: "common courtesan chest",
		shortDesc: "@@.item-common;A small trendy looking treasure chest@@",
		longDesc: "This small black treasure chest sports an attractive sequined exterior and is wrapped \
    with a shiny white ribbon and bow.",
		message: Data.defaultLootBotMessage,
		type: ItemTypeConsumable.LootBox,
		// Effect : [ TABLE, Minimum Roll, Bonus to roll
		effects: ["COURTESAN_LOOT_BOX_COMMON"],
	};

	App.Data.lootBoxes["uncommon courtesan loot box"] = {
		name: "uncommon courtesan chest",
		shortDesc: "@@.item-uncommon;A trendy looking treasure chest@@",
		longDesc: "This black treasure chest sports an attractive sequined exterior and is wrapped with a shiny \
    white ribbon and bow.",
		message: Data.defaultLootBotMessage,
		type: ItemTypeConsumable.LootBox,
		// Effect : [ TABLE, Minimum Roll, Bonus to roll
		effects: ["COURTESAN_LOOT_BOX_UNCOMMON"],
	};

	App.Data.lootBoxes["rare courtesan loot box"] = {
		name: "rare courtesan chest",
		shortDesc: "@@.item-rare;A large trendy looking treasure chest@@",
		longDesc: "This large black treasure chest sports an attractive sequined exterior and is wrapped with a \
    shiny white ribbon and bow.",
		message: Data.defaultLootBotMessage,
		type: ItemTypeConsumable.LootBox,
		// Effect : [ TABLE, Minimum Roll, Bonus to roll
		effects: ["COURTESAN_LOOT_BOX_RARE"],
	};

	App.Data.lootBoxes["legendary courtesan loot box"] = {
		name: "legendary courtesan chest",
		shortDesc: "@@.item-legendary;A huge trendy looking treasure chest@@",
		longDesc: "This huge black treasure chest sports an attractive sequined exterior and is wrapped with a \
    shiny white ribbon and bow.",
		message: Data.defaultLootBotMessage,
		type: ItemTypeConsumable.LootBox,
		// Effect : [ TABLE, Minimum Roll, Bonus to roll
		effects: ["COURTESAN_LOOT_BOX_LEGENDARY"],
	};

	App.Data.lootTables["COURTESAN"] = [
		makeLootTableItemNoFilter("coins", 100, 1, 50),
		makeLootTableItem(Items.Category.Clothes, 100, 1, "style", [Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady]),
		makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		makeLootTableItem(Items.Category.Food, 75, 2, "effects", [
			"BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY",
			"FITNESS_XP_COMMON", "FITNESS_XP_UNCOMMON", "FITNESS_XP_RARE", "FITNESS_XP_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"WAIST_XP_COMMON", "WAIST_XP_UNCOMMON", "WAIST_XP_RARE", "WAIST_XP_LEGENDARY",
		]),
		makeLootTableItem(Items.Category.Food, 30, 4, "effects", ["WHOLESOME_MEAL", "SNACK", "LIGHT_WHOLESOME_MEAL"]),
		makeLootTableItem(Items.Category.Food, 50, 5, "effects", ["HARD_ALCOHOL"]),
		makeLootTableItem(Items.Category.Drugs, 75, 2, "effects", [
			"BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY",
			"FITNESS_XP_COMMON", "FITNESS_XP_UNCOMMON", "FITNESS_XP_RARE", "FITNESS_XP_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"WAIST_XP_COMMON", "WAIST_XP_UNCOMMON", "WAIST_XP_RARE", "WAIST_XP_LEGENDARY",
		]),
		makeLootTableItem(Items.Category.Drugs, 50, 2, "effects", [
			"ENERGY_COMMON", "ENERGY_UNCOMMON", "ENERGY_RARE", "ENERGY_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"PERVERSION_XP_COMMON", "PERVERSION_XP_UNCOMMON", "PERVERSION_XP_RARE", "PERVERSION_XP_LEGENDARY",
			"WAIST_XP_COMMON", "WAIST_XP_UNCOMMON", "WAIST_XP_RARE", "WAIST_XP_LEGENDARY",
		]),
	];
}
