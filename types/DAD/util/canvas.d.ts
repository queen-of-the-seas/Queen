/**
 * Canvas drawing layer order; higher value is further up
 * @readonly
 * @enum {number}
 */
export enum Layer {
	BASE = 0,
	BACK = 1,
	FRONT = 2,
	SHADING_FRONT = 3,
	MALE_GENITALS = 4,
	MIDRIFT = 5,
	SHADING_MIDRIFT = 6,
	ARMS = 7,
	SHADING_ARMS = 8,
	GENITALS = 9, // Technically just breasts now.
	SHADING_GENITALS = 10,
	BELOW_HAIR = 11,
	SHADING_BELOW_HAIR = 12,
	HAIR = 13,
	SHADING_HAIR = 14,
	ABOVE_HAIR = 15,
	EFFECTS = 16,
	NUM_LAYERS = 17
}

export const ShadingLayers: Layer[];

/**
 * Minimal distance drawing on canvas to eliminate seams
 */
export const seamWidth: number;

interface CanvasStyleOverrideCustom {
	width: number;
	height: number;
	parent: HTMLElement | null;
}

type CanvasStyleOverride = Omit<CSSStyleDeclaration, "width" | "height"> & CanvasStyleOverrideCustom;
/**
 * Get access to a canvas group DOM element, creating it if it doesn't exist
 * @memberof module:da
 * @param groupname Id of the canvas group holder, or the DOM element
 * @param styleOverride Object holding canvas style overrides
 * @returns The canvas group holder HTML DOM element
 */
export function getCanvasGroup(groupname: string | HTMLElement, styleOverride: Partial<CanvasStyleOverride>): HTMLElement;


/**  Get a canvas DOM element with id=canvasName, generating it if necessary
 styleOverride is the additional/overriding css style object to apply over
 defaults
 likely, you'd want to define its location:

 styles = {
     position:"absolute",
     top:"10px",
     left:"10px",
     parent: document.getElementById("canvas_holder"),
     }
 */
export function getCanvas(canvasName: string | HTMLCanvasElement, styles: Partial<CanvasStyleOverride>): HTMLCanvasElement;

/**
 * Hide a canvas group from view (but not delete it)
 * @param groupName Either the id of the group element or the element itself
 */
export function hideCanvasGroup(groupName: string|HTMLElement): void;

/**
 * Show a canvas group from view
 * @memberof module:da
 * @param groupName Either the id of the group element or the element itself
 */
export function showCanvasGroup(groupName: string | HTMLElement): void;

export function getCanvasHandle(canvasName: string | HTMLCanvasElement): HTMLCanvasElement;
