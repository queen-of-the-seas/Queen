namespace App.UI.Widgets {
	function makeStoreTable(name: string, restockingText: string | null): HTMLTableElement {
		const inventoryTable = makeElement('table', {classNames: 'store-inventory-table'});
		const colgroup = appendNewElement('colgroup', inventoryTable);
		appendNewElement('col', colgroup, {classNames: 'name'});
		appendNewElement('col', colgroup, {classNames: 'quantity'});
		appendNewElement('col', colgroup, {classNames: 'price'});
		appendNewElement('col', colgroup, {classNames: 'buy'});
		appendNewElement('col', colgroup, {classNames: 'examine'});

		const tHeader = appendNewElement('thead', inventoryTable);
		const header = appendNewElement('tr', tHeader);
		const caption = appendNewElement('td', header);
		caption.colSpan = 3;
		caption.style.textAlign = "center";
		const shopName = appendNewElement('span', caption, {content: name, classNames: 'shopName'});
		if (restockingText) {
			if (settings.inlineItemDetails) {
				appendNewElement('td', header, {content: restockingText}).colSpan = 2;
			} else {
				appendNewElement('span', shopName, {content: restockingText, classNames: 'tooltip'});
			}
		}
		return inventoryTable;
	}

	export function storeInventory(player: Entity.Player, npc: Entity.NPC): HTMLTableElement {
		const styles = {
			storeButtton: ['store-button'],
			disabledButton: ['disabled-store-button'],
		};

		const store = App.storeEngine.openStore(player, npc);

		const restockingText = store.ownerMood >= 80 ?
			(store.daysUntilRestocking() > 0 ? `Restocking in ${store.daysUntilRestocking()} days` : "Fresh supplies arrived this morning!")
			: null;

		const missingItemsForQuests = Quest.allMissingItems(player);

		const inventoryTable = makeStoreTable(store.name, restockingText);

		const makeStoreBuyButtons = (item: Data.StoreInventoryItem) => {
			const buyButton = makeElement('button', {content: "Buy", classNames: styles.storeButtton});
			buyButton.addEventListener('click', () => {
				store.buyItems(item, 1);
				Engine.play("Shop"); // FIXME
			});
			let noBuyAllButton = false;
			const price = store.getPrice(item);
			if (item.qty < 1) {
				buyButton.classList.add(...styles.disabledButton);
				noBuyAllButton = true;
			}
			if (player.isAtMaxCapacity(item.type, item.tag)) {
				buyButton.classList.add(...styles.disabledButton);
				buyButton.textContent = "Full";
				noBuyAllButton = true;
			}
			if (player.money < price) {
				buyButton.classList.add(...styles.disabledButton);
				noBuyAllButton = true;
			}
			if (item.type === Items.Category.Clothes || item.type === Items.Category.Weapon) {
				noBuyAllButton = true;
				if (player.ownsWardrobeItem(item.tag)) {
					buyButton.textContent = "Owned";
					buyButton.classList.add(...styles.disabledButton);
				}
			}

			if (item.qty < 2 || player.money < price * 2) {
				noBuyAllButton = true;
			}

			if (noBuyAllButton) {
				return buyButton;
			}

			const buyAllButton = makeElement('button', {content: "Buy all", classNames: styles.storeButtton});
			buyAllButton.addEventListener('click', () => {
				store.buyItems(item);
				Engine.play("Shop"); // FIXME
			})

			const res = new DocumentFragment();
			res.append(buyButton);
			res.append(" ");
			res.append(buyAllButton);
			return res;
		};

		const makeStoreExamineButton = (item: Data.StoreInventoryItem) => {
			const button = makeElement('button', {content: "Examine", classNames: styles.storeButtton});
			button.addEventListener('click', () => {
				UI.replace('#storeExamine', store.printItemLong(item));
			})
			return button;
		};

		const appendSection = (container: HTMLTableSectionElement, name: string, styleName: string, inv: Data.StoreInventoryItem[]): void => {
			const headerRow = appendNewElement('tr', container, {classNames: 'section-header'});
			const nameCell = appendNewElement('td', headerRow, {classNames: 'name'});
			appendNewElement('span', nameCell, {content: name, classNames: styleName});
			appendNewElement('td', headerRow, {content: "Quantity", classNames: 'quantity'});
			appendNewElement('td', headerRow, {content: "Price", classNames: 'price'});
			appendNewElement('td', headerRow, {classNames: 'buy'});
			appendNewElement('td', headerRow, {classNames: 'examine'});

			if (inv.length === 0) {
				const row = appendNewElement('tr', container);
				const note = appendNewElement('td', row);
				note.colSpan = 5;
				appendNewElement('span', note, {content: "Nothing for sale!", classNames: 'attention'});
				return;
			}

			for (const itm of inv) {
				const row = appendNewElement('tr', container);
				const itemDescritpionCell = appendNewElement('td', row);
				if (missingItemsForQuests.hasOwnProperty(Items.makeId(itm.type, itm.tag))) {
					appendNewElement('span', itemDescritpionCell, {content: "(!) ", classNames: 'item-required-for-quest'});
				}
				$(itemDescritpionCell).wiki(store.printItem(itm));
				const quantityCell = appendNewElement('td', row, {content: `${itm.qty}`});
				quantityCell.style.textAlign = 'center';
				const priceCell = appendNewElement('td', row);
				priceCell.style.textAlign = 'right';
				$(priceCell).wiki(`${store.getPrice(itm)}`);
				const buyCell = appendNewElement('td', row);
				buyCell.style.textAlign = 'left';
				buyCell.append(makeStoreBuyButtons(itm));
				const examineCell = appendNewElement('td', row);
				examineCell.style.textAlign = 'center';
				examineCell.append(makeStoreExamineButton(itm));
			}
		}

		const inventory = appendNewElement('tbody', inventoryTable);
		appendSection(inventory, "General Items", 'item-category-general', store.getCommonInventory());
		appendSection(inventory, "Rare Items", 'item-category-rare', store.getRareInventory());
		const footer = appendNewElement('tfoot', inventoryTable);
		const fRow = appendNewElement('tr', footer);
		const cExamine = appendNewElement('td', fRow);
		cExamine.colSpan = 5;
		const examine = appendNewElement('span', cExamine);
		examine.id = "storeExamine";

		return inventoryTable;
	}
}

Macro.add("storeInventory", {
	handler() {
		this.output.append(App.UI.Widgets.storeInventory(setup.world.pc, variables().menuAction as App.Entity.NPC));
	},
});
