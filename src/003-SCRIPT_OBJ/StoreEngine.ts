namespace App {
	export const storeEngine = {
		/**
		 * Creates a store object and returns it to the Twine engine.
		 */
		openStore(player: Entity.Player, npc: Entity.NPC): Store {
			const storeName = npc.storeName;
			if (storeName !== undefined) {
				return new Store(player, npc, storeName);
			}
			throw `NPC "${npc.name}" has no store`;
		},

		/**
		 * Checks to see if an NPC has a store inventory or not attached to them.
		 */
		hasStore(npc: Entity.NPC): boolean {
			const storeName = npc.storeName;
			if (!storeName || !Data.stores.hasOwnProperty(storeName)) {
				return false;
			}

			const storeDefinition = Data.stores[storeName];
			const unlockQuest = storeDefinition.unlockingQuest;
			return !unlockQuest ||
				(Quest.byId(unlockQuest).status(setup.world.pc) === (storeDefinition.unlockingQuestStatus ?? QuestStatus.Completed));
		},

		/**
		 * Is the NPCs store open.
		 */
		isOpen(npc: Entity.NPC): boolean {
			const storeName = npc.storeName;
			return !storeName || Data.stores[storeName]?.open.includes(setup.world.phase);
		},

		/**
		 * Lock/unlock a store item.
		 */
		toggleStoreItem(storeKey: string, itemTag: string, locked: 0 | 1): void {
			for (const item of setup.world.storeInventory[storeKey].inventory.filter(item => item.tag === itemTag)) {
				item.lock = locked;
			}
		},
	};

	interface ItemOverview {
		type: Items.Category;
		tag: string;
		price?: number;
	}

	interface ItemOverviewForBuying extends ItemOverview {
		qty: number;
	}
	/**
	 * Store Container Object
	 */
	export class Store {
		readonly #player: Entity.Player;
		readonly #npc: Entity.NPC;
		readonly #data: Data.StoreDesc;
		readonly #id: string;

		constructor(player: Entity.Player, npc: Entity.NPC, storeId: string) {
			this.#player = player;
			this.#npc = npc;
			this.#data = Data.stores[storeId];
			this.#id = storeId;
			// Hack to add store to player state if the store doesn't exist already.
			if (!setup.world.storeInventory.hasOwnProperty(this.#id)) {
				setup.world.storeInventory[this.#id] = {lastStocked: 0, inventory: [], rare: []};
			}

			this.stockInventory();
		}

		get name(): string {return this.#data.name;}

		getInventory(rarity: Items.Rarity): Data.StoreInventoryItem[] {
			const mood = this.#npc.mood;
			const player = this.#player;

			const inventory = setup.world.storeInventory[this.#id].inventory.filter(
				item => (item.lock !== 1) && (item.category === rarity) && (mood >= item.mood)
			);

			// return Inventory; // Show item, but no buy.
			return inventory.filter(item => !Items.isEquipmentCategory(item.type) || !player.ownsWardrobeItem(item.tag));
		}

		getCommonInventory(): Data.StoreInventoryItem[] {
			return this.getInventory(Items.Rarity.Common);
		}

		getRareInventory(): Data.StoreInventoryItem[] {
			const mood = this.#npc.mood;
			return setup.world.storeInventory[this.#id].rare.filter(
				item => (item.lock !== 1) && (item.category === Items.Rarity.Rare) && (mood >= item.mood)
			);
		}

		/**
		 * Can the player afford this onion? Er.. item.
		 * @param item The object that represents the stores inventory for the item
		 */
		playerCanAfford(item: ItemOverview): boolean {
			return (this.#player.money >= this.getPrice(item));
		}

		/**
		 * Fetch the price from the Item calculator. Applies store bonus and discount for good npc mood.
		 * @param item The object that represents the stores inventory for the item
		 */
		getPrice(item: ItemOverview): number {
			let price = Items.calculateBasePrice(item.type, item.tag);
			if (item.price !== undefined) price = Math.ceil(price * item.price);

			// Up to 30% discount with maximum NPC mood. Mood must be above 50
			const discount = Math.floor(price * 0.3);
			return (this.#npc.mood > 50) ? Math.ceil(price - (discount * ((this.#npc.mood - 50) / 50))) : price;
		}

		buyItems(item: ItemOverviewForBuying, count?: number): void {
			if (count === undefined) count = item.qty;
			const itemPrice = this.getPrice(item);
			// looping because some items contain more than 1 charge
			// and we can't fetch that here
			for (let i = 0; i < count; ++i) {
				if (this.#player.money < itemPrice || item.qty === 0 || this.#player.isAtMaxCapacity(item.type, item.tag)) break;
				item.qty = Math.max(0, (item.qty - 1));
				this.#player.addItem(item.type, item.tag, 0);
				this.#player.spendMoney(itemPrice, GameState.SpendingTarget.Shopping, item.type);
			}
		}

		generateMarket(): void {
			if (this.#data.restock === 0) {
				// Clear all the data…
				setup.world.storeInventory[this.#id].inventory = [];
				setup.world.storeInventory[this.#id].rare = [];

				const itemsCount = _.random(3, 6);

				const selectRandomItem = (list: Record<string, Data.ItemDesc>): string => { // TODO eliminate duplicates (i.e. track selected items)
					return Object.entries(list)
						.filter(itemEntry => itemEntry[1].inMarket ?? true)
						.randomElement()[0];
				};

				for (let i = 0; i < itemsCount; i++) {
					const roll = State.random();
					let entry = "";
					let qty = 0;

					if (roll < .2) {  // cosmetics
						entry = selectRandomItem(Data.cosmetics);
						setup.world.storeInventory[this.#id].inventory.push(
							{
								category: Items.Rarity.Common, type: Items.Category.Cosmetics,
								qty: 12, max: 12, price: 1.3, mood: 0, lock: 0, tag: entry,
							});
					} else if (roll < .6) { // food
						entry = selectRandomItem(Data.food);
						qty = (1 + (Math.floor(State.random() * 4)));
						setup.world.storeInventory[this.#id].inventory.push(
							{
								category: Items.Rarity.Common, type: Items.Category.Food,
								qty: qty, max: qty, price: 1.3, mood: 0, lock: 0, tag: entry,
							});
					} else if (roll < .8) { // drugs
						entry = selectRandomItem(Data.drugs);
						qty = (1 + (Math.floor(State.random() * 4)));
						setup.world.storeInventory[this.#id].inventory.push(
							{
								category: Items.Rarity.Common, type: Items.Category.Drugs,
								qty: qty, max: qty, price: 1.3, mood: 0, lock: 0, tag: entry,
							});
					} else if (roll <= .95) { // clothes
						// if roll == 0.95 then include legendary items
						const keys = roll === 0.95
							? Object.keys(Data.clothes).filter(c => Data.clothes[c].inMarket ?? true)
							: Object.keys(Data.clothes).filter(
								c => (Data.clothes[c].inMarket ?? true) && Data.clothes[c].rarity !== Items.Rarity.Legendary);

						if (keys.length > 0) {
							entry = keys.randomElement();
							if (Data.clothes[entry].rarity === Items.Rarity.Legendary) {
								setup.world.storeInventory[this.#id].rare.push(
									{
										category: Items.Rarity.Rare,
										type: Items.Category.Clothes,
										qty: 1,
										max: 1,
										price: 1.3,
										mood: 0,
										lock: 0,
										tag: entry,
									});
							} else {
								setup.world.storeInventory[this.#id].inventory.push(
									{
										category: Items.Rarity.Common,
										type: Items.Category.Clothes,
										qty: 1,
										max: 1,
										price: 1.3,
										mood: 0,
										lock: 0,
										tag: entry,
									});
							}
						}
					} else { // (roll > .95) // slot reels.
						entry = Object.keys(Data.slots).randomElement();
						setup.world.storeInventory[this.#id].inventory.push(
							{category: Items.Rarity.Common, type: Items.Category.Reel, qty: 1, max: 1, price: 1.3, mood: 0, lock: 0, tag: entry});
					}
				}
			}
		}

		/**
		 * Returns days until the next restocking
		 */
		daysUntilRestocking(): number {
			// Don't stock stuff in markets
			if (this.#data.restock === 0) return 0;
			return this.#data.restock - (setup.world.day
				- setup.world.storeInventory[this.#id].lastStocked);
		}

		/**
		 * Owner's mood
		 */
		get ownerMood(): number {
			return this.#npc.mood;
		}

		/* FIXME: Let's make this trigger for the SHIP whenever you land at a port. But not otherwise. */
		/* FIXME: This entire system is a load of complicated garbage. Rewrite and simplify it in the future */
		stockInventory(): void {
			// Don't stock stuff in markets
			if (this.#data.restock === 0) return;

			if ((setup.world.storeInventory[this.#id].lastStocked === 0)
				|| (setup.world.storeInventory[this.#id].lastStocked + this.#data.restock <= setup.world.day)) {
				// Something is bugged, prepare the inventory array.
				if (setup.world.storeInventory[this.#id].inventory.length === 0)
					setup.world.storeInventory[this.#id].inventory = this.#data.inventory;

				// Add any records in Data that do not exist in the player state.Typically we added something to a shop
				// and the player already has a shop record in their state object.
				const toAdd = this.#data.inventory
					.filter(f1 => !setup.world.storeInventory[this.#id].inventory.some(f2 => f1.tag === f2.tag));
				setup.world.storeInventory[this.#id].inventory.push(...toAdd);

				// Restock qty on items.
				for (let i = 0; i < setup.world.storeInventory[this.#id].inventory.length; i++)
					setup.world.storeInventory[this.#id].inventory[i].qty = setup.world.storeInventory[this.#id].inventory[i].max;

				setup.world.storeInventory[this.#id].rare = []; // Clear array.

				const mood = this.#npc.mood;
				const maxRares = this.#data.maxRares ?? 1;
				// List of all possible rares from inventory listing.
				let rares = setup.world.storeInventory[this.#id].inventory.filter(
					item => (item.category === Items.Rarity.Rare) && (mood >= item.mood) && (item.lock !== 1)
				);

				// Add multiple rare items to the store inventory.
				if (rares.length > 0) {
					for (let i = 0; i < maxRares; i++) {
						// Filter out Rares that already exist in the rares entry.
						rares = rares.filter((o) => {
							const current = setup.world.storeInventory[this.#id].rare;
							for (let i = 0; i < current.length; i++)
								if (current[i].type === o.type && current[i].tag === o.tag) return false;
							return true;
						});

						if (rares.length > 0) {
							// Copy data record object into new variable or we get some bad reference juju.
							setup.world.storeInventory[this.#id].rare.push(clone(rares.randomElement()));
						}
					}
				}

				setup.world.storeInventory[this.#id].lastStocked = setup.world.day;
			}
		}

		printItem(item: ItemOverview): string {
			const oItem = Items.factory(item.type, item.tag);
			let res = "<span class='inventoryItem'>" + oItem.description;
			if (this.#player.inventory.isFavorite(oItem.id)) {
				res += "&nbsp;" + PR.getItemFavoriteIcon(true);
			}

			if (SugarCube.settings.inlineItemDetails) {
				res += '<span class="tooltip">' + oItem.examine(this.#player, false) + '</span></span>';
				res += "<br><div class='inventoryItemDetails'>" + oItem.examine(this.#player, true) + '</div>';
			} else {
				res += '</span>';
			}
			return res;
		}

		/**
		 * Used for examining items through shop interface
		 */
		printItemLong(item: ItemOverview): string {
			const oItem = Items.factory(item.type, item.tag);
			const player = this.#player;
			return "@@.action-general;You take a look at the @@" + oItem.description + ".\n" + oItem.examine(player, false);
		}
	}
}
