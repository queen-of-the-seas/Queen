namespace App.Entity {
	export class GameCharacter extends Human {
		constructor(id: string) {
			super();
			this._id = id;
		}

		protected get state(): GameState.Human {
			return setup.world.state.character[this._id];
		}

		override get inventory(): InventoryManager {
			if (!this.#inventory) {
				this.#inventory = new Entity.InventoryManager(this._id);
			}
			return this.#inventory;
		}

		get clothing(): Entity.ClothingManager {
			if (!this.#clothing) {
				this.#clothing = new Entity.ClothingManager();
			}
			return this.#clothing;
		}

		override consumeItem(item: Items.Consumable): string {
			item.applyEffects(this);
			return "";
		}

		protected override makeActor(): Text.ActorHuman {
			return new Text.ActorHuman(this, Text.Pronouns.get(this.gender));
		}

		private readonly _id: string;
		#inventory: InventoryManager | null = null;
		#clothing: ClothingManager | null = null;
	}

	export enum CharacterId {
		Player = 'pc',
		Girlfriend = 'girlfriend'
	}
}
