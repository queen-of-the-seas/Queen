import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Jewelry} from "./jewelry";



export class TriangleEarringsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class CrystalEarringsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class LoopEarringsFrontPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class LoopEarringsBackPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class ChainEarringsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class BallEarringsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class RhombEarringsFrontPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class RhombEarringsBackPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}



export class EarPiercingFrontPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class EarPiercingBackPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Earrings extends Jewelry {
	constructor(...data: object[]);
}
/**/

export class TriangleEarrings extends Earrings {
	constructor(...data: object[]);

	length: number;
	width: number;
	hanger: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class CrystalEarrings extends Earrings {
	constructor(...data: object[]);

	length: number;
	width: number;
	hanger: number;
	alt: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class LoopEarrings extends Earrings {
	constructor(...data: object[]);

	length: number;
	width: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ChainEarrings extends Earrings {
	constructor(...data: object[]);

	length: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BallEarrings extends Earrings {
	constructor(...data: object[]);

	length: number;
	size: number;
	alt: number;
	thickness: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class RhombEarrings extends Earrings {
	constructor(...data: object[]);

	length: number;
	width: number;
	alt: number;
	thickness: number;

	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class EarPiercing extends Earrings {
	constructor(...data: object[]);

	length: number;
	width: number;
	thickness: number;
	rotation: number;
	dx: number;
	dy: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
