namespace App.Resources {
	interface SlotPicCounts {
		anal: number;
		bj: number;
		hand: number;
		tit: number;
	}

	function countSlotPics(): SlotPicCounts {
		const slotPassages = Story.filter(p => p.name.startsWith('ss_') && p.tags.includes('Twine.image'));
		return {
			anal: slotPassages.countWith(p => p.name.includes('anal')),
			bj: slotPassages.countWith(p => p.name.includes('bj')),
			hand: slotPassages.countWith(p => p.name.includes('hand')),
			tit: slotPassages.countWith(p => p.name.includes('tit')),
		};
	}

	const slotPicCounts: SlotPicCounts = countSlotPics();

	export function randomReelActPic(act: Data.ReelAction | Skills.Sexual): string {
		switch (act) {
			case "ass":
			case Skills.Sexual.AssFucking:
				return `ss_anal_${random(1, slotPicCounts.anal)}`;
			case "bj":
			case Skills.Sexual.BlowJobs:
				return `ss_bj_${random(1, slotPicCounts.bj)}`;
			case "hand":
			case Skills.Sexual.HandJobs:
				return `ss_hand_${random(1, slotPicCounts.hand)}`;
			case "tits":
			case Skills.Sexual.TitFucking:
				return `ss_tit_${random(1, slotPicCounts.bj)}`;
			default: return `ss_anal_${random(1, slotPicCounts.anal)}`;
		}
	}

	export function addRandomActPic(element: HTMLElement, act: Data.ReelAction): void {
		element.style.backgroundImage = `url(${Story.get(randomReelActPic(act)).text})`;
	}
}
