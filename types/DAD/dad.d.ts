import * as dp from 'drawpoint/dist';

//export as namespace da;

export enum Layer {
	BASE = 0,
	BACK = 1,
	FRONT = 2,
	SHADING_FRONT = 3,
	MALE_GENITALS = 4,
	MIDRIFT = 5,
	SHADING_MIDRIFT = 6,
	ARMS = 7,
	SHADING_ARMS = 8,
	GENITALS = 9, // Technically just breasts now.
	SHADING_GENITALS = 10,
	BELOW_HAIR = 11,
	SHADING_BELOW_HAIR = 12,
	HAIR = 13,
	SHADING_HAIR = 14,
	ABOVE_HAIR = 15,
	EFFECTS = 16,
	NUM_LAYERS = 17
}

export const ShadingLayers: Layer[];

/**
 * Minimal distance drawing on canvas to eliminate seams
 */
export const seamWidth = 0.0;

/**
 * Get access to a canvas group DOM element, creating it if it doesn't exist
 * @param groupname Id of the canvas group holder, or the DOM element
 * @param  styleOverride Object holding canvas style overrides
 * @returns  The canvas group holder HTML DOM element
 */
export function getCanvasGroup(groupname: string | HTMLElement, styleOverride: object): HTMLElement;


/**  Get a canvas DOM element with id=canvasName, generating it if necessary
 styleOverride is the additional/overriding css style object to apply over
 defaults
 likely, you'd want to define its location:

 styles = {
	 position:"absolute",
	 top:"10px",
	 left:"10px",
	 parent: document.getElementById("canvas_holder"),
	 }
 * @memberof module:da
 */
export function getCanvas(canvasName: string | HTMLCanvasElement, styles: object): HTMLCanvasElement;

/**
 * Hide a canvas group from view (but not delete it)
 * @memberof module:da
 * @param {(string|HTMLElement)} groupName Either the id of the group element or the element itself
 */
export function hideCanvasGroup(groupName: string | HTMLElement): void;

/**
 * Show a canvas group from view
 * @memberof module:da
 * @param {(string|HTMLElement)} groupName Either the id of the group element or the element itself
 */
export function showCanvasGroup(groupName: string | HTMLElement): void;

export function getCanvasHandle(canvasName: string | HTMLElement): HTMLElement;

export function addPattern(patternName: string, patternSource: string): void;

export function load(): Promise<void>;
/**
 * Dimension calculation based on avatar statistics. These are callback methods that
 * the user should define to link extended gameplay stats on the Player to physical dimensions.
 * @param base this dimension as calculated by all previous calculations
 */
export type dimensionCalculation = (this: Player, base: number) => any;

/**
 * Extend the way a dimension is calculated to plug in a user statistics system
 * @param dimDesc Either the dimension descriptor object, or
 * a string in the format of "skeleton.dimension"
 * @param newCalc User method for calculating dimension
 */
export function extendDimensionCalc(dimDesc: object | string, newCalc: dimensionCalculation): any;

/**
* Extract the side component from a location string
* @param partLoc Location string of a part
* @returns Either the side string or null if no side indicated
*/
export function extractSideLocation(partLoc: string): string;

/**
 * Extract the base component from a location string
 * @param partLoc Location string of a part
 * @returns The base component
 */
export function extractBaseLocation(partLoc: string): string;

/**
 * Extract the underlying location (side + base) of a location after stripping it of
 * potential modifier characters, which are prepended (use + for non-restrictive, - for exclusive)
 * @param partLoc location name of part
 */
export function extractUnmodifiedLocation(partLoc: string): string;

/**
 * Extract modifier characters (+ for non-restrictive, - for exclusive)
 * @param partLoc Location name of part
 * @returns Modifier characters for this location
 */
export function extractLocationModifier(partLoc: string): string;

/**
 * List of all base locations (without side and modifiers)
 */
export enum Location {
	ARM = "arm",
	CHEST = "chest",
	TORSO = "torso",
	BUTT = "butt",
	FEET = "feet",
	GROIN = "groin",
	HAND = "hand",
	HEAD = "head",
	HAIR = "hair",
	LEG = "leg",
	NECK = "neck",
	PENIS = "penis",
	VAGINA = "vagina",
	TESTICLES = "testicles",
	GENITALS = "genitals",
	EAR = "ears",
	EYEBROW = "brow",
	EYELASH = "eyelash",
	EYELID = "eyelid",
	EYES = "eyes",
	IRIS = "iris",
	LIPS = "lips",
	MOUTH = "mouth",
	NOSE = "nose",
	PUPIL = "pupil",
}

export function locationIsSideless(loc: Location): boolean;

/**
 * Get a location relative to a drawpoint
 * @param ex
 * @param drawpoint
 * @param dx
 * @param dy
 */
export function locateRelativeToDrawpoint(ex: object, {drawpoint, dx, dy}): dp.DrawPoint;

declare namespace Clothes {
	/**
	 * Clothing holds statistics and information about the clothes, but no drawing methods
	 * Instead, it holds ClothingParts that each know how to draw themselves
	 */
	export class Clothing {
		constructor(...data: object[]);
		stroke(): string;
		fill(): string;
	}
}

/**
 * All parts should go in this namespace
 */
declare namespace Part {
	export class BodyPart {
		/**
		 * Location of the part, prepend with + to mean allow other parts to also occupy this location,
		 *  - to mean restrict any other part from being here
		 */
		loc: string;
		parentPart: BodyPart;
		/**
		 * Drawing layer
		 */
		layer: Layer;
		/**
		 * Whether this part should be drawn on the other side as well
		 */
		reflect: boolean;
		/**
		 * Whether this part should not be drawn if there are clothes covering its location
		 */
		coverConceal: string[];
		/**
		 * Whether it's possible to wear anything over this part (mutually exclusive with converconceal)
		 */
		uncoverable: boolean;
		/**
		 * List of "{part group} {location}" strings that specify which parts of which part groups this part
		 * should be drawn above. For example, having "decorativeParts torso" will ensure this part gets drawn
		 * above any parts in that location. Specifying only a location will mean to be above any part in that
		 * location regardless of group.
		 */
		aboveParts: string[];
		/**
		 * Opposite to aboveParts.
		 */
		belowParts: string[];

		side: string;

		constructor(...data: object[]);

		/**
		 * Set the stroke pattern for this part
		 *
		 */
		stroke(): string;

		/**
		 * Set the fill pattern for this part
		 */
		fill(): string;

		/**
		 * Set how thick the stroke line should be
		 */
		getLineWidth(): number;

		/**
		 * Calculate drawpoints associated with this part and return it in the sequence to be drawn.
		 * @this Calculated dimensions of the player owning the part
		 * @param ex Exports from draw that should hold draw points calculated up to now;
		 * additional draw points defined by this part should be defined on ex
		 * @param mods Combined modifiers of the part and the Player owning the part
		 * @param calculate
		 * @param part The body part itself
		 * @return List of draw points (or convertible to draw point objects)
		 */
		calcDrawPoints(ex: object, mods: object, calculate: boolean, part: BodyPart): object[];
	}

	export enum Side {
		RIGHT = 0,
		LEFT = 1
	}

	export interface IPart {
		/**
		 * Right side of the body for anything taking side
		 */
		readonly RIGHT: Side.RIGHT,
		/**
		 * Left side of the body for anything taking side
		 */
		readonly LEFT: Side.LEFT,
		/**
		 * Give me a base body part and a side it's supposed to be on
		 * I'll return to you a body part specific to that side
		 * @memberof module:da.Part
		 * @param {BodyPart} PartPrototype Prototype to instantiate with
		 * @param {...object} userData Overriding data
		 * @returns {BodyPart}
		 */
		create(PartPrototype: BodyPart, ...userData: object[]): BodyPart;
	}

	export const Part: IPart;


	/**
	 * Get a side string in a well defined format
	 * @param side A side string
	 * @returns Either the side string or null if unrecognized
	 */
	export function getSideLocation(side: string): string;


	/**
	 * Check whether two parts conflict
	 * @param partA
	 * @param partB
	 * @returns True if the two parts have conflicting locations
	 */
	export function partConflict(partA: BodyPart, partB: BodyPart): boolean;

	export function getChildLocation(parentLoc: string, child: string): {childSide: string, childLoc: string};

	export function getSideValue(side: string | Side): Side;

	export function getAttachedLocation(partPrototype: object): string;
}

declare namespace Expression {
	interface IExpression {
		/**
		 * Create an expression to a certain degree
		 * @param expression Expression object with modifiers in Mods
		 * @param degree How fully the expression should be displayed; > 0
		 * @returns The expression to be applied
		 */
		create(expression: object, degree?: number): object;

		neutral: object;
		suspicious: object;
		angry: object;
		sad: object;
		surprised: object;
		mischievous: object;
		happy: object;
		sleepy: object;
		aroused: object;
		bliss: object;
	}

	export const Expression: IExpression;
}

/**
* Core stats (where user would define their gameplay stats)
* Each are statistic descriptions with low, high, average, and stdev (assume normal)
* @memberof module:da
*/
export const statLimits: {
	age: {
		low: 0,
		high: 1e9,
		avg: 30,
		stdev: 6,
		bias: 0,
	},
	fem: {
		low: 0,
		high: 11,
		avg: 5,
		stdev: 1,
		bias: 2,
	},
	sub: {
		low: 0,
		high: 11,
		avg: 4,
		stdev: 1,
		bias: 1,
	},
	/**
	 * Stages of pregnancy
	 */
	pregnancy: {
		low: 0,
		high: 10,
		avg: 0,
		stdev: 0,
		bias: 0,
	},
}

/**
 * Discrete core stats
 * @memberof module:da
 */
export const statDiscretePool: {
	// pool of available values for discrete properties
	skeleton: ["human"],  // underlying racial structure of player
}


/**
 * Player holds all data necessary for draw to render it.
 * It is also meant to be extended by the user of the library to include gameplay statistics.
 * The gameplay statistics can then be linked to the calculation of drawing dimensions.
 */
export class Player {
	static defaultStats(): typeof statLimits & typeof statDiscretePool;

	static defaultMods(): object;

	static defaultVitals(): object;

	static defaultClothes(): Clothes.Clothing[];


	/**
	 * @constructor
	 * @param {object} data Properties of the Player object (override the default values)
	 */
	constructor(data: object);

	toString(): string;

	/**
	 * Keep stats within boundaries
	 */
	clampStats(): void;

	/**
	 * Keep vitals within max vitals
	 */
	clampVitals(): void;


	// property getters
	/**
	 * Get modified statistic
	 * @param param Name of statistic
	 * @returns  Modified statistic value
	 */
	get(param: string): number;

	/**
	 * Get difference of statistic relative to the average
	 * @param param Name of statistic
	 * @returns Modified statistic value relative to average
	 */
	diff(param: string): number;

	/**
	 * Get modified dimensions
	 * @param param Name of dimension
	 * @returns Modified dimension value
	 */
	getDim(param: string): number;

	getBaseVitals(param: string): number;

	/**
	 * Get modifier
	 * @param  param Name of modifier
	 * @returns Modifier value
	 */
	getMod(param: string): number;

	/**
	 * Get statistical description of a dimension
	 * @param param Name of dimension
	 * @returns Statistical dimension description with low, high, stdev, avg
	 */
	getDimDesc(param: string): object;

	/**
	 * Calculate all components of the avatar, making it ready for drawing/interacting.
	 * This is needed because there are derived stats that this function resolves.
	 * It is called automatically every time the avatar is drawn, but if you interact with it in an RPG
	 * system, you should call it after every modification
	 */
	calcAll(): void;

	private _clampMods(): void;

	/**
	 * Calculate each dimension with 'this' set as the player inside the function
	 * This is automatically called when drawing, so it's rarely called manually.
	 */
	private _calcDimensions(): void;

	/**
	 * Calculate max vitals; this should be called after clamping mods
	 * This is automatically called when drawing, so it's rarely called manually
	 */
	private _calcVitals(): void;

	// body part interaction

	/**
	 * Attach a new body part, replacing any conflicting parts if necessary
	 * @param newPart New body part to be added
	 * @param parts Part group of the Player to attach to
	 * @returns Either the part that was removed, or null if nothing was removed
	 */
	attachPart(newPart: Part.BodyPart, parts?: Part.BodyPart[]): Part.BodyPart;

	/**
	 * Either returns a reference to the part in a specific location or null if the part doesn't
	 * exist
	 * @param location Where the part is located
	 * @param parts Part group to search in
	 * @param siblingIndex Index relative to other parts in same location in same group
	 * @returns Part in this location or null
	 */
	getPartInLocation(location: string, parts?: Part.BodyPart[], siblingIndex?: number): Part.BodyPart;

	removeSpecificPart(partPrototype: object, parts?: Part.BodyPart[], siblingIndex?: number): Part.BodyPart;

	/**
	 * Return whether a part is covered by clothing
	 * Depends on the part to specify what locations are considered coverable
	 * with part.coverConceal
	 * @param part
	 * @returns
	 */
	checkPartCoveredByClothing(part: Part.BodyPart): boolean;

	/**
	 * Remove a part at a specific location
	 * @param loc Where the part is located
	 * @param parts Part group of the Player to remove from
	 * @param siblingIndex Index relative to other parts in same location in same group
	 * @returns Part removed or null if nothing was removed
	 */
	removePart(loc: string, parts?: Part.BodyPart[], siblingIndex?: number): Part.BodyPart;

	private doRemovePart(oldPart, parts): void;

	replaceHair(newHair: object): void;

	// expression manipulation
	/**
	 * Apply an expression, replacing the current expression
	 * @param expression All expression are kept in Expression @see Expression.create()
	 */
	applyExpression(expression: object): void;
	removeExpression(): void;

	// clothing manipulation
	/**
	 * Find the clothes that occupies a certain body location
	 * @param location Unmodified body location
	 * @returns list of Clothes objects that cover that body location
	 */
	getClothingInLocation(location: string): Clothes.Clothing[];

	/**
	 * Find currently worn clothing that conflicts with given clothing
	 * @param clothing The
	 * @returns list of Clothes objects that conflict with this clothing
	 */
	getConflictingClothing(clothing: Clothes.Clothing): Clothes.Clothing[]

	/**
	 * Wear a Clothing item
	 * @param  clothing Clothing to be worn
	 * @returns null if failed to wear clothing, or the list of removed
	 * conflicting clothes
	 */
	wearClothing(clothing: Clothes.Clothing): Clothes.Clothing[]
	/**
	 * Remove an article of clothing
	 * @param clothing
	 * @returns Whether clothing was successfully removed
	 */
	removeClothing(clothing: Clothes.Clothing): boolean;
	/**
	 * Remove any clothing of article that can be removed
	 * @returns {Array} Array of clothing that was removed
	 */
	removeAllClothing(): Clothes.Clothing[];

	// item manipulation
	wieldItem(item): void;
	removeItem(item): void;

	addTattoo(tattoo): void;
	removeTattoo(tattoo): void;

	heightAdjust(): void;

	isFemale(): boolean;
	isMale(): boolean;

	/**
	 * Provide equivalent values for missing drawpoints
	 * @param ex
	 */
	fillMissingDrawpoints(ex): void;
}

export const vitalLimits: object;
