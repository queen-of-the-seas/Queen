import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {NeckAccessory} from "./neck_accessory";



export class BiChainPart extends ClothingPart {
	constructor(...data: object[]);

	//chain: false,
	//neckCoverage: 0.14,
	//cleavageCoverage: 0.09,
	curveX: number;
	curveY: number;
	thickness: number;

	//beadsSize: 4,
	//spaceSize: 4,
	//beadThickness: 1,
	//highlight: "red"

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class TearPart extends ClothingPart {
	constructor(...data: object[]);

	width: number;
	length: number;
	size: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class DoubleNecklacePart extends ClothingPart {
	constructor(...data: object[]);

	neckCoverage: number;
	cleavageCoverage: number;
	cleavageCoverageTop: number;
	curveX: number;
	curveY: number;
	thickness: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class MultiNecklacePart extends ClothingPart {
	constructor(...data: object[]);

	cleavageCoverage: number;
	neckCoverage: number;
	curveX: number;
	curveY: number;
	thickness: number;

	size: number;
	multiple: number;
	distance: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class NecklaceChainPart extends ClothingPart {
	constructor(...data: object[]);

	chain: boolean;
	neckCoverage: number;
	cleavageCoverage: number;
	curveX: number;
	curveY: number;
	thickness: number;
	dash: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class StarPart extends ClothingPart {
	constructor(...data: object[]);

	starThickness: number;
	radius: number;
	spikes: number;
	upwards: boolean;
	styleOuter: number; //1 = circle around; 0 = nothing; -1 = polygon around
	styleInner: number; //1 = circle inside; 0 = nothing; -1 = polygon inside

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class TNecklacePart extends ClothingPart {
	constructor(...data: object[]);

	chain: boolean;
	neckCoverage: number;
	cleavageCoverage: number;
	curveX: number;
	curveY: number;
	thickness: number;

	size: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Necklace extends NeckAccessory {
	constructor(...data: object[]);

	//stroke: string;
	//fill: string;
}

export class BiChain extends Necklace {
	constructor(...data: object[]);

	cleavageCoverage: number;
	chain: boolean;
	curveX: number;
	neckCoverage: number;


	beadsSize: number;
	spaceSize: number;
	beadThickness: number;
	thickness: number;

	highlight: string;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class DoubleNecklace extends Necklace {
	constructor(...data: object[]);

	cleavageCoverage: number;
	cleavageCoverageTop: number;
	chain: boolean
	neckCoverage: number;
	thickness: number;
	highlight: string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class MultiNecklace extends Necklace {
	constructor(...data: object[]);

	//curveX: -1,
	cleavageCoverage: number;
	neckCoverage: number;
	chain: boolean;
	curveX: number;

	thickness: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class PearlNecklace extends Necklace {
	constructor(...data: object[]);

	//curveX: -1,
	cleavageCoverage: number;
	chain: boolean;
	curveX: number;
	neckCoverage: number;

	beadsSize: number;
	beadThickness: number;
	spaceSize: number;
	thickness: number;
	highlight: string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class SimpleChain extends Necklace {
	constructor(...data: object[]);

	cleavageCoverage: number;
	chain: boolean;
	curveX: number;
	neckCoverage: number;
	thickness: number;
	dash: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class StarNecklace extends Necklace {
	constructor(...data: object[]);

	curveX: number;
	cleavageCoverage: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class TNecklace extends Necklace {
	constructor(...data: object[]);

	//curveX: -1,
	cleavageCoverage: number;
	chain: boolean;
	curveX: number;
	neckCoverage: number;
	thickness: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class TearNecklace extends Necklace {
	constructor(...data: object[]);

	cleavageCoverage: number;
	chain: boolean;
	curveX: number;
	neckCoverage: number;
	thickness: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ThickChain extends Necklace {
	constructor(...data: object[]);

	cleavageCoverage: number;
	chain: boolean;
	curveX: number;
	neckCoverage: number;
	thickness: number;
	dash: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
