import {DecorativePart} from "./decorative_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {BodyPart} from "../parts/part";

export class QoSBeard extends DecorativePart {
	constructor(...data: object[]);

	stroke(): string;

	fill(ignore: any, ex: DrawingExports): string;
	clipFill(): void;
	calcDrawPoints(ex: DrawingExports, mods: any, calculate: boolean, part: BodyPart): DrawPoint[];
}
