// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"sexy nun cowl": { // +12
			name: "sexy nun cowl", shortDesc: "sexy {COLOR} nun cowl and scapular",
			longDesc: "\
    This combination of cowl and scapular seems mostly typical with the exception that instead of a solid \
    piece of cloth, the scapular component is made out of flowing black lace, all the better to show off \
    the cleavage of whomever is wearing it.\
    ",
			slot: ClothingSlot.Hat,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.NaughtyNun],
			inMarket: false,
		},

		// NECK SLOT

		// NIPPLES SLOT

		// BRA SLOT

		// CORSET SLOT

		// PANTY SLOT

		// STOCKINGS SLOT

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"naughty nun dress": { // +40
			name: "naughty nun dress", shortDesc: "sexy {COLOR} religious dress",
			longDesc: "\
    This form fitting dress is short, shoulderless and backless, with a corsetting of string running down the back and ending \
    just below your ass cleavage. The piping around the top of the dress is a solid white color and extends downwards towards \
    your navel, flaring out in a copy of a popular religious symbol.\
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.NaughtyNun],
			inMarket: false,
		},

		// COSTUME SLOT
		"sexy nun habit": { // +40
			name: "sexy nun habit", shortDesc: "{COLOR} sexy nun's costume",
			longDesc: "\
    This costume is designed to copy a typical nun's habit. It is daringly short and made out of silk with a revealing \
    neckline and a fake belt made out of beads ending in a dangling religious symbol.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.NaughtyNun, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// SHOES SLOT
		"sexy nun boots": { // +20
			name: "sexy nun boots", shortDesc: "pair of {COLOR} thigh high leather boots",
			longDesc: "\
    This boots are made of supple leather that fits closely to your leg. The heel is a daringly tall and pointed \
    6 inch stiletto. The tips of the shoes are pointed and running up the front of each shoe to the top of the \
    boots is a white leather stripe that forms the iconography of a popular religious symbol.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FANCY_MOVES"],
			style: [Fashion.Style.NaughtyNun, Fashion.Style.SexyDancer],
			inMarket: false,
		},
		// BUTT SLOT

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
