import {FacePart} from "./face_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class Brow extends FacePart {
	constructor(...data: object[]);
}


export class BrowHuman extends Brow {
	constructor(...data: object[]);

	fill(ignore:any, ex:DrawingExports): string;
    calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
