namespace App.Data {
	Object.append(jobs, {
		ISLA_MASCOT: {
			title: "Store Mascot",
			giver: "IslaShopKeeper",
			pay: 40,
			rating: 3, // of 5
			phases: [0, 1, 2, 3],
			days: 1,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 1},
			],
			requirements: [
				{type: "meta", name: "style",  value: 50, condition: "gte"},
				{type: Stat.Core, name: CoreStat.Femininity, value: 10, condition: "gte"},
			],
			variables: {
				LOOT: 0,
				REWARD: false,
			},
			start: "\
        NPC_NAME eyes you up and down, muttering to herself and then nods slowly in your direction.\n\n\
        s(I'll be honest dear, this job doesn't pay much, but it doesn't require much from you either. \
        All you need to do is wear one of the outfits from my shop and beckon customers inside. You can \
        start immediately and I'll pay you at the end of your shift.)\n\n\
        This sounds okay to you. Sure, it's probably not a lot of money, but it should be easy work. \
        But what about that costume…\n\n\
        Almost on cue NPC_NAME reaches behind her counter and pulls out a frilly pink dress along with a \
        pair of the highest platform shoes you've ever seen before. What kind of clothes does this shop sell \
        anyway? Before you can ask, she shoves the garments into your hands and directs you to a back room \
        to change.\n\n\
        Moments later you are standing in front of shop looking like a cross between a little girl and a prostitute. \
        You sigh to yourself and begin to call out to random people passing by.\
        ",
			intro: "\
        NPC_NAME says, s(I'm looking for help driving customers into the shop. The pay's not great, \
        but it's easy work.)\
        ",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Charisma.Seduction, value: 50}],
					post: [
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "A"},
						{type: "money", value: 20, factor: "A"},
						{type: "statXp", name: CoreStat.Femininity, value: 50, factor: "A"},
					],
					text: [
						"Several potential customers, both men and women, walk by. You call out to them extolling the \
            virtues of the wares inside the shop. At least you think you are considering you're not exactly \
            sure what this shop sells, or if it's any good. Still, lying isn't difficult and you do your best.\
            ",
						{
							A: 33,
							text: "\
                        Despite your efforts and outgoing antics @@not very many@@ customers enter the store. \
                        Hopefully it'll be enough.",
						},
						{
							A: 66,
							text: "You seem to have a @@modest success@@ at cajoling people to check out the store. \
                    It's surprisingly more tiring than you thought to be so outgoing.",
						},
						{
							A: 500,
							text: "You put on your best face and try to exert your 'girlish charm'. It seems to \
                    @@have the desired effect@@ and you draw a respectable amount of customers to the shop.",
						},
						"Eventually your shift ends and you saunter back inside.",
					],
				},
				{   // If pass check 'A' and If counter is at MAX (5), reward player and set counter to 0.
					id: "SCENE04a",
					triggers: [
						{type: "tag", name: "A"},
						{type: 'var:n', name: "LOOT", value: 5, condition: "gte"},
					],
					post: [
						{type: "var", name: "LOOT", value: 0},
						{type: "var", name: "REWARD", value: true},
						{type: Items.Category.LootBox, name: "common sissy loot box", value: 1},
					],
					text: "\
                NPC_NAME says, s(Here, you've earned this PLAYER_NAME.) She hands you a small novelty gift box \
                shaped like a treasure chest, you wonder what's inside it?\
                ",
				},
				{   // If passed the check then increment counter.
					// Doesn't trigger if the counter is already at MAX (5) or the reward has been given this time.
					id: "SCENE04b",
					triggers: [
						{type: "tag", name: "A"},
						{type: "var:n", name: "LOOT", value: 4, condition: "lte"},
						{type: "var:b", name: "REWARD", value: false},
					],
					post: [{type: "var", name: "LOOT", value: 1, op: "add"}],
					text: "\
                NPC_NAME says, s(Nice work today. You really brought in those customers. Keep it up and I \
                    might slip you a little bit something extra…)\
                    ",
				},
				{   // Just unset the flag that shows we received loot and make sure we clear the counter.
					id: "SCENE04c",
					triggers: [{type: "var:b", name: "REWARD", value: true}],
					post: [{type: "var", name: "REWARD", value: false}],
				},
			],
			end: "\
        NPC_NAME comes up to you and hands you your pay for the shift. JOB_RESULTS",
			jobResults: [
				{
					A: 33,
					text: "It seems that she was a @@little disappointed@@ in the turn out, but hey at least you still got paid.",
				},
				{
					A: 66,
					text: "It appears you did @@reasonably well@@ and the pay reflects that.",
				},
				{
					A: 500,
					text: "You definitely did a @@good job@@ and the pay reflects that.",
				},
			],
		},
	});
}
