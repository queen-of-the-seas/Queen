namespace App.Data {
	Object.append(jobs, {
		SHIP_NAV01: {
			title: "Assist at the helm", tags: ["location:HELM"], pay: 100,
			rating: 3, // of 5
			phases: [0],
			days: 1,
			cost: [
				{type: "time", value: 3},
				{type: Stat.Core, name: CoreStat.Energy, value: 5},
			],
			requirements: [
				{type: Stat.Skill, name: Skills.Piracy.Navigating, condition: "gte", value: 20},
				{type: Stat.Skill, name: Skills.Piracy.Sailing, condition: "gte", value: 20},
				{type: "var:b", scope: {type: "job", id: "MATE03"}, name: "NAV_REWARD", value: true, condition: "eq", hideTask: true},
				{type: "inPort", value: 0, condition: false, hideTask: true},
			],
			intro: "Not used",
			start: `You approach the ships helm, nodding briefly to @@.npc;Kipler@@ as you assume your station. Normally the huge \
    pirate stares at you with lascivious eyes, but when it comes his job of piloting the @@.npc;Salty Mermaid@@ he's quite \
    the professional.`,
			scenes: [
				{
					id: "SCENE01",
					checks: [
						{tag: "A", type: Stat.Skill, name: Skills.Piracy.Sailing, value: 60},
						{tag: "B", type: Stat.Skill, name: Skills.Piracy.Navigating, value: 60},
					],
					post: [
						{type: "money", value: 30, factor: "A"},
						{type: "money", value: 30, factor: "B"},
					],
					text: [
						"The morning starts off well enough, easy sailing as they say. However by mid-afternoon a small squall looms on the horizon. \
            You do your best to try and avoid it, but ultimately you have no choice but to try and steer through it.",
						{
							A: 40, text:
								"Unfortunately it becomes quickly apparent that @@your skills are not up for the task@@ and Kipler rushes over to the helm to assist \
                    you. Within moments he has the ship smoothly breaking the waves and heading on it's course, leaving you looking like a fool.",
						},
						{
							A: 80, text:
								"You @@struggle painfully against the wheel@@ of the helm for what seems like hours, occasionally Kipler will come by and help you out \
                    of a particularly bad swell, but other than that you're left to fend for yourself.",
						},
						{
							A: 500, text:
								"Your skills at the helm surprise even you as you @@masterfully navigate each swell@@ and guide the ship towards it's course.",
						},
						"Eventually the clouds break and the seas calm. With a steady wind at your back you do your best to make up for lost time.",
					],
				},
				{
					id: "GOOD_SAILING",
					triggers: [
						{type: "tag", name: "A"},
						{type: "tag", name: "B"},
					],
					post: [{type: "sailDays", value: 1}],
					text: "You made amazing time today, even @@.npc;Kipler@@ noticed how switched on you were. You feel as if \
            the wind was pushing you towards your destination and all you had to do was just gently nudge the ship in the right \
            direction. It's a satisfying feeling.",
				},
			],
		},
	});
}
