import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";



//TO DO WIP
export function calcGlove(ex: DrawingExports): {
	outerArmPoints: DrawPoint[],
	innerArmPoints: DrawPoint[]
};


export class GloveSleevePart extends ClothingPart {
	constructor(...data: object[]);
	armCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class GlovePart extends ClothingPart {
	constructor(...data: object[]);

	armCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class FingerlessGlovePart extends ClothingPart {
	constructor(...data: object[]);
	armCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class BraceletPart extends ClothingPart {
	constructor(...data: object[]);
	armCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class Glove extends Clothing {
	constructor(...data: object[]);

	thickness: number;

	fill(): string;
}



export class GloveSleeve extends Glove {
	constructor(...data: object[]);

	armCoverage: number;
	thickness: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class LongGloves extends Glove {
	constructor(...data: object[]);

	fill(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class FingerlessGloves extends Glove {
	constructor(...data: object[]);

	armCoverage: number;

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Bracelet extends Glove {
	constructor(...data: object[]);

	armCoverage: number;

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class BraceletLeft extends Glove {
	constructor(...data: object[]);

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BraceletRight extends Glove {
	constructor(...data: object[]);

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
