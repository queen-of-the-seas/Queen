import {DecorativePart} from "./decorative_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {BodyPart} from "../parts/part";
import {DrawingExports} from "../draw/draw";

export class BeautyMark extends DecorativePart {
	constructor(...data: object[]);

	stroke(): string;
	clipFill(): void;
	fill(ignore: any, ex: DrawingExports): string;
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean, part: BodyPart): DrawPoint[];
}
