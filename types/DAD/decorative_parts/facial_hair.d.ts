import {DrawingExports} from "../draw/draw";
import {DecorativePart} from "./decorative_part";

declare class FacialHair extends DecorativePart {
	constructor(...data: object[]);

	stroke(): string;
	clipFill(): void;
	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
}


export class Mustache extends FacialHair {
	constructor(...data: object[]);

	stroke(): string;
	clipFill(): void;
	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
}
