import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class LeftBaseShoeShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


export class RightBaseShoeShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


export class ShoeSidePart extends ClothingPart {
	constructor(...data: object[]);
}


export class ShoePart extends ClothingPart {
	constructor(...data: object[]);
}


export class ShoeBasePart extends ShoePart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Shoe extends Clothing {
	constructor(...data: object[]);

	/**
	 * How high the heel is to lift the player's height
	 */
	shoeHeight: number;
	/**
	 * How high the tongue of the shoe (assuming it has one) goes up
	 */
	tongueDeflection: number;
	/**
	 * How high the toe-box of the shoe is
	 */
	toeHeight: number;
	Mods: {
		feetLength: -10,
		feetWidth: -2
	}
}


export class FlatShoes extends Shoe {
	constructor(...data: object[]);

	shoeHeight: number;
	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
