// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange

namespace App.Data {
	Object.append(lootBoxes, {
		"common lolita loot box": {
			name: "common lolita chest",
			shortDesc: "a @@.item-common;small novelty bag shaped like a teddy bear@@",
			longDesc: "This small pink bag is shaped like a stuffed toy and opens with a drawstring on it's back. Wonder what's inside?",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["LOLITA_SISSY_LOOT_BOX_COMMON"],
		},

		"uncommon lolita loot box": {
			name: "uncommon lolita chest",
			shortDesc: "a @@.item-uncommon;novelty bag shaped like a teddy bear@@",
			longDesc: "This pink bag is shaped like a stuffed toy and opens with a drawstring on it's back. Wonder what's inside?",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["LOLITA_SISSY_LOOT_BOX_UNCOMMON"],
		},

		"rare lolita loot box": {
			name: "rare lolita chest",
			shortDesc: "a @@.item-rare;large novelty bag shaped like a teddy bear@@",
			longDesc: "This large pink bag is shaped like a stuffed toy and opens with a drawstring on it's back. Wonder what's inside?",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["LOLITA_SISSY_LOOT_BOX_RARE"],
		},

		"legendary lolita loot box": {
			name: "legendary lolita chest",
			shortDesc: "a @@.item-legendary;huge novelty bag shaped like a teddy bear@@",
			longDesc: "This huge pink bag is shaped like a stuffed toy and opens with a drawstring on it's back. Wonder what's inside?",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["LOLITA_SISSY_LOOT_BOX_LEGENDARY"],
		},
	});

	App.Data.lootTables["LOLITA_SISSY"] = [
		makeLootTableItemNoFilter("coins", 100, 1, 50),
		makeLootTableItem(Items.Category.Clothes, 100, 1, "style", [Fashion.Style.SissyLolita, Fashion.Style.DaddyGirl]),
		makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		makeLootTableItem(Items.Category.Food, 75, 2, "effects", [
			"LIPS_XP_COMMON", "LIPS_XP_UNCOMMON",
			"LIPS_XP_RARE", "LIPS_XP_LEGENDARY",
		]),
		makeLootTableItem(Items.Category.Food, 50, 4, "effects", ["WHOLESOME_MEAL", "SNACK", "LIGHT_WHOLESOME_MEAL"]),
		makeLootTableItem(Items.Category.Food, 25, 2, "effects", ["HARD_ALCOHOL"]),
		makeLootTableItem(Items.Category.Drugs, 75, 2, "effects", [
			"LIPS_XP_COMMON", "LIPS_XP_UNCOMMON", "LIPS_XP_RARE", "LIPS_XP_LEGENDARY",
			"FACE_XP_COMMON", "FACE_XP_UNCOMMON", "FACE_XP_RARE", "FACE_XP_LEGENDARY",
		]),
		makeLootTableItem(Items.Category.Drugs, 50, 2, "effects", [
			"LIPS_XP_COMMON", "LIPS_XP_UNCOMMON", "LIPS_XP_RARE", "LIPS_XP_LEGENDARY",
			"FEMININITY_XP_COMMON", "FEMININITY_XP_UNCOMMON", "FEMININITY_XP_RARE", "FEMININITY_XP_LEGENDARY",
		]),
	];
}
