// Gothic Lolita Style, contributed by 'littleslavegirl' @ wwww.tfgamessite.com Forums

// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"red and black flower hair pin": { // +9
			name: "red and black flower hair pin", shortDesc: "a red and {COLOR} flower",
			longDesc: "A black rose that adorns your hair, a trio of black ribbons hang down to which are attached 3 blood red stars.",
			slot: ClothingSlot.Hat, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita],
		},

		"dark dreams bonnet": { // +12
			name: "dark dreams bonnet", shortDesc: "DarkDreams &trade, brand hair bonnet",
			longDesc: "\
    This elegant bonnet is doesn't fully cover the head, instead resting on top of ones hair and tied with a \
    simple black string. The bonnet itself is flat and wide, made of burgundy velvet with black piping and adorned \
    with an array of black roses.\
    ",
			slot: ClothingSlot.Hat,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.GothicLolita],
			inMarket: false,
		},

		// NECK SLOT
		"black gothic collar": { // +9
			name: "black gothic collar", shortDesc: " a {COLOR} ribbon-lined leather collar emblazoned with the word 'SLAVE' burned into it.",
			longDesc: "A snug leather collar lined on both sides in black lace, slave is burned into the leather to remind you exactly what you are..",
			slot: ClothingSlot.Neck, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita],
		},

		// NIPPLES SLOT

		// BRA SLOT
		"gothic black bra": { // +10
			name: "gothic black bra", shortDesc: "a {COLOR} lace gothic style bra",
			longDesc: "A sexy black lace gothic style bra.",
			slot: ClothingSlot.Bra, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita, Fashion.Style.Bimbo, Fashion.Style.SexyDancer],
		},

		// CORSET SLOT

		// PANTY SLOT
		"black panties": { // +10
			name: "black panties", shortDesc: "a pair of {COLOR} panties",
			longDesc: "Black sexy silk panties just asking for someone to remove them.",
			slot: ClothingSlot.Panty, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita, Fashion.Style.Bimbo, Fashion.Style.SexyDancer],
		},

		"gothic frilly bloomers": { // +15
			name: "gothic frilly bloomers", shortDesc: "a pair of frilly {COLOR} cotton bloomers",
			longDesc: "These bloomers are cut short and have a frilly bottom.",
			slot: ClothingSlot.Panty, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita],
		},

		// STOCKINGS SLOT
		"gothic striped stockings": { // +15
			name: "gothic striped stockings", shortDesc: "a pair of {COLOR} and purple striped stockings",
			longDesc: "These stockings come up over your knees. They are striped, black and purple, with a slight lacey top that is adorned with tiny black roses.",
			slot: ClothingSlot.Stockings, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita],
		},

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"gothic frilly dress": { // +40
			name: "gothic frilly dress", shortDesc: "a {COLOR} gothic dress",
			longDesc: "A short sleeved snug black knee length dress. Lace along the the collar, sleeves and along the bottom of the heavily petticoated skirt that bounces with every step you take.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita],
		},

		"dark dreams dress": { // +40
			name: "dark dreams dress", shortDesc: "DarkDreams &trade, brand lolita dress",
			longDesc: "\
    This extravagant dress is made from dark burgundy velvet with black piping and lace. The bodice is narrow and \
    form fitting as it meets a lace covered plunging neckline. The arms are frilly and equally as trimmed with cuffs \
    that nearly bury your hands. The skirt is provocatively short, ending at about mid thigh and just barely offering \
    a tantalizing glimpse of what lies underneath.\
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "burgundy", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.GothicLolita],
			inMarket: false,
		},

		// COSTUME SLOT
		"gothic maid outfit": { // +40
			name: "gothic maid outfit", shortDesc: "dark velvet gothic maid outfit",
			longDesc: "\
    This costume is a racy version of the uniform you might find on a high class maid. A velvet black bodice \
    meets a laced covered plunging neckline and the skirt, while far too short to be worn by a real maid, is packed \
    with black crinole and a short black and white striped imitation of a petticoat designed to draw the attention \
    of the eye.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.GothicLolita, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// SHOES SLOT
		"black high heeled boots": { // +15
			name: "black high heeled boots", shortDesc: "A pair of black knee length heeled boots",
			longDesc: "A pair of knee high black boots with a 5 inch heel and 1 inch platform. Red stars dangle from the zipper.",
			slot: ClothingSlot.Shoes, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita, Fashion.Style.SexyDancer],
		},

		"black platform mary janes": { // +15
			name: "black platform mary janes", shortDesc: "a pair of {COLOR} platform mary janes",
			longDesc: "These black platform shoes have Have red stars for the buckles at the ankles and a chunky platform heel. Difficult to walk in but sexy.",
			slot: ClothingSlot.Shoes, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.GothicLolita],
		},

		"dark dreams shoes": { // +20
			name: "dark dreams shoes", shortDesc: "DarkDreams &trade, brand platform shoes",
			longDesc: "\
    These shoes are made in the popular 'maryjane' style, complete with toestrap, buckle and a chunk heel. The \
    platform is tall and the shoe is heavy causing you to walk in an exaggerated prancing gait. They are extremely \
    well made and durable and despite everything incredibly comfortable to walk in.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.GothicLolita, Fashion.Style.SexyDancer],
			inMarket: false,
		},
		// BUTT SLOT

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
