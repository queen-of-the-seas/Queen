Macro.add("playScene", {
	handler() {
		const scenePath = this.args[0] as string;
		const sceneData = App.Data.scenes[scenePath];
		const sceneDiv = document.createElement('div');
		void App.Text.playScene(sceneData, setup.world, sceneDiv);
		this.output.append(sceneDiv);
	},
});
