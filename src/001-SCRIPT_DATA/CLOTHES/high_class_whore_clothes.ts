// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"gold hairpin": {
			name: "gold hairpin", shortDesc: "a {COLOR} hair pin",
			longDesc: "\
    This delicate hair pin is plated in 24 karat gold and adds a touch of class to your appearance\
    ",
			slot: ClothingSlot.Hat,
			color: "gold", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: [],
			activeEffect: [],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady],
			inMarket: true,
		},

		// NECK SLOT
		"gold necklace": {
			name: "gold necklace", shortDesc: "a flash {COLOR} necklace with gems",
			longDesc: "\
    This flashy necklace is encrusted with sparkly fake gemstones. \
    ",
			slot: ClothingSlot.Neck,
			color: "gold", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: [],
			activeEffect: [],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady],
			inMarket: true,
		},

		// NIPPLES SLOT

		// BRA SLOT
		"luxurious purple bra": { // +20
			name: "luxurious purple bra",
			shortDesc: "{COLOR} strapless lace bra",
			longDesc: "\
    Besides having expertly crafted sexy lace, this bra can support quite a generous bust without straps and without \
    covering cleavage area. It is ideal for wearing with dresses with uncovered shoulders and deep cleavage.",
			slot: ClothingSlot.Bra,
			color: "purple", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady],
		},

		// CORSET SLOT
		"whore belt": { // +40
			name: "whore belt", shortDesc: "golden chain with the word 'WHORE'",
			longDesc: "\
    This delicate golden chain is designed to be worn off the hips and has large golden letters \
    hanging from it that spell out the word 'W-H-O-R-E' just incase anyone had any doubts in their mind.\
    ",
			slot: ClothingSlot.Corset,
			color: "gold", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// PANTY SLOT
		"luxurious purple panties": { // +20
			name: "luxurious purple panties",
			shortDesc: "{COLOR} satin lace panties",
			longDesc: "These panties are a perfect combination of conservatism and sexiness. They are not panties that are pushed aside because the man is too horny to bother to take them of. These panties are removed in a richly decorated room, illuminated by candles, while orchestra can be faintly heard from the ball room.",
			slot: ClothingSlot.Panty,
			color: "purple", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady],
		},

		// STOCKINGS SLOT
		"luxurious purple stockings": { // +20
			name: "luxurious purple stockings",
			shortDesc: "pair of {COLOR} stockings with an intricate pattern",
			longDesc: "These stockings end halfway between knee and crotch, leaving enough skin to expose under a miniskirt or through a dress cut. They seem to have combination of several patterns of different scale, first coarse, then finer, then even finer, you aren't even sure you can see the finest. This creates almost hypnotic effect, drawing eyes to the legs of the wearer.",
			slot: ClothingSlot.Stockings,
			color: "purple", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SluttyLady],
		},

		// SHIRT SLOT
		"red halter top": { // +20
			name: "red halter top",
			shortDesc: "a low cut {COLOR} halter top",
			longDesc: "\
    This thin halter top exposes a generous amount of cleavage. The flimsy material allows for your nipples \
    to clearly be seen if you are not wearing a bra.",
			slot: ClothingSlot.Shirt,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "red",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.HighClassWhore],
		},

		// PANTS SLOT
		"white microskirt": { // +20
			name: "white microskirt",
			shortDesc: "a very short {COLOR} microskirt",
			longDesc: "\
    This outrageously short skirt is little more than a strip of thin fabric designed to show off more than it conceals.",
			slot: ClothingSlot.Pants,
			restrict: [ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "white",
			rarity: Items.Rarity.Legendary,
			type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.HighClassWhore],
		},

		// DRESS SLOT
		"slutty strumpet dress": { // +40
			name: "slutty strumpet dress", shortDesc: "slutty {COLOR} dress",
			longDesc: "\
    This backless dress plunges down to the middle of your arsecheeks, displaying a generous amount of \
    butt-cleavage. It has a tight bodice that lifts and display your breasts prominently and a hemline \
    that is just short of scandalous. Pefect for advertising your wares. \
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "red", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.HighClassWhore],
			inMarket: false,
		},

		// COSTUME SLOT
		"sexy librarian outfit": { // +40
			name: "sexy librarian outfit", shortDesc: "sexy librarian outfit",
			longDesc: "\
    A classic combination of demure and slutty, the high waisted pencil skirt is designed to hug and stretch \
    over the buttocks while the flimsy white ruffled top is casually left unbuttoned to show off an impressive \
    amount of décolletage.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// SHOES SLOT
		"whore sandals": { // +20
			name: "whore sandals", shortDesc: "pair of {COLOR} high heeled sandals",
			longDesc: "\
    These sandals have a medium height heel on them and consist of a series of delicate straps \
    that wind upwards and around your leg to be tied in a bow.\
    ",
			slot: ClothingSlot.Shoes,
			color: "red", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FANCY_MOVES"],
			style: [Fashion.Style.HighClassWhore, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		"red wedges": { // +20
			name: "red wedges", shortDesc: "pair of wedged {COLOR} shoes",
			longDesc: "\
    These shoes support your feet with a large thick wedge that adds an extra few inches onto your height. \
    They are surprisingly comfortable and easy to walk in despite this. \
    ",
			slot: ClothingSlot.Shoes,
			color: "red", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.HighClassWhore],
			inMarket: true,
		},

		// BUTT SLOT

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
