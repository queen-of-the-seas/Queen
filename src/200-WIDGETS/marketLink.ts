Macro.add("marketLink", {
	handler() {
		const se = App.storeEngine;
		const npc = setup.world.npc(this.args[0] as string);
		if (se.hasStore(npc) && se.isOpen(npc) && setup.world.phase < App.DayPhase.LateNight) {
			if (setup.world.pc.coreStats.energy < 1) {
				App.UI.appendFormattedText(this.output, {text: 'Shop - Too tired.', style: 'state-disabled'});
			} else {
				const link = App.UI.passageLink("Shop", "Shop", () => {
					const st = se.openStore(setup.world.pc, npc);
					st.generateMarket();
					variables().menuAction = npc;
					setup.world.pc.adjustCoreStat(App.CoreStat.Energy, -1);
					setup.world.nextPhase(1);
					if (!tags().includes("custom-menu")) {
						variables().gameBookmark = passage();
					}
				});
				this.output.append(link);
				App.UI.appendFormattedText(this.output,
					' - [', {text: "Time 1", style: 'item-time'}, ' ', {text: "Energy 1", style: 'item-energy'}, ']'
				);
			}
		} else {
			App.UI.appendNewElement('span', this.output, {content: 'Shop - closed', classNames: 'state-disabled'});
		}
	},
});
