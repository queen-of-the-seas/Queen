namespace App.DebugUtils {
	export function checkTravelDestinations(): void {
		const allStartPoints = Object.keys(App.Data.Travel.destinations);
		const checkDestinationExists = (d: string, from: string) => {
			if (!allStartPoints.includes(d)) {
				console.error(`No routes from ${d}, enter from ${from}`);
			}
		}

		const allDestinations: Set<string> = new Set();

		for (const k of allStartPoints) {
			for (const d of Data.Travel.destinations[k]) {
				if (typeof d === "string") {
					checkDestinationExists(d, k);
					allDestinations.add(d);
				} else if (UI.isTravelDestinationWithScene(d)) {
					if (Data.Travel.scenes[d.scene] === undefined) {
						console.error(`No travel scene ${d.scene} defined for location ${k}`);
					}
				} else {
					const destination = d.destination;
					if (destination) {
						const destinationString = typeof destination === "function" ? destination(setup.world.pc) : destination;
						checkDestinationExists(destinationString, k);
						allDestinations.add(destinationString);
					}
				}
			}
		}

		Story.filter(p => !p.tags.includesAny('custom-menu', 'Twine.audio', 'Twine.image'))
			.filter(p => !allDestinations.has(p.name))
			.forEach((p) => {console.log(`No route to ${p.name}`);})
	}

	export function enableAllTasksDebugOutputs(): void {
		Task.debugPrintEnabled = true;
	}

	export function debugColorScale(): string {
		let str = "";
		for (let i = 0; i < 16; i++)
			str += `<span style='color:${UI.colorScale(i, 16)}'>Index ${i} = ${UI.colorScale(i, 16)}</span><br/>`;
		return str;
	}

	// eslint-disable-next-line @typescript-eslint/ban-types
	export function dirObject(o: object): string {
		const res: string[] = [];
		for (const entry of Object.entries(o)) {
			res.push(`${String(entry[0])} = ${entry[1]}`); // eslint-disable-line @typescript-eslint/restrict-template-expressions
		}
		return res.join(', ');
	}
}
