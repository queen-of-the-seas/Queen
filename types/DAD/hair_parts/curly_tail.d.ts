import {HairPart} from "./hair_part";

export class CurlyTailFront extends HairPart {
	constructor(...data: object[]);

	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}


export class CurlyTailMedium extends HairPart {
	constructor(...data: object[]);

	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}
