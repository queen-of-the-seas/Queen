namespace App.Data {
	stores.smugglers = {
		name: "The Smuggler's Den", open: [0, 1, 2, 3, 4], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "cheap wine"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "rum"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "smugglers ale"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 4, max: 4, price: 1.0, mood: 40, lock: 0, tag: "fairy dust"},
			{category: Items.Rarity.Common, type: Items.Category.MiscConsumable, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "torch"},
			{category: Items.Rarity.Common, type: Items.Category.MiscConsumable, qty: 8, max: 8, price: 1.0, mood: 0, lock: 0, tag: "shovel"},
			// LOCKED ITEMS
			{category: Items.Rarity.Common, type: Items.Category.Weapon, qty: 1, max: 1, price: 1.0, mood: 50, lock: 1, tag: "steel cutlass"},
			// RARE ITEMS
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "female mandrake"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "male mandrake"},
		],
	};
}
