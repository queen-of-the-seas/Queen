namespace App.Data {
	Object.append(events, {
		Portside: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.stat(Stat.Body, BodyPart.Bust) >= 50 && State.random() < 0.33,
			},
		],

		Bazaar: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.stat(Stat.Body, BodyPart.Bust) >= 50 && State.random() < 0.33,
			},
		],

		GI_GovernorsMansionInside: [
			{
				id: 'BertieMapStolen',
				from: 'GI_GovernorsMansion',
				force: true,
				maxRepeat: 1,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BertieMapStolenEvent',
				check: p => Quest.byId('BERTIE_SISSY').completedOn(p, Number.POSITIVE_INFINITY) + 2 <= setup.world.day,
			},
		],
	});
}
