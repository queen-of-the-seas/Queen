namespace App.UI.Widgets.Wardrobe {

	function toggleCollapsible(this: HTMLButtonElement) {
		this.classList.toggle("wardrove-desc-cover-active");
		const content = this.nextElementSibling as HTMLDivElement;
		content.style.maxHeight = content.style.maxHeight ? "" : `${content.scrollHeight}px`;
	}

	const enum Style {
		ItemDetails = 'wardrove-item-details',
		DescCover = 'wardrove-desc-cover',
	}

	class SlotPanel {
		readonly #table: WardrobeTable;
		readonly #slot: ClothingSlot;

		constructor(table: WardrobeTable, slot: ClothingSlot) {
			this.#table = table;
			this.#slot = slot;
		}

		render(): HTMLTableRowElement {
			const eqInSlot = this.#table.player.equipment[this.#slot];
			const res = document.createElement('tr');
			appendNewElement('td', res, {content: this.#slot});
			const descCell = appendNewElement('td', res);
			const desc = appendNewElement('button', descCell);
			const descContent = appendNewElement('div', descCell, {classNames: Style.ItemDetails});
			desc.classList.add(Style.DescCover);
			desc.addEventListener("click", toggleCollapsible.bind(desc));
			if (eqInSlot) {
				$(desc).wiki(eqInSlot.description);
			} else {
				appendNewElement('span', desc, {content: 'Nothing', classNames: 'state-disabled'});
			}
			// details and options
			if (eqInSlot) {
				descContent.append(this._makeRemoveCard(eqInSlot)); // will handle locked items
			}
			if (!eqInSlot?.isLocked) {
				const items = this.#table.player.wardrobeItemsBySlot(this.#slot);
				items.sort(Items.compareByRank);
				for (const itm of items) {
					descContent.append(this._makeWearCard(itm));
				}
			}

			return res;
		}

		private _makeOptionCard(item: Items.IEquipmentItem, actionCaption: string, action?: () => void) {
			const element = makeElement('div', {classNames: 'wardrove-change-option'});
			const buttonAction = appendNewElement("button", element, {content: actionCaption, classNames: "wardrobe"});
			if (action) {
				buttonAction.addEventListener('click', action);
			} else {
				buttonAction.classList.add('disabled-wardrobe');
			}
			const title = appendNewElement("span", element, {classNames: 'wardrobe-option-caption'});
			$(title).wiki(item.description);
			const desc = appendNewElement("div", element, {classNames: 'wardrobe-item-description'});
			$(desc).wiki(item.examine(this.#table.player));
			return element;
		}

		private _wear(item: Items.IEquipmentItem) {
			this.#table.player.wear(item);
			this.#table.parent.update();
		}

		private _remove(item: Items.IEquipmentItem) {
			this.#table.player.remove(item);
			this.#table.parent.update();
		}

		private _makeWearCard(item: Items.IEquipmentItem) {
			return this._makeOptionCard(item, "Wear", this._wear.bind(this, item));
		}

		private _makeRemoveCard(item: Items.IEquipmentItem) {
			return item.isLocked ?
				this._makeOptionCard(item, "Locked") :
				this._makeOptionCard(item, "Remove", this._remove.bind(this, item));
		}
	}

	class WardrobeTable extends SelfUpdatingTable {
		readonly #player: Entity.Player;
		readonly #slotPanels: SlotPanel[] = [];
		readonly #parent: WardrobeUI;

		constructor(player: Entity.Player, parent: WardrobeUI) {
			super();
			this.#parent = parent;

			const footer = appendNewElement('tfoot', this.table);
			const footerRow = appendNewElement('tr', footer);
			appendNewElement('td', footerRow);
			const footerCell = appendNewElement('td', footerRow);
			const getNakedLink = App.UI.appendNewElement('a', footerCell, {content: "Get naked", classNames: 'link-internal'});
			getNakedLink.addEventListener('click', () => {
				setup.world.pc.strip();
				this.#parent.update();
			});
			this.#player = player;

			this.table.classList.add('wardrobe-table');

			const orderedSlots: ClothingSlot[] = [
				ClothingSlot.Wig, ClothingSlot.Hat, ClothingSlot.Neck,
				ClothingSlot.Bra, ClothingSlot.Nipples,
				ClothingSlot.Corset, ClothingSlot.Panty, ClothingSlot.Stockings,
				ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume,
				ClothingSlot.Shoes, ClothingSlot.Butt, ClothingSlot.Penis, ClothingSlot.Weapon,
			];

			for (const slot of orderedSlots) {
				this.#slotPanels.push(new SlotPanel(this, slot));
			}

			this.header.append(WardrobeTable.wardrobeTableHeader())
		}

		protected static wardrobeTableHeader(): HTMLTableRowElement {
			const res = document.createElement('tr');
			const slotCol = appendNewElement('th', res, {content: "Slot"});
			slotCol.style.width = "12em";
			const descCol = appendNewElement('th', res, {content: "Item"});
			descCol.style.width = "100%";
			return res;
		}

		get player() {
			return this.#player;
		}

		get parent() {
			return this.#parent;
		}

		update() {
			const rows = this.#slotPanels.map(p => p.render());
			replace(this.body, ...rows);
		}
	}

	class OutfitAutoStyleTable extends SelfUpdatingTable {
		readonly #player: Entity.Player;
		readonly #parent: OutfitStylePanel;

		constructor(player: Entity.Player, parent: OutfitStylePanel) {
			super();
			this.#player = player;
			this.#parent = parent;
		}

		update() {
			replace(this.body, this._makeBody());
		}

		private _makeBody(): DocumentFragment {
			const rows = new DocumentFragment();
			const style = Data.Fashion.Style;
			rows.append(this._styleRow([style.Ordinary, style.PirateSlut, style.Bimbo]));
			rows.append(this._styleRow([style.SissyLolita, style.GothicLolita, style.Bdsm]));
			rows.append(this._styleRow([style.DaddyGirl, style.NaughtyNun, style.PetGirl]));
			rows.append(this._styleRow([style.HighClassWhore, style.SluttyLady, style.SexyDancer]));
			return rows;
		}

		private _styleItem(styleName: Data.Fashion.Style): [HTMLTableCellElement, HTMLTableCellElement] {
			const nameCell = document.createElement('td');
			if (styleName === Data.Fashion.Style.Ordinary) {
				nameCell.textContent = "Ordinary:";
			} else {
				const link = appendNewElement('a', nameCell, {content: styleName, classNames: 'link-internal'});
				link.addEventListener('click', this._wearStyle.bind(this, styleName));
				nameCell.append(':');
			}
			const meterCell = document.createElement('td');
			const meterSpan = appendNewElement('span', meterCell, {classNames: 'fixed-font'});
			meterSpan.append(rMeter(setup.world.pc.getStyleSpecRating(styleName), {min: 0, max: 100}, {showScore: settings.displayMeterNumber}));
			return [nameCell, meterCell];
		}

		private _styleRow(styles: Data.Fashion.Style[]) {
			const row = document.createElement('tr');
			for (const st of styles) {
				row.append(...this._styleItem(st));
			}
			return row;
		}

		private _wearStyle(cat: Data.Fashion.Style) {
			this.#player.autoWearStyle(cat);
			this.#parent.styleChanged();
		}
	}

	class OutfitStylePanel {
		readonly #styleDesc: HTMLSpanElement;
		readonly #stylingChoices: OutfitAutoStyleTable;
		readonly #player: Entity.Player;
		readonly #parent: WardrobeUI;
		readonly #element: HTMLDivElement;

		constructor(player: Entity.Player, parent: WardrobeUI) {
			this.#player = player;
			this.#parent = parent;
			this.#element = document.createElement('div');
			const caption = appendNewElement('button', this.#element, {content: "The style and fashion of your clothing is ", classNames: Style.DescCover});
			this.#styleDesc = appendNewElement('span', caption);
			const content = appendNewElement('div', this.#element, {classNames: Style.ItemDetails});
			caption.addEventListener("click", toggleCollapsible.bind(caption));
			this.#stylingChoices = new OutfitAutoStyleTable(player, this);
			content.append("Current Fashion Breakdown (click a name to automatically dress that way)");
			content.append(this.#stylingChoices.table);
			this.#stylingChoices.render();
			this.update();
		}

		get element() {
			return this.#element;
		}

		update() {
			replace(this.#styleDesc, PR.humanSelfRating(this.#player, 'clothing', {colorize: true}));
			this.#stylingChoices.update();
		}

		styleChanged() {
			this.#parent.update();
		}
	}

	class EquipmentSetsPanel {
		constructor(player: Entity.Player, parent: WardrobeUI) {
			this.#player = player;
			this.#parent = parent;
			this.#element = document.createElement('div');
			const caption = appendNewElement('button', this.#element, {content: "Saved equipment sets", classNames: Style.DescCover});
			const content = appendNewElement('div', this.#element, {classNames: Style.ItemDetails});
			caption.addEventListener("click", toggleCollapsible.bind(caption));
			const selectorId = "equipment-set-selector";

			const wearActionsDiv = content.appendNewElement('div', {classNames: 'spaced-horizontal-container'});
			wearActionsDiv.appendNewElement('label', {content: "Select and apply a saved set: "}).htmlFor = selectorId;
			this.#setSelector = wearActionsDiv.appendNewElement("select");
			this.#setSelector.id = selectorId;
			this.#setSelector.addEventListener('change', this._onSetSelected.bind(this));
			this.#wear = wearActionsDiv.appendNewElement('button', {content: "Wear", classNames: "wardrobe"});
			this.#wear.addEventListener('click', () => {
				this.#player.clothing.wearSet(this._selectedSetIndex());
				this.#parent.update();
			});

			const editActionsDiv = content.appendNewElement('div', {classNames: 'spaced-horizontal-container'});
			this.#name = editActionsDiv.appendNewElement('input');
			this.#name.required = true;
			this.#save = editActionsDiv.appendNewElement('button', {content: "Save", classNames: "wardrobe"});
			tippy(this.#save, {content: "Save currently worn equipment as a named set"});
			this.#save.addEventListener('click', this._saveSet.bind(this));
			this.#remove = editActionsDiv.appendNewElement('button', {content: "Delete", classNames: "wardrobe"});
			this.#remove.addEventListener('click', () => {
				this.#player.clothing.deleteSet(this._selectedSetIndex());
				this.update();
			});

			this.update();
		}

		update(): void {
			this._refreshSavedSets();
			this._onSetSelected();
		}

		get element() {
			return this.#element;
		}

		private _refreshSavedSets() {
			const sets = this.#player.clothing.storedSets.map((set, index) => {return {index, set};});
			sets.sort((a, b) => a.set.name < b.set.name ? -1 : (a.set.name === b.set.name ? 0 : 1));
			const options: HTMLOptionElement[] = [];
			for (const s of sets) {
				const option = document.createElement('option');
				option.value = s.index.toString();
				option.append(s.set.name);
				options.push(option);
			}
			this.#setSelector.replaceChildren(...options);
		}

		private _onSetSelected() {
			const setIndex = this._selectedSetIndex();
			if (setIndex >= 0) {
				const set = this.#player.clothing.storedSets[setIndex];
				const tooltip = new DocumentFragment;
				tooltip.append(`Wear selected set '${set.name}', which consists of:`);
				const slotList = appendNewElement('ul', tooltip);
				for (const [s, it] of Object.entries(set.items)) {
					slotList.appendNewElement('li', {content: `${s}: ${Items.splitId(it).tag}`})
				}
				tippy(this.#wear, {content: tooltip});
				this.#name.value = set.name;
			} else {
				tippy(this.#wear);
				this.#name.value = "";
			}
		}

		private _saveSet() {
			const name = this.#name.value;
			if (name.length === 0) {
				alert("Please provide a name for the set");
				return;
			}
			const existing = this.#player.clothing.storedSets.find(s => s.name === name);
			if (existing) {
				alert(`Set named '${name}' already exists. Please choose a different name.`);
				return;
			}
			this.#player.clothing.saveCurrentSet(this.#name.value);
			this.update();
		}

		private _selectedSetIndex(): number {
			if (this.#setSelector.selectedOptions.length === 0) {
				return -1;
			}
			return Number.parseInt(this.#setSelector.selectedOptions[0].value);
		}

		readonly #player: Entity.Player;
		readonly #parent: WardrobeUI;
		readonly #element: HTMLDivElement;
		readonly #setSelector: HTMLSelectElement;
		readonly #name: HTMLInputElement;
		readonly #wear: HTMLButtonElement;
		readonly #save: HTMLButtonElement;
		readonly #remove: HTMLButtonElement;
	}

	export class WardrobeUI {
		readonly #element: HTMLDivElement;
		readonly #slotsTable: WardrobeTable;
		readonly #stylePanel: OutfitStylePanel;
		readonly #setsPanel: EquipmentSetsPanel;
		readonly #mirror: HTMLDivElement | undefined;

		constructor(player: Entity.Player) {
			this.#element = document.createElement('div');
			this.#stylePanel = new OutfitStylePanel(player, this);
			this.#slotsTable = new WardrobeTable(player, this);
			this.#setsPanel = new EquipmentSetsPanel(player, this);

			this.#element.append(this.#stylePanel.element);
			this.#element.append(this.#setsPanel.element);
			const header = App.UI.appendNewElement('div', this.#element);
			header.style.display = "flex";
			header.style.flexWrap = "nowrap";

			const captionDiv = App.UI.appendNewElement('div', header);
			captionDiv.style.width = "100%";
			const captionH = App.UI.appendNewElement('h3', captionDiv);
			App.UI.appendNewElement('span', captionH, {content: "Worn Items", classNames: 'item-category-general'});
			const slotsAndMirrorContainer = App.UI.appendNewElement('div', this.#element);
			if (settings.displayAvatar) {
				const mirrorContainer = App.UI.appendNewElement('div', slotsAndMirrorContainer);
				mirrorContainer.id = 'mirrorContainer';
				const elementId = 'avatarUI';
				this.#mirror = App.UI.appendNewElement('div', mirrorContainer);
				this.#mirror.id = elementId;
				setup.avatar.drawCanvas(elementId, 800, 360);
			} else {
				this.#mirror = undefined;
			}
			slotsAndMirrorContainer.append(this.#slotsTable.render());
		}

		get element(): HTMLElement {
			return this.#element;
		}

		update(): void {
			this.#stylePanel.update();
			this.#setsPanel.update();
			this.#slotsTable.update();
			if (this.#mirror) {
				setup.avatar.update();
			}
		}
	}
}

Macro.add("WardrobeUI", {
	handler() {
		this.output.append(new App.UI.Widgets.Wardrobe.WardrobeUI(this.args[0] as App.Entity.Player).element);
	},
});
