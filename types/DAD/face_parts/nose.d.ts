import {FacePart} from "./face_part";
import {ShadingPart} from "../draw/shading_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class Nose extends FacePart {
	constructor(...data: object[]);
}


declare class NoseHumanShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object): DrawPoint[];
}


export class NoseHuman extends Nose {
	constructor(...data: object[]);

	fill(ignore: any, ex: DrawingExports): string;
	clipFill(): void;
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
