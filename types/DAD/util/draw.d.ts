import { BodyPart } from "../parts/part";
import {Point} from "drawpoint/dist-esm/point"

//#region Conversion functions
/**
 * conversions between canvas units (cu) to centimeters (cm) and inches (in)
 */
export function incu(inches: number): number;
export function cmcu(cm: number): number;
export function cucm(cu: number): number;

export function convertPointsToCanvasUnits(points: Point | Point[]): void;
//#endregion

/**
 * Set stroke and fill for given context
 * @param ctx
 * @param part Some kind of Part object
 * @param ex Export from draw
 */
export function setStrokeAndFill(ctx: CanvasRenderingContext2D, part: BodyPart, ex: object): void;

/**
 * Inherit the stroke style of its parent part (must have this bound to a part)
 * @param ctx
 * @param ex
 * @returns {*}
 */
export function inheritStroke(ctx: CanvasRenderingContext2D, ex: object): any;

export function inheritFill(ctx: CanvasRenderingContext2D, ex: object): any;

export function requirePart(partName: string, ex: object): void;

export function dist(pointA: Point, pointB: Point): number;

/**
 * DEPRECATED function, use simpleQuadratic instead
 */
export function averageQuadratic(p1: Point, p2: Point, t?: number, dx?: number, dy?: number, st?: number, et?: number): Point;
