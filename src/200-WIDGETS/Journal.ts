namespace App.UI.Widgets {
	interface HeaderContent {
		content: string | HTMLElement | DocumentFragment;
		display?: string;
	}

	interface JournalCard {
		card: HTMLDivElement;
		header: HTMLDivElement | null;
		content: HTMLDivElement;
	}

	// force html tags to render
	function formatterForced(cell: Tabulator.CellComponent): string {
		return cell.getValue() as string;
	}

	function addJournalCard(parent: ParentNode, headerContent?: HeaderContent): JournalCard {
		const card = appendNewElement('div', parent, {classNames: 'journal-card'});
		let header: HTMLDivElement | null = null;
		if (headerContent) {
			header = appendNewElement('div', card, {classNames: 'journal-card-header'});
			if (typeof headerContent.content === "string") {
				header.append(Wikifier.wikifyEval(headerContent.content));
			} else {
				header.append(headerContent.content);
			}
			if (headerContent.display) {
				header.style.display = headerContent.display;
			}
		}
		const content = appendNewElement('div', card);
		return {
			card,
			header,
			content,
		};
	}

	// 'active' 'JournalTabs' 'activeQuests' 'Active Quests'
	function rQuestListTab(player: Entity.Player, questState: QuestStatus.Active | QuestStatus.Completed): DocumentFragment {
		function makeHeader(quest: Quest, npc: Entity.NPC): DocumentFragment {
			const res = new DocumentFragment();
			const acceptDay = quest.acceptedOn(player);
			const completeDay = quest.completedOn(player);
			const date = appendNewElement('div', res);
			if (questState === QuestStatus.Active) {
				appendTextNode(date, `Day ${acceptDay}`);
				if (setup.world.day !== acceptDay) {
					appendTextNode(date, ` (${setup.world.day - acceptDay} days ago)`);
				}
			} else {
				const completionTime = completeDay - acceptDay;
				appendTextNode(date, `Day ${completeDay} (${completionTime === 0 ? ' on the same day ' : `in ${completionTime} days`} after accepting)`);
			}
			appendNewElement('div', res).style.flexGrow = "1";
			const npcAndTitle = appendNewElement('div', res);
			appendTextNode(npcAndTitle, "[ ");
			const npcName = appendNewElement('span', npcAndTitle, {content: npc.name, classNames: 'npc'});
			tippy(npcName, {content: Wikifier.wikifyEval(PR.tokenizeString(player, npc, npc.briefDesc))});
			appendFormattedText(npcAndTitle, ` - ${quest.title()} ]`);
			return res;
		}

		const ql = App.Quest.list(questState ===  QuestStatus.Active ? [QuestStatus.Active, QuestStatus.CanComplete] : questState, player);
		const res = new DocumentFragment();
		if (ql.length === 0) {
			return res;
		}
		const entryType: QuestStage = questState === QuestStatus.Active ? QuestStage.JournalEntry : QuestStage.JournalComplete;

		if (questState === QuestStatus.Completed) {
			ql.sort(([l, _ls], [r,_rs]) => r.completedOn(player) - l.completedOn(player));
		} else {
			ql.sort(([l, _ls], [r, _rs]) => r.acceptedOn(player) - l.acceptedOn(player));
		}

		for (const [q, _] of ql) {
			if ((entryType === QuestStage.JournalEntry && q.journalEntry === "HIDDEN") ||
				(entryType === QuestStage.JournalComplete && q.journalCompleteEntry === "HIDDEN")) {
				continue;
			}

			const npc = q.giver;
			const card = addJournalCard(res, {content: makeHeader(q, npc), display: 'flex'});
			const desc = appendNewElement('p', card.content);
			$(desc).wiki(PR.pQuestDialog(q.id, entryType, player, npc));
			if (questState === QuestStatus.Active && q.checks !== undefined) {
				const requirements = appendNewElement('p', card.content);
				const computedRequirements = Task.evaluateRequirements(q.checks, q.definitionContext);
				const renderVisitor = new Conditions.RequirementRenderer(computedRequirements.ctx, requirements);
				computedRequirements.root.acceptVisitor(renderVisitor);
			}
		}
		return res;
	}

	interface SexSkillUsageLogStrings {
		general: string;
		success: string;
		noSuccess: string;
	}

	function makeCardCaption(caption: string): HeaderContent {
		return {content: makeElement('span', {content: caption, classNames: 'journal-card-caption'})};
	}

	function rGameStats(player: Entity.Player): DocumentFragment {
		const res = new DocumentFragment();
		const gs = player.gameStats;
		const totalMoneyEarned = Object.values(gs.moneyEarned).reduce((a, c) => a + c, 0);
		if (totalMoneyEarned > 0) {
			const card = addJournalCard(res, makeCardCaption("Earnings"));
			const moneyEarned = appendNewElement('p', card.content, {content: `You managed to earn ${totalMoneyEarned} coins. Those include:`});
			const list = appendNewElement('ul', moneyEarned);
			const nameMapping: Record<GameState.CommercialActivity | GameState.IncomeSource, string> = {
				betting: "betting",
				gambling: "gambling",
				jobs: "regular jobs",
				loot: "looting",
				quests: "quest tasks",
				sexualJobs: "sexual jobs",
				whoring: "whoring yourself",
				unknown: "other incomes",
			};
			const incomes: {lbl: string, amount: number}[] = [];
			for (const entry of Object.entries(gs.moneyEarned)) {
				const amount = entry[1];
				if (amount > 0) {
					incomes.push({lbl: nameMapping[entry[0]], amount: amount});
				}
			}

			incomes.sort((a, b) => b.amount - a.amount);
			for (const income of incomes) {
				appendNewElement('li', list, {content: `${income.amount} coins from ${income.lbl}.`});
			}

			if (gs.tokensEarned > 0) {
				appendNewElement('p', card.content, {content: `You also earned ${gs.tokensEarned} courtesan tokens.`});
			}

			// expenditures
			const ms = gs.moneySpendings;
			const totalShoppingSpend = Object.values(ms.shopping).reduce((a, c) => a + c, 0);
			const totalOtherSpend = ms.betting + ms.gambling + ms.jobs + ms.quests;

			if (totalShoppingSpend + totalOtherSpend > 0) {
				const expenditures = addJournalCard(res, makeCardCaption("Expenditures"));
				appendTextNode(expenditures.content,
					`Your expenditures so far are ${totalShoppingSpend + totalOtherSpend} coins. Those include:`);
				const list = appendNewElement('ul', expenditures.content);
				const spendingNameMapping: Record<GameState.SpendingTarget | GameState.CommercialActivity, string> = {
					betting: "paid for bets",
					gambling: "lost gambling",
					jobs: "paid for services",
					quests: "spent on mission",
					shopping: "laid out at shops",
					unknown: "unknown expenditures",
				};
				for (const sp of [
					GameState.CommercialActivity.Betting, GameState.CommercialActivity.Gambling,
					GameState.CommercialActivity.Jobs, GameState.CommercialActivity.Quests,
				]) {
					if (ms[sp] > 0) {
						appendNewElement('li', list, {content: `${ms[sp]} coins were ${spendingNameMapping[sp]}.`});
					}
				}
				if (totalShoppingSpend > 0) {
					appendNewElement('li', list, {content: `${totalShoppingSpend} coins were ${spendingNameMapping[GameState.SpendingTarget.Shopping]}.`});
				}

				if (totalShoppingSpend > 0) { // shopping details
					const shoppingCategories: Record<Items.Category, string> = {
						[Items.Category.Clothes]: "clothing",
						[Items.Category.Cosmetics]: Items.Category.Cosmetics,
						[Items.Category.Drugs]: "drugs",
						[Items.Category.Food]: "food",
						[Items.Category.LootBox]: "loot",
						[Items.Category.MiscConsumable]: "miscellaneous",
						[Items.Category.MiscLoot]: "miscellaneous loot",
						[Items.Category.Quest]: "important mission items",
						[Items.Category.Reel]: "whoring manuals (reels)",
						[Items.Category.Weapon]: "weapons",
					};
					const shopSpendings: {lbl: string, amount: number}[] = [];
					for (const [key, v] of Object.entries(ms.shopping)) {
						if (v > 0) {
							shopSpendings.push({lbl: shoppingCategories[key], amount: v});
						}
					}
					shopSpendings.sort((a, b) => b.amount - a.amount);
					const shoppingPara = appendNewElement('p', expenditures.content, {content: "Your shopping expenses are as follows:"});
					const list = appendNewElement('ul', shoppingPara);
					for (const s of shopSpendings) {
						appendNewElement('li', list, {content: `${s.amount} coins paid for ${s.lbl}.`});
					}
				}
			}

			const sk = gs.skills;
			const statDataPresent = (stat: GameState.SkillUseStat | undefined) => stat && stat.failure + stat.success > 0;
			const appendSexStat = (parent: ParentNode, stat: GameState.SkillUseStat | undefined, ss: SexSkillUsageLogStrings) => {
				if (stat && statDataPresent(stat)) {
					const para = appendNewElement('p', parent, {content: String.format(ss.general, stat.success + stat.failure)});
					para.appendTextNode(', ');
					if (stat.success > 0) {
						appendTextNode(para, String.format(ss.success, stat.success));
					} else {
						appendTextNode(para, ss.noSuccess);
					}
				}
			};

			if (statDataPresent(sk.assFucking) || statDataPresent(sk.blowJobs) || statDataPresent(sk.handJobs) || statDataPresent(sk.titFucking)) {
				const sexStatsDiv = addJournalCard(res, makeCardCaption("Sexual Stats"));
				appendSexStat(sexStatsDiv.content, sk.assFucking, {
					general: "Your ass was used {0} times",
					success: "and {0} times among them it gave a pleasure!",
					noSuccess: "but no one enjoyed that.",
				});
				appendSexStat(sexStatsDiv.content, sk.blowJobs, {
					general: "You've sucked {0} dicks",
					success: "and satisfied {0} of them!",
					noSuccess: "but each time time you've failed to give a pleasure.",
				});
				appendSexStat(sexStatsDiv.content, sk.handJobs, {
					general: "{0} times you've used your hands to make men happy",
					success: "and {0} times it worked quite well!",
					noSuccess: "yet nobody enjoyed that.",
				});
				appendSexStat(sexStatsDiv.content, sk.titFucking, {
					general: "Your tits were fucked {0} times",
					success: "and {0} dicks enjoyed that!",
					noSuccess: "and it was nothing but waste of time.",
				});
			}

			const coffin = gs.coffin;
			if (coffin.played > 0) {
				const card = addJournalCard(res, makeCardCaption("Coffin game"));
				appendNewElement('p', card.content, {content: `You've played Coffin Dice ${coffin.played} times.`});
				appendNewElement('p', card.content, {content: `Matches won: ${coffin.won}.`});
				appendNewElement('p', card.content, {content: `Matches lost: ${coffin.lost}.`});
				appendNewElement('p', card.content, {content: `Matches drawn: ${coffin.played - coffin.won - coffin.lost}.`});
				if (coffin.coinsWon > 0) {
					const coffinNet = coffin.coinsWon - coffin.coinsLost;
					appendNewElement('p', card.content, {
						content: `You've won ${coffin.coinsWon} coins ${coffin.coinsLost > 0 ?
							` and lost ${coffin.coinsLost} for a net earnings of ${coffinNet}.` : '.'}`,
					});
				} else if (coffin.coinsLost > 0) {
					appendNewElement('p', card.content, {content: `You've lost ${coffin.coinsLost} coins.`});
				}

				if (coffin.itemsWon > 0) {
					appendNewElement('p', card.content, {content: `You've won ${coffin.itemsWon} items, for an estimated value of ${coffin.itemsWonValue} coins.`});
				}

				if (coffin.sexPaid > 0) {
					appendNewElement('p', card.content, {content: `You used your body to pay ${coffin.sexPaid} coins worth of debts.`});
				}
			}
		}
		return res;
	}

	function printMemory(
		clothesElement: HTMLElement,
		foodElement: HTMLElement,
		drugsElement: HTMLElement
	): void {
		const clothes = getMemory(Items.Category.Clothes, setup.world.pc.history.clothingEffectsKnown);
		new Tabulator(clothesElement, {
			layout: "fitColumns",
			height: '45ex',
			data: clothes,
			columns: [
				{title: "Slot", field: "slot", width: 90},
				{title: "Category", field: "category", width: 150},
				{title: "Rank", field: "rank", width: 100, formatter: formatterForced},
				{title: "Name", field: "name", widthGrow: 1, formatter: formatterForced},
				{title: "Effects", field: "effects", widthGrow: 1, formatter: formatterForced},
			],
			initialSort: [{column: "name", dir: "desc"}],
		});

		const food = getMemory(Items.Category.Food, setup.world.pc.history.items);
		new Tabulator(foodElement, {
			height: "311px",
			layout: "fitColumns",
			data: food,
			columns: [
				{title: "Name", field: "name", minWidth: 200, formatter: formatterForced},
				{title: "Effects", field: "effects", minWidth: 600, formatter: formatterForced},
			],
			initialSort: [{column: "name", dir: "desc"}],
		});

		const drugs = getMemory(Items.Category.Drugs, setup.world.pc.history.items);
		new Tabulator(drugsElement, {
			height: "311px",
			layout: "fitColumns",
			data: drugs,
			columns: [
				{title: "Name", field: "name", minWidth: 200, formatter: formatterForced},
				{title: "Effects", field: "effects", minWidth: 600, formatter: formatterForced},
			],
			initialSort: [{column: "name", dir: "desc"}],
		});
	}

	interface MemoryItem {
		name: string;
		category: string;
		rank: string;
		slot: string;
		effects: string;
	}
	/**
     * Helper function for journal memory.
     * Reduces the src dictionary down to valid entries in the player Dictionary
     * eg (clothes, player.history.clothingEffectsKnown)
     */
	function getMemory(category: Data.ItemCategoryAny, dict: Record<string, number>): MemoryItem[] {
		const source = Items.tryGetItemsDictionary(category);

		if (!source) return []; // empty or couldn't find

		// Reduce dictionary down to items we known about.
		const allowed = Object.keys(dict);
		return Object.keys(source)
			.filter(key => allowed.includes(key))
			.map((f) => {
				const o = Items.factory(category, f);
				return {
					name: o.description,
					category: o instanceof Items.Clothing ? o.style.join(", ") : category,
					rank: o instanceof Items.Clothing ? o.rankDescription : '',
					slot: o instanceof Items.Clothing ? o.slot.capitalizeFirstLetter() : '',
					effects: o instanceof Items.Clothing || o instanceof Items.Consumable ?
						o.printEffectsOnly(setup.world.pc) : '',
				};
			});
	}

	class JournalTab extends App.UI.Widgets.Tab {
		private readonly _func: () => DocumentFragment;

		constructor(id: string, name: string, contentFunction: () => DocumentFragment) {
			super(id, name, `Journal - ${name}`);
			this._func = contentFunction;
		}

		override render() {
			const res = new DocumentFragment();
			appendNewElement('hr', res);
			res.append(this._func());
			return res;
		}
	}

	class JournalTabActiveQuests extends JournalTab {
		constructor() {
			super("quests-active", "Active Quests", () => rQuestListTab(setup.world.pc, QuestStatus.Active));
		}
	}

	class JournalTabCompletedQuests extends JournalTab {
		constructor() {
			super("quests-completed", "Completed Quests", () => rQuestListTab(setup.world.pc, QuestStatus.Completed));
		}
	}

	class JournalTabGameStats extends JournalTab {
		constructor() {
			super("game-stats", "Game statistic", () => rGameStats(setup.world.pc));
		}
	}

	class JournalTabMemory extends App.UI.Widgets.Tab {
		constructor() {
			super("memory", "Memory", "Journal - Memory");
		}

		private static _makeCaption(caption: string): HeaderContent {
			const res = new DocumentFragment();
			appendNewElement('div', res).style.flexGrow = '1';
			appendNewElement('div', res, {content: caption, classNames: 'journal-card-caption'});
			return {content: res, display: 'flex'};
		}

		// eslint-disable-next-line class-methods-use-this
		override render() {
			const res = new DocumentFragment();
			appendNewElement('hr', res);
			const clothings = appendNewElement('div', addJournalCard(res, JournalTabMemory._makeCaption("Clothing")).content);
			const food = appendNewElement('div', addJournalCard(res, JournalTabMemory._makeCaption("Food")).content);
			const drugs = appendNewElement('div', addJournalCard(res, JournalTabMemory._makeCaption("Drugs and Potions")).content);
			printMemory(clothings, food, drugs);
			return res;
		}
	}

	export class Journal extends TabWidget{
		constructor() {
			super();
			this.addTab(new JournalTabActiveQuests());
			this.addTab(new JournalTabCompletedQuests());
			this.addTab(new JournalTabGameStats());
			this.addTab(new JournalTabMemory());
		}
	}
}

Macro.add("Journal", {
	skipArgs: true,
	handler() {
		const tabs = new App.UI.Widgets.Journal();
		tabs.selectTabByIndex(0);

		this.output.append(tabs.element);
	},
});
