import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";


export class QueenHat extends Clothing {
	constructor(...data: object[]);
}

// Nun habit.
export class QueenHabitBase extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenHabitTop extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenHabitBand extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenHabitBack extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenHabit extends QueenHat {
	constructor(...data: object[]);

	ScapularFill: string;
	ScapularStroke: string;
	Mods: {
		hairStyle: number, // kludge!
		hairLength: number
	};

	fill(): string;
	stroke(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

//Hair Pin.

export class QueenHairPinBase extends ClothingPart {
	constructor(...data: object[]);

	// WARNING requires these propertirs for drawing, but does not declare them
	// need to be passed via constructor
	hairpinSize: number;
	ballSize: number;
	pinLength: number;
	pinWidth: number;
	rotateAngle: number;
	xOffset: number;
	yOffset: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenHairPin extends QueenHat {
	constructor(...data: object[]);

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

// Pirate Hat

export class QueenPirateHatBack extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenPirateHatFront extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class QueenPirateHat extends QueenHat {
	constructor(...data: object[])

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
