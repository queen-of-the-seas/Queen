namespace App {
	Macro.add("printStyleDescription", {
		skipArgs: true,
		handler() {
			const element = document.createElement('span');
			element.id = "StyleDesc";
			const qe = $(element);
			const pc = setup.world.pc;
			const actor = pc.actor;
			const printOptions: Text.Human.AspectDescriptionOptions = {colorize: true};
			qe.wiki(`${actor.body.hair.describe(printOptions)} ${actor.look.makeup.describe(printOptions)}. The style and fashion of your clothing is ${actor.look.clothing.describe(printOptions)}. Your hygiene and styling is ${actor.look.style.describe(printOptions)}.`);
			this.output.append(element);
		},
	});
}
