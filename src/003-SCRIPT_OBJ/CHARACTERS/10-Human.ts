namespace App.Entity {
	export abstract class Human implements IHuman {
		protected abstract get state(): GameState.Human;

		private _statXPObject<T extends keyof StatTypeObjectMap>(type: T): GameState.StatAndXp<StatTypeMap[T]> {
			switch (type) {
				case Stat.Core: return this.state.core as GameState.StatAndXp<StatTypeMap[T]>;
				case Stat.Skill: return this.state.skill as GameState.StatAndXp<StatTypeMap[T]>;
				case Stat.Body: return this.state.body as GameState.StatAndXp<StatTypeMap[T]>;
			}
			throw "WTF TS?!"
		}

		/**
		 * Returns object that holds stat values
		 */
		statObject<T extends keyof StatTypeObjectMap>(type: T): StatTypeObjectMap[T] {
			return this._statXPObject(type).value as StatTypeObjectMap[T];
		}

		/**
		 * Returns object that holds stat XP values
		 */
		statXPObject<T extends keyof StatTypeObjectMap>(type: T): StatTypeObjectMap[T] {
			return this._statXPObject(type).xp as StatTypeObjectMap[T];
		}

		/**
		 * Return a statistic value (raw)
		 */
		stat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number;
		stat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T]): number;
		stat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T]): number;
		stat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): StatTypeObjectMap[T][S] | number {
			const statXpObject = this.statObject(type);
			if (!statXpObject.hasOwnProperty(statName)) {
				throw new Error(`No human stat '${String(statName)}' of type '${type}'`);
			}
			return statXpObject[statName];
		}

		/**
		 * Returns the current XP of a statistic.
		 */
		statXP<T extends keyof StatTypeMap, S extends StatTypeMap[T]>(type: T, statName: S): number;
		statXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): number;
		statXP<T extends keyof StatTypeStrMap, S extends StatTypeStrMap[T]>(type: T, statName: S): number;
		statXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): StatTypeObjectMap[T][S] | number {
			const statXpObject = this.statXPObject(type);
			if (!statXpObject.hasOwnProperty(statName)) {
				throw new Error(`No human stat '${String(statName)}' of type '${type}'`);
			}
			return statXpObject[statName];
		}

		maxStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number {
			return PR.statConfig(type)[statName].max;
		}

		getMinStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number {
			return PR.statConfig(type)[statName].min;
		}

		statFraction<T extends StatTypeStr>(type: T, statName: StatTypeStrMap[T], statValue?: number): number;
		statFraction<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number;
		statFraction<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number {
			const minStatValue = this.getMinStat(type, statName);
			const value = (statValue ?? this.stat(type, statName));
			return (value - minStatValue) / (this.maxStat(type, statName) - minStatValue);
		}

		statPercent<T extends StatTypeStr>(type: T, statName: StatTypeStrMap[T], statValue?: number): number;
		statPercent<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number;
		statPercent<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number {
			return Math.floor(this.statFraction(type, statName, statValue) * 100);
		}

		/**
		 * Set raw statistic value
		 */
		setStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], value: number): void;
		setStat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T], value: number): void;
		setStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], value: number): void;
		setStat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S, value: StatTypeObjectMap[T][S]): void {
			this.statObject(type)[statName] = value;
			this.statXPObject(type)[statName] = 0 as StatTypeObjectMap[T][S];
		}

		/**
		 * Derived statistic, lends itself to Beauty. WaistRating, BustRating, HipsRating and AssRating contribute.
		 */
		get figure(): number {
			const tFig = Math.round((this.waistRating + this.bustRating + this.hipsRating + this.assRating) / 4);
			return Math.clamp(tFig, 1, 100); // Normalize between 1 - 100
		}

		/**
		 * Calculates "golden ratio" for waist @ Player's height and then returns a score relative their current waist.
		 */
		get waistRating(): number {
			const goldenWaist = Math.round((PR.statToCm(this, BodyProperty.Height) * 0.375)); // 54cm to 78cm
			return Math.round(((goldenWaist / PR.waistInCM(this)) / 1.8) * 100);
		}

		/**
		 * Calculates "golden ratio" for bust @ Player's height and then returns a score relative their current bust.
		 */
		get bustRating(): number {
			const goldenBust = (Math.round((PR.statToCm(this, BodyProperty.Height) * 0.375)) * 1.5);
			return Math.round(((PR.busInCM(this) / goldenBust) / 1.6) * 100);
		}

		/**
		 * Calculates "golden ratio" for hips @ Player's height and then returns a score relative their current hips.
		 */
		get hipsRating(): number {
			const goldenHips = (Math.round((PR.statToCm(this, BodyProperty.Height) * 0.375)) * 1.5);
			return Math.round(((PR.hipsInCM(this) / goldenHips) / 1.6) * 100);
		}

		/**
		 * Combination of Ass + Hips.
		 */
		get assRating(): number {
			return Math.round((this.statPercent(Stat.Body, BodyPart.Ass) + this.statPercent(Stat.Body, BodyPart.Hips)) / 2);
		}

		/**
		 * For now just the percentage of the lips 1-100.
		 */
		get lipsRating(): number {
			return this.statPercent(Stat.Body, BodyPart.Lips);
		}

		/**
		 * Number in range [0:100] that describes perceived perkiness of the bust
		 *
		 * Includes clothing effects
		 */
		get breastPerkiness(): number {
			let result = this.bodyStats.bustFirmness;
			for (const eq of Object.values(this.equipment)) {
				if (!eq || !eq.activeEffect) {continue;}
				for (const effect of eq.activeEffect) {
					result += Data.effectLibraryEquipment.active[effect].fun(BodyProperty.BustFirmness, eq);
				}
			}
			return Math.clamp(result, 0, 100);
		}

		/**
		 * Returns XP cost for increasing stat level by one in @see delta
		 * @param type Stat name (e.g. Stat.Body)
		 * @param statName Stat name (e.g. "lips")
		 * @param delta Raise level when delta is positive, loose when negative
		 * @returns XP cost
		 */
		getLeveling<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], delta: number): {cost: number, step: number};
		getLeveling<T extends keyof Data.StatConfigMap>(type: T, statName: StatTypeMap[T], delta: number): {cost: number, step: number};
		getLeveling<T extends keyof Data.StatConfigMap>(type: T, statName: StatTypeMap[T], delta: number): {cost: number, step: number} {
			const statCfg = PR.statConfig(type)[statName];
			const levels = statCfg.levelingCost ?? statCfg.leveling;
			// var Percent = Math.round(( (( TargetScore - this.getMinStat(Type, StatName)) /
			//    (this.maxStat(Type, StatName) - this.getMinStat(Type, StatName))) * 100));
			const statValue = this.stat(type, statName);
			if (levels instanceof Function) {
				return {cost: levels(statValue + delta) - levels(statValue), step: 1};
			}

			if (Leveling.isNoneLeveling(levels)) {
				return {
					cost: levels.none.cost,
					step: levels.none.step,
				}
			}

			if (Leveling.isFixedLeveling(levels)) {
				return {
					cost: levels.fixed.cost,
					step: levels.fixed.step,
				}
			}

			if (Leveling.isExplicitLeveling(levels)) {
				for (const [property, value] of Object.entries(levels)) {
					if ((statValue <= Number.parseInt(property))) {
						return {
							cost: value.cost,
							step: ((this.maxStat(type, statName) / 100) * value.step),
						};
					}
				}
			}

			return {cost: 100, step: 1};
		}

		getCapStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): number {
			return Math.round((Math.max(this.getMinStat(type, statName), Math.min(amount, this.maxStat(type, statName)))) * 100) / 100;
		}

		adjustStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void;
		adjustStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number): void;
		adjustStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void {
			if (setup.world.debugMode) console.log(`AdjustStat: type=${type}, name=${statName}, amount=${amount}`);
			this._statXPObject(type).value[statName] = this.getCapStat(type, statName, (this.stat(type, statName) + amount));
		}

		adjustCoreStat(statName: CoreStat, amount: number): void;
		adjustCoreStat(statName: CoreStatStr, amount: number): void;
		adjustCoreStat(statName: CoreStat, amount: number): void {
			this.adjustStat(Stat.Core, statName, amount);
		}

		adjustBody(statName: BodyStat, amount: number): void;
		adjustBody(statName: BodyStatStr, amount: number): void;
		adjustBody(statName: BodyStat, amount: number): void {
			this.adjustStat(Stat.Body, statName, amount);
		}

		adjustSkill(statName: Skills.Any, amount: number): void;
		adjustSkill(statName: Skills.AnyStr, amount: number): void;
		adjustSkill(statName: Skills.Any, amount: number): void {
			this.adjustStat(Stat.Skill, statName, amount);
		}

		// Used to directly set XP to a specific amount.
		setXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void;
		setXP<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number): void;
		setXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void {
			switch (type) {
				case Stat.Core:
					this.state.core.xp[statName as CoreStat] = amount;
					break;
				case Stat.Skill:
					this.state.skill.xp[statName as Skills.Any] = amount;
					break;
				case Stat.Body:
					this.state.body.xp[statName as BodyStat] = amount;
					break;
			}
			if (setup.world.debugMode)
				console.debug("SetXP: set '%s' to %d", statName, amount);
		}

		adjustXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number, limiter?: number, noCap?: boolean): void;
		adjustXP<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number, limiter?: number, moCap?: boolean): void;
		adjustXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number, limiter = 0, noCap = false): void {
			amount = Math.ceil(amount); // No floats.
			if (setup.world.debugMode)
				console.debug(`AdjustXP: type=${type},stat=${statName},amount=${amount},limit=${limiter},noCap=${noCap}`);
			const currentStatValue = this.stat(type, statName);

			// FIXME the next two lines lead to an "unstable equilibrium" at both extrems, because at those points XP points can only be accumulated
			// outwards of the extremum. Thus very little XP changes can not be countered.
			if ((amount > 0) && (((currentStatValue >= limiter) && (limiter !== 0)) || (currentStatValue >= this.maxStat(type, statName)))) return;
			if ((amount < 0) && (((currentStatValue <= limiter) && (limiter !== 0)) || (currentStatValue <= this.getMinStat(type, statName)))) return;

			if (!noCap) {
				const currentStatXpAbs = Math.abs(this.statXP(type, statName));
				if (currentStatXpAbs >= 1000) {
					amount = Math.ceil(amount / 10);
				} else if (currentStatXpAbs >= 500) {
					amount = Math.ceil(amount / 4);
				} else if (currentStatXpAbs >= 250) {
					amount = Math.ceil(amount / 2);
				}
			}

			this._statXPObject(type).xp[statName] += amount;

			if (setup.world.debugMode)
				console.debug(`AdjustXP: Adjusted by ${amount}`);
		}

		adjustCoreStatXP(statName: CoreStatStr, amount: number, limiter = 0): void {
			return this.adjustXP(Stat.Core, statName, amount, limiter);
		}

		adjustBodyXP(statName: BodyStatStr, amount: number, limiter = 0): void {
			return this.adjustXP(Stat.Body, statName, amount, limiter);
		}

		adjustSkillXP(statName: Skills.Any, amount: number, limiter = 0): void {
			return this.adjustXP(Stat.Skill, statName, amount, limiter);
		}

		protected levelStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): void {
			const targetScore = this.statXP(type, statName) < 0 ? -1 : 1;
			const leveling = this.getLeveling(type, statName, targetScore);
			if ((Math.abs(this.statXP(type, statName))) < leveling.cost) return;

			if (targetScore < 0 && this.stat(type, statName) === this.getMinStat(type, statName)) return;
			if (targetScore > 0 && this.stat(type, statName) === this.maxStat(type, statName)) return;

			console.log(`Leveling Stat:${statName}(${this.stat(type, statName)})`);

			const cost = (targetScore < 0) ? leveling.cost : (leveling.cost * -1);
			const step = (targetScore < 0) ? (leveling.step * -1) : leveling.step;

			switch (type) {
				case Stat.Skill:
					this.adjustSkill(statName as Skills.Any, step);
					break;

				case Stat.Core:
					this.adjustCoreStat(statName as CoreStat, step);
					break;

				case Stat.Body:
					this.adjustBody(statName as BodyStat, step);
					break;
			}

			this.adjustXP(type, statName, cost, 0, true);
		}

		get flags(): GameState.GameVariableMap {return this.state.flags;}

		protected levelStatGroup(type: Stat): void {
			switch (type) {
				case Stat.Core:
					for (const k of Object.keys(this.state.core.value)) {this.levelStat(type, k);}
					break;
				case Stat.Skill:
					for (const k of Object.keys(this.state.skill.value)) {this.levelStat(type, k);}
					break;
				case Stat.Body:
					for (const k of Object.keys(this.state.body.value)) {this.levelStat(type, k);}
					break;
			}
		}

		// Resting and Sleeping functions.
		nextDay(): void {
			this.levelStatGroup(Stat.Core);
			this.levelStatGroup(Stat.Body);
			this.levelStatGroup(Stat.Skill);
		} // NextDay

		/**
		 * Fetish rating is derived from the enlarged (or minimized) size of the Players body parts.
		 * Generally however, bigger (what) means more points. Used to calculate whoring pay and some
		 * other activities.
		 */
		get fetish(): number {
			let score = 0;
			// 5 - 15 for boobs and ass each   (30 pts)
			if (this.statPercent(Stat.Body, BodyPart.Bust) >= 30) {
				score += Math.round((5 + (this.statPercent(Stat.Body, BodyPart.Bust) / 10)));
			}
			if (this.statPercent(Stat.Body, BodyPart.Ass) >= 30) {
				score += Math.round((5 + (this.statPercent(Stat.Body, BodyPart.Ass) / 10)));
			}
			// 10 pts for extra firm or extra sagging boobs
			if (this.statPercent(Stat.Body, BodyProperty.BustFirmness) > 90) {
				score += this.statPercent(Stat.Body, BodyProperty.BustFirmness) - 90;
			}
			if (this.statPercent(Stat.Body, BodyProperty.BustFirmness) < 10) {
				score += 10 - this.statPercent(Stat.Body, BodyProperty.BustFirmness);
			}

			// up to 10 each for lips and waist and hips (35 pts)
			if (this.statPercent(Stat.Body, BodyPart.Lips) >= 80) {
				score += Math.round((this.statPercent(Stat.Body, BodyPart.Lips) / 10));
			}
			if (this.statPercent(Stat.Body, BodyPart.Hips) >= 80) {
				score += Math.round((this.statPercent(Stat.Body, BodyPart.Hips) / 10));
			}
			if (this.statPercent(Stat.Body, BodyPart.Waist) <= 30) score += 5;
			if (this.statPercent(Stat.Body, BodyPart.Waist) <= 15) score += 5;
			if (this.statPercent(Stat.Body, BodyPart.Waist) <= 1) score += 5;

			// Penis and Balls, 10 each. (10 pts)
			if ((this.statPercent(Stat.Body, BodyPart.Penis) <= 10) && (this.statPercent(Stat.Body, BodyPart.Balls) <= 10)) {
				score += 10; // tiny genitals!
			}
			if ((this.statPercent(Stat.Body, BodyPart.Penis) >= 90) && (this.statPercent(Stat.Body, BodyPart.Balls) >= 90)) {
				score += 10; // big genitals
			}

			return Math.clamp(Math.round((score / 75) * 100), 0, 100);
		}

		/**
		 * shim function that returns the "hair rating" of the character, but checks first if they are wearing
		 * a wig and then reports that number instead.
		 */
		get hairRating(): number {
			return Math.clamp(this.equipmentInSlot(ClothingSlot.Wig)?.hairBonus ?? this.state.hair.bonus, 0, 100);
		}

		/**
		 * The makeup rating of the character.
		 */
		get makeupRating(): number {
			return Math.clamp(this.state.makeup.bonus, 0, 100);
		}

		/**
		 * Derived statistic (face + makeup). Bonus payout for hand jobs if you have a good face.
		 */
		get faceRating(): number {
			return Math.ceil(Math.max(0, Math.min(((this.makeupRating + this.stat(Stat.Body, BodyPart.Face)) / 2), 100)));
		}

		/**
		 * Calculates the human's "Beauty" based on other statistics.
		 */
		get beauty(): number {
			const cBeauty = Math.round(
				(this.stat(Stat.Body, BodyPart.Face) * 0.4) +
				(this.figure * 0.4) +
				(this.stat(Stat.Core, CoreStat.Fitness) * 0.3));
			return Math.clamp(cBeauty, 0, 100);
		}

		/**
		 * Derived statistic that reports "style" made up out of hair, makeup and clothing.
		 */
		get style(): number {
			const cStyle = Math.round((this.hairRating * 0.25) +
				(this.makeupRating * 0.25) + (this.clothesRating * 0.5));
			return Math.clamp(cStyle, 0, 100);
		}

		/**
		 * Iterates through human's worn items and sums .styleBonus property.
		 */
		get clothesRating(): number {
			const cStyle = Object.values(this.clothing.equipment).reduce((total, item) => {
				return item ? total + item.styleBonus : total;
			}, 0);
			return Math.clamp(Math.round(((cStyle / 100) * 100)), 1, 100); // 1 - 100 rating
		}

		get naming(): GameState.CharacterName {
			return this.state.name;
		}

		get originalName(): string {return this.naming.original;}
		set originalName(n: string) {this.naming.original = n;}
		get slaveName(): string | undefined {return this.naming.slave;}
		set slaveName(n: string | undefined) {
			if (n) {
				this.naming.slave = n;
			} else {
				delete this.naming.slave;
			}
		}

		get nickName(): string {return this.naming.nick;}

		get name(): string {
			return this.naming.slave ?? this.naming.given;
		}

		get gender(): Gender {
			return this.state.gender;
		}

		get isAlive(): boolean {
			return this.coreStats.health > 0;
		}

		get isFuta(): boolean {
			return this.state.core.value.futa > 0;
		}

		get coreStats(): Record<CoreStat, number> {return this.state.core.value;}
		get coreStatsXP(): Record<CoreStat, number> {return this.state.core.xp;}

		get bodyStats(): Record<BodyStat, number> {return this.state.body.value;}
		get bodyXP(): Record<BodyStat, number> {return this.state.body.xp;}

		get skills(): Record<Skills.Any, number> {return this.state.skill.value;}
		get skillsXP(): Record<Skills.Any, number> {return this.state.skill.xp;}

		get faceData(): DaBodyPreset {return this.state.faceData;}

		setFace(presetID: string): void {
			// TODO copy data
			this.state.faceData = Data.DAD.facePresets[presetID];
		}

		get hairColor(): Data.HairColor {return this.state.hair.color;}
		set hairColor(c: Data.HairColor) {this.state.hair.color = c;}
		get hairStyle(): Data.Fashion.HairStyle {return this.state.hair.style;}
		set hairStyle(s: Data.Fashion.HairStyle) {this.state.hair.style = s;}
		get hairBonus(): number {return this.state.hair.bonus;}
		set hairBonus(v: number) {this.state.hair.bonus = v;}
		get makeupStyle(): Data.Fashion.Makeup {return this.state.makeup.style;}
		set makeupStyle(s: Data.Fashion.Makeup) {this.state.makeup.style = s;}
		get makeupBonus(): number {return this.state.makeup.bonus;}
		set makeupBonus(v: number) {this.state.makeup.bonus = v;}
		get eyeColor(): string {return this.state.eyeColor;}

		// #region Inventory
		abstract get inventory(): InventoryManager;
		abstract get clothing(): Entity.ClothingManager;

		get equipment(): Partial<Record<ClothingSlot, Items.Clothing | Items.Weapon| null>> {return this.clothing.equipment;}
		get wardrobe(): Array<Items.Clothing | Items.Weapon> {return this.clothing.wardrobe;}

		/**
		 * Returns total number of item charges in inventory
		 */
		inventoryItemsCount(): number {
			let res = 0;
			this.inventory.forEachItemRecord(undefined, undefined, (n /* , tag, itemClass */) => {res += n;});
			return res;
		}

		/**
		 * Returns total number of different item types in inventory
		 */
		inventoryItemsTypes(): number {
			let res = 0;
			this.inventory.forEachItemRecord(undefined, undefined, (/* n, tag, itemClass */) => {res += 1;});
			return res;
		}
		// #endregion

		// Equipment and Inventory Related Functions

		/**
		 * Does the character own the item in question
		 * @param name
		 */
		ownsWardrobeItem(name: string): boolean {
			if (this.clothing.wardrobe.some(o => o.name === name)) return true;
			return this.clothing.equipment[Data.clothes[name].slot]?.name === name;
		}

		isAtMaxCapacity(category: Items.Category, tag: string): boolean {
			switch (category) {
				case Items.Category.Clothes:
				case Items.Category.Weapon:
					return this.ownsWardrobeItem(tag);
				default:
					return this.inventory.isAtMaxCapacity(category, tag);
			}
		}

		wardrobeItem(id: string): Items.Clothing | Items.Weapon {
			return this.clothing.wardrobe.retrieve(o => o.id === id);
		}

		wardrobeItemsBySlot(slot: ClothingSlot.Weapon): Array<Items.Weapon>;
		wardrobeItemsBySlot(slot: ClothingSlot.Wig): Array<Items.Wig>;
		wardrobeItemsBySlot(slot: ClothingSlot): Array<Items.Clothing | Items.Weapon>;
		wardrobeItemsBySlot(slot: ClothingSlot): Array<Items.Clothing | Items.Weapon> {
			const res = this.clothing.wardrobe.filter(item => item.slot === slot);
			res.sort((a, b) => a.name.localeCompare(b.name));
			return res;
		}

		/* currently unused
		PrintEquipment(Slot: ClothingSlot): string {
			const eq = this.Clothing.Equipment[Slot];
			if (!eq) return "@@.state-disabled;Nothing@@";
			return eq.Description;
		}
		*/

		equipmentInSlot(slot: ClothingSlot.Weapon): Items.Weapon | null;
		equipmentInSlot(slot: ClothingSlot.Wig): Items.Wig | null;
		equipmentInSlot(slot: ClothingSlot.Bra): Items.Clothing | null;
		equipmentInSlot(slot: ClothingSlot): Items.Clothing | Items.Weapon | null;
		equipmentInSlot(slot: ClothingSlot): Items.Clothing | Items.Weapon | null {
			const eq = this.clothing.equipment[slot];
			return eq ?? null;
		}

		equipmentItem(id: string): Items.Clothing | Items.Weapon | null {
			for (const item of Object.values(this.clothing.equipment)) {
				if (item && item.id === id) {
					return item;
				}
			}
			return null;
		}

		/**
		 * Search equipped items
		 * @param name
		 * @param slotFlag Search by slot instead of item name.
		 */
		isEquipped(slot: ClothingSlot | ClothingSlot[], slotFlag: true): boolean;
		isEquipped(name: string | string[], slotFlag?: false): boolean;
		isEquipped(nameOrSlot: string | string[], slotFlag = false): boolean {
			const isEquipped = (name: string, flag: boolean): boolean => {
				if (flag) {
					return this.clothing.equipment[nameOrSlot as ClothingSlot] != null;
				}

				for (const itm of Object.values(this.clothing.equipment)) {
					if (itm && (itm.name === name || itm.id === name)) return true;
				}
				return false;
			};

			if (Array.isArray(nameOrSlot)) {
				return nameOrSlot.some(n => isEquipped(n, slotFlag));
			}

			return isEquipped(nameOrSlot, slotFlag);
		}

		isItemEquipped(item: Items.Clothing): boolean {
			return this.equipment[item.slot]?.id === item.id;
		}

		wear(item: Items.IEquipmentItem | Data.ItemNameTemplateEquipment, lock = false): void {
			if (typeof item === "string") {
				this.clothing.wear(item, lock);
			} else {
				this.clothing.wear(item.id, lock);
			}
			setup.avatar.drawPortrait();
		}

		autoWearStyle(style: Data.Fashion.Style): void {
			const everything = this.clothing.wardrobe;

			// Get all matching items by Category
			const matchingItems = everything.filter(item => item.style.contains(style));
			if (matchingItems.length === 0) return; // Nothing matching

			// Sorting by style descending
			matchingItems.sort((left, right) => right.styleBonus - left.styleBonus);
			const takenSlots = new Set(Object.entries(this.equipment)
				.filter(value => value?.[1]?.isLocked)
				.map(value => value[0]));

			for (const item of matchingItems) {
				if (takenSlots.has(item.slot) || item.restrict.some(slot => takenSlots.has(slot))) continue;
				const wornItem = this.equipmentInSlot(item.slot);
				if (!wornItem || !wornItem.style.contains(style) || wornItem.styleBonus < item.styleBonus) {
					this.clothing.wear(item.id);
					takenSlots.add(item.slot);
					for (const slot of item.restrict) {
						takenSlots.add(slot);
					}
				}
			}

			setup.avatar.drawPortrait();
		}

		strip(): void {
			for (const item of Object.values(this.clothing.equipment)) {
				if (item && !item.isLocked) {
					this.clothing.takeOff(item.id);
				}
			}
			setup.avatar.drawPortrait();
		}

		/**
		 * Tests is any of the named clothes worn in the given slot
		 * @returns True is any of the items is worn, false otherwise
		 */
		isAnyClothingWorn(tags: string[], slot: ClothingSlot): boolean {
			return tags.some(t => this.clothing.isWorn(Items.makeId(Items.Category.Clothes, t), slot));
		}

		remove(item: Items.IEquipmentItem): void {
			if (!item) return;
			this.clothing.takeOff(item.id);
			setup.avatar.drawPortrait();
		}

		/**
		 * Hair style taking wig into account
		 * @returns wig style if worn, natural hair style otherwise
		 */
		getHairStyle(): string {
			return this.equipmentInSlot(ClothingSlot.Wig)?.hairStyle ?? this.state.hair.style;
		}

		/**
		 * Hair color taking wig into account
		 * @returns wig color if worn, natural hair color otherwise
		 */
		getHairColor(): Data.HairColor {
			return this.equipmentInSlot(ClothingSlot.Wig)?.hairColor ?? this.state.hair.color;
		}

		/** @deprecated */
		getItemByName(name: string): Items.InventoryItem {
			return this.inventory.filter(o => o.tag === name)[0];
		}

		getItemById<T extends keyof Items.ItemTypeMap>(id: Data.ItemNameTemplate<T>): Items.ItemTypeMap[T] | undefined {
			const splittedId = Items.splitId(id);

			if (splittedId.category === Items.Category.Clothes || splittedId.category === Items.Category.Weapon) {
				const list = this.clothing.wardrobe.filter(o => o.id === id);
				if (list.length > 0) {
					return list[0] as Items.ItemTypeMap[T];
				}
				for (const itm of Object.values(this.clothing.equipment)) {
					if (itm && itm.id === id) {
						return itm as Items.ItemTypeMap[T];
					}
				}
				return undefined;
			}
			const itemList = this.inventory.filter(o => o.id === id); // Look in items first.
			return itemList.length > 0 ? itemList[0] as Items.ItemTypeMap[T] : undefined;
		}

		getItemByTypes(types: string[], sort = false): Items.InventoryItem[] {
			const res = this.inventory.filter(o => types.contains(o.type));

			if (!sort) return res;

			res.sort((a, b) => {
				const af = this.inventory.isFavorite(a.id);
				if (af !== this.inventory.isFavorite(b.id)) {return af ? -1 : 1;}
				return a.name.localeCompare(b.name);
			});
			return res;
		}

		/**
		 * Returns the total number of charges across all items belonging to a certain type.
		 */
		itemChargesOfType(type: Data.ItemTypeInventory): number {
			return this.inventory
				.filter(o => o.type === type)
				.reduce((total, current) => {return total + current.charges}, 0);
		}

		/**
		 * Attempt to iterate through all items of same type and consume charges from them until Amount has been
		 * satisfied. It will delete items if it consumes all their charges.
		 */
		useItemCharges(type: Data.ItemTypeInventory, amount: number): void {
			if (amount <= 0) return;
			const items = this.inventory.filter(o => o.type === type);
			let i = 0;

			while (amount > 0 && i < items.length) {
				const item = items[i];
				if (item instanceof Items.Consumable) {
					const usableCharges = Math.min(amount, item.charges);
					if (usableCharges > 0) {
						item.removeCharges(usableCharges);
						amount -= usableCharges;
						i++;
					}
				}
			}
		}

		deleteItem(item: Items.InventoryItem): void {
			this.inventory.removeItem(item.id);
		}

		/**
		 * Create and add an item to the player.
		 */
		addItem(category: Items.CategoryEquipment, name: string, unused: undefined, opt?: "wear"): Items.Clothing | null;
		addItem<T extends keyof Items.InventoryItemTypeMap>(category: T, name: string, count?: number): Items.InventoryItemTypeMap[T];
		addItem<T extends keyof Items.ItemTypeMap>(category: T, name: string, count?: number, opt?: "wear"): Items.ItemTypeMap[T] | null;
		addItem<T extends keyof Items.ItemTypeMap>(category: T, name: string, count?: number, opt?: "wear"): Items.ItemTypeMap[T] | null {
			if (category === Items.Category.Clothes || category === Items.Category.Weapon) {
				if (this.ownsWardrobeItem(name)) return null; // No duplicate equipment allowed.
				return this.clothing.addItem(name, opt === "wear") as Items.ItemTypeMap[T];
			} else {
				return this.inventory.addItem(category, name, count ?? 1) as Items.ItemTypeMap[T];
			}
		}

		/**
		 * Find item and reduce charges. Delete from inventory if out of charges.
		 * @param itemId
		 * @param charges to consume
		 * @returns The item object
		 */
		takeItem<T extends keyof Items.ItemTypeMap>(itemId: Data.ItemNameTemplate<T>, charges = 1): Items.ItemTypeMap[T] | undefined {
			const o = this.getItemById(itemId);
			if (!o) {
				return o;
			}

			type R = Items.ItemTypeMap[T];
			if (o instanceof Items.Equipment || o instanceof Items.Reel) {
				return o as R;
			}
			if (o) {
				o.removeCharges(charges); // will remove the item from inventory if charges reaches 0
			}
			return o as R;
		}

		/**
		 * Use an item. Apply effects. Delete from inventory if out of charges.
		 */
		useItem(itemId: Data.ItemNameTemplateConsumable): string {
			const o = this.takeItem(itemId);
			if (o instanceof Items.Consumable) {
				return this.consumeItem(o);
			}
			return "";
		}

		get actor(): Text.ActorHuman {
			if (!this.#actor) {
				this.#actor = this.makeActor();
			}
			return this.#actor;
		}

		protected abstract makeActor(): Text.ActorHuman;

		get cssClasses(): string[] {
			return this.state.cssClasses;
		}

		protected abstract consumeItem(item: Items.Consumable): string;

		#actor: Text.ActorHuman | undefined;
	}
}
