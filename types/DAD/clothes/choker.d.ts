import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {NeckAccessory} from "./neck_accessory";


export class ChokerPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class NeckCorsetPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class CollarPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class TagPart extends ClothingPart {
	constructor(...data: object[]);

	text: string;
	fontHeight: number;
	dx: number;
	dy: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class ChokerCrossPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Concrete Clothing classes
 */
export class Choker extends NeckAccessory {
	constructor(...data: object[]);

	neckCoverage: number;
	neckBotCoverage: number;
	center: boolean;
	thickness: number;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class NeckCorset extends NeckAccessory {
	constructor(...data: object[]);

	neckCoverage: number;
	neckBotCoverage: number;
	crossings: number;
	highlight: string;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Collar extends NeckAccessory {
	constructor(...data: object[]);

	neckCoverage: number;
	neckBotCoverage: number;
	ring: string;
	highlight: string;
	thickness: number;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class TaggedCollar extends NeckAccessory {
	constructor(...data: object[]);

	neckCoverage: number;
	neckBotCoverage: number;

	highlight: string;
	thickness: number;
	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class CrossedChoker extends NeckAccessory {
	constructor(...data: object[]);

	neckCoverage: number;
	neckBotCoverage: number;
	crossings: number;
	thickness: number;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
