import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Makeup} from "./makeup";

export class LipstickPart extends ClothingPart {
	constructor(...data: object[]);

	fill: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Lipstick extends Makeup {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
