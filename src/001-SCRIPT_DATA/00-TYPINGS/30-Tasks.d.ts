declare namespace App.Data {
	export namespace Tasks {
		namespace Costs {
			interface Base<TType extends string> {
				type: TType;
				value: number;
			}

			interface Named<TType extends string, TName> extends Base<TType> {
				name: TName;
			}

			type Stat = Named<Stat.Core, CoreStatStr>;
			type Skill = Named<Stat.Skill, Skills.AnyStr>;
			type Body = Named<Stat.Body, BodyStatStr>;
			type Money = Base<"money">;
			type Tokens = Base<"tokens">;
			type Item = Named<"item", ItemNameTemplateAny>;
			type Time = Base<"time">;

			export type Any = Stat | Skill | Body | Money | Tokens | Item | Time;
			export type CostType = Any['type'];
		}

		export const enum ProgressType {
			Progress,
			TrackCustomers,
		}

		interface Task {
			title: string;
			/** Id of the NPC who gives the task
			 * If empty, the npc has to be supplied when listing the task
			*/
			giver?: string;
			tags?: string[];
			variables?: Record<string, GameVariable>;
			progressMeters?: Record<string, ProgressType>;
		}

		interface TextFragment {
			text: string;
		}

		interface ConditionalTextFragment extends TextFragment {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			[x: string]: any;
		}

		interface SceneContent {
			post?: NonEmptyArray<Actions.Any>;
			text?: string | NonEmptyArray<ConditionalTextFragment | string>;
		}

		interface StaticSceneData {
			id: string;
			triggers?: Conditions.Expression;
			checks?: Conditions.Expression;
		}

		type Scene = StaticSceneData & AtLeastOne<SceneContent>;

		export interface Job extends Task {
			pay?: number;
			tokens?: number;
			rating: 1 | 2 | 3 | 4 | 5;
			phases: NonEmptyArray<DayPhase>;
			/** Minimal number of days until the job can be repeated, can be 0 which means the same day */
			days: number;
			cost?: NonEmptyArray<Costs.Any>;
			requirements?: Conditions.Expression;
			intro: string;
			start?: string;
			scenes: NonEmptyArray<Scene>;
			jobResults?: NonEmptyArray<ConditionalTextFragment>;
			end?: string;
		}

		export interface Quest extends Task {
			/** Id of the NPC who concludes the task. Defaults to giver when not explicitly set. */
			receiver?: string;
			/** Flags that are required to trigger quest. */
			pre?: Conditions.Expression;
			/** Flags that are set when quest is completed. */
			post?: NonEmptyArray<Actions.Any>;
			checks?: ReadonlyArray<Conditions.Any>;
			onAccept?: NonEmptyArray<Actions.Any>;
			activeEffect?: (this: App.Quest, player: Entity.Player) => void;
			completedEffect?: (this: App.Quest, player: Entity.Player) => void;
			reward?: NonEmptyArray<Actions.Any>;
			intro: string;
			middle: string;
			finish: string;
			journalEntry: string;
			journalComplete: string;
		}
	}
}
