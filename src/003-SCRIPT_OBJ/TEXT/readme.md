# Text expansion inside a scene

Knowing an actor id (a string), the following texts can be generated:

| property                             | syntax                                                       | desc                |
|--------------------------------------|--------------------------------------------------------------|---------------------|
| actor name                           | `@{actorId}`                                                 |          |
| actor property                       | `@{actorId.prop.chain\|.another.prop.chain}`                 |      |
| actor name with property             | `@{actorId .prop.chain}`                                     |      |
| actor verb                           | `@{actorId(verb)}`                                           |      |
| actor name with verb                 | `@{actorId\|(verb)}`                                          |      |
| actor-flavoured text                 | `@{<actor ref>\|text @@with the flavoured@@ part}`            |
| flavoured text with additional styles| `@{<actor ref>\|text @@style-list;with the flavoured@@ part}` | |
