import {DrawingExports} from "../draw/draw";
import {ShadingPartConstructor} from "../draw/shading_part";
import {SideValue} from "../parts/part";
import {Player} from "../player/player";
import {Layer} from "../util/canvas";

/**
 * Where all Clothing and ClothingPart should go
 */

/**
 * Clothing holds statistics and information about the clothes, but no drawing methods
 * Instead, it holds ClothingParts that each know how to draw themselves
 */
export class Clothing {
	/**
	 * Layer relative to other clothing (how close is the clothing to the skin)
	 */
	clothingLayer: Clothes.Layer;
	/**
	 * Pairs of side and ClothingPart prototypes
	 */
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
	thickness: number;
	/**
	 * Whether this piece of clothing explicitly does not cover whatever is beneath it
	 * even when the part has coverConceal populated. (e.g. hair clips shouldn't hide
	 * the hair parts its attached to)
	 */
	noCover: boolean;

	constructor(...data: object[]);
}

declare interface ClothingConstructor {
	new(...data: object[]): Clothing;
}

export function processClothingPartLocation(side: string, part: ClothingPart): ClothingPart;
export namespace Clothes {
    /**
     * Clothing layers (separate from drawing layers)
     * @readonly
     * @enum
     * @memberof module:da.Clothes
     */
	enum Layer {
		BASE = 0,
		INNER = 1,
		MID = 2,
		OUTER = 3,
		NUM_LAYERS = 4,
	}
    /**
     * Create a Clothing instance
     * @param ClothingClass Clothing prototype to instantiate
     * @param data Overriding data
     * @returns  Instantiated clothing object
     */
	export function create(ClothingClass: ClothingConstructor, ...data: object[]): Clothing;
	export function simpleStrokeFill(ctx: CanvasRenderingContext2D, ex: object, clothing: Clothing): void;
}

interface ClothingPartConstructor {
	new(...data: object[]): ClothingPart;
}

/**
 * Holds no statistical information (state)
 * instead relying on the owning Clothing object to do so
 */
export class ClothingPart {
	constructor(...data: object[]);

	layer: Layer;
	loc: string;
	reflect: boolean;
	aboveSameLayerParts?: string[];
	belowSameLayerParts?: string[];

	aboveParts?: string[];
	belowParts?: string[];

	shadingParts?: [ShadingPartConstructor];

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object, avatar: Player): void;
}
