namespace App.Data {
	stores.islatavern = {
		name: "Isla Harbor Tavern", open: [1, 2, 3, 4], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "cheap wine"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "bread and cheese"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "meat and beans"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "roast fish"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "rum"},
		],
	};
}
