namespace App.Entity {

	/**
	 * NPC Entity
	 */
	export class NPC {
		private readonly _id: string;
		private readonly _data: Data.NPCDesc;
		private _state: GameState.NPC;
		private _nameOverride?: string;

		constructor(id: string, data: Data.NPCDesc, state: GameState.NPC) {
			this._id = id;
			this._data = data;
			this._state = state;
		}

		get id(): string {return this._id;}

		get name(): string {return this._nameOverride ?? this._data.name;}
		set name(v: string) {this._nameOverride = v;}

		get pName(): string {return "@@.npc;" + this.name + "@@";}
		get mood(): number {return this._state.mood;}
		get pMood(): string {return this._ratedString(NpcStat.Mood);}
		get lust(): number {return this._state.lust;}
		get pLust(): string {return this._ratedString(NpcStat.Lust);}
		get title(): string {
			return this._data.title.replace("{NAME}", this.name);
		}

		get gender(): Gender {
			return this._data.gender;
		}

		get briefDesc(): string {
			if (this._data.briefDesc) {
				return this._data.briefDesc;
			}
			return `${this.title} at @@.location-name;${App.UI.resolveDynamicText(setup.world.pc, this._data.location)}@@`;
		}

		get shortDesc(): string {return this.title + " (" + this.pMood + ", " + this.pLust + ")";}
		get longDesc(): string {return this._data.longDesc.replace("{NAME}", this.name);}
		get hasStore(): boolean {return (this._data.store !== undefined);}
		get storeName(): string | undefined {return this._data.store;}
		get storeTitle(): string | null {
			return this._data.store ? Data.stores[this._data.store].name : null;
		}

		getFlag(flag: string): GameState.NPCQuestFlagValue {
			if (this._state.questFlags[flag] === undefined) return 0;
			return this._state.questFlags[flag].value;
		}

		setFlag(flag: string, value: GameState.NPCQuestFlagValue, temporary = 0): void {
			this._state.questFlags[flag] = {value,  tmp: temporary};
		}

		clearFlag(flag: string): void {
			if (this._state.questFlags[flag] === undefined) return;
			delete this._state.questFlags[flag];
		}

		resetFlags(): void{
			for (const property of Object.keys(this._state.questFlags)) {
				if (this._state.questFlags[property].tmp !== 0) delete this._state.questFlags[property];
			}
		}

		/**
		 * Adjust an NPC statistic (dictionary value)
		 */
		adjustCoreStat(stat: NpcStat, shift: number): void {
			this._state[stat] = Math.ceil(Math.max(1, Math.min((this._state[stat] + shift), 100)));
		}

		/**
		 * Get the value of an NPC stat.
		 */
		getStat(stat: NpcStat): number {
			return this._state[stat];
		}

		adjustFeelings(): void {
			this.adjustCoreStat(NpcStat.Mood, this._data.dailyMood);
			this.adjustCoreStat(NpcStat.Lust, this._data.dailyLust);
		}

		get data(): Data.NPCDesc {
			return this._data;
		}

		static statString(stat: NpcStat, value: number): string {
			return PR.colorizeString(value, Leveling.levelingRecord(Data.npcRatings[stat], value));
		}

		private _ratedString(stat: NpcStat): string {
			return NPC.statString(stat, this.getStat(stat));
		}
	}

	export enum NpcId {
		Crew = "Crew",
		Dummy = "Dummy",
	}
}
