declare namespace App.Data {
	export namespace Actions {
		interface Base<T extends string> {
			type: T;
		}

		interface Option<T> {
			opt?: T;
		}

		interface Name<T> {
			name: T;
		}

		interface WearableOption {
			wear?: "wear";
		}

		interface Value<Type> {
			value: Type;
		}

		interface NumericValue extends Value<number>, Option<'random'> {
			/** tag name to read factor value from */
			factor?: string;
		}

		type GenericValue<T> = Exclude<T, undefined> extends void ? Record<string, never> : T extends number ? NumericValue : Value<T>;

		type BaseValue<TType extends string, Value = number> = Base<TType> & GenericValue<Value>;
		interface BaseFlag<Type extends string> extends Base<Type> {
			name: string;
			value: GameVariable;
			op?: "set" | "add" | "reset"
		}

		type Named<TType extends string, N, V = number> = BaseValue<TType, V> & Name<N>;
		type OptionallyNamed<TType extends string, N, V = number> = BaseValue<TType, V> & Partial<Name<N>>;

		type Consumable = Named<Items.Category.Food | Items.Category.Drugs | Items.Category.Cosmetics| Items.Category.LootBox, string>;
		type Clothing = Named<Items.Category.Clothes | Items.Category.Weapon, string> & WearableOption;

		type Slot = Base<"slot">;
		type Item = Named<"item", ItemNameTemplateAny>;
		type PickItem = Named<"pickItem", ItemCategoryAny, {price: number; metaKey?: string;}> & WearableOption;
		type BodyXp = Named<"bodyXp", BodyStatStr>;
		type StatXp = Named<"statXp", CoreStatStr>;
		type SkillXp = Named<"skillXp", Skills.AnyStr>;
		type Stat = Named<Stat.Core, CoreStatStr>;
		type Body = Named<Stat.Body, BodyStatStr>;
		type Skill = Named<Stat.Skill, Skills.AnyStr>;

		interface NpcStat extends Named<"npcStat", App.NpcStat> {
			npc?: string | string[];
		}

		interface VariableOptions {
			/**
			 * Change variable value immediately when action is encountered, and not at the commit stage
			 */
			immediate?: boolean;
		}

		type Variable = BaseFlag<"var"> & VariableScope.Scope & VariableOptions;
		type Quest = OptionallyNamed<"quest", string, "start" | "complete" | "cancel">;
		// type Counter = Named<"counter", string, number | undefined>;
		type Money = BaseValue<"money">;
		type Tokens = BaseValue<"tokens">;
		type Store = OptionallyNamed<"store", string, string> & Option<"lock" | "unlock">;
		type ResetStore = Base<"resetShop"> & Partial<Name<string>>;
		type CorruptWillpower = Named<"corruptWillpower", "low" | "medium" | "high">;// & Option<number>;
		type SailDays = BaseValue<"sailDays">;

		interface ProgressOp {
			op?: "set" | "add" | "reset" | "multiply"
		}

		type TrackCustomers = Base<"trackCustomers"> & Name<string> & VariableScope.Scope;
		type TrackProgress = Named<"trackProgress", string, number> & ProgressOp & VariableScope.Scope;
		type SaveDate = Base<"saveDate"> & Name<string> & VariableScope.Scope;

		interface SetClothingLock extends Base<"setClothingLock"> {
			slot: ClothingSlot;
			value: boolean;
		}

		type WorldState = Named<"worldState", "dayPhase">;
		type BodyEffect = Named<"bodyEffect", string, boolean>;

		/** Apply an effect from EffectLib */
		type Effect = Base<"effect"> & Name<string>;

		export type SingleAny = BodyXp | SkillXp | StatXp | Body | Skill | Stat | Slot | Consumable | Clothing | Item |
			PickItem | NpcStat | SaveDate |
			Variable | Quest | Store | ResetStore | Money | Tokens | CorruptWillpower | SailDays |
			TrackCustomers | TrackProgress | SetClothingLock | WorldState | BodyEffect | Effect;

		interface Choice {
			choice: NonEmptyArray<SingleAny>;
			// max?: number;
		}

		export type Any = SingleAny | Choice;
	}
}
