namespace App.Data {
	export enum CosmeticItem {
		HairAccessories = "hair accessories",
		HairProducts = "hair products",
		BasicMakeup = "basic makeup",
		ExpensiveMakeup = "expensive makeup",
	}
	Object.append(App.Data.cosmetics, {
		[CosmeticItem.HairAccessories]: {
			name: CosmeticItem.HairAccessories,
			shortDesc: CosmeticItem.HairAccessories,
			longDesc: "A collection of cheap brushes, combs, ribbons and other accessories for styling hair. Usually comes in packages of 5.",
			type: CosmeticsType.HairTool,
			charges: 5,
			skillBonus: {styling: [5, 0, 0]},
			value: 20,
		},
		[CosmeticItem.HairProducts]: {
			name: CosmeticItem.HairProducts,
			shortDesc: "haircare supplies",
			longDesc: "Various substances for styling and maintaining hair. Usually comes in packages of 5.",
			type: CosmeticsType.HairTreatment,
			charges: 5,
			skillBonus: {styling: [10, 0, 0]},
			value: 30,
		},
		[CosmeticItem.BasicMakeup]: {
			name: CosmeticItem.BasicMakeup,
			shortDesc: CosmeticItem.BasicMakeup,
			longDesc: "A collection of cheap makeup. Basic pigments and foundation with lipstick. Usually comes in packages of 5.",
			type: CosmeticsType.BasicMakeup,
			charges: 5,
			skillBonus: {styling: [10, 0, 0]},
			value: 20,
		},
		[CosmeticItem.ExpensiveMakeup]: {
			name: CosmeticItem.ExpensiveMakeup,
			shortDesc: CosmeticItem.ExpensiveMakeup,
			longDesc: "A collection of nice makeup. It contains attractive pigments, eyeshadow and lipstick. Usually comes in packages of 5.",
			type: CosmeticsType.ExpensiveMakeup,
			charges: 5,
			skillBonus: {styling: [20, 0, 0]},
			value: 40,
		},
	});
}
