declare namespace App.Data {
	namespace Text {
		namespace Scenes {
			type FragmentId = string | number;

			export interface SceneContext {
				world: Entity.World;
				pc: Entity.Player;
				actor(id: string): App.Text.Actor;
			}

			export type DynamicSceneValue<T> = T | ((ctx: SceneContext) => T);
			export type DynamicText = DynamicSceneValue<string>;
			// type DynamicSceneFlag = DynamicSceneValue<boolean>;

			namespace Choices {
				type Base = App.Data.Choices.Choice<SceneContext>;

				interface NextFragment extends Base {
					link: DynamicSceneValue<FragmentId>;
				}

				interface Passage extends Base {
					passage: DynamicSceneValue<string>;
				}

				export type Any = NextFragment | Passage;
			}

			namespace Fragments {
				interface Base<T extends string> {
					type: T;
					next?: FragmentId | DynamicText | Choices.Any[];
					action?: (ctx: SceneContext) => void;
					enabled?: (ctx: SceneContext) => boolean;
				}

				interface Text extends Base<'text'> {
					text: DynamicText;
				}

				export type Any = Text;
			}

			export interface Scene {
				/**
				 * Scene actors: internal moniker for the scene -> character id.
				 * @note This dictionary implicitly contains the "pc" entry.
				 */
				actors: Record<string, string>;
				defaultCaption?: DynamicText
				entry: DynamicSceneValue<FragmentId>;
				fragments: Record<FragmentId, Fragments.Any>;
			}
		}
	}
}
