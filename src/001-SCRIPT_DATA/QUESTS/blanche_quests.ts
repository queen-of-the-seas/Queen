namespace App {
	Object.append(Data.quests, {
		VOODOO_PICKUP: {
			title: "Voodoo Supplies",
			giver: "Blanche",
			pre: [{type: "quest", name: "PAOLA_FETCH", property: 'status', value: QuestStatus.Active}],
			checks: [{type: "quest", name: "PAOLA_FETCH", property: 'status', value: QuestStatus.Active}],
			reward: [{type: "item", name: "quest/bag of mojo", value: 1}],
			intro: "",
			middle: "",
			finish:
				"NPC_NAME says s(So that crazy witch finally sent someone to pick this feral shit up, did she? Don't look at me like \
        that, if you had this bag of rotting garbage festering in your house, you'd be mad too.)\n\n\
        She sighs sadly and then reaches behind a counter to retrieve a large brown bag. You swear it's dripping with… \
        well, something.\n\n\
        s(Just get this out of my sight, please!) she pleads as she pushes the bag into your hands."
			,
			// Don't display in journal.
			journalEntry: "HIDDEN",
			journalComplete: "HIDDEN",
		},
	});
}
