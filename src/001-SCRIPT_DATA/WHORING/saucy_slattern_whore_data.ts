namespace App.Data {
	whoring.SaucySlattern = {
		// Shows in the UI as location.
		desc: "The Saucy Slattern",
		// Sex act weighted table.
		wants: {hand: 1, ass: 4, bj: 4, tits: 3},
		// Minimum payout rank.
		minPay: 2,
		// Maximum payout rank.
		maxPay: 3,
		// Bonus payout, weighted table
		bonus: {
			style: {Bimbo: 2, PirateSlut: 1, SexyDancer: 3, SluttyLady: 2},
			body: {bust: 4, ass: 3, face: 2, lips: 2},
			stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 2},
		},
		names: Data.names.Male,
		title: null,
		rares: [],
		npcTag: "SlatternCustomers",
		available: [{type: 'quest', name: "CHARMAINE_CUSTOMER_SERVICE", property: 'status', value: [QuestStatus.Active, QuestStatus.Completed]}],
	};
}
