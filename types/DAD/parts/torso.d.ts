import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {BodyPart} from "./part";

declare class BellyTopShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}

declare class TorsoShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Torso extends BodyPart {
	constructor(...data: object[]);
}


export class TorsoHuman extends Torso {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
