namespace App.Data.Combat {
	// weak goblin
	enemyData['weak goblin'] = {
		name: 'weak goblin',
		title: 'NAME',
		health: 30,
		maxHealth: 30,
		energy: 2,
		attack: 20,
		defense: 10,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['goblin_a', 'goblin_b'],
	};

	// weak slime
	enemyData['weak slime'] = {
		name: 'weak slime',
		title: 'NAME',
		health: 30,
		maxHealth: 30,
		energy: 2,
		attack: 20,
		defense: 10,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: -1,
		portraits: ['green_slime_a'],
	};

	// Goblin Scout
	enemyData['goblin scout'] = {
		name: 'goblin scout',
		title: 'NAME',
		health: 40,
		maxHealth: 40,
		energy: 2,
		attack: 30,
		defense: 15,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['goblin_a', 'goblin_b'],
	};

	// Blue Slime
	enemyData['blue slime'] = {
		name: 'blue slime',
		title: 'NAME',
		health: 45,
		maxHealth: 45,
		energy: 2,
		attack: 30,
		defense: 20,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: -1,
		portraits: ['blue_slime_a'],
	};

	// Goblin Hunter
	enemyData['goblin hunter'] = {
		name: 'goblin hunter',
		title: 'NAME',
		health: 50,
		maxHealth: 50,
		energy: 2,
		attack: 40,
		defense: 20,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['goblin_a', 'goblin_b'],
	};

	// Brown Slime
	enemyData['brown slime'] = {
		name: 'brown slime',
		title: 'NAME',
		health: 60,
		maxHealth: 60,
		energy: 2,
		attack: 40,
		defense: 25,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: -1,
		portraits: ['brown_slime_a'],
	};

	// Goblin Warrior
	enemyData['goblin warrior'] = {
		name: 'goblin warrior',
		title: 'NAME',
		health: 60,
		maxHealth: 60,
		energy: 2,
		attack: 50,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['goblin_a', 'goblin_b'],
	};

	// Pink Slime
	enemyData['pink slime'] = {
		name: 'pink slime',
		title: 'NAME',
		health: 75,
		maxHealth: 75,
		energy: 2,
		attack: 50,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: -1,
		portraits: ['pink_slime_a'],
	};

	// Cave Crawler
	enemyData['cave crawler'] = {
		name: 'cave crawler',
		title: 'NAME',
		health: 65,
		maxHealth: 65,
		energy: 2,
		attack: 55,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: -1,
		portraits: ['crawler_a'],
	};

	// Zombie
	enemyData.zombie = {
		name: 'zombie',
		title: 'NAME',
		health: 65,
		maxHealth: 65,
		energy: 2,
		attack: 55,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['zombie_a'],
	};

	// Dark Cave Crawler
	enemyData['dark cave crawler'] = {
		name: 'dark cave crawler',
		title: 'NAME',
		health: 70,
		maxHealth: 70,
		energy: 2,
		attack: 65,
		defense: 35,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['crawler_a'],
	};

	// Dark Zombie
	enemyData['dark zombie'] = {
		name: 'dark zombie',
		title: 'NAME',
		health: 70,
		maxHealth: 70,
		energy: 2,
		attack: 65,
		defense: 35,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['zombie_a'],
	};

	// Deep Crawler
	enemyData['deep cave crawler'] = {
		name: 'deep cave crawler',
		title: 'NAME',
		health: 75,
		maxHealth: 75,
		energy: 3,
		attack: 70,
		defense: 35,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['crawler_a'],
	};

	// Deep Zombie
	enemyData['deep zombie'] = {
		name: 'deep zombie',
		title: 'NAME',
		health: 75,
		maxHealth: 75,
		energy: 3,
		attack: 70,
		defense: 35,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['zombie_a'],
	};

	// Hobgoblin Chief
	enemyData['hobgoblin chief'] = {
		name: 'hobgoblin chief',
		title: 'NAME',
		health: 80,
		maxHealth: 80,
		energy: 3,
		attack: 75,
		defense: 35,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['goblin_a', 'goblin_b'],
	};

	// Gelatinous Cube
	enemyData['gelatinous cube'] = {
		name: 'gelatinous cube',
		title: 'NAME',
		health: 80,
		maxHealth: 80,
		energy: 3,
		attack: 75,
		defense: 35,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['gelatinous_cube_a'],
	};

	// Hobgoblin Sorcerer
	enemyData['hobgoblin sorcerer'] = {
		name: 'hobgoblin sorcerer',
		title: 'NAME',
		health: 85,
		maxHealth: 85,
		energy: 3,
		attack: 80,
		defense: 40,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['goblin_a', 'goblin_b'],
	};

	// Death Jelly
	enemyData['death jelly'] = {
		name: 'death jelly',
		title: 'NAME',
		health: 85,
		maxHealth: 85,
		energy: 3,
		attack: 80,
		defense: 40,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['gelatinous_cube_a'],
	};

	// Succubus
	enemyData.succubus = {
		name: 'succubus',
		title: 'NAME',
		health: 100,
		maxHealth: 100,
		energy: 3,
		attack: 85,
		defense: 45,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 0,
		portraits: ['succubus_a'],
	};

	// Incubus
	enemyData.incubus = {
		name: 'incubus',
		title: 'NAME',
		health: 100,
		maxHealth: 100,
		energy: 3,
		attack: 85,
		defense: 45,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 0,
		portraits: ['incubus_a'],
	};
}
