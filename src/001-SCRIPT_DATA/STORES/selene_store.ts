namespace App.Data {
	stores.selene = {
		name: "Lady Selene's School Supply Emporium",
		open: [0, 1, 2],
		restock: 7,
		unlockingQuest: 'COURTESAN_GUILD_JOIN',
		maxRares: 2,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, tag: "expensive makeup", qty: 5, max: 5, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, tag: "hair accessories", qty: 5, max: 2, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "face cream", qty: 2, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "lip balm", qty: 2, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "luminescent eye drops", qty: 2, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "astringent eye drops", qty: 2, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "brow filler", qty: 2, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "brow thinner", qty: 2, max: 1, price: 1.0, mood: 0, lock: 0},
			// Rare Items - High Class Whore
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "gold hairpin", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "gold necklace", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "red wedges", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "luxurious purple bra", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "luxurious purple panties", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "luxurious purple stockings", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "red halter top", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "white microskirt", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
		],
	};
}
