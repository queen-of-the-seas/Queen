namespace App {
	export class EventEngine {
		#passageOverride: string | null = null;
		#fromPassage: string | null = null;
		#toPassage: string | null = null;

		constructor() {
			// no-op
		}

		get passageOverride(): string | null {return this.#passageOverride;}
		set passageOverride(n: string | null) {this.#passageOverride = n;}

		get fromPassage(): string | null {
			if (this.#fromPassage == null) this._loadState();
			return this.#fromPassage;
		}

		get toPassage(): string | null {
			if (this.#toPassage == null) this._loadState();
			return this.#toPassage;
		}

		nextPassage(): void {
			if (this.#toPassage == null) this._loadState();
			Engine.play(this.#toPassage as string);
		}

		backPassage(): void {
			if (this.#fromPassage == null) this._loadState();
			Engine.play(this.#fromPassage as string);
		}

		/**
		 * Checks a dictionary of destinations (including 'any') for conditions to process.
		 * On a successful evaluation return a string of a twine passage to the initiating handler
		 * that overrides the characters navigation to that passage. Also, save both the calling
		 * passage and the original destination passage for later use by the passage we are
		 * redirecting to.
		 * @param player
		 * @param fromPassage
		 * @param toPassage
		 * @returns OverridePassage
		 */
		checkEvents(player: Entity.Player, fromPassage: string, toPassage: string): string | null {
			if (!PR.isGameInInteractiveState()) return null;

			// Init the audio engine on passage click to get around browsers blocking auto play
			setup.audio.init();
			setup.audio.transition(toPassage);

			EventEngine._d("From: " + fromPassage + ",To:" + toPassage);

			// One time override
			if (this.passageOverride != null) {
				const temporary = this.passageOverride;
				this.passageOverride = null;
				return temporary;
			}

			// Check gameover conditions first. Hardcoded just because they are rare.
			if (player.stat(Stat.Core, CoreStat.Health) <= 0) {
				EventEngine._d("Player died event.");
				// State.temporary.followup = passageName;
				return "DeathEnd";
			}

			if (player.stat(Stat.Core, CoreStat.Willpower) <= 15) {
				EventEngine._d("Player lost too much willpower event.");
				// State.temporary.followup = passageName;
				return "WillPowerEnd";
			}

			// Location specific events get checked first.
			return this._tryFireEvent(player, fromPassage, toPassage) ??
				this._tryFireEvent(player, fromPassage, "Any");
		}

		static eventCountKey(eventId: string): string {
			return `EE_${eventId}_COUNT`;
		}

		static eventLastDayKey(eventId: string): string {
			return `EE_${eventId}_LAST`;
		}

		private _saveState(fromPassage: string, toPassage: string) {
			this.#fromPassage = fromPassage;
			this.#toPassage = toPassage;
			sessionStorage.setItem('QOS_EVENT_FROM_PASSAGE', this.#fromPassage);
			sessionStorage.setItem('QOS_EVENT_TO_PASSAGE', this.#toPassage);
		}

		private _loadState() {
			this.#fromPassage = sessionStorage.getItem('QOS_EVENT_FROM_PASSAGE');
			this.#toPassage = sessionStorage.getItem('QOS_EVENT_TO_PASSAGE');
		}

		private static _eventFireCount(player: Entity.Player, eventId: string): number {
			return player.flags[EventEngine.eventCountKey(eventId)] as number ?? 0;
		}

		/**
		 * Filter events down to only potentially valid ones.
		 */
		private static _filterEvents(player: Entity.Player, fromPassage: string, toPassage: string) {
			if (!Data.events[toPassage]?.length) return [];
			return Data.events[toPassage].filter((o) => {
				return ((o.from === 'Any' || o.from === fromPassage) && (setup.world.day >= o.minDay)
					&& (o.phase.includes(setup.world.phase))
					&& (o.maxDay === 0 || setup.world.day <= o.maxDay)
					&& (o.maxRepeat === 0 || (EventEngine._eventFireCount(player, o.id) < o.maxRepeat))
					&& (EventEngine.eventFired(player, o.id) < setup.world.day)
					&& (o.check(player))
				);
			});
		}

		/**
		 * Select an event from the available ones.
		 */
		private static _selectEvent(player: Entity.Player, eventArray: Data.EventDesc[]): Data.EventDesc | null{
			EventEngine._d("_SelectEvent:");
			EventEngine._d(eventArray);
			if (eventArray.length === 0) {
				return null;
			}
			const res = eventArray.find(o => o.force) ?? eventArray.randomElement();
			// For now, let's throttle events to 1 per day unless it's a forced event.
			if (!res.force && player.flags.LAST_EVENT_DAY === setup.world.day) return null;
			return res;
		}

		private static _findEvent(player: Entity.Player, fromPassage: string, toPassage: string): Data.EventDesc | null {
			return EventEngine._selectEvent(player, EventEngine._filterEvents(player, fromPassage, toPassage));
		}

		private _tryFireEvent(player: Entity.Player, fromPassage: string, toPassage: string): string | null {
			const event = EventEngine._findEvent(player, fromPassage, toPassage);
			if (event) {
				EventEngine._setFlags(player, event.id);
				this._saveState(fromPassage, toPassage);
				return event.passage;
			}
			return null;
		}

		/**
		 * Helper Method to set quest flags in player state for event tracking.
		 * @param player
		 * @param key
		 */
		private static _setFlags(player: Entity.Player, key: string) {
			const countKey = EventEngine.eventCountKey(key);
			const lastKey = EventEngine.eventLastDayKey(key);

			player.flags["LAST_EVENT_DAY"] = setup.world.day;
			player.flags[lastKey] = setup.world.day;

			player.increaseFlagValue(countKey);
		}

		/**
		 * @param player
		 * @param id Event Id
		 * @returns Last Day event was fired. 0 if never fired
		 */
		static eventFired(player: Entity.Player, id: string): number {
			return player.flags[EventEngine.eventLastDayKey(id)] as number ?? 0;
		}

		// /**
		//  * Manually trigger an event and ignore checks, coolsdowns, etc.
		//  * @param passage
		//  * @param id
		//  */
		// fireEvent(passage: string, id: string): void {
		// 	const event = Data.events[passage].retrieve(o => o.id == id);
		// 	console.log(event);
		// 	EventEngine._setFlags(setup.world.pc, event.id);
		// 	this._saveState(event.from, passage);
		// 	Engine.play(event.passage);
		// }

		private static _d(m: unknown) {
			if (setup.world.debugMode) console.debug(m);
		}
	}
}
