namespace App.Data {
	Object.append(jobs, {
		COOK01: {
			title: "Helping out in the Galley", giver: "Cook", pay: 10,
			rating: 2, // of 5
			phases: [0, 1, 2, 3],
			days: 1,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 1},
			],
			variables: {
				COOKIE_LOOT: 0,
				COOKIE_REWARD: false,
			},
			requirements: [{type: Stat.Skill, name: Skills.Domestic.Cooking, value: 20, condition: "gte"}],
			intro: "NPC_NAME says, s(PLAYER_NAME, you've become a decently good hand in the galley recently, so if you'd \
    like to earn some extra coin, how 'bout giving me an the boys a 'hand'… or an arse even! \
    Ye have no idea how boring it is to slop up gruel all day long - having a sBLOW_JOBS like you \
    around'd be a nice change o' pace…)",
			start: "",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Domestic.Cooking, value: 30}],
					post: [
						{type: "npcStat", name: NpcStat.Mood, value: 5},
						{type: "money", value: 10, factor: "A"},
					],
					text: [
						"The galley is small and cramped and it doesn't help matters that NPC_NAME himself is enormous. \
            You do your best to mostly try to stay out of the way, but every now and then his hands \
            seem to find you with a not so subtle shove or a grope of your nBUST or nASS.",
						{A: 25, text: "The @@distraction is too much for you@@ and you end up burning some food. That'll come out of your pay, or your ass, later."},
						{A: 50, text: "Despite being distracted by the constant groping you @@manage to keep your food from burning@@."},
						{
							A: 500, text: "You @@expertly manage to keep your eyes on the dishes@@ you are preparing even as your tits and ass are being " +
								"mauled by NPC_NAME and his staff. It seems you're quite used to being sexually harassed.",
						},
					],
				},
				{
					id: "SCENE02",
					triggers: [{type: "npcStat", name: NpcStat.Lust, value: 80, condition: "gte"}],
					checks: [
						{tag: "B", type: Stat.Skill, name: Skills.Domestic.Cooking, value: 40},
						{tag: "C", type: Stat.Skill, name: Skills.Sexual.AssFucking, value: 30},
					],
					post: [
						{type: "npcStat", name: NpcStat.Lust, value: -20},
						{type: "npcStat", name: NpcStat.Mood, value: 5},
						{type: "money", value: 10, factor: "B"},
						{type: "money", value: 10, factor: "C"},
					],
					text: [
						"You're chopping some vegetables for NPC_NAME's famous 'mystery stew' when you feel a pair of large arms \
            wrap around you. It's NPC_NAME and he whispers in your ear, s(Just relax and keep chopping those veggies, slut.) \
            You take a deep breath and continue chopping while his burly hands start removing your lower clothes. Within minutes \
            you feel something wet and slick being rubbed all over your asshole and shortly after that the sensation of NPC_NAME's \
            cock being pushed up against your sphincter.",
						{
							B: 25, text: "You try to steady yourself, but the feeling of the rough penetration is too much for your inexperienced ass and @@you \
                make a huge mess of the vegetables@@ as NPC_NAME pillages your butt hole.",
						},
						{B: 50, text: "It's difficult, but you @@somehow manage to not make too much of a mess@@ as NPC_NAME rams his tool up your slick asshole."},
						{
							B: 500, text: "You steel yourself for a moment and force your asshole to gape open so as to accommodate NPC_NAME's tool. Doing so \
				allows him to easily achieve maximum penetration into your sissy ass and @@keeps you from ruining the food in front of you@@.",
						},
						{
							C: 25, text: "He pumps away at your pASS butt for what seems like ages, you try to encourage him, just to get the ordeal over, but it \
                @@doesn't seem to have much effect@@. Eventually he does manage to climax, and dumps a hot load of cum up your whore ass.",
						},
						{
							C: 50, text: "He ravages your whore ass, pushing his meaty cock so deep into you that you @@gasp in a mixture of pain and arousal@@. \
				This goes on for a long time until you feel his cock twitch and start squirting hot jizz up your pASS butt.",
						},
						{
							C: 500, text: "He plows your whore ass with extreme vigour and you do your best to respond and encourage him, @@grinding your own ass \
				against his cock and bucking your hips in time with him@@. It doesn't take long for NPC_NAME to loudly groan and climax, his meaty cock \
				shooting an enormous amount of cum up your backside. As he pulls out, NPC_NAME gives you a swat on the ass and says, \"Good job, whore.\"",
						},
						"NPC_NAME cleans off his cock on a nearby rag and wanders off, leaving you humiliated and with a slippery ass dripping cum.",
					],
				},
				{
					id: "SCENE03",
					triggers: [{type: "random", value: 0.33, condition: "lte"}],
					post: [{type: Items.Category.Food, name: "bread crust", value: 3, opt: "random"}],
					text: "The work in the galley is winding down and there are less people about, you take the opportunity to pocket a few crusts of bread.",
				},
				{   // If pass both cooking checks and If counter is at MAX (5), reward player and set counter to 0.
					id: "SCENE04a",
					triggers: [
						{type: "tag", name: "A"},
						{type: "tag", name: "B"},
						{type: "var:n", name: "COOKIE_LOOT", value: 4, condition: "gte"},
					],
					post: [
						{type: "var", name: "COOKIE_LOOT", value: 0},
						{type: "var", name: "COOKIE_REWARD", value: true, immediate: true},
						{type: Items.Category.LootBox, name: "common food loot box", value: 1},
					],
					text: "NPC_NAME says, s(Here, you've earned this PLAYER_NAME.) He hands you a small wooden crate, you wonder what's inside it?",
				},
				{   // If passed both cooking checks then increment counter.
					// Doesn't trigger if the counter is already at MAX (5) or the reward has been given this time.
					id: "SCENE04b",
					triggers: [
						{type: "tag", name: "A"},
						{type: "tag", name: "B"},
						{type: "var:n", name: "COOKIE_LOOT", value: 3, condition: "lte"},
						{type: "var:b", name: "COOKIE_REWARD", value: false},
					],
					post: [{type: "var", name: "COOKIE_LOOT", value: 1, op: "add"}],
					text: "<p>NPC_NAME says, s(Nice work today. It's a pleasure to have you around the galley… in more ways than one. Keep it up and I might slip you a little bit something extra…)</p>",
				},
				{   // Just unset the flag that shows we received loot and make sure we clear the counter.
					id: "SCENE04c",
					triggers: [{type: "var:b", name: "COOKIE_REWARD", value: true}],
					post: [{type: "var", name: "COOKIE_REWARD", value: false}],
				},
			],
			end: "<p>Finally, your shift in the galley is over. NPC_NAME comes up to you and says, s(JOB_RESULTS) He then hands you some coins.</p>",
			jobResults: [
				{A: 25, text: "Your @@cooking was pretty horrible@@, don't complain about the pay."},
				{A: 50, text: "Your @@cooking was passable@@, but then again these slobs here aren't used to fine chow anyway."},
				{A: 500, text: "I'm impressed that your @@cooking was so good@@. Who knew you had that kind of talent as well?"},
				{C: 25, text: "How can you be such @@a bad fuck@@? You'd think after all the dicks you've taken you'd be halfway decent at it by now."},
				{C: 50, text: "Your ass @@was pretty good@@ - but I think you need more practice. Like tomorrow, and here."},
				{C: 500, text: "But who cares about your cooking when you can @@work a cock with your ass like that@@. Come back later if you want some more fun."},
			],
		},

		COOK02: {
			title: "Cooking Lessons", giver: "Cook", pay: 0,
			rating: 1, // of 5
			phases: [0, 1, 2, 3],
			days: 1,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 1},
				{type: "money", value: 20},
			],
			requirements: [{type: Stat.Skill, name: Skills.Domestic.Cooking, value: 5, condition: "gte"}],
			intro:
				"NPC_NAME says, s(We can always use help here in the galley, PLAYER_NAME. And if yer crap at cooking, \
        then I might could be obliged to give you a 'personal lesson' fer a small fee…)",
			start:
				"NPC_NAME leads you to the back of the galley and lumbers up to a large table where he has laid out a collection of \
        cooking tools and ingredients. You're surprised that he's put so much thought into this considering the quality of \
        the food that the pirates on board the ship eat.",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Domestic.Cooking, value: 30}],
					post: [
						{type: "skillXp", name: Skills.Domestic.Cooking, value: 10},
						{type: "skillXp", name: Skills.Domestic.Cooking, value: 100, opt: "random"},
					],
					text: [
						"NPC_NAME starts by giving you some basic instruction on how to use various tools and implements in the \
                galley. He goes into a practical demonstration of some simple dishes by explaining their ingredients and \
                preparation. After watching him, it's then your turn to repeat the recipes.",
						{A: 25, text: "Unfortunately, it seems that the lesson didn't really stick and your @@dishes came out horrible@@. NPC_NAME sneers at your various piles of glop and shakes his head, not at all pleased with the results."},
						{A: 50, text: "With a small amount of effort you manage to cook a @@dish that isn't half bad@@. NPC_NAME takes a few tastes and shrugs in passable acceptance."},
						{A: 500, text: "You take your time and manage to @@skillfully recreate the dishes@@ that you were shown. NPC_NAME looks at them, takes a few tastes, and then nods in approval."},
					],
				},
				{
					id: "SCENE02",
					triggers: [{type: "npcStat", name: NpcStat.Lust, value: 50, condition: "gte"}],
					checks: [{tag: "B", type: Stat.Skill, name: Skills.Sexual.BlowJobs, value: 30}],
					post: [
						{type: "npcStat", name: NpcStat.Lust, value: -20},
						{type: "npcStat", name: NpcStat.Mood, value: 5},
						{type: Stat.Core, name: CoreStat.Nutrition, value: 5},
						{type: "money", value: 10, factor: "B"},
					],
					text: [
						"After the cooking lesson, NPC_NAME says, s(Now that we got the business out of the way, it's high time for the pleasure! \
                So how about you get on your knees, ya little cunt, and try to earn back a little bit of your tuition with that slutty mouth \
                of yours?)\n\n\
                He doesn't wait for a response before he abruptly shoves you to your knees and fishes his stiffening prick from his filthy \ trousers. You wince at the sight of the sweaty length of veiny fuckmeat, but you quickly resign yourself to the task of sucking \
                off his clammy cock by telling yourself that you have no choice in the matter.\n\n\
                NPC_NAME joggles his shaft in the endeavor to harden himself up. The plump purpled bulb-end bobbles a mere inch from your face. \
                You can see glistening precum already starting to ooze from his puckered piss slit.He jabs his swollen sausage against your nLIPS, \
                smearing salty pre around your mouth and chin, and sputters an array of vulgarities down at you: s(Suck, you little whore!) s(Lick \
                it!) s(Gulp my hot gravy!)\n\n\
                As ordered, you begin to slowly lick the glans of his wobbly wang in the attempt to arouse him to full-mast.",
						{
							B: 25, text:
								"Without warning, NPC_NAME crams the whole of his still quite spongy shlong full on into your mouth. You try to accommodate the bulging mouthful of half-hard chub, @@but you struggle to find the right movement and tempo@@ to provide any sort of satisfying service to it.\n\n\
                In a fit of frustration, NPC_NAME clamps your head betwixt his huge hands and settles for smashing his groin against you. Thankfully his burgeoning bush of bristy pubes cushions the impact but having your face repeatedly squashed into his nappy nest of oily public hair isn't much better. Struggling for a breath, you slap your hands on his thighs uselessly as he skids his rubbery rod into your mouth and throat again and again. You gag noisily as he finally stiffens up and, of fucking course, that seems to do it for him.\n\n\
                Brackish baby-batter boils out of his gullet-gorging cock and your cheeks bulge out with the sultry spooge. He gleefully crams the salty spunk down your throat, triumphantly groaning with each thrust. Finally content, he releases you and cuffs your head away from his softening prick as if you'd done him a dire disservice.\n\n\
                NPC_NAME says, s(That was downright horrible, PLAYER_NAME! You ought to be paying me for such a rotten showing at that. You best get some practice, god dammmit. It's not like there isn't a ship full of dicks to suck here, ya crummy cockswab.)",
						},
						{
							B: 50, text:
								"Taking note of his growing impatience, you quickly commence fondling his balls while you smush your mouth and tongue against the underside of his shaft, squashing his stiffening prick into his squishy stomach with your cheek like a ditzy cock-starved slut. You sputter out as much saliva as you can manage, coating everything you can in quick-and-dirty lube, so you can slide your face up and down his slowly plumping pecker.\n\n\
                You let out a soft, girlish mewl to feign your own arousal in an attempt to provoke a proper erection. He replies with a merry grunt and caresses your hair with his fat sausage fingers. His heartbeat pulsates on your lips as you lick and smooch his twitching fuckstick, coaxing him to serviceable rigidity. Satisfied that he's ready at long last, you envelope his prick with your mouth and @@begin to suck it in earnest@@. You bob your head back and forth, spearing his dick through your pursed lips and over your rippling tongue while purposefully but softly huffing a breath out of your nose with every intake. He begins to mimic your panting. Every time he moans, you return it with purr. And every time he grunts, you let slip a shamelessly sloppy sucking sound.\n\n\
                It doesn't take much longer before he grabs ahold of your head and rams his throbbing knob into your mouth as deep as he can. You try your best to relax -- despite having your esophagus gorged with a cum spewing cock -- so that he can unload his balls straight down your gullet. With a shuttering sigh, he finishes filling your belly with his sultry spunk and releases you. You take a gasping breath, errant cum burbling obscenely in your throat, and sit back on your feet to compose yourself.\n\n\
                s(Not bad, PLAYER_NAME,) he says, simpering down at you. s(It's right good to see yer enjoying yerself, lass. Nothing as rousing as having a happy cock-sucker handy, I always say! And, here now, I'll give you back some coins fer yer sweet spirit.)",
						},
						{
							B: 500, text:
								"But, wise to this silly half-hard nonsense a few of these men are inclined towards, you simply slurp his spongy schlong into your mouth, work the bouncy bastard into your throat, and then @@you commence to gluttonously gulp against his rubbery rod@@ while lashing at the naughty non-erection with your tongue.\n\n\
                In less than a minute, NPC_NAME is reluctantly pushing you away, whimpering from such surprisingly speedy over-stimulation. After some nominal resistance, you let him shove you off his palpitating pecker but you immediately shoot him a lusty scowl that makes him shiver. The expression reads, 'How dare you deprive me of this dick!' He swallows hard and draws back his hands. Graciously, you settle for gently smooching his shaft and cuddling his doughy sack in your hand while his overwhelmed arousal calms to a more pleasurable level. After some time, his ragged breath becomes much less hurried.\n\n\
                You throw him a lewd pout that tells him you're done waiting and then you instantly sheath the swashbucker's cocksword in your sultry, silky throat. Your tongue slithers out to work the base of his shaft and you tenderly squeeze his balls, coaxing out his seed, as you zealously attempt to swallow his dick right off his body. In no time, you feel him getting close again -- his fingernails graze against your skin as he prepares to unload -- and you open your mouth wide and abandon his pulsating prick with only a breathy exhale as a farewell.\n\n\
                You watch in ruthless delight as he rides the very edge of orgasm: his breath stuck, his eyes frozen, his twitching cock struggling mightily to disgorge itself. Precum swelters and trickles from the angry purple head. You reach out your tongue and lightly lick a tongueful of the salty scum into your mouth. You swallow it with a husky purr while staring up into his eyes. You wait. And you wait some more. His respiration returns to him and he slowly comes to his senses as his imminent orgasm subsides.\n\n\
                But you don't let it get too far away. With a blustery assault of hungry kisses, frenzied licks, and sloppy wet suckles, NPC_NAME is promptly teetering on the brink of nut-busting once again. Having had your fun, you fall into the complacent routine of bobbing your head back and forth on his fevered fuckstick, just sucking and slurping yet another cock like a typical shameless slut. You swill down several spates of hot viscous jizz without missing a beat and then pop your lips off his squeaky-clean prick with a satisfied smack. You wipe your mouth with your fingers and gaze up at him with a desirous look to ensure he observes you dutifully suck them clean as well.\n\n\
                Finding his breath at last, NPC_NAME says, s(That was… right sufficient, PLAYER_NAME.) He wipes the sweat off his forehead with the back of his arm and heaves out a breath of relief. s(I'd heavily wager you enjoyed yerself even more than I did, lass, but I'm a man of me word. I'll have you back some coins for yer… most merciful attention.)",
						},
						"NPC_NAME pulls up his cruddy trousers, gives you a rough pat on the head, and then lumbers away.",
					],
				},
				{
					id: "SCENE03",
					triggers: [{type: "random", value: 0.50, condition: "lte"}],
					post: [{type: Items.Category.Food, name: "bread crust", value: 3, opt: "random"}],
					text:
						"With the lesson in galley over, you notice an opportunity for you to steal some food. Normally you're not a thief, but these are not " +
						"normal times. When you're sure no one is looking you manage to swipe some bread crusts. At least it's better than the semen gruel they keep feeding you.",
				},
			],
			end:
				"You feel like you made some JOB_RESULTS.",
			jobResults: [
				{A: 25, text: "@@minimal progress@@ today"},
				{A: 50, text: "@@moderate progress@@ today"},
				{A: 500, text: "@@good progress@@ today"},
				{B: 25, text: " even if you did have to @@suck a cock@@ to get the lesson"},
				{B: 50, text: " even if you did have to @@suck a cock@@ to get the lesson"},
				{B: 500, text: " even if you did have to @@suck a cock@@ to get the lesson"},
			],
		},

		COOK03: {
			title: "A 'Free Meal'", giver: "Cook", pay: 0,
			rating: 1, // of 5
			phases: [0, 1, 2, 3],
			days: 1,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 1},
			],
			requirements: [
				{type: Stat.Core, name: CoreStat.Nutrition, value: 70, condition: "lte"},
				{type: "npcStat", name: NpcStat.Lust, value: 60, condition: "gte"},
			],
			intro:
				"NPC_NAME says, s(You're looking a little hungry PLAYER_NAME… and I don't just mean for cock. Tell you what, if you want to get a \
    'free meal' I might be willing to oblige if you have a little fun with me in return.) He gestures his head in the direction of a \
    large 'high chair' across the room, the kind you might feed a child from, but obviously built for an adult. You have a bad feeling about this.",
			start:
				"NPC_NAME leads you over to the looming high chair and shoves you into it. Before you can even catch your breath, you find yourself \
    locked in behind the large wooden tray. Just as you are about to protest, NPC_NAME puts one of his massive fingers against your lips \
    and goes, s(Shh… just be quiet like a good little sissy baby.)\n\n\
    He then manacles you to the chair and produces a large pink bonnet that he secures to your head as well as a frilly pink bib that he \
    places around your neck. He chuckles mirthfully to himself and then says, s(One minute sissy girl. Daddy has a treat for you.) You  \
    hear some shuffling behind you as NPC_NAME rummages around in the kitchen. Within moments he returns with a large bowl of yellowish \
    goop and ominously, no spoon. He sets the bowl down on the counter next to him and pulls down his trousers, revealing a fat and massive cock.",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Sexual.BlowJobs, value: 30}],
					post: [
						{type: "statXp", name: CoreStat.Nutrition, value: 100},
						{type: Stat.Core, name: CoreStat.Nutrition, value: 20},
						{type: "bodyXp", name: BodyPart.Lips, value: 50},
						{type: "bodyXp", name: BodyPart.Lips, value: 100, opt: "random"},
						{type: "statXp", name: CoreStat.Hormones, value: 100, opt: "random"},
						{type: "npcStat", name: NpcStat.Mood, value: 5},
						{type: "npcStat", name: NpcStat.Lust, value: -20},
						{type: "npcStat", name: NpcStat.Mood, value: 10, factor: "A"},
					],
					text: [
						"With one hand he scoops up some of the mysterious goop and slathers a large heaping on his turgid member. He approaches you eagerly, \
        his massive cock waving in front of your face.\n\n NPC_NAME says, s(Now be a good sissy and open wide…)\n\n\
        You quickly turn your head away, but it's to no avail as NPC_NAME easily palms your entire skull with one of his massive meat hooks and \
        turns you to face his cock. Without much ceremony he pushes his meat past your nLIPS.\n\n\
        Inwardly you sigh. This entire act is humiliating and degrading, but you're the one that wanted a free meal. Faking a smile you lean \
        forward and start licking the goop from NPC_NAME's dick, taking care to run your tongue and nLIPS over his glans while consuming as much \
        of the 'food' as you can.\n\n\
        NPC_NAME continues feeding you this way by putting more and more of the mystery gruel on his cock and forcing it past your lips. Somewhere \
        after the fourth or fifth serving you start to feel your head get light and your lips start to tingle. Without even noticing it you have \
        been absently slurping and licking the gruel off his massive member and licking your lips in between servings. Eventually NPC_NAME gets \
        tired of the game and begins to fuck your face in earnest. You think that this is supposed to hurt, but for some reason your entire face \
        and brain feel numb. Maybe there was something in the gruel?\n\n\
        NPC_NAME grabs your head and forces it down on his pole. The strength of this man is not to be underestimated as he easily buries \
        your nLIPS down to the root of his shaft. You feel his already massive member swell in size and then start to spasm as he shoots an \
        enormous amount of cum past your tonsils and down your throat.",
						{A: 25, text: "NPC_NAME says, s(That was fun, but you're a @@lousy cock sucker@@. Oh well at least you'll have time to learn.)"},
						{
							A: 50, text: "NPC_NAME says, s(Not bad PLAYER_NAME. Not bad at all. You keep @@sucking my dick like that@@ and I might keep \
            you as my personal little girl.)",
						},
						{
							A: 500, text: "NPC_NAME says, s(Damn you're a pretty @@good cock sucker@@ ain't ya? Come back later if you want to 'fill your \
            belly' again slut.)",
						},
						"After all is said and done and after NPC_NAME has unlocked you from the chair, you let out a small burp flavored with sperm and say \
            to yourself, sp(I guess there's no such thing as a 'free meal')",
					],
				},
			],
			end: "",
		},

		COOK04: {
			title: "Fresh Dairy in the Galley", giver: "Cook",
			rating: 3, // of 5
			phases: [0],
			days: 1,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: CoreStat.Energy, value: 1},
			],
			requirements: [{type: Stat.Body, name: BodyProperty.Lactation, value: 1, condition: ">=", raw: true, hideTask: true}],
			intro:
				"You stand next to the cauldron, stirring it with a great wooden ladle. It is full of oatmeal for the crew's morning meal. It is a \
    bit awkward today, your breasts are still swollen from your session with the first mate last night.",
			start:
				"NPC_NAME comes up behind you and grinds his groin into your backside with his hands on your hips. He leans into it and breaths into your ear \
    saying, s(I think I know who's going to get a little cream for their oatmeal today. Maybe I should just put it straight up into your belly \
    from behind right now!). You smell the rum on his breath. He guffaws at his own crude joke and presses his rotund belly into you with more \
    aggression, pinning you to the cauldron. With you trapped he reaches around you in a bear hug while still grinding away. As you feel his \
    manhood hardening and pressing against you, you can not help but let out a bit of a squeal. He fully cups both of your pBUST breasts with his \
    large hands, your nipples centered in his palms. He begins to roughly kneed your boobs as you begin to squirm frantically trying to get away \
    from both him and the growing heat from being pressed onto the cauldron.",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Body, name: BodyPart.Bust, value: 50, raw: true}],
					post: [
						{type: "bodyXp", name: BodyProperty.BustFirmness, value: -80, factor: "A"},
						{type: "bodyXp", name: BodyProperty.BustFirmness, value: -40, factor: "A", opt: "random"},
						{type: "bodyXp", name: BodyProperty.Lactation, value: 100, factor: "A"},
						{type: "money", value: 150, factor: "A"},
					],
					text:
						"s(Whats this!) he bellows, face askew.\n\n\
            You stop your struggle as he eases back a bit to look over your shoulder at his hands grasping at your breasts. You also focus where \
            he looks to see that his hands are wet with a white liquid. Your own breast milk you realize. tp(Still?) you think to yourself, \
            tp(What is Kipler injecting me with? What is it going to do to me in the long haul? I think my boobs never shrink back down all the way again afterwards. And why am I producing so much milk? It's seems to be more than normal for a woman, let alone me!).\n\n\
            s(Well, well, what do we have here? Are you trying to find your place in the crew by replacing the dairy goat? Come over here!) \
            NPC_NAME grabs your upper arm and drags you over to the high chair. s(Sit!) he orders, and you do. He clears any obstruction to \
            gain access to your bounty and locks the wood tray into place. It is low, just on top your thighs. He barks, s(Sit up straight and \
            stay still. Do not cease to do so if you don't want to be backhanded.) He goes and fetches a large clean oven pan and places it on \
            the tray under your boobs.\n\n\
            He fondles your your breasts and gently strokes them. Being a bit afraid you do as he said and in no time your nipples engorge and \
            begin to drip into the pan. He teases your nipples and your breasts begin to let down. And then with a surprisingly efficient skill \
            he proceeds to milk you like a dairy beast.\n\n\
            He is firm and purposeful, but he is tender with his touch and speaks to you soothingly. You suppose he does not wish to sour the milk. \
            After a while you begin to find that you quite enjoy the feeling, and you think under the right conditions if it was to go on long enough \
            you might find a release into a state of bliss.\n\n\
            Tug right, tug left, tug right, tug left, right, left, right, left, tug tug tug…\n\n\
            You lost yourself there, but you snap back. The milk is pooling now in the pan you see. Your breasts feel lighter, and look softer. The milk \
            being released is tapering off.\n\n\
            NPC_NAME ceases to milk you, wipes his hands on his dirty apron, takes the pan and pores it into the kitchens magically cooled milk barrel. \
            s(Not bad, Not bad at all.) His hairy fist comes towards you with some gold coins in it and rains it over your teats onto the tray. s(Those \
            are for you. The more you produce the more I'll pay you. A bit for enough for the captains iced coffees, and some real coin if you give me \
            enough to to sell fresh milkshakes to the crew. Be sure to eat well now cabin COW! I guess cabin girl wasn't good enough for ya! HA- HA!)",
				},
				{
					id: "REWARD",
					triggers: [{type: "tag", name: "A", value: 0.9}],
					post: [
						{type: "pickItem", name: Items.Category.Clothes, value: {price: 5000, metaKey: 'cow outfit'}},
						{type: Items.Category.Food, name: "milkdew melon", value: 2},
					],
					text:
						"Cookie seems pleased with the amount of milk you gave. He's greedily rubbing his hands together and smacking his lips. Without a second \
            thought he reaches for a small bag and throws it your way. s(Here, take that, you've earned it!)",
				},
			],
			end: "He releases you from the chair and yells, s(Back to work, back to work!) and gropes your ass till your back at it with the ladle.",
		},

		GALLEY_WORKFORCE: {
			title: "Galley work horse",
			giver: "Cook",
			days: 1,
			rating: 3,
			phases: [0],
			cost: [
				{type: "time", value: 4},
				{type: Stat.Core, name: CoreStat.Energy, value: 6},
			],
			progressMeters: {GALLEY_WORKFORCE_DAYS: Tasks.ProgressType.Progress},
			requirements: [
				{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "lt", hideTask: true},
				{type: "quest", name: "FEMINITY_BOOST:COOK_HELP", property: "status", value: QuestStatus.Active, hideTask: true},
				{type: "npcStat", name: NpcStat.Mood, value: 60, condition: "gte"},
			],
			intro: "s(Mornin', darlin'!) greets you NPC_NAME. s(Another day of hard work ahead!)",
			scenes: [
				{
					id: "FIRST_GREETING_INTRO",
					triggers: [{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 0, condition: "eq"}],
					text: "s(So, your job will be to ensure I have some free time in the evening, ho-ho…) \
				You will be doing a lot more than during those regular helping sessions we had before. \
				Peeling, scaling, cutting are not the only required things to get started with cooking, \
				you know, and you are going to lean how hard is to keep everything running day by day.",
				},
				{
					id: "GREETING_INTRO",
					triggers: [{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 0, condition: "gt"}],
					text: "s(So, you are ready to work for another day, ya? Nice, let's see how can you help me \
				to clear another evening for gambling!)",
				},
				{
					id: "ASSIGNMENT_CHOICE",
					triggers: [{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "lt"}], // first 6 days
					checks: [{tag: "ASSIGNMENT", type: "random", factor: 4, value: 100, round: "floor", condition: null}],
					text: "s(Let's look what can you do today…)",
				},
				{
					id: "ASSIGNMENT_GALLEY_CLEANING",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 0, condition: "eq", raw: true},
					],
					checks: [{tag: "CLEANING", type: Stat.Skill, name: Skills.Domestic.Cleaning, value: 50}],
					text: [
						"s(Not much fun for today, PLAYER_NAME, just a mundane work for you.) tp(That, actually, might not be bad, given the humiliation \
				usually associated with 'interesting' ones). s(But that should still provide me with a free evening, which is our primary goal, \
				right, PLAYER_NAME? Whatever… Your task for today is to keep this galley clean at all times!) The task indeed seems to be mundane. \n\
				You nod and begin to clean the galley up, which needs to be done quickly if you want to sort up the clatter before the crew finish \
				with lunch and fill the galley with dirty dishes again. Somewhen during the first hours of work NPC_NAME leaves galley and you are left alone between dirty pots, dishes, and brooms.\n\
				To your surprise the guys begin to arrive earlier than expected, looking for NPC_NAME, and crew members do not look happy. When you \
				inform them that NPC_NAME is not at the galley and you do not know where to find him, some of them seem to get angry. s(Did you cook \
				or helped NPC_NAME to cook today?) No, you did not, you replied with some relieve, as they seem to be extremely unhappy with their lunch \
				today. As more pirates arrive looking for NPC_NAME, apparently bearing him responsible for their unhappy mood and unwell look, the air in \
				the little galley becomes even more stifling than usually. In a few moments you overhear s(Back off, guys…) and someone vomits. That acts as \
				a trigger for the rest of pirates with nausea here and many of them empty their stomachs right away. s(A good portion of grog shall get this shit out of us!) somebody shouts and the mod quickly backs up the request. They seem to forget about NPC_NAME and move out looking for \
				beverages.\n\n\
				The galley and the space near it looks terrible, covered with food leftovers, dirty bowls, vomit, and a mix of that. You'll meed a lot of \
				time to make the space relatively clean again. And so you change water in the pail, wash the mop and start to clean. NPC_NAME shows up \
				shortly after four bells, while you are still working hard tidying up the galley. s(The bastards did not took it well, ye? But in the East \
				they eat it regularly! Maybe less spicy next time…?)\n",
						{
							CLEANING: 25, text: "You spent the rest of the day and the evening in @@mostly hopless attempts@@ to remove the stench from the galley \
					and its surroundings.",
						},
						{
							CLEANING: 100, text: "In a few of hours you managed to @@bring the galley back to a normal condition@@, and the regular cleaning \
					took time until the evening.",
						},
						{CLEANING: 150, text: "You @@quickly finished@@ cleaning up the galley and managed to @@get some rest today@@."},
					],
				},
				{
					id: "ASSIGNMENT_GALLEY_CLEANING:REWARD",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 0, condition: "eq", raw: true},
						{type: "tag", name: "CLEANING", value: 75, condition: "gte", raw: true},
					],
					post: [{type: Stat.Core, name: CoreStat.Energy, value: 3}],
				},
				{
					id: "ASSIGNMENT_GALLEY_CLEANING_REWARD",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 0, condition: "eq", raw: true},
						{type: "tag", name: "CLEANING", value: 50, condition: "gte", raw: true},

					],
					text: "s(It did not went the way I expected), says NPC_NAME, s(but at lest you did a good job here, so here's your pay)",
					post: [
						{type: "money", value: 0.2, factor: "CLEANING"},
						{type: Items.Category.Food, name: "smugglers ale", value: 2},
						{type: Items.Category.Food, name: "bread and cheese", value: 1},
					],
				},
				{
					id: "ASSIGNMENT_COOKING_INTRO",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 1, condition: "gte", raw: true},
					],
					checks: [{tag: "COOKING", type: Stat.Skill, name: Skills.Domestic.Cooking, value: 40}],
					text: [
						"You'd expect the job to be similar to what you already used to do at the galley, helping them and pleasuring them time to time. Yet today \
				you seem to be the only one who is performing actual work. NPC_NAME and his guys do not care much what you do. Soon you realise you have to \
				make the lunch and the dinner for the whole crew today. ",
						{
							COOKING: 33, text: "A @@quick panic wave@@ runs through your mind when you imagine failing your task and the whole crew, hungry and \
					with nothing to eat this evening.",
						},
						{
							COOKING: 66, text: "@@You shivered for a moment@@ imagining failing your task and the whole crew, hungry and \
					with nothing to eat this evening, but that feeling disappeared as you get absorbed by the work.",
						},
						{
							COOKING: 500, text: "@@tp(I have a freedom to cook whatever I want today!)@@ you told yourself. You like to cook, will be nice to \
					dive into it for the whole day!",
						},
						" You check over available ingredients to plan today's meals and learn there is no much at your disposal. ",
						{COOKING: 33, text: "@@Will you be able to make anything@@ out of that debris?"},
						{COOKING: 66, text: "@@It will be not easy@@ to make something good out of that debris!"},
						{COOKING: 200, text: "@@tp(Let's improvise!)@@ you decide and dive into experimentation."},
						" You decide the best option for you is to make a ragoût using as many various vegetables and meat leftovers as possible. Peeling, \
				cutting, frying and stewing takes the next few hours and you ",
						{
							COOKING: 33, text: "@@completely forgot about lunch@@, being busy with the dinner. And now only a little bit more than two \
					bells left until the lunch time. You have to put away half-made dinner and switch over to lunch. There is no enough time to do it \
					properly, thus you quickly slash and fry potatoes, serve them with fish and meat remains from yesterday and hope the crew will \
					not be too exigent today.",
						},
						{
							COOKING: 66, text: "@@managed to make a passable lunch@@ and continue to cook the dinner. The lunch dish can be named exactly \
					delicious, but then again, the crew are not hourmans too.",
						},
						{
							COOKING: 500, text: "@@are just in time to collect what you've prepared for lunch@@ earlier and make the final touch mixing \
					together the components and adding a pre-heated source. Sadly, there are no hourmans among the crew to pay a tribute to your \
					meals.",
						},
					],
				},
				{
					id: "ASSIGNMENT_COOKING_DINER",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 1, condition: "gte", raw: true},
					],
					checks: [{tag: "COOKING_MAIN", type: Stat.Skill, name: Skills.Domestic.Cooking, value: 60}],
					text: [
						" With the lunch sorted out, you return to making the dinner. Again some chopping and boiling ahead, and after a while ",
						{
							COOKING_MAIN: 33, text: "a good quantity of @@something eatable@@ is ready in pots, waiting to be served. This is not quite \
					the delicious ragoût you imagined, but it certainly nutrient. Maybe just a little too sticky and some pieces are a bit burned.",
						},
						{
							COOKING_MAIN: 66, text: "you finish the ragoût in time. It tastes as a @@healthy mix of vegetable and meat flavours@@, nothing \
					truly delicious, but Salty Mermaid is not a restoraint either.",
						},
						{
							COOKING_MAIN: 500, text: "the ragoût, @@just as you have imagined it@@, is ready for serving. You quickly cook a souse for \
					hardtacks, which should specially pleasure the crew, tired of eating the same hardtacks year by year",
						},
						" You ate with the crew and certainly will not go to bed with an empty stomach.",
					],
					post: [{type: Stat.Core, name: CoreStat.Nutrition, value: 30}],
				},
				{
					id: "DINNER_WENT_BAD",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 1, condition: "gte", raw: true},
						{type: "tag", name: "COOKING_MAIN", value: 0.33, condition: "lte"},
					],
					text: "When you join the crew at the dinner table, they make no effort to hide how bad the food is, and how exactly they do not like it.",
					post: [{type: "npcStat", name: NpcStat.Mood, npc: "Crew", value: -5}],
				},
				{
					id: "DINNER_WENT_OK",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 1, condition: "gte", raw: true},
						{type: "tag", name: "COOKING_MAIN", value: 0.33, condition: "gt", raw: true},
						{type: "tag", name: "COOKING_MAIN", value: 0.66, condition: "lte"},
					],
					text: "Nobody complained today on the dinner, neither about quality nor quantity.",
				},
				{
					id: "DINNER_WENT_GOOD",
					triggers: [
						{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "<"},
						{type: "tag", name: "ASSIGNMENT", value: 1, condition: "gte", raw: true},
						{type: "tag", name: "COOKING_MAIN", value: 0.66, condition: "gt"},
					],
					text: "Your efforts paid well, the dinner was good and the crew seems to be enjoying the meal.",
					post: [{type: "npcStat", name: NpcStat.Mood, npc: "Crew", value: 5}],
				},
				{
					id: "DAY_FINISH",
					text: "You leave the galley exhausted.",
					post: [{type: "trackProgress", name: "GALLEY_WORKFORCE_DAYS", value: 1, op: "add"}],
				},
			],
			end: "",
		},

		GALLEY_WORKFORCE_FINALE: {
			title: "Galley work horse",
			giver: "Cook",
			days: 1,
			rating: 3,
			phases: [0],
			cost: [
				{type: "time", value: 4},
				{type: Stat.Core, name: CoreStat.Energy, value: 6},
			],
			requirements: [
				{type: "trackProgress", scope: {type: "job", id: "GALLEY_WORKFORCE"}, name: "GALLEY_WORKFORCE_DAYS", value: 6, condition: "==", hideTask: true},
				{type: "quest", name: "FEMINITY_BOOST:COOK_HELP", property: "status", value: QuestStatus.Active, hideTask: true},
				{type: "npcStat", name: NpcStat.Mood, value: 60, condition: "gte"},
			],
			intro: "s(Mornin', darlin'!) greets you NPC_NAME. s(Another day of hard work ahead!)",
			scenes: [
				{
					id: "ASSIGNMENT_MEAL_DISTRIBUTOR", // assignment for the last day
					triggers: [],
					checks: [
						{tag: "HAND", type: Stat.Skill, name: Skills.Sexual.HandJobs, value: 40},
						{tag: "BLOW", type: Stat.Skill, name: Skills.Sexual.BlowJobs, value: 40},
					],
					text: [
						"s(Okay, PLAYER_NAME, today we are organising a food court instead of the regular dinner. In fact, the food court will be \
				open for the whole day to replace the breakfast too. And you will be the Lady of the Court! Meaning you will be distributing \
				the meals.) Sounds interesting but you can't imagine where would NPC_NAME get required variety of meals to set up a food court \
				s(Come here, here is your work place!) NPC_NAME shows you a table near the galley entrance. The table is low, roughly up to your \
				knees, and is rather small overall. s(So, you will work on this table. The job is to sell deserts.) sp(Sell deserts? But crew members \
				do not pay for their rations on-board, do they?) you wondered. s(Or, no, they don't, but this gonna be a special occasion, and \
				the payment is absolutely voluntary, so no worries here.) replies NPC_NAME. s(Okay, here are the deserts) he continues, bringing \
				two casks you very well remember, as they were filled with overdried old hardtacks, and NPC_NAME once disallowed you to use one \
				of them as a trash bin, saying the dried biscuits will be used later one way or another. tp(He can't propose them as deserts for a \
				special occasion, can he? No, that would be completely impossible). However, when NPC_NAME opens one of the casks you see those \
				old hardtacks, some of them show clear signs of molding. Who would want to eat them, let alone to pay for them? NPC_NAME continues \
				s(These are the biscuits for you to sell. Yes, I know they don't look neither fresh nor tasty, but for their price I expect them to sell \
				good. What's the price you ask? The price is their semen, for those who want to pay. Remember, it's voluntary. And your job is to \
				collect the payments. Me? I will be busy wih pl… with another business for the whole day, so take your place and start working. The \
				bastards will be here soon, demanding something to eat). With these words NPC_NAME leaves you alone at the galley.\n \
				Shocked, you position the open cask in front of the table, try to rearrange biscuits to hide the molded ones beneath the relatively \
				good looking ones, kneel on the table and wait. In the morning you hope that not everyone will volunteer to pay, but seems like \
				NPC_NAME told them everything, the little pot you selected for collecting payments is full by the noon, when you get a little \
				break from your duties.\n\
				Evening brings another wave of customers, requiring your hands and mouth to work, at times processing 'the payment' from two crew \
				members at once. Finally, they get what they wanted and you are alone again. Two pots of semen are nearly full and sits beside you. ",
						{
							HAND: 25, text: "For sure, you have never had to touch so many dicks a day, but @@disgusting is only the one side@@ of the new \
					experience, there was something else too… something charming, maybe? You wonder how perverted your true self is. ",
						},
						{
							HAND: 50, text: "Pleasing so @@many dicks in a single day was a new experience for you@@, but only because they were many; while \
					caresing a dick with your hand is not new to you, feeling variety of sizes and shapes was a new touch. Did you enjoy just the \
					variety or the dickmeat stream on itself? ",
						},
						{
							HAND: 500, text: "It was @@exciting to lay hands on so many dickmeat@@ in a row, wasn't it? tp(I should do that more frequently) \
					you says to yourself. ",
						},
						{
							BLOW: 25, text: "Also @@you had to suck many dicks@@ today, fortunately you did not have to swallow the semen, and you preferred \
					the guys to finish off by themselves, filling up the pot for \"payments\".",
						},
						{
							BLOW: 50, text: " @@Significant part of the \"payments\" you collected with the help of your nLIPS@@ and mouth. With some of the \
					guys you even enjoined that, didn't you?",
						},
						{
							BLOW: 500, text: " You also @@enjoined blowing many dicks with your nLIPS@@, sadly you were not allowed to swallow all the semen, \
					as some of that had to be collected as \"payment\", but hopefully that can be compensated the other day when you will be able to \
					suck on a dick as you like, don't you?",
						},
						"NPC_NAME returns a bit later. Around half of the hardtack cask is gone and NPC_NAME seems to be satisfied with that. s(You may \
				take a few biscuits for you as well, if you want).",
					],
					post: [
						{type: Items.Category.Food, name: "hardtack", value: 6},
						{type: "npcStat", name: NpcStat.Mood, value: 15},
						{type: "npcStat", name: NpcStat.Mood, npc: "Crew", value: 2, factor: "BLOW"},
						{type: "npcStat", name: NpcStat.Mood, npc: "Crew", value: 1, factor: "HAND"},
						{type: "statXp", name: CoreStat.Perversion, value: 20, factor: "HAND"},
						{type: "statXp", name: CoreStat.Perversion, value: 30, factor: "BLOW"},
						{type: "trackProgress", scope: {type: "job", id: "GALLEY_WORKFORCE"}, name: "GALLEY_WORKFORCE_DAYS", value: 1, op: "add"},
					],
				},
			],
			end: "This day concludes your agreement with NPC_NAME. You have being worked at the galley for seven full days!",
		},
	});
}
