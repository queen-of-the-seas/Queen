namespace App.Data {
	Object.append(quests, {
		GAME_WON: {
			title: "virtual quest to track game won conditions",
			variables: {MONEY: false},
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
		GFWARDROBE: {
			title: "virtual quest for accessing the girlfriend wardrobe",
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
		FUTACOLLAR: {
			title: "virtual quest for finding the ancient futa collar",
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
    GLUTEZON_CHAMP_SUB1: {
			title: "virtual quest to track Glutezon Champ sub quest",
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
		BERTIE_QUEEN_PT2_INFO: {
			title: "virtual quest to track various info sources",
			intro: "",
			middle: "",
			finish: "",
			journalComplete: "HIDDEN",
			journalEntry: "HIDDEN",
		},
	});
}
