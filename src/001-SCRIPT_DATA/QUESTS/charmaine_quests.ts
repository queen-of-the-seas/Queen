namespace App.Data {
	Object.append(quests, {
		CHARMAINE_CUSTOMER_SERVICE: {
			title: "Customer Service - The Saucy Slattern",
			giver: "Charmaine",
			pre: [
				{type: "var:b", scope: {type: "job", id: "CHARMAINE_JOB_HAND"}, name: "UNLOCK", value: true},
				{type: "var:b", scope: {type: "job", id: "CHARMAINE_JOB_BLOW"}, name: "UNLOCK", value: true},
				{type: "var:b", scope: {type: "job", id: "CHARMAINE_JOB_TIT"}, name: "UNLOCK", value: true},
				{type: "var:b", scope: {type: "job", id: "CHARMAINE_JOB_ASS"}, name: "UNLOCK", value: true},
			],
			checks: [{type: "trackCustomers", name: "SaucySlattern", value: 20, condition: "gte"}],
			onAccept: [{type: "trackCustomers", name: "SaucySlattern"}],
			reward: [
				{type: "money", value: 1000},
				{type: "npcStat", name: NpcStat.Mood, value: 15},
				{type: "slot"},
				{
					choice: [
						{type: "item", name: "reel/rareHandmaiden", value: 1},
						{type: "item", name: "reel/rareBreathMint", value: 1},
						{type: "item", name: "reel/rareAnalAngel", value: 1},
						{type: "item", name: "reel/rareBoobjitsuMatriarch", value: 1},
						{type: "item", name: "reel/uncommonConcubine", value: 1},
						{type: "item", name: "reel/legendaryWhore", value: 1},
					],
				},
			],
			intro:
				"NPC_NAME greets you warmly with a smile and says s(Well, if it isn't PLAYER_NAME? The boys here at the Lass sure can't \
        get enough of you, can they?)\n\n\
        She seems to consider something for a moment and then grabs you by the arm, pulling you in close.\n\n\
        s(You've become pretty popular here and I'm thinkin' there's a way we could both benefit,) she says. The tone of her voice \
        makes you sure that any sort of agreement is definitely going to favor her, but still you're intrigued.\n\n\
        NPC_NAME leads you over to the main floor, looking out upon the clientele gathered for the days festivities. There's a literal \
        throbbing mass of horny men and dozens of girls working the crowd, in more ways than one.\n\n\
        s(Tell you what, you work for me for a bit all proper like - fuck my customers, and fuck them //good//. Drive them crazy. Make \
        it so they can't shut up about the whores here at the @@.location-name;Saucy Slattern@@ and I'll give you a cut of the house \
        profits, and maybe a little present. What do you say to that?)\n\n\
        It's an interesting deal, and obviously it's not the first time you've worked for NPC_NAME. You know she pays well and keeps \
        her word."
			,
			middle: "NPC_NAME says s(Hey, PLAYER_NAME, keeping your holes busy I hope? The words starting to get around, just keep at it.)",
			finish:
				"NPC_NAME says s(Nicely done, PLAYER_NAME. Just this morning I heard a chaplain talking to a baker outside his stall about \
        the class of sluts we have here and I'm sure we owe a lot of that to you.)\n\n\
        It's dubious praise, but considering your circumstances, you'll take it.\n\n\
        s(Like I promised, here's your cut of the take, oh and also, I threw in a little something extra that'd be of particular use \
        to a working girl like yourself.)"
			,
			journalEntry: "NPC_NAME has asked you to work at her establishment, whoring for her customers and satisfying them so that \
        they'll spread the word about the experience.",
			journalComplete: "You completed NPC_NAME's dubious task, screwing and sucking your way through her customer base and \
        helping increase the notoriety of the @@.location-name;Saucy Slattern@@. "+
				"NPC_NAME was so grateful she gave you a fair sized cut of the profits and overall increased your whoring ability.",
		},
	});
}
