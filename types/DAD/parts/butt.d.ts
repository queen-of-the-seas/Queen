import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {DrawPoint} from "drawpoint/dist-esm";
import {Player} from "../player/player";

declare class Butt extends BodyPart {
	constructor(...data: object[]);
}


export class ButtHuman extends Butt {
	constructor(...data: object[]);

	getLineWidth(avatar: Player): number;
	calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}
