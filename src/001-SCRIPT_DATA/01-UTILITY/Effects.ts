namespace App {
	export namespace Effects {
		function statEffectFunStat<T extends Stat>(
			human: Entity.IHuman, statType: T, effect: Partial<Record<StatTypeMap[T], Data.StatChangeEffect>>
		) {
			for (const [stat, value] of Object.entries(effect)) {
				if (value.xp) {
					human.adjustXP(statType, stat as StatTypeMap[T], value.xp, value.limiter);
				}
				if (value.value) {
					human.adjustStat(statType, stat as StatTypeMap[T], value.value);
				}
			}
		}

		function statEffectFun(statChanges: Data.EffectStatChanges, human: Entity.IHuman) {
			if (statChanges.core) {
				statEffectFunStat(human, Stat.Core, statChanges.core)
			}
			if (statChanges.body) {
				statEffectFunStat(human, Stat.Body, statChanges.body);
			}
			if (statChanges.skill) {
				statEffectFunStat(human, Stat.Skill, statChanges.skill);
			}
		}

		function makeStatEffectKnowledge(_statChanges: Data.EffectStatChanges): string[] {
			return [];
		}

		export function makeStatEffect(statChanges: Data.EffectStatChanges): Data.EffectDesc {
			return {
				value: statChanges.value,
				fun: h => statEffectFun(statChanges, h),
				knowledge: makeStatEffectKnowledge(statChanges),
			};
		}

		export type TemplateAction = (human: Entity.IHuman, amount: number) => void;
		export type TemplateData = Record<string, {points: number, value?: number, suffixCount: number}>;
		export interface ActionSet {
			knowledge: string;
			knowledgeSuffix: string;
			pointsFactor?: number;
			valueFactor?: number;
			data: TemplateData;
		}
		export function makeEffects(action: TemplateAction, data: Record<string, ActionSet>): Record<string, Data.EffectDesc> {
			const res: Record<string, Data.EffectDesc> = {};
			for (const baseName in data) {
				const set = data[baseName];
				for (const [act, d] of Object.entries(set.data)) {
					res[_.snakeCase(`${baseName}_${act}`).toUpperCase()] = {
						fun: p => action(p, d.points * (set.pointsFactor ?? 1)),
						value: (d.value ?? d.points) * (set.valueFactor ?? 1),
						knowledge: [`${set.knowledge}${(set.knowledgeSuffix ?? "").repeat(d.suffixCount)}`],
					};
				}
			}
			return res;
		}
	}
}
