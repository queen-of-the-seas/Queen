import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPartConstructor} from "./clothing";
import {Sock, SockPart} from "./socks";



export class Garter extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class SuperSockBandPart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class SuperSockPart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class PantyhosePart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class SuperSocks extends Sock {
	constructor(...data: object[]);

	legCoverage: number;
	thickness: number;
	lockGroin: boolean;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Stockings extends Sock {
	constructor(...data: object[]);

	legCoverage: number;
	thickness: number;
	bandWidth: number;
	highlight: string;
	lockGroin: boolean;

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class StockingsGarter extends Sock {
	constructor(...data: object[]);

	legCoverage: number;
	thickness: number;
	bandWidth: number;
	beltWidth: number;
	highlight: string;
	waistCoverage: number;
	lockGroin: boolean;

	fill(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}



export class Pantyhose extends Sock {
	constructor(...data: object[]);

	waistCoverage: number;
	thickness: number;
	open: number;

	fill(): string;
	stroke(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
