import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {BodyPart} from "./part";

declare class RightLegShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class LeftLegShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class ThighShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Leg extends BodyPart {
	constructor(...data: object[]);
}


export class LegHuman extends Leg {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, ignore: object, calculate: boolean): DrawPoint[];
}
