namespace App.UI.Passages {
	class NPCTester extends DOMPassage {
		#dadNpc: App.Entity.DADNPC.Avatar | null;

		constructor() {
			super("NPCTester");
			this.#dadNpc = null; // lazy init
		}

		private get _dadNpc(): App.Entity.DADNPC.Avatar {
			if (!this.#dadNpc) {
				this.#dadNpc = new App.Entity.DADNPC.Avatar();
				this.#dadNpc.init('002');
			}

			return this.#dadNpc;
		}

		public override render(_title: string, content: HTMLElement): DocumentFragment {
			const res = new DocumentFragment();

			const header = appendNewElement('div', res, {classNames: 'npc-workshop-header'});
			const npcWorkshopLabel = appendNewElement('div', header, {content: "NPC Workshop"});
			npcWorkshopLabel.style.fontSize = '18pt';

			const npcList = appendNewElement('div', header);
			npcList.append(this._npcList('obj', "npclist", "LOAD ID"));

			const npcWorkshopMain = appendNewElement('div', res, {classNames: 'npc-workshop-section'});

			const npcRender = appendNewElement('div', npcWorkshopMain);
			npcRender.id = 'npcRender';

			const npcControls = appendNewElement('div', npcWorkshopMain, {classNames: 'npc-controls'});
			npcControls.id = 'npcControls';

			npcControls.append(NPCTester._header("GENERAL CONFIG", "npcGeneralHeader"));

			npcControls.append(this._npcSlider("base_gender", "Gender", 0, 2),
				this._npcSlider("base_fem", "Fem Bias", 0, 11),
				this._npcSlider("basedim_height", "Height", 150, 190),
				this._npcSlider("basedim_upperMuscle", "Muscle", 1, 50),
				this._npcSlider("basedim_breastSize", "Boobs", 0, 50),
				this._npcSlider("basedim_buttFullness", "Ass", 0, 50),
				this._npcSlider("basedim_hipWidth", "Hips", 80, 200),
				this._npcSlider("basedim_waistWidth", "Waist", 70, 300),
				this._npcSlider("basedim_penisSize", "Penis", 0, 200),
				this._npcSlider("basedim_testicleSize", "Balls", 0, 100),
				this._npcSlider("Mods_skinHue", "Skin (H)", 0, 360),
				this._npcSlider("Mods_skinSaturation", "Skin (S)", 0, 100),
				this._npcSlider("Mods_skinLightness", "Skin (L)", -100, 0),
				this._npcSlider("basedim_neckLength", "Neck Length", 60, 90),
				this._npcSlider("basedim_neckWidth", "Neck Width", 35, 60),
				this._npcSlider("basedim_shoulderWidth", "Shoulders", 25, 100),

				NPCTester._header("EQUIPMENT CONFIG", "npcEquipHeader"),

				this._npcList("clothes", ClothingSlot.Mascara),
				this._npcList("clothes", ClothingSlot.Hat),
				this._npcList("clothes", ClothingSlot.Neck),
				this._npcList("clothes", ClothingSlot.Nipples),
				this._npcList("clothes", ClothingSlot.Bra),
				this._npcList("clothes", ClothingSlot.Corset),
				this._npcList("clothes", ClothingSlot.Panty),
				this._npcList("clothes", ClothingSlot.Stockings),
				this._npcList("clothes", ClothingSlot.Shirt),
				this._npcList("clothes", ClothingSlot.Pants),
				this._npcList("clothes", ClothingSlot.Dress),
				this._npcList("clothes", ClothingSlot.Costume),
				this._npcList("clothes", ClothingSlot.Shoes),
				this._npcList("clothes", ClothingSlot.Penis)
			);

			const faceConfig = appendNewElement('div', res, {classNames: 'npc-workshop-section'});
			const divPortrait = appendNewElement('div', faceConfig);
			divPortrait.id = 'npcRenderPortrait';
			divPortrait.style.display = 'inline-block';

			const faceControls = appendNewElement('div', faceConfig, {classNames: 'npc-controls'});
			faceControls.id = 'faceControls';

			faceControls.append(NPCTester._header("FACE CONFIG", 'npcFaceHeader'),

				this._npcSlider("basedim_hairStyle", "Hair Style", 0, 7),
				this._npcSlider("basedim_hairLength", "Hair Length", 0, 200),
				this._npcSlider("basedim_hairHue", "Hair (H)", 0, 360),
				this._npcSlider("basedim_hairSaturation", "Hair (S)", 0, 100),
				this._npcSlider("basedim_hairLightness", "Hair (L)", 0, 100),
				this._npcSlider("basedim_faceFem", "Face Fem", 0, 50),
				this._npcSlider("basedim_faceLength", "Face Length", 180, 300),
				this._npcSlider("basedim_faceWidth", "Face Width", 70, 120),
				this._npcSlider("basedim_chinWidth", "Chin Width", 50, 12),
				this._npcSlider("Mods_jawJut", "Jaw Jut", -10, 10),
				this._npcSlider("basedim_lipSize", "Lip Size", 0, 35),
				this._npcSlider("Mods_lipTopSize", "Lip Top", -100, 100),
				this._npcSlider("Mods_lipBotSize", "Lip Bot", -100, 100),
				this._npcSlider("Mods_lipParting", "Lip Part", 0, 50),
				this._npcSlider("Mods_lipBias", "Lip Bias", -100, 100),
				this._npcSlider("Mods_lipCurl", "Lip Curl", -100, 100),
				this._npcSlider("Mods_lipCupidsBow", "Cupids Bow", -100, 100),
				this._npcSlider("Mods_lipHeight", "Lip Height", -5, 5),
				this._npcSlider("Mods_lipWidth", "Lip Width", -100, 100),
				this._npcSlider("Mods_noseHeight", "Nose Height", -10, 5),
				this._npcSlider("Mods_noseLength", "Nose Length", 0, 50),
				this._npcSlider("Mods_noseRidgeHeight", "Nose Ridge", -12, 12),
				this._npcSlider("Mods_noseRoundness", "Nose Round", -5, 10),
				this._npcSlider("Mods_noseWidth", "Nose Width", 0, 50),
				this._npcSlider("Mods_nostrilSize", "Nostril", 0, 20)
			);

			const eyesConfig = appendNewElement('div', res, {classNames: 'npc-workshop-section'});
			const eyeCanvas = appendNewElement('div', eyesConfig);
			eyeCanvas.id = 'npcRenderPortraitEye';

			const eyeControls = appendNewElement('div', eyesConfig, {classNames: 'npc-controls'});
			eyeControls.id = 'eyeControls';
			eyeControls.append(NPCTester._header("EYE CONFIG", 'npcEyeHeader'),

				this._npcSlider("basedim_eyeSize", "Eye Size", -50, 50),
				this._npcSlider("Mods_eyeBias", "Eye Bias", -10, 10),
				this._npcSlider("Mods_eyeCloseness", "Eye Close", -50, 50),
				this._npcSlider("Mods_eyeHeight", "Eye Height", -10, 0),
				this._npcSlider("Mods_eyeTilt", "Eye Tilt", -50, 50),
				this._npcSlider("Mods_eyeWidth", "Eye Width", -10, 10),
				this._npcSlider("Mods_irisSize", "Iris Size", 1, 50),
				this._npcSlider("Mods_irisHeight", "Iris Height", -20, 20),
				this._npcSlider("Mods_limbalRingSize", "Iris Ring", 1, 40),
				this._npcSlider("Mods_irisHue", "Iris (H)", 0, 360),
				this._npcSlider("Mods_irisSaturation", "Iris (S)", 0, 100),
				this._npcSlider("Mods_irisLightness", "Iris (L)", 0, 100),
				this._npcSlider("Mods_pupilSize", "Pupil Size", 1, 50),
				this._npcSlider("Mods_eyelidHeight", "Lid Height", -10, 10),
				this._npcSlider("Mods_eyelidBias", "Lid Bias", -10, 10),
				this._npcSlider("Mods_eyelashAngle", "Lash Angle", -10, 10),
				this._npcSlider("basedim_eyelashLength", "Lash Length", 0, 10),
				this._npcSlider("Mods_browThickness", "Brow Thick", -10, 10),
				this._npcSlider("Mods_browSharpness", "Brow Sharp", -10, 10),
				this._npcSlider("Mods_browLength", "Brow Length", -10, 10),
				this._npcSlider("Mods_browHeight", "Brow Height", -10, 10),
				this._npcSlider("Mods_browCloseness", "Brow Close", -10, 10)
			);

			const exports = appendNewElement('div', res);
			exports.style.marginTop = "40px";
			exports.append(NPCTester._header("FULL EXPORT DATA", "npcDataHeader"));
			const dataOutput = appendNewElement('textarea', exports, {classNames: 'npcScrollText'});
			dataOutput.id = 'dataOutput';
			exports.append(NPCTester._header("FACE EXPORT DATA", "npcFaceDataHeader"));
			const faceDataOutput = appendNewElement('textarea', exports, {classNames: 'npcScrollText'});
			faceDataOutput.id = 'faceDataOutput';

			const hiddenPortraitCanvas = appendNewElement('div', res);
			hiddenPortraitCanvas.id = "hiddenPortraitCanvas";
			hiddenPortraitCanvas.style.display = "none";

			content.append(res);

			this._dadNpc.drawUI("npcControls");
			this._dadNpc.drawCanvas('npcRender', 560, 266);
			this._dadNpc.drawPortrait('npcRenderPortrait', 266, 266);

			return res;
		}

		private _npcList<T extends "obj" | "clothes">(type: T, attrib: App.Entity.DADNPC.ListItemTypeMap[T], label?: string) {
			const id = this._dadNpc.addList(type, attrib, label ?? attrib.capitalizeFirstLetter());
			const res = makeElement('div', {classNames: ['npcSliderWidget', 'npcDropDown']});
			res.id = id;
			return res;
		}

		private _npcSlider(id: Entity.DADNPC.IDStringSlider, name: string, min: number, max: number, step = 1) {
			const sliderId = this._dadNpc.addSlider(id, name, min, max, step);
			const res = makeElement('div', {classNames: ['npcSliderWidget']});
			res.id = sliderId;
			return res;
		}

		private static _header(label: string, id: string) {
			const npcWorkshopHeader = makeElement('div', {content: label, classNames: 'npcWorkshopHeader'});
			npcWorkshopHeader.id = id;

			const npcExitLink = appendNewElement('span', npcWorkshopHeader, {classNames: 'npcExitLink'});
			npcExitLink.append(passageLink("QUIT", "DebugPassage"));

			return npcWorkshopHeader;
		}
	}

	new NPCTester();
}
