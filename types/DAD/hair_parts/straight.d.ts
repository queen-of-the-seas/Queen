import {HairPart} from "./hair_part";

export class StraightFrontShine extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}


export class StraightFront extends HairPart {
	constructor(...data: object[]);
	stroke(): string;
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}

export class StraightSideBang extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}

export class StraightBack extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}
