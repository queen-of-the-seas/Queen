namespace App.Combat {
	/**
	 * This class is used for spectating and betting on fights.
	 * TODO: Allow the player to input the amount they want to bet.
	 * TODO: Calculate odds of match up and pay out on them.
	 */
	export class SpectatorEngine {
		// initialized by loadEncounter()
		opA!: Combatant;
		#opB!: Combatant;
		#betA!: number;
		#betB!: number;
		#betOn: string | null = null;
		#chatLog: string[] = [];
		#lootBuffer = [];
		#maxBet = 0;
		#club!: string;
		#currentOpponent!: Combatant;

		get opponentA(): Combatant {return this.opA;}
		get opponentB(): Combatant {return this.#opB;}
		get betOn(): string | null {return this.#betOn;}
		get club(): string {return this.#club;}
		get maxBet(): number {return this.#maxBet;}
		get currentOpponent(): Combatant {return this.#currentOpponent;}
		get currentTarget(): Combatant {return this.currentOpponent.id == 'A' ? this.opponentB : this.opponentA;}
		get winner(): string {
			if (!this.opponentA && !this.opponentB) return "no one";
			return this?.opponentA?.isDead ? this.opponentB?.name : this.opponentA?.name;
		}

		get payoutStr(): HTMLSpanElement | null {
			if (this.betOn == null) return null;

			const res = document.createElement('span');
			if (this.didIWin) {
				res.appendTextNode("You win ");
			} else {
				res.appendTextNode("You lose ");
			}
			UI.appendFormattedFragment(res, {text: this.maxBet.toString(), style: 'item-money'});
			res.appendTextNode(" coins!");
			return res;
		}

		loadEncounter(club: string, maxBet: number): void {
			sessionStorage.setItem('QOS_FIGHTCLUB_NAME', club);
			sessionStorage.setItem('QOS_FIGHTCLUB_MAXBET', maxBet.toString());

			this.#betA = 0;
			this.#betB = 0;
			this.#chatLog = [];
			this.#lootBuffer = [];
			this.#club = club;
			this.#maxBet = maxBet;

			this.opA = this._addEnemy(Data.Combat.clubBetData[club].randomElement());
			this.opA.id = 'A';

			// Don't allow the same "name" to fight each other. Useful for unique encounters.
			while (true) {
				this.#opB = this._addEnemy(Data.Combat.clubBetData[club].randomElement());
				this.#opB.id = 'B';
				if (this.opponentA?.name != this.opponentB?.name) break;
			}

			this.#currentOpponent = this.opA;
		}

		drawUI(): void {
			if (this.club == null) { // Reload from session state if browser refreshed.
				this.loadEncounter(sessionStorage.getItem('QOS_FIGHTCLUB_NAME') as string,
					Number.parseInt(sessionStorage.getItem('QOS_FIGHTCLUB_MAXBET') as string));
			}

			$(document).one(":passageend", this._drawUI.bind(this));
		}

		drawResults(): void{
			$(document).one(":passageend", this._drawWinLog.bind(this));
		}

		betLink(target: Combatant): HTMLAnchorElement {
			return UI.passageLink(`Bet ${this.maxBet} coins on  ${target.title}`, 'FightBetOverUI',
				() => {
					setup.world.nextPhase(1);
					setup.spectator.placeBet(target.id);
				});
		}

		watchLink(): HTMLAnchorElement {
			return UI.passageLink(`Just watch`, 'FightBetOverUI',
				() => {
					setup.world.nextPhase(1);
					setup.spectator.spectateFight();
				});
		}

		betALink(): HTMLAnchorElement {
			return this.betLink(this.opponentA);
		}

		betBLink(): HTMLAnchorElement {
			return this.betLink(this.opponentB);
		}

		placeBet(targetId: string): void {
			this.#betOn = targetId;
			this._simulateCombat();
			this._doPayout();
		}

		spectateFight(): void {
			this.#betOn = null;
			this._simulateCombat();
		}

		get didIWin(): boolean {
			const winner = this.opponentA?.isDead ? this.opponentB : this.opponentA;
			return (winner.id == this.betOn);
		}

		private _drawUI(): void {
			this._drawEnemyContainers();
		}

		private _drawEnemyContainers(): void {
			const root = $('#EnemyGUI');
			root.empty();
			SpectatorEngine._drawPortrait(this.opponentA);
			const vs = $('<div>').attr('id', 'versusTxt').addClass('EnemyContainer').text('VS');
			root.append(vs);
			SpectatorEngine._drawPortrait(this.opponentB);
		}

		private static _drawPortrait(fighter: Combatant): void {
			const root = $('#EnemyGUI');
			const container = $('<div>').attr('id', 'EnemyContainer' + fighter.id).addClass('EnemyContainer');
			container.append('<div>').addClass('EnemyTitle').text(fighter.title);
			const frame = $('<div>').attr('id', 'EnemyPortrait' + fighter.id).addClass('EnemyPortrait');
			frame.addClass(fighter.portrait);
			container.append(frame);
			root.append(container);
		}

		private _drawWinLog(): void {
			const root = $('#WinDiv');
			const log = $('<div>').attr('id', 'WinChatLog');
			root.append(log);
			for (let i = 0; i < this.#chatLog.length; i++) {
				log.append("<P class='ChatLog'>" + this.#chatLog[i] + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight") as number);
		}

		/**
		 * Add an enemy object to the encounter.
		 */
		private _addEnemy(enemyId: string): Combatant {
			return new Combat.NPCCombatant(Data.Combat.enemyData[enemyId],
				this._updateNPCStatusCB.bind(this),
				this._updatePlayerStatusCB.bind(this),
				this._chatLogCB.bind(this));
		}

		private static _formatMessage(m: string, o: Combatant) {
			if (!o) {
				return m;
			}

			const npc = setup.world.npc(Entity.NpcId.Dummy); // Fake NPC
			npc.data.name = o.name; // Swapsie name
			// Custom replacer(s)
			/*
			m = m.replace(/(ENEMY_([0-9]+))/g, function (m, f, n) {
				return "<span style='color:cyan'>" + that.#enemies[n].Name + "</span>";
			}); */

			m = m.replace(/NPC_PRONOUN/g, () => o.gender == Gender.Male ? "his" : "her");

			m = PR.tokenizeString(setup.world.pc, npc, m);
			return m;
		}

		/**
		 * Cheap and dirty hack to re-use combat messages.
		 */
		private _substituteYou(m: string, o: Combatant) {
			const target = (o.id === this.opponentA.id ? this.opponentB : this.opponentA);
			m = m.replace(/((\s)(you)([^a-zA-z]))/g, (_m, _p1, p2: string, _p3, p4: string) => p2 + target.name + p4);

			m = m.replace(/^You/g, target.name);
			return m;
		}

		private _writeMessage(m: string, o: Combatant) {
			m = this._substituteYou(m, o);
			m = SpectatorEngine._formatMessage(m, o);
			this.#chatLog.push(m);
		}

		// Dummy callbacks

		private _chatLogCB(m: string, o: Combatant) {
			this._writeMessage(m, o);
		}

		private _updatePlayerStatusCB(_m: Combatant) {
			// no-op
		}

		private _updateNPCStatusCB(_npc: Combatant) {
			// no-op
		}

		private _nextRound() {
			this.#currentOpponent = this.currentOpponent.id == 'A' ? this.opponentB : this.opponentA;
		}

		private _simulateCombat() {
			while (!this.opponentA?.isDead || !this.opponentB?.isDead) {
				this.currentOpponent.startTurn();
				this.currentOpponent.doAI(this.currentTarget, this._chatLogCB.bind(this));
				this.currentOpponent.endTurn();
				this._nextRound();
			}
		}

		private _doPayout() {
			if (this.didIWin) {
				setup.world.pc.earnMoney(this.maxBet, GameState.CommercialActivity.Betting);
			} else {
				setup.world.pc.spendMoney(this.maxBet, GameState.CommercialActivity.Betting);
			}
		}
	}
}
