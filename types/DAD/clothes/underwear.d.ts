import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Player} from "../player/player";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class BraTopStrapPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class BraBotStrapPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class BraPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export function calcBra(ex: DrawingExports): {out: DrawPoint, tip: DrawPoint, top: DrawPoint};

declare class BreastWrapPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D, ignore: object, avatar: Player): void;
}


export class BreastWrapStrapPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D, ignore: object, avatar: Player): void;
}


export class PantiesPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Underwear extends Clothing {
	constructor(...data: object[]);

}


export class Bra extends Underwear {
	constructor(...data: object[]);

	// stroke() {
	//     return "hsl(0,30%,30%)";
	// }

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BreastWrap extends Underwear {
	constructor(...data: object[]);

	chestCoverage: number;
	thickness: number;
	wrapSize: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Panties extends Underwear {
	constructor(...data: object[]);

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
