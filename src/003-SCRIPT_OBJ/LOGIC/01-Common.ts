namespace App {
	export interface DefinitionContext {
		world: Entity.World;
		npc?: Entity.NPC | string;
		/** The task we compute conditions/post for */
		task?: Task;
	}

	function expandNPCTag(ctx: EvaluationContextBase, tag?: string | string[], npcs?: Entity.NPC[]): Entity.NPC[] {
		const res = npcs ?? [];
		if (tag) {
			if (Array.isArray(tag)) {
				for (const t of tag) {
					expandNPCTag(ctx, t, res);
				}
			} else {
				if (tag.startsWith('@')) { // npc group
					res.push(...ctx.world.npcGroup(tag.slice(1)));
				} else {
					res.push(ctx.world.npc(tag));
				}
			}
		}
		return res;
	}

	export class EvaluationContextBase {
		readonly #definition: DefinitionContext;

		constructor(definition: DefinitionContext) {
			this.#definition = definition;
		}

		get definition(): DefinitionContext {
			return this.#definition;
		}

		get world(): Entity.World {
			return this.#definition.world;
		}

		get player(): Entity.Player {
			return this.world.pc;
		}

		quest(id?: string): Quest {
			if (id) {
				return Quest.byId(id);
			} else if (this.#definition.task instanceof Quest) {
				return this.#definition.task;
			} else {
				throw new TypeError("Quest is not defined for quest worker");
			}
		}

		job(id?: string): Job {
			if (id) {
				return Job.byId(id);
			} else if (this.#definition.task instanceof Job) {
				return this.#definition.task;
			} else {
				throw new TypeError("Job is not defined for job worker");
			}
		}

		get task(): Task {
			if (!this.#definition.task) {
				throw new Error("Task is not defined for task worker");
			}
			return this.#definition.task;
		}

		get isTaskDefined(): boolean {
			return this.#definition.task !== undefined;
		}

		npc(tag?: string | string[]): Entity.NPC[] {
			if (tag) {
				return expandNPCTag(this, tag);
			}
			const npc = this.#definition.npc;
			if (npc) {
				return typeof npc === 'string' ?  expandNPCTag(this, npc): [npc];
			}
			if (this.isTaskDefined) {
				const npc = this.task.giver;
				if (npc) {
					return [npc];
				}
			}
			throw new Error("NPC is not defined in the context");
		}

		private _detectDefaultScope(): VariableScope.Defined {
			const task = this.task;
			if (task instanceof App.Job) {
				return {type: "job", id: task.id};
			} else if (task instanceof App.Quest) {
				return {type: "quest", id: task.id};
			} else {
				return {type: "player"};
			}
		}

		definedVariableScope(scope?: VariableScope.Any): VariableScope.Defined {
			return scope && scope.type !== "current" ? scope : this._detectDefaultScope();
		}

		private _gameVariableValue<T extends GameVariable>(name: string, scope: VariableScope.Defined): T {
			switch (scope.type) {
				case "player":
					return (this.player.flags[name] ?? 0) as T;
				case "job":
					return Job.byId(scope.id).variable<T>(this.player, name);
				case "quest":
					return Quest.byId(scope.id).variable<T>(this.player, name);
			}
		}

		gameVariableValue<T extends GameVariable>(name: string, scope?: VariableScope.Any): T {
			return this._gameVariableValue<T>(name, this.definedVariableScope(scope));
		}

		setGameVariableValue(name: string, value: GameVariable, scope: VariableScope.Defined): void {
			switch (scope.type) {
				case "player":
					this.player.flags[name] = value;
					break;
				case "job":
					Job.byId(scope.id).setVariable(this.player, name, value);
					break;
				case "quest":
					Quest.byId(scope.id).setVariable(this.player, name, value);
					break;
			}
		}
	}

	export class EvaluationContext<Worker, WorkerResult> extends EvaluationContextBase {
		readonly #results: Map<Worker, WorkerResult> = new Map();

		store(worker: Worker, value: WorkerResult, overwrite = false) {
			if (!overwrite && this.#results.has(worker)) {
				throw new Error("Overwriting worker results are not allowed");
			}
			this.#results.set(worker, value);
		}

		get<T>(worker: Worker, defaultValue?: T): T {
			const value = this.#results.get(worker) as T|undefined;
			if (value === undefined) {
				if (defaultValue === undefined) {
					throw "No stored value for this action";
				} else {
					return defaultValue;
				}
			}
			return value;
		}
	}
}
