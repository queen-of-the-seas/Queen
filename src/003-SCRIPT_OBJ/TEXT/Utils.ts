namespace App.Text {
	const vowels: Set<string> = new Set(['a', 'e', 'i', 'o', 'u']);

	export function isVowel(ch: string): boolean {
		return vowels.has(ch);
	}

	export function isConsonant(ch: string): boolean {
		return !isVowel(ch);
	}

	const pReplacer = (m: string, cap: string): string => {
		const stat = _.camelCase(cap);
		if (CoreStat.hasKey(stat)) {
			return `[#o.co.${stat}.adj]`;
		}
		if (BodyPart.hasKey(stat)) {
			return `[#o.body.${stat}.adj]`;
		}
		throw new Error(`Could not find a stat for ${m}`);
	};

	export function preprocessBodyRatingString(str: string, bodyPart: BodyPart): string {
		return str
			.replaceAll('ADJECTIVE', `[#o.body.${bodyPart}.adj]`)
			.replaceAll('NOUN', `[#o.body.${bodyPart}.noun]`)
			.replaceAll(/p([A-Z_]+)/g, pReplacer);
	}

	/**
	 * A character description is a short scene with a single actor, the character himself
	 * @param character
	 * @param text
	 */
	export function expandCharacterDescription(character: Entity.Human, text: string): string {
		const sceneData: Data.Text.Scenes.Scene = {
			actors: {},
			entry: 'desc',
			fragments: {
				desc: {
					type: 'text',
					text: text,
				},
			},
		};
		const sceneContext = new SceneContext(setup.world, sceneData);
		sceneContext.addActor('o', character.actor);
		return expandSceneText(text, sceneContext)
	}
}
