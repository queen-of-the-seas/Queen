import {BodyPart} from "./part";
import {DrawPoint} from "drawpoint/dist-esm";

// classic mixin pattern
declare class Feet extends BodyPart {
	constructor(...data: object[]);
}


export class FeetHuman extends Feet {
	constructor(...data: object[]);

	calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}

export class HoofHorse extends Feet {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}
