namespace App.Combat {
	const combatStyleNames: Record<Style, string> = {
		[Style.AssFu]: 'Ass Fu',
		[Style.BoobJitsu]: 'Boob-Jitsu',
		[Style.Boobpire]: 'Boobpirism',
		[Style.Kraken]: 'Kraken',
		[Style.Siren]: 'Siren',
		[Style.Swashbuckling]: 'Swashbuckling',
		[Style.Unarmed]: 'Unarmed',
	};

	/**
	 * Make sure to instantiate this object to a namespace that will not be cleared during
	 * normal play. Prior to drawing the UI with the <<combatUI>> widget, make sure that you
	 * call this.InitializeScene() to clear variables and then this.loadEncounter({}) from the
	 * PREVIOUS passage inside a <<link>> macro, so that the state of this object will not be
	 * overwritten if the player goes into another menu screen while playing.
	 */
	export class CombatEngine {
		#target: Combatant | null = null;
		#encounterData: Data.Combat.EncounterDesc | null = null;
		#enemies: Combatant[] = [];
		#player: Player | null = null;
		#timeline: Combatant[] = [];
		#uiMaxInitiative = 13;
		#hpBars: Record<string, JQuery<HTMLElement>> = {};
		#staminaBars: Record<string, JQuery<HTMLElement>> = {};
		#chatLog: string[] = [];
		#lastSelectedStyle = Style.Unarmed; // Don't overwrite
		#flee = 0;
		#noWeapons = false;
		#fleePassage = 'Cabin';
		#lootBuffer: string[] = [];
		#introChat: string | null = null;

		get lastSelectedStyle(): Style {
			return this.#lastSelectedStyle;
		}

		set lastSelectedStyle(s: Style) {
			this.#lastSelectedStyle = s;
			sessionStorage.setItem('QOS_ENCOUNTER_COMBAT_STYLE', this.#lastSelectedStyle.toString());
		}

		get enemies(): Combatant[] {return this.#enemies;}

		get fatal(): boolean {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.fatal;
		}

		get winPassage(): string {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.winPassage;
		}

		get losePassage(): string {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.losePassage;
		}

		/**
		 * Call before load encounter.
		 * @param options option object
		 */
		initializeScene(options: {flee?: number, fleePassage?: string, noWeapons?: boolean, intro?: string}): void {
			this.#encounterData = null;
			this.#enemies = [];
			this.#player = null;
			this.#timeline = [];
			this.#hpBars = {};
			this.#staminaBars = {};
			this.#chatLog = [];
			this.#lootBuffer = [];
			this.#noWeapons = false;

			if (options) {
				if (options.flee !== undefined) this.#flee = options.flee;
				if (options.fleePassage !== undefined) this.#fleePassage = options.fleePassage;
				if (options.noWeapons !== undefined) this.#noWeapons = options.noWeapons;
				if (options.intro !== undefined) {
					this.#introChat = options.intro;
					sessionStorage.setItem('QOS_ENCOUNTER_INTRO', this.#introChat);
				}
			}

			sessionStorage.setItem('QOS_ENCOUNTER_FLEE', this.#flee.toString());
			sessionStorage.setItem('QOS_ENCOUNTER_FLEE_PASSAGE', this.#fleePassage);
			sessionStorage.setItem('QOS_ENCOUNTER_NO_WEAPONS', this.#noWeapons ? "true" : "false");
		}

		/**
		 * Load an encounter to the engine.
		 * Come back later and serialize the enemy stats and player stats to save to sessionStorage
		 * and then reconstitute them on reload of page.
		 * @param encounter encounter data record
		 */
		loadEncounter(encounter: string): void {
			sessionStorage.setItem('QOS_ENCOUNTER_KEY', encounter);
			const eqData = clone(Data.Combat.encounterData[encounter]);
			if (!eqData) {
				throw "Encounter name invalid";
			}
			this.#encounterData = eqData;
			for (const enemy of eqData.enemies) this._addEnemy(enemy);
			this._addPlayer(setup.world.pc);
			if (this.#introChat && this.#introChat != null && this.#introChat != 'null') {
				this._addChat(this.#introChat);
			} else if (typeof eqData.intro !== "undefined") {
				this._addChat(eqData.intro);
			}
		}

		drawUI(): void {
			// Reconstitute some data from the session storage if the player reloaded the page to be a cheat.
			if (this.#encounterData == null) {
				this.#lastSelectedStyle = Number.parseInt(sessionStorage.getItem('QOS_ENCOUNTER_COMBAT_STYLE') as string);
				this.#flee = Number.parseInt(sessionStorage.getItem('QOS_ENCOUNTER_FLEE') as string);
				this.#fleePassage = sessionStorage.getItem('QOS_ENCOUNTER_FLEE_PASSAGE') as string;
				this.#noWeapons = (sessionStorage.getItem('QOS_ENCOUNTER_NO_WEAPONS') === "true");
				this.loadEncounter(sessionStorage.getItem('QOS_ENCOUNTER_KEY') as string);
				this.#introChat = sessionStorage.getItem('QOS_ENCOUNTER_INTRO');
			}

			$(document).one(":passageend", this._drawUI.bind(this));
		}

		drawResults(): void {
			$(document).one(":passageend", this._drawWinLog.bind(this));
		}

		startCombat(): void {
			this._startRound();
		}

		// UI COMPONENTS

		private _drawUI(): void {
			// Main UI Components.
			this._drawInitiativeBar();
			this._drawEnemyContainers();
			this._drawChatLog();
			this._drawStyles();
			this._drawCombatButtons();

			// Default Control buttons
			this._addDefaultCommands();

			// Status for players.
			this._updatePlayerComboBar();
			this._updatePlayerStaminaBar();
			// Start the fight.
			this._startRound();
		}

		private _drawCombatButtons(): void {
			const root = $('#CombatCommands');
			root.empty();

			const d = this._player.engine.moves;

			for (const prop in d) {
				const m = d[prop];
				const container = $('<div>').addClass("CombatButtonContainer");
				const span = $('<span>').addClass("CombatToolTip").html(
					"<span style='color:yellow'>" + prop + "</span><br>" + m.description);
				const button = $('<div>').attr('id', 'combatButton' + prop.replace(/ /g, '_')).addClass("CombatButton");
				button.addClass(m.icon);
				if (this._player.engine.checkCommand(m)) {
					button.on("click", {cmd: prop}, this._combatCommandHandler.bind(this));
					button.addClass("CombatButtonEnabled");
				} else {
					button.addClass("CombatButtonDisabled");
				}
				container.append(span);
				container.append(button);
				root.append(container);
			}
		}

		private _updateCombatButtons() {
			const d = this._player.engine.moves;
			assertIsDefined(d);

			for (const prop in d) {
				const button = $("#combatButton" + prop.replace(/ /g, '_'));
				button.off('click');
				assertIsDefined(this.#player);
				if (this.#player.engine.checkCommand(d[prop])) {
					button.on("click", {cmd: prop}, this._combatCommandHandler.bind(this));
					button.removeClass("CombatButtonDisabled");
					button.addClass("CombatButtonEnabled");
				} else {
					button.removeClass("CombatButtonEnabled");
					button.addClass("CombatButtonDisabled");
				}
			}
		}

		private _addDefaultCommands() {
			const defendButton = $('#cmdDefend');
			defendButton.on("click", {cmd: 'defend'}, this._combatCommandHandler.bind(this));
			const fleeButton = $('#cmdFlee');
			fleeButton.on("click", {cmd: 'flee'}, this._combatCommandHandler.bind(this));
			const restoreButton = $('#cmdRestoreStamina');
			restoreButton.on("click", {cmd: 'restoreStamina'}, this._combatCommandHandler.bind(this));
		}

		private _drawStyles() {
			const root = $('#combatStyles');
			assertIsDefined(this.#player);
			const styles = this.#player.availableMoveset;
			if (!(styles.has(this.lastSelectedStyle))) {
				this.lastSelectedStyle = Style.Unarmed;
			}

			for (const style of styles.values()) {
				if (this.#noWeapons && style === Style.Swashbuckling) continue; // Don't allow weapon combat.
				const opt = document.createElement('option');
				opt.value = style.toString();
				opt.text = combatStyleNames[style];
				if (style === this.lastSelectedStyle) opt.selected = true;
				root.append(opt);
			}

			root.on("change", this._combatStyleChangeHandler.bind(this));
		}

		private _drawChatLog() {
			const root = $('#EnemyGUI');
			const log = $('<div>').attr('id', 'CombatChatLog');
			root.append(log);
			for (const clm of this.#chatLog) {
				log.append("<p class='ChatLog'>" + clm + "</p>");
			}

			log.scrollTop(log.prop("scrollHeight") as number);
		}

		private _drawWinLog() {
			const root = $('#WinDiv');
			const log = $('<div>').attr('id', 'WinChatLog');
			root.append(log);
			for (const clm of this.#chatLog) {
				log.append("<p class='ChatLog'>" + clm + "</p>");
			}

			log.scrollTop(log.prop("scrollHeight") as number);
		}

		private _drawEnemyContainers() {
			const root = $('#EnemyGUI');
			root.empty();

			for (let i = 0; i < this.#enemies.length; i++) {
				const container = $('<div>').attr('id', `EnemyContainer${i}`).addClass('EnemyContainer');
				container.append('<div>').addClass('EnemyTitle').text(this.#enemies[i].title);
				const frame = $('<div>').attr('id', `EnemyPortrait${i}`).addClass('EnemyPortrait');
				frame.addClass("EnemySelectEnabled");
				frame.addClass(this.#enemies[i].portrait);
				frame.on("click", {index: i}, this._selectEnemyHandler.bind(this));
				container.append(frame);
				root.append(container);
				this._drawStatus(frame, i);
				this._updateHPBar(this.#enemies[i]);
				this._updateStaminaBar(this.#enemies[i]);
				this._updateEnemyPortraits();
			}
		}

		private _drawStatus(container: JQuery<HTMLElement>, enemyIndex: number) {
			const frame = $('<div>').attr('id', `StatusFrame${enemyIndex}`).addClass('EnemyStatusFrame');

			const hp = $('<div>').attr('id', `HpHolder${enemyIndex}`).addClass('EnemyHPHolder');
			const hpBar = $('<div>').attr('id', `EnemyHpBar${enemyIndex}`).addClass('EnemyHpBar');
			const hpCover = $('<div>').attr('id', `EnemyHpBarCover${enemyIndex}`).addClass('EnemyHpBarCover');
			hp.append(hpBar);
			hpBar.append(hpCover);
			frame.append(hp);
			this.#hpBars[this.#enemies[enemyIndex].id] = hpCover;

			const stamina = $('<div>').attr('id', `StaminaHolder${enemyIndex}`).addClass('EnemyStaminaHolder');
			const staminaBar = $('<div>').attr('id', `EnemyStaminaBar${enemyIndex}`).addClass('EnemyStaminaBar');
			const staminaCover = $('<div>').attr('id', `EnemyStaminaBarCover${enemyIndex}`).addClass('EnemyStaminaBarCover');

			stamina.append(staminaBar);
			staminaBar.append(staminaCover);
			frame.append(stamina);
			this.#staminaBars[this.#enemies[enemyIndex].id] = staminaCover;

			container.append(frame);
		}

		private _updateEnemyPortraits() {
			for (let i = 0; i < this.#enemies.length; i++) {
				const frame = $(`#EnemyPortrait${i}`);
				frame.removeClass("ActivePortraitFrame");
				frame.removeClass("InactivePortraitFrame");

				if (this.#enemies[i].isDead) {
					frame.css("opacity", 0.5);
					frame.addClass("InactivePortraitFrame");
					frame.removeClass("EnemySelectEnabled");
					if (this.#enemies[i] === this.#target) this.#target = null;
					frame.off("click");
				} else {
					if (this.#enemies[i] === this.#target) {
						frame.addClass("ActivePortraitFrame");
					} else {
						frame.addClass("InactivePortraitFrame");
					}
				}
			}
		}

		private _updateHPBar(obj: Combatant) {
			const element = this.#hpBars[obj.id];
			const x = Math.ceil(190 * (obj.health / obj.mMaxHealth));
			element.css('width', `${x}px`);
		}

		private _updateStaminaBar(obj: Combatant) {
			const element = this.#staminaBars[obj.id];
			const x = Math.ceil(190 * (obj.stamina / obj.maxStamina));
			element.css('width', `${x}px`);
		}

		private _updatePlayerStaminaBar() {
			assertIsDefined(this.#player);
			const element = $('#PlayerStaminaBar');
			const x = Math.ceil(120 * (this.#player.stamina / this.#player.maxStamina));
			element.css('width', `${x}px`);
		}

		private _updatePlayerComboBar() {
			assertIsDefined(this.#player);
			const element = $('#PlayerComboBar');
			const x = Math.ceil(120 * (this.#player.combo / this.#player.maxCombo));
			element.css('width', `${x}px`);
		}

		private _drawInitiativeBar() {
			this._generateTimeLine(); // Generate data structure

			const root = $('#InitiativeBar');
			root.empty(); // Clear all elements that already exist.

			// Title
			root.append($('<div>').addClass('InitiativePortrait').text('TURN'));

			const max = this.#timeline.length >= this.#uiMaxInitiative ? this.#uiMaxInitiative : this.#timeline.length;
			// Create portrait frames.
			for (let i = 0; i < max; i++) {
				const frame = $('<div>').attr('id', `InitiativePortrait${i}`).addClass('InitiativePortrait');

				frame.addClass(i === 0 ? 'ActiveInitiativeFrame' : 'InactiveInitiativeFrame');
				frame.text(this.#timeline[i].name);
				root.append(frame);
			}
		}
		// OTHER INTERNAL METHODS

		private get _player() {
			assertIsDefined(this.#player);
			return this.#player;
		}

		private _addChat(m: string) {
			m = this._formatMessage(m);
			this.#chatLog.push(m);
		}

		private _formatMessage(m: string, o?: Combatant) {
			const enemy = o ?? this.#enemies[0];

			const npc = setup.world.npc(Entity.NpcId.Dummy); // Fake NPC
			npc.data.name = enemy.name; // Swapsie name
			// Custom replacer(s)
			m = m.replace(/(ENEMY_(\d+))/g,
				(_m, _f, n: string) => `<span class='npc'>${this.#enemies[Number.parseInt(n)].name}</span>`);
			m = m.replace(/NPC_PRONOUN/g,  () => Text.Pronouns.get(enemy.gender).his);

			m = PR.tokenizeString(setup.world.pc, npc, m);
			return m;
		}

		private _writeMessage(m: string, o?: Combatant) {
			m = this._formatMessage(m, o);
			this.#chatLog.push(m);
			const log = $('#CombatChatLog');
			log.append("<P class='ChatLog'>" + m + "</P>");
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			log.animate({scrollTop: log.prop("scrollHeight")}, 500);
		}

		/**
		 * Add an enemy object to the encounter.
		 */
		private _addEnemy(enemyDataId: string) {
			const d = Data.Combat.enemyData[enemyDataId];
			const enemy = new Combat.NPCCombatant(d,
				this._updateNPCStatusCB.bind(this),
				this._updatePlayerStatusCB.bind(this),
				this._chatLogCB.bind(this));
			enemy.id = `ENEMY_${this.#enemies.length}`;
			this.#enemies.push(enemy);
		}

		/**
		 * Add the player to the encounter.
		 */
		private _addPlayer(p: Entity.Player) {
			const data: Data.Combat.Enemy = {
				name: p.name,
				title: 'NAME',
				health: p.stat(Stat.Core, CoreStat.Health),
				maxHealth: 100,
				energy: p.stat(Stat.Core, CoreStat.Energy),
				maxStamina: 100,
				stamina: 100,
				speed: 50,
				moves: Style.Unarmed,
				// FIXME Player should not inherit base enemy
				attack: -1,
				defense: -1,
				gender: Number.NaN,
			};

			this.#player = new Combat.Player(p, data,
				this._updatePlayerStatusCB.bind(this),
				this._updateNPCStatusCB.bind(this),
				this._chatLogCB.bind(this),
				this._loseFight.bind(this),
				this._winFight.bind(this));
			this.#player.id = "PLAYER";

			if (this.#noWeapons && this.lastSelectedStyle === Style.Swashbuckling) {
				this._switchMoveSet(Style.Unarmed);
			} else if (this.lastSelectedStyle === Style.Swashbuckling && !this.#player.isEquipped(ClothingSlot.Weapon)) {
				this._switchMoveSet(Style.Unarmed);
			} else {
				this._switchMoveSet(this.lastSelectedStyle);
			}
		}

		private _switchMoveSet(style: Style) {
			assertIsDefined(this.#player);
			this.#player.changeMoveSet(style, this._updatePlayerStatusCB.bind(this),
				this._updateNPCStatusCB.bind(this),
				this._chatLogCB.bind(this));
			this.lastSelectedStyle = style;
		}

		/**
		 * Creates an array of the predicted order of turns.
		 */
		private _generateTimeLine() {
			assertIsDefined(this.#player);

			this.#timeline = [];
			const timeline: Record<number, Combatant[]> = {};
			CombatEngine._populateTimeline(this.#player, timeline);
			for (let i = 0; i < this.#enemies.length; i++) {
				if (this.#enemies[i].isDead) continue;
				CombatEngine._populateTimeline(this.#enemies[i], timeline);
			}

			// Navigate timeline. Rely on the expected behavior of first set keys being retrieved first.
			// If this doesn't work then get keys as array, sort, navigate that. Dumb but whatever.
			let position = 0;
			for (const property in timeline) {
				if (position >= this.#uiMaxInitiative) break; // Abort out of range of the portraits.

				const array = timeline[property];
				for (let x = 0; x < array.length; x++) {
					if (position >= this.#uiMaxInitiative) break; // Abort nested loop
					this.#timeline.push(timeline[property][x]);
					position++;
				}
			}
		}

		private static _populateTimeline(o: Combatant, timeline: Record<number, Combatant[]>) {
			const time = o.getTimeline(5); // Get 5 turns.
			for (const t of time) {
				if (timeline.hasOwnProperty(t)) {
					timeline[t].push(o);
				} else {
					timeline[t] = [o];
				}
			}
		}

		private _startRound() {
			// Who's turn is it?
			if (this._getCombatant().isNPC) {
				this._enemyTurn();
			} else {
				this._playerTurn();
			}
		}

		private _nextRound() {
			this._drawInitiativeBar();
			this._startRound();
		}

		private _getCombatant() {
			return this.#timeline[0];
		}

		private _enemyTurn() {
			assertIsDefined(this.#player);

			this._getCombatant().startTurn();
			this._getCombatant().doAI(this.#player, this._chatLogCB.bind(this));
			this._getCombatant().endTurn();

			if (!this.#player.isDead)
				setTimeout(this._nextRound.bind(this), 500); // Don't attack if PC is defeated.
		}

		private _playerTurn() {
			assertIsDefined(this.#player);

			if (this.#player.isDead) return; // Don't do anything if defeated.

			if (this.#player.hasEffect(Effect.Stunned)) {
				this._writeMessage("<span style='color:red'>You are stunned!</span>", this.#player);
				this._getCombatant().startTurn();
				this._getCombatant().endTurn();
				this._nextRound();
			}

			if (!this.#enemies.some(o => !o.isDead)) {
				this._writeMessage("You have defeated all your foes!", this.#player);
				setTimeout(this._winFight.bind(this), 500);
			}
			// this._WriteMessage("It is your turn!", this.#player);
		}

		// CALLBACKS

		private _combatCommandHandler(event: JQuery.ClickEvent<unknown, {cmd: string}>) {
			if (this._getCombatant() !== this.#player) return;

			if (event.data.cmd === 'flee') {
				if (this.#flee === 0) {
					this._writeMessage("<span style='color:red'>You cannot flee!</span>", this.#player);
					return;
				}
				if (Math.floor(State.random() * 100) > this.#flee) {
					this._writeMessage("<span style='color:red'>You attempt to flee, but fail!</span>", this.#player);
					this._getCombatant().startTurn();
					this._getCombatant().addWeaponDelay(10);
					this._getCombatant().endTurn();
					this._nextRound();
					return;
				} else {
					assertIsDefined(this.#encounterData);
					this.#encounterData.fleeHandler?.(this.#player.player, this.#encounterData);
					Engine.play(this.#fleePassage);
				}
				return;
			}

			if (event.data.cmd === 'defend') {
				this._getCombatant().startTurn();
				this._getCombatant().engine.defend();
				this._getCombatant().endTurn();
				this._nextRound();
				return;
			}

			if (event.data.cmd === 'restoreStamina') {
				if (setup.world.pc.coreStats.energy <= 0) {
					this._writeMessage("<span style='color:red'>You do not have enough energy!</span>", this.#player);
					return;
				} else {
					this._getCombatant().startTurn();
					this._getCombatant().engine.recover();
					this._getCombatant().endTurn();
					this._nextRound();
					return;
				}
			}

			if (this.#target == null) {
				this._writeMessage("<span style='color:red'>Select a target first!</span>");
				return;
			}

			this._getCombatant().startTurn();
			this._getCombatant().engine.attackTarget(this.#target, event.data.cmd);
			this._getCombatant().endTurn();
			this._drawInitiativeBar();
			this._startRound();
		}

		private _combatStyleChangeHandler() {
			const list = $('#combatStyles');
			const style = Number.parseInt(list.val() as string) as Style;
			this._switchMoveSet(style);
			this._drawCombatButtons();
		}

		private _selectEnemyHandler(event: JQuery.ClickEvent<any, {index: number}>) {
			const obj = this.#enemies[event.data.index];
			if (!obj.isDead) {
				this.#target = obj;
			}

			this._updateEnemyPortraits();
		}

		/**
		 * call back for writing to the chat log window
		 */
		private _chatLogCB(m: string, o: Combatant) {
			this._writeMessage(m, o);
		}

		private _updatePlayerStatusCB() {
			this._updatePlayerComboBar();
			this._updatePlayerStaminaBar();
			this._updateCombatButtons();
		}

		private _updateNPCStatusCB(npc: Combatant) {
			this._updateHPBar(npc);
			this._updateStaminaBar(npc);
			this._updateEnemyPortraits();
		}

		private _loseFight(player: Entity.Player) {
			assertIsDefined(this.#encounterData);
			const lh = this.#encounterData.loseHandler;
			if (lh)
				lh(player, this.#encounterData);

			if (this.fatal) {
				// you died.
				setup.eventEngine.passageOverride = this.losePassage;
			} else {
				player.coreStats.health = 10; // Don't kill them…
			}
			Engine.play(this.losePassage);
		}

		private _winFight() {
			const loot = this.#encounterData?.loot;
			if (loot !== undefined) {
				// Generate loot.
				for (const l of loot) {
					this._grantLoot(l);
				}

				if (this.#lootBuffer.length > 0) {
					const lootMessage = this.#encounterData?.lootMessage;
					if (lootMessage !== undefined) {
						this._addChat("<span style='color:yellow'>" + lootMessage + "</span>");
					} else {
						this._addChat("<span style='color:yellow'>You find some loot…</span>")
					}

					for (const l of this.#lootBuffer)
						this._addChat("<span style='color:yellow'>&check; </span>" + l);
				}
			}

			if (this.#encounterData?.winHandler !== undefined)
				this.#encounterData.winHandler(setup.world.pc, this.#encounterData);

			Engine.play(this.winPassage);
		}

		private _grantLoot(data: Data.Combat.EncounterLoots.Any) {
			if (data.chance === 100 || data.chance <= Math.floor(State.random() * 100)) {
				const amt = random(data.min, data.max);
				switch (data.type) {
					case 'Coins':
						this._player.player.earnMoney(amt, GameState.IncomeSource.Loot);
						this.#lootBuffer.push(`${amt} coins`);
						break;
					case 'Random': {
						const item = Items.pickItem([Items.Category.Food, Items.Category.Drugs, Items.Category.Cosmetics], {price: amt});
						if (item != null) {
							this._player.player.addItem(item.cat, item.tag, 0);
							this.#lootBuffer.push(item.desc);
						}
					}
						break;
					default: {
						// Specific loot.
						const item = this._player.player.addItem(data.type, data.tag, amt);
						if (item) {
							if (amt < 2) {
								this.#lootBuffer.push(item.description);
							} else {
								this.#lootBuffer.push(`${item.description} × ${amt}`);
							}
						}
					}
						break;
				}
			}
		}

		// Somewhat hackish stuff to support fight club
	}
}
