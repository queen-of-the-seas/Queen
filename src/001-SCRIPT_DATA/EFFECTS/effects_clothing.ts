namespace App.Data {
	/** CLOTHING PASSIVE WORN EFFECTS **/
	Object.append(App.Data.effectLibraryEquipment.wear, {
		//======================================
		FEMININE_CLOTHING: {
			fun: (p, o) => {
				let gain = 0;
				let limit = 0;
				switch (o.rarity) {
					case Items.Rarity.Common: gain = 5; limit = 30; break;
					case Items.Rarity.Uncommon: gain = 10; limit = 60; break;
					case Items.Rarity.Rare: gain = 15; limit = 80; break;
					case Items.Rarity.Legendary: gain = 20; limit = 0; break;
				}
				switch (o.equipmentType) {
					case Items.ClothingType.Accessory: gain = Math.ceil(gain * 0.8); break;
					case Items.ClothingType.OnePiece: gain = Math.ceil(gain * 2.2); break;
				}
				p.adjustCoreStatXP(CoreStat.Femininity, gain, limit);
			},
			value: 50,
			knowledge: ["Feminine ClothingRANK"],
		},
		SEXY_CLOTHING: {
			fun: (p, o) => {
				let gain = 0;
				let limit = 0;
				switch (o.rarity) {
					case Items.Rarity.Common: gain = 5; limit = 30; break;
					case Items.Rarity.Uncommon: gain = 10; limit = 60; break;
					case Items.Rarity.Rare: gain = 15; limit = 80; break;
					case Items.Rarity.Legendary: gain = 20; limit = 0; break;
				}
				switch (o.equipmentType) {
					case Items.ClothingType.Accessory: gain = Math.ceil(gain * 0.8); break;
					case Items.ClothingType.OnePiece: gain = Math.ceil(gain * 2.2); break;
				}
				p.adjustCoreStatXP(CoreStat.Femininity, gain, limit);
				p.adjustSkillXP(Skills.Charisma.Seduction, gain, limit);
			},
			value: 50,
			knowledge: ["Sexy ClothingRANK"],
		},
		PERVERTED_CLOTHING: {
			fun: (p, o) => {
				let gain = 0;
				let limit = 0;
				switch (o.rarity) {
					case Items.Rarity.Common: gain = 5; limit = 30; break;
					case Items.Rarity.Uncommon: gain = 10; limit = 60; break;
					case Items.Rarity.Rare: gain = 15; limit = 80; break;
					case Items.Rarity.Legendary: gain = 20; limit = 0; break;
				}
				switch (o.equipmentType) {
					case Items.ClothingType.Accessory: gain = Math.ceil(gain * 0.8); break;
					case Items.ClothingType.OnePiece: gain = Math.ceil(gain * 2.2); break;
				}
				p.adjustCoreStatXP(CoreStat.Perversion, gain, limit);
			},
			value: 100,
			knowledge: ["Pervert!RANK"],
		},
		KINKY_CLOTHING: {
			fun: (p, o) => {
				let gain = 0;
				let limit = 0;
				switch (o.rarity) {
					case Items.Rarity.Common: gain = 5; limit = 15; break;
					case Items.Rarity.Uncommon: gain = 10; limit = 30; break;
					case Items.Rarity.Rare: gain = 15; limit = 40; break;
					case Items.Rarity.Legendary: gain = 20; limit = 60; break;
				}
				switch (o.equipmentType) {
					case Items.ClothingType.Accessory: gain = Math.ceil(gain * 0.8); break;
					case Items.ClothingType.OnePiece: gain = Math.ceil(gain * 2.2); break;
				}
				p.adjustCoreStatXP(CoreStat.Perversion, gain, limit);
			},
			value: 100,
			knowledge: ["KinkyRANK"],
		},
		SLAVE_COLLAR: {
			fun: (p) => {p.adjustCoreStatXP(CoreStat.Willpower, -20, 50); p.adjustCoreStatXP(CoreStat.Perversion, 20, 50);},
			value: 0,
			knowledge: ["Slave Breaking--", "Kinky+"],
		},
		WAIST_CINCHING: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, -20, 30);},
			value: 100,
			knowledge: ["Waist Training+"],
		},
		WAIST_TRAINING: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, -20, 20);},
			value: 100,
			knowledge: ["Waist Training++"],
		},
		EXTREME_WAIST_TRAINING: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, -20, 10);},
			value: 250,
			knowledge: ["Waist Training++++"],
		},
		GAPE_TRAINING: {
			fun: (p) => {p.adjustSkillXP(Skills.Sexual.AssFucking, 20, 80);},
			value: 200,
			knowledge: ["Gape Training++"],
		},
		CHASTITY_CAGE: {
			fun: (p) => {
				p.adjustBodyXP(BodyPart.Penis, -20, 20);
				p.adjustBodyXP(BodyPart.Balls, -20, 10);
				p.adjustCoreStatXP(CoreStat.Perversion, 20, 70);
			},
			value: 0,
			knowledge: ["Ball Breaking--", "Kinky++"],
		},
	});

	//======================================
	/** CLOTHING ACTIVE WORN EFFECTS **/
	//======================================
	const skillInSet = (s: WearSkill, ...skills: WearSkill[]): boolean => {
		return skills.includes(s);
	}

	Object.append(App.Data.effectLibraryEquipment.active, {
		SUCCUBUS_ALLURE: {
			fun: s => s === Skills.Charisma.Seduction ? 20 : 0,
			value: 200,
			knowledge: ["Allure of the Succubus++++"],
		},
		FLIRTY: {
			fun: s => s === Skills.Charisma.Seduction ? 10 : 0,
			value: 100,
			knowledge: ["Flirty++"],
		},
		MINOR_PIRATES_GRACE: {
			fun: s => skillInSet(s, Skills.Piracy.Navigating, Skills.Piracy.Sailing) ? 10 : 0,
			value: 200,
			knowledge: ["Pirate's Grace++"],
		},
		MAJOR_PIRATES_GRACE: {
			fun: s => skillInSet(s, Skills.Piracy.Navigating, Skills.Piracy.Sailing, Skills.Piracy.Swashbuckling) ? 20 : 0,
			value: 500,
			knowledge: ["Pirate's Grace++++"],
		},
		CUT_THROAT: {
			fun: s => s === Skills.Piracy.Swashbuckling ? 10 : 0,
			value: 200,
			knowledge: ["Cut Throat++"],
		},
		MINOR_STRIPPERS_ALLURE: {
			fun: s => skillInSet(s, CoreStat.Fitness, Skills.Charisma.Dancing, Skills.Charisma.Seduction) ? 10 : 0,
			value: 300,
			knowledge: ["Stripper's Allure++"],
		},
		MAJOR_STRIPPERS_ALLURE: {
			fun: s => skillInSet(s, CoreStat.Fitness, Skills.Charisma.Dancing, Skills.Charisma.Seduction) ? 15 : 0,
			value: 500,
			knowledge: ["Stripper's Allure+++"],
		},
		GREATER_STRIPPERS_ALLURE: {
			fun: s => skillInSet(s, CoreStat.Fitness, Skills.Charisma.Dancing, Skills.Charisma.Seduction) ? 20 : 0,
			value: 800,
			knowledge: ["Stripper's Allure++++"],
		},
		FANCY_MOVES: {
			fun: s => s === Skills.Charisma.Dancing ? 10 : 0,
			value: 200,
			knowledge: ["Fancy Moves++"],
		},
		REALLY_FANCY_MOVES: {
			fun: s => skillInSet(s, Skills.Charisma.Dancing, Skills.Charisma.Seduction) ? 15 : 0,
			value: 600,
			knowledge: ["Fancy Moves+++"],
		},
		MAIDS_PROWESS: {
			fun: s => skillInSet(s, Skills.Domestic.Cooking, Skills.Domestic.Cleaning, Skills.Domestic.Serving) ? 10 : 0,
			value: 300,
			knowledge: ["Maid's Prowess++"],
		},

		DAMAGE_RESIST_MINOR: {
			fun: s => s === 'damageResistance' ? 5 : 0,
			value: 500,
			knowledge: ["Damage Resistance+"],
		},

		DAMAGE_RESIST_MAJOR: {
			fun: s => s === 'damageResistance' ? 10 : 0,
			value: 1200,
			knowledge: ["Damage Resistance++"],
		},

		SHARP_BLADE_COMMON: {
			fun: s => s === 'sharpness' ? 1 : 0,
			value: 250,
			knowledge: ["Sharpness+"],
		},

		SHARP_BLADE_UNCOMMON: {
			fun: s => s === 'sharpness' ? 2 : 0,
			value: 500,
			knowledge: ["Sharpness++"],
		},

		SHARP_BLADE_RARE: {
			fun: s => s === 'sharpness' ? 3 : 0,
			value: 750,
			knowledge: ["Sharpness+++"],
		},

		SHARP_BLADE_LEGENDARY: {
			fun: s => s === 'sharpness' ? 4 : 0,
			value: 750,
			knowledge: ["Sharpness++++"],
		},

		PUSH_UP_BRA: {
			fun: s => s === BodyProperty.BustFirmness ? 50 : 0,
			value: 10,
			knowledge: ["Breast Perkiness++"]
		}
	});
}
