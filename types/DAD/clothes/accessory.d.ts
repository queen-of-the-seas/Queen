import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";


export class FaceAccessoryPart extends ClothingPart {
	constructor(...data: object[]);
}

export class GagStrapPart extends FaceAccessoryPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class TopTriangularStrap extends FaceAccessoryPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class RingGagPart extends FaceAccessoryPart {
	constructor(...data: object[]);

	ringStroke: string;
	ringThickness: number;
	spiderLegs: boolean;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class BallGagPart extends FaceAccessoryPart {
	constructor(...data: object[]);

	ballFill: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class BlindFoldPart extends FaceAccessoryPart {
	constructor(...data: object[]);

	width: number;
	splitAlongSkull: number;
	fromSkullDistance: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class GlassesPart extends FaceAccessoryPart {
	constructor(...data: object[]);

	eccentricity: number;
	height: number;
	thickness: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class SimpleBeltPart extends ClothingPart {
	constructor(...data: object[]);

	waistCoverage: number;
	beltWidth: number;
	beltCurve: number;
	highlight: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export function calcBelt(ex: DrawingExports): DrawPoint[];

export class ApronPart extends ClothingPart {
	constructor(...data: object[]);

	waistCoverage: number;
	beltWidth: number;
	beltCurve: number;
	coverage: number;
	length: number;
	curveX: number;
	curveY: number;
	highlight: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Accessory extends Clothing {
	constructor(...data: object[]);
}


/**
 * Concrete Clothing classes
 */
export class Glasses extends Accessory {
	constructor(...data: object[]);

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class Gag extends Accessory {
	constructor(...data: object[]);
	/**
	 * How much the gag forces the lips to part, similar in effect to some facial expressions
	 */
	lipParting: number;
	/**
	 * Controls the width of the straps
	 */
	thickness: number;

	fill(): string;
	stroke(): string;
}

export class SimpleRingGag extends Gag {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class SimpleBallGag extends Gag {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class MediumRingGag extends Gag {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class MediumBallGag extends Gag {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BlindFold extends Accessory {
	constructor(...data: object[]);

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class SimpleBelt extends Accessory {
	constructor(...data: object[]);

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class Apron extends Accessory {
	constructor(...data: object[]);

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
