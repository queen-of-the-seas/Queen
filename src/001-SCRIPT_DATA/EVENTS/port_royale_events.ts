namespace App.Data {
	Object.append(events, {
		PR_BackAlley: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.stat(Stat.Body, BodyPart.Bust) >= 50 && State.random() < 0.33,
			},
		],

		PR_DarkAlley: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.stat(Stat.Body, BodyPart.Bust) >= 50 && State.random() < 0.33,
			},
			{
				id: 'LevantBoobpireAttack',
				from: 'PR_Levant',
				force: true,
				maxRepeat: 1,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'LevantBoobpireEvent',
				check: player => Quest.isCompleted(player, "BERTIE_QUEEN_PT2_INFO_SOLENN"),
			},
		],

		EntertainmentDistrict: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.stat(Stat.Body, BodyPart.Bust) >= 50 && State.random() < 0.33,
			},
		],

		WealthyDistrict: [
			{
				id: 'CourtesanQuestEvent',
				from: 'CourtesansGuild',
				maxRepeat: 1,
				minDay: 0,
				maxDay: 0,
				cool: 1,
				phase: [0, 1, 2, 3, 4],
				passage: 'CourtesanQuestEvent',
				check: player => Quest.isActive(player, "COURTESAN_GUILD_JOIN"),
			},
		],

		// Queen favor part 2 ambush
		PortRoyale: [
			{
				id: 'QueenFavorAmbushEvent',
				from: 'Deck',
				maxRepeat: 1,
				minDay: 0,
				maxDay: 0,
				cool: 1,
				phase: [0, 1, 2, 3, 4],
				passage: 'QueenFavorAmbushEvent',
				check: player => Quest.isActive(player, "BERTIE_QUEEN_PT2_DELIVERY"),
			},
		],
	});
}
