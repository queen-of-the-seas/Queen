namespace App {
	export namespace Entity {
		export interface IHuman {

			coreStats: Record<CoreStat, number>;
			coreStatsXP: Record<CoreStat, number>;

			bodyStats: Record<BodyStat, number>;
			bodyXP: Record<BodyStat, number>;

			skills: Record<Skills.Any, number>;
			skillsXP: Record<Skills.Any, number>;

			hairColor: Data.HairColor;
			faceData: DaBodyPreset;

			/**
			 * Return a statistic value (raw)
			 */
			stat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number;
			stat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T]): number;
			stat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T]): number;

			statPercent<T extends StatTypeStr>(type: T, statName: StatTypeStrMap[T], statValue?: number): number;
			statPercent<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number;

			adjustStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void;
			adjustStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number): void;

			adjustCoreStat(statName: CoreStat, amount: number): void;
			adjustCoreStat(statName: CoreStatStr, amount: number): void;

			adjustBody(statName: BodyStat, amount: number): void;
			adjustBody(statName: BodyStatStr, amount: number): void;

			/**
			 * Returns the current XP of a statistic.
			 */
			statXP<T extends keyof StatTypeMap, S extends StatTypeMap[T]>(type: T, statName: S): number;
			statXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): number;
			statXP<T extends keyof StatTypeStrMap, S extends StatTypeStrMap[T]>(type: T, statName: S): number;

			adjustXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number, limiter?: number, noCap?: boolean): void;
			adjustXP<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number, limiter?: number, moCap?: boolean): void;

			/** Used to directly set XP to a specific amount. */
			setXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void;
			setXP<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number): void;

			adjustCoreStatXP(statName: CoreStatStr, amount: number, limiter?: number): void;

			adjustBodyXP(statName: BodyStatStr, amount: number, limiter?: number): void;

			adjustSkillXP(statName: Skills.Any, amount: number, limiter?: number): void;

			/**
			 * Create and add an item to the player.
			 */
			addItem(category: Items.CategoryEquipment, name: string, unused: undefined, opt?: "wear"): Items.Clothing | null;
			addItem<T extends keyof Items.InventoryItemTypeMap>(category: T, name: string, count?: number): Items.InventoryItemTypeMap[T];
			addItem<T extends keyof Items.ItemTypeMap>(category: T, name: string, count?: number, opt?: "wear"): Items.ItemTypeMap[T] | null;

			flags: GameState.GameVariableMap;
		}
	}
}
