import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

/**
 * ClothingPart drawn classes/components
 */
export class NecktiePart extends ClothingPart {
	constructor(...data: object[]);
	/**
	 * Positional offset of the top of the knot relative to the center of the neck
	 */
	offsetWidth: number;
	offsetHeight: number;
	knotWidth: number;
	knotHeight: number;
	loopWidth: number;
	// top part of tongue's width
	tongueTopWidth: number;
	// widest part (at the bottom) of the tongue's width
	tongueBotWidth: number;
	// total length of tongue
	tongueHeight: number;
	// from the bottom, where the widest part occurs
	tongueBotHeight: number;
	// rotation of the tip of the tongue
	tongueRotation: number;


	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class NeckAccessory extends Clothing {
	constructor(...data: object[]);
}


/**
 * Concrete Clothing classes
 */
export class NeckTie extends NeckAccessory {
	constructor(...data: object[]);

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
