import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class UndershirtStrapPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class UndershirtBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class UndershirtChestPart extends ClothingPart {
	constructor(...data: object[]);

    /**
     * Draw an undershirt base. Start in the center of the chest, then to the armpit, down to the hip
     * and through to the pelvis. and back up to the center of the chest. Only stroke the outer lines.
     */
	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Base Clothing classes
 */
export class QueenTops extends Clothing {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
}

export class QueenUndershirt extends QueenTops {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
