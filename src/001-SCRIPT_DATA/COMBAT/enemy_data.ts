namespace App.Data.Combat {
	function withName(type: string): string {
		return `${type} NAME`;
	}

	// Bare knuckled fighters
	enemyData['Weak Pugilist'] = {
		name: 'RANDOM_MALE_NAME',
		title: withName('Fighter'),
		health: 60,
		maxHealth: 60,
		energy: 3,
		attack: 60,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c'],
	}

	enemyData['Pugilist'] = {
		name: 'RANDOM_MALE_NAME',
		title: 'Brawler NAME',
		health: 80,
		maxHealth: 80,
		energy: 3,
		attack: 80,
		defense: 40,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c'],
	};

	enemyData['Champion Pugilist'] = {
		name: 'RANDOM_MALE_NAME',
		title: 'Master NAME',
		health: 100,
		maxHealth: 100,
		energy: 3,
		attack: 100,
		defense: 50,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['pugilist_a', 'pugilist_b', 'pugilist_c'],
	};

	// Pirates
	enemyData['Weak Pirate'] = {
		name: 'RANDOM_MALE_NAME',
		title: 'Squabby NAME',
		health: 50,
		maxHealth: 50,
		energy: 3,
		attack: 30,
		defense: 20,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Swashbuckling,
		gender: 1,
		portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d'],
	}

	enemyData['Pirate'] = {
		name: 'RANDOM_MALE_NAME',
		title: 'Pirate NAME',
		health: 70,
		maxHealth: 70,
		energy: 3,
		attack: 50,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Swashbuckling,
		gender: 1,
		portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d'],
	}

	enemyData['Champion Pirate'] = {
		name: 'RANDOM_MALE_NAME',
		title: 'Buccaneer NAME',
		health: 90,
		maxHealth: 90,
		energy: 3,
		attack: 70,
		defense: 40,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Swashbuckling,
		gender: 1,
		portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d'],
	}

	// Mamazon
	enemyData['Weak Mamazon'] = {
		name: 'RANDOM_FEMALE_NAME',
		title: 'Mamazon NAME',
		health: 60,
		maxHealth: 60,
		energy: 3,
		attack: 60,
		bust: 30,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.BoobJitsu,
		gender: 0,
		portraits: ['mamazon_a', 'mamazon_b', 'mamazon_c'],
	}

	enemyData['Mamazon'] = {
		name: 'RANDOM_FEMALE_NAME',
		title: 'Mamazon NAME',
		health: 80,
		maxHealth: 80,
		energy: 3,
		attack: 80,
		bust: 50,
		defense: 40,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.BoobJitsu,
		gender: 0,
		portraits: ['mamazon_a', 'mamazon_b', 'mamazon_c'],
	}

	enemyData['Champion Mamazon'] = {
		name: 'RANDOM_FEMALE_NAME',
		title: 'Mamazon NAME',
		health: 100,
		maxHealth: 100,
		energy: 3,
		attack: 100,
		bust: 70,
		defense: 50,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.BoobJitsu,
		gender: 0,
		portraits: ['mamazon_a', 'mamazon_b', 'mamazon_c'],
	}

	// Gluteus Tribe
	enemyData['Weak Glutezon'] = {
		name: 'RANDOM_FEMALE_NAME',
		title: 'Glutezon NAME',
		health: 60,
		maxHealth: 60,
		energy: 3,
		attack: 60,
		ass: 30,
		defense: 30,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.AssFu,
		gender: 0,
		portraits: ['gluteus_tribe_a', 'gluteus_tribe_b', 'gluteus_tribe_c'],
	}

	enemyData['Glutezon'] = {
		name: 'RANDOM_FEMALE_NAME',
		title: 'Glutezon NAME',
		health: 80,
		maxHealth: 80,
		energy: 3,
		attack: 80,
		ass: 50,
		defense: 40,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.AssFu,
		gender: 0,
		portraits: ['gluteus_tribe_a', 'gluteus_tribe_b', 'gluteus_tribe_c'],
	}

	enemyData['Champion Glutezon'] = {
		name: 'RANDOM_FEMALE_NAME',
		title: 'Glutezon NAME',
		health: 100,
		maxHealth: 100,
		energy: 3,
		attack: 100,
		ass: 70,
		defense: 50,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.AssFu,
		gender: 0,
		portraits: ['gluteus_tribe_a', 'gluteus_tribe_b', 'gluteus_tribe_c'],
	}

	// Kraken
	enemyData['Kraken Tentacle'] = {
		name: 'Tentacle',
		title: 'Kraken NAME',
		health: 50,
		maxHealth: 50,
		energy: 3,
		attack: 60,
		defense: 20,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Kraken,
		gender: 1,
		portraits: ['kraken_a'],
	};

	enemyData['Kipler'] = {
		name: 'Kipler',
		title: 'First Mate NAME',
		health: 250,
		maxHealth: 250,
		energy: 5,
		attack: 130,
		defense: 50,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Swashbuckling,
		gender: 1,
		portraits: ['kipler_combat'],
	}

	enemyData['Kipler Unarmed'] = {
		name: 'Kipler',
		title: 'First Mate NAME',
		health: 250,
		maxHealth: 250,
		energy: 5,
		attack: 130,
		defense: 50,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Unarmed,
		gender: 1,
		portraits: ['kipler_combat'],
	}

	// Siren

	enemyData['Siren'] = {
		name: 'Siren',
		title: 'Enchanting NAME',
		health: 140,
		maxHealth: 140,
		energy: 3,
		attack: 70,
		defense: 40,
		maxStamina: 150,
		stamina: 150,
		speed: 50,
		moves: App.Combat.Style.Siren,
		gender: 0,
		portraits: ['siren_a'],
	}

	enemyData['Champion Siren'] = {
		name: 'Siren',
		title: 'Black Witch NAME',
		health: 200,
		maxHealth: 200,
		energy: 3,
		attack: 100,
		defense: 50,
		maxStamina: 200,
		stamina: 200,
		speed: 50,
		moves: App.Combat.Style.Siren,
		gender: 0,
		portraits: ['siren_a'],
	}

	enemyData['Boobpire'] = {
		name: 'Boobpire',
		title: 'NAME',
		health: 150,
		maxHealth: 150,
		energy: 3,
		attack: 100,
		defense: 50,
		maxStamina: 200,
		stamina: 200,
		speed: 50,
		moves: App.Combat.Style.Boobpire,
		gender: 0,
		portraits: ['harlot_a', 'harlot_b'],
	}

	// Bertie's Part 2 Quest
	enemyData['HoodedAssassain'] = {
		name: 'Assassain',
		title: 'Hooded NAME',
		health: 40,
		maxHealth: 40,
		energy: 3,
		attack: 30,
		defense: 20,
		maxStamina: 100,
		stamina: 100,
		speed: 50,
		moves: App.Combat.Style.Swashbuckling,
		gender: 1,
		portraits: ['pirate_a', 'pirate_b', 'pirate_c', 'pirate_d'],
	}
}
