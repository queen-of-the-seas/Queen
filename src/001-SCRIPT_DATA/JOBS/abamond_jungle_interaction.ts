namespace App.Data {
	Object.append(jobs, {
		JUNGLE: {
			title: "Explore the Jungle", tags: ["location:JUNGLE"],
			rating: 1, // of 5
			// Can't do late at night
			phases: [0, 1, 2, 3],
			// Can do as many times as wanted in day.
			days: 0,
			cost: [
				{type: "time", value: 1},
				{type: Stat.Core, name: "energy", value: 2},
			],
			variables: {
				COVE_FOUND: false,
				RUINS_FOUND: false,
				COUNTER: 0,
				REWARD: false,
			},
			requirements: [
				{
					op: '||',
					altTitle: "findings",
					args: [
						{type: "var:b", name: "COVE_FOUND", value: false},
						{type: "var:b", name: "RUINS_FOUND", value: false},
					],
				},
				// Check to make sure player is equipped the worn machete
				{type: "equipped", name: "clothes/worn machete", value: true},
			],
			intro:
				// This is run from a room, so no NPC teaser text.
				"",
			start:
				"You pull out your trusty @@.item-quest;worn machete@@ and begin hacking away at the underbrush.",
			scenes: [
				{
					// Just check the players navigation skill and store the result.
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Piracy.Navigating, value: 50}],
					text: [
						{A: 50, text: "You spend what seems like hours traipsing through the jungle, @@not really sure@@ which way you are heading."},
						{A: 99, text: "You do your best to clear a straight path, trying to follow the sun, however, your managed to @@get turned around someplace@@… how unfortunate."},
						{A: 500, text: "You do your best to clear a straight path, trying to follow the sun. @@It seems to have worked@@ and you managed to survey a large part of the jungle."},
					],
				},
				{   // If pass check 'A' and If counter is at MAX (3) and the cove hasn't been found yet.
					id: "SCENE04a",
					triggers: [
						{type: "tag", name: "A"},
						{type: "var:n", name: "COUNTER", value: 3, condition: ">="},
						{type: "var:b", name: "COVE_FOUND", value: false},
					],
					post: [
						// Clear counter, remember we just found something, set flag for finding the cove (used by jungle.twee)
						{type: "var", name: "COUNTER", value: 0},
						{type: "var", name: "REWARD", value: true},
						{type: "var", name: "COVE_FOUND", value: true},
					],
					text: "Along your adventure you seemed to have stumbled across a trail that doesn't look natural. It's cleverly disguised but leads towards the leeward side of the island and a small hidden cove.",
				},
				{   // If pass check 'A' and If counter is at MAX (3) and if we haven't just found the cove
					// (reward flag) and the ruins hasn't been found yet.
					id: "SCENE04a",
					triggers: [
						{type: "tag", name: "A"},
						{type: "var:n", name: "COUNTER", value: 3, condition: ">="},
						{type: "var:b", name: "REWARD", value: false},
						{type: "var:b", name: "RUINS_FOUND", value: false},
						{type: "var:b", name: "COVE_FOUND", value: true},
					],
					post: [
						// Clear counter, remember we just found something, set flag for finding the cove (used by jungle.twee)
						{type: "var", name: "COUNTER", value: 0},
						{type: "var", name: "REWARD", value: true},
						{type: "var", name: "RUINS_FOUND", value: true},
					],
					text: "As you're hacking across the jungle, you stumble across what appears to be a large stone stature. You cautiously sneak up closer to inspect it. As you approach the fallen statue, you choke back a gasp as you realize that it's part of a larger structure, almost buried under the foliage. You are tempted to explore it immediately, but for now you return to the jungle clearing to plan your next move.",
				},
				{
					// On a skill success we want to increment our success counter. Don't process if we just hit max (3) and set the reward counter.
					id: "SCENE04b",
					triggers: [
						{type: "tag", name: "A"},
						{type: "var:n", name: "COUNTER", value: 2, condition: "<="},
						{type: "var:b", name: "REWARD", value: false},
					],
					post: [
						// Increment counter.
						{type: "var", name: "COUNTER", value: 1, op: 'add'},
					],
					text: "You didn't find anything today, but you have a feeling that you're getting closer…",
				},
				{   // Just unset the flag that shows we found a path and make sure we clear the counter.
					id: "SCENE04c",
					triggers: [{type: "var:b", name: "REWARD", value: true}],
					post: [{type: "var", name: "REWARD", value: undefined, op: "reset"}],
				},
			],
		},
	});
}
