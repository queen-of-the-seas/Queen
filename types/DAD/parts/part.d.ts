import {Layer} from "../util/canvas";
import {Player} from "../player/player"
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

/**
 * All parts should go in this namespace
 * @namespace Part
 */

export type PartSideNumeric = 0 | 1;
export class BasePart {
	/**
	 * Parent part
	 */
	public parentPart: BasePart | null;

	/**
	 * Location of the part, prepend with + to mean allow other
	 * parts to also occupy this location, - to mean restrict any other part from being here
	 */
	public loc: string;

	/**
	 * Drawing layer
	 */
	public layer: number;

	/**
	 * Whether this part should be drawn on the other side as well
	 */
	public reflect: boolean;

	/**
	 * Whether this part should not be drawn if there are clothes
	 * covering its location
	 */
	public coverConceal: string[];

	/**
	 * Whether it's possible to wear anything over this part
	 * (mutually exclusive with converconceal)
	 */
	public uncoverable: boolean;

	/**
	 * List of "{part group} {location}" strings that specify
	 * which parts of which part groups this part should be drawn above. For example, having
	 * "decorativeParts torso" will ensure this part gets drawn above any parts in that location.
	 * Specifying only a location will mean to be above any part in that location regardless of group.
	 */
	public aboveParts: string[];

	/**
	 * Opposite to aboveParts.
	 */
	public belowParts: string[];

	public forcedSide: boolean;

	public side: PartSideNumeric;
	public _owner?: any;
	public Mods?: any;

	// TODO add shared data here
	constructor(...data: any);

	stroke(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;

	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;

	// how thick the stroke line should be
	getLineWidth(avatar?: Player): number;

	toString(): string;

	/**
	 * Calculate drawpoints associated with this part and return it in the sequence to be drawn.
	 * @this {object} Calculated dimensions of the player owning the part
	 * @param {object} ex Exports from draw that should hold draw points calculated up to now;
	 * additional draw points defined by this part should be defined on ex
	 * @param {object} mods Combined modifiers of the part and the Player owning the part
	 * @param {boolean} calculate
	 * @param {module:da.BodyPart} part The body part itself
	 * @return {object[]} List of draw points (or convertible to draw point objects)
	 */
	calcDrawPoints(ex: any, mods: any, calculate: boolean, part: BasePart): DrawPoint[];
}

export class BodyPart extends BasePart {
	constructor(...data: any);
	/**
     * Set the stroke pattern for this part
     * @returns {string}
     */
	stroke(ctx?: CanvasRenderingContext2D, ex?: any): string;

    /**
     * Set the fill pattern for this part
     * @returns {string}
     */
	fill(ctx?: CanvasRenderingContext2D, ex?: any): string;

    /**
     * Set how thick the stroke line should be
     * @returns {number}
     */
	getLineWidth(avatar?: Player): number;
}

export enum SideValue {
	RIGHT = 0,
	LEFT = 1
}

interface BodyPartConstructor {
	new(data: []): BodyPart;
}

export const Part: {

	/**
	 * Right side of the body for anything taking side
	 * @readonly
	 * @type {number}
	 */
	RIGHT: SideValue.RIGHT,
	/**
	 * Left side of the body for anything taking side
	 * @readonly
	 * @type {number}
	 */
	LEFT: SideValue.LEFT,
	/**
	 * Give me a base body part and a side it's supposed to be on
	 * I'll return to you a body part specific to that side
	 * @memberof module:da.Part
	 * @param {BodyPart} PartPrototype Prototype to instantiate with
	 * @param {...object} userData Overriding data
	 * @returns {BodyPart}
	 */
	create(PartPrototype: BodyPartConstructor, ...userData: object[]): BodyPart;
}

/**
 * Get a side string in a well defined format
 * @param side A side string
 * @returns Either the side string or null if unrecognized
 */
export function getSideLocation(side: string): string;

/**
 * Check whether two parts conflict
 * @param partA
 * @param partB
 * @returns True if the two parts have conflicting locations
 */
export function partConflict(partA: BodyPart, partB: BodyPart): boolean;

export function getChildLocation(parentLoc: string, child: string): {childSide: string, childLoc: string};

export function getSideValue(side: string | SideValue): SideValue;

export function getAttachedLocation(partPrototype: object): string;
