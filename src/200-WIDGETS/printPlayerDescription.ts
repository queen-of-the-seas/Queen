namespace App {
	Macro.add("printPlayerDescription", {
		skipArgs: true,
		handler() {
			const mirrorContainer = document.createElement('div');
			mirrorContainer.id = 'mirrorContainer';
			if (settings.displayAvatar) {
				const elementId = 'avatarUI';
				App.UI.appendNewElement('div', this.output).id = elementId;
				setup.avatar.drawCanvas(elementId, 800, 360);
			}

			const textContainer = settings.displayAvatar ? App.UI.appendNewElement('div', mirrorContainer) : mirrorContainer;
			void App.Text.playScene(App.Data.scenes.playerDescription, setup.world, textContainer)
			/*
			const p = App.PR.pCharacterRating;
			const pl = setup.world.pc;
			const a = pl.actor;

			const pArguments: Parameters<PR.HumanAspectDescriptionGenerator> = [a, {colorize: true}, a];

			const actorHas = `${a.pr.He} ${a.verb("have")}`;
			const actorIs = `${a.pr.He} ${a.verb("is")}`;

			$(textContainer).wiki(`<p>${actorIs} ${p.height(...pArguments)} and ${p.fitness(...pArguments)}</p>\
			<p>${actorHas} a ${p.face(...pArguments)} ${p.hair(...pArguments)} ${p.eyes(...pArguments)} ${actorHas} ${p.lips(...pArguments)}</p>\
			<p>${p.bust(...pArguments)} ${pl.stat("body", "bust") > 7 ? p.bustFirmness(...pArguments) : ""} ${pl.stat("body", "lactation") > 0 ? p.lactation(...pArguments) : ""}</p>\
			<p>${p.ass(...pArguments)} ${p.hips(...pArguments)} ${p.waist(...pArguments)} and ${p.figure(...pArguments)}.</p>\
			<p>${actorHas} ${p.penis(...pArguments)} and ${p.balls(...pArguments)} between ${a.pr.his} legs. ${pl.isFuta ? App.PR.pFutaStatus(pl) : ""}</p>\

			<p>${a.pr.He} ${a.verb("consider")} ${a.pr.his} natural beauty to be ${p.beauty(...pArguments)}.\nThe fetish appeal of ${a.pr.his} body is ${p.fetish(...pArguments)}.</p>`);
			*/

			this.output.append(mirrorContainer);
		},
	});
}
