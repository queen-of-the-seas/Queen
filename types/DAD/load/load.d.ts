export let loaded: boolean;

/**
 * Load all necessary module components and assets
 * This function should be called AFTER all extensions of the system is done
 * and BEFORE calling any drawing methods
 */
export function load(): Promise<void>;
