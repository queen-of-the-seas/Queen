import {DecorativePart} from "./decorative_part";

export class LegFur extends DecorativePart {
	constructor(...data: object[]);

	calcDrawPoints(ex: object): [];
}
