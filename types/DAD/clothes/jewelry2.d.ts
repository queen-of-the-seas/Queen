import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Jewelry, Piercing} from "./jewelry";





export class BellyPiercingSimplePart extends ClothingPart {
	constructor(...data: object[]);

	radius: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}



export class BellyPiercingAdvancedPart extends ClothingPart {
	constructor(...data: object[]);

	thickness: number;
	radius: number;
	secondaryRadius: number;
	distance: number;
	above: boolean;
	above2: boolean;
	bellow: boolean;
	bellow2: boolean;
	chain: boolean;
	belt: boolean;
	link: boolean;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class NipplePiercingPart extends ClothingPart {
	constructor(...data: object[]);

	bar: boolean;
	ring: boolean;
	radius: number;
	ringRadius: number;
	thickness: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class StudPart2 extends ClothingPart {
	constructor(...data: object[]);
	radius: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class BridgePart extends ClothingPart {
	constructor(...data: object[]);
	radius: number;
	distance: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class NipplePiercing extends Jewelry {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BellyPiercingSimple extends Jewelry {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BellyPiercingAdvanced extends Jewelry {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class StudPiercing2 extends Piercing {
	constructor(...data: object[]);

	relativeLocation: {
		drawpoint: string;
		dx: number;
		dy: number;
	};

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Bridge extends Piercing {
	constructor(...data: object[]);

	relativeLocation: {
		drawpoint: string;
		dx: number;
		dy: number;
	};

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

/*	version for eyebrow:

export class Bridge extends Piercing {
    constructor(...data: object[]);
        super({
            relativeLocation: {
                drawpoint: "brow.outbot",
                dx       : -1.5,
                dy       : 0.4
            },
            loc             : `${Location.NOSE}`,
            requiredParts   : "faceParts",

        },...data);
    }

    get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
*/
