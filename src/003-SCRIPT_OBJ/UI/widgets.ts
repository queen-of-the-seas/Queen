namespace App.UI {
	export abstract class SelfUpdatingTable {
		readonly #table: HTMLTableElement;
		readonly #header: HTMLTableSectionElement;
		readonly #tbody: HTMLTableSectionElement;

		constructor() {
			this.#table = UI.makeElement('table');
			this.#header = UI.appendNewElement('thead', this.#table);
			this.#tbody = UI.appendNewElement('tbody', this.#table);
		}

		get table(): HTMLTableElement {return this.#table;}
		get header(): HTMLTableSectionElement {return this.#header;}
		get body(): HTMLTableSectionElement {return this.#tbody;}

		render(): Node {
			this.update();
			return this.#table;
		}

		protected abstract update(): void;
	}

	export type PassageReference = string | [string, string | null];

	const cssStyles = {
		action: {
			general: 'action-general',
			interact: 'action-interact',
			caption: 'action-caption',
			links: 'action-links',
			row: 'action-row',
			travel: 'action-travel',
		},
		choices: {strip: 'choices-strip'},
		state: {
			disabled: 'state-disabled',
			enabled: 'state-enabled',
		},
		meter: {score: 'score-meter'},
	};

	function pPassageLinkStrip(passages: PassageReference[], prefix: string, className: string): string {
		if (passages.length === 0) {
			return '';
		}

		return `@@.${className};${prefix}@@: ` + passages.map((p) => {
			if (typeof p === 'string') {
				return p.startsWith('[[') || p.startsWith('<') ? p : `[[${p}]]`; // works for "caption|passage_name" too
			}
			// disabled links are passed in as ["text", null]
			return p[1] === null ? `@@.state-disabled;[${p[0]}]@@` : `[[${p[0]}|${p[1]}]]`;
		}).join('&thinsp;|&thinsp;');
	}

	export function pActionLinkStrip(passages: PassageReference[]): string {
		return pPassageLinkStrip(passages, 'Action', cssStyles.action.general);
	}

	export function pInteractLinkStrip(passages?: PassageReference[], exitCaption: string | null = 'Exit'): string {
		let links: PassageReference[] = [];
		if (exitCaption !== null) {
			const gbm = State.variables.gameBookmark;
			if (!_.isEmpty(gbm) && gbm !== passage()) {
				links.push(`[[${exitCaption}|${gbm}]]`);
			}
		}
		if (passages?.length) {
			links = links.concat(passages);
		}
		return pPassageLinkStrip(links, 'Interact', cssStyles.action.interact);
	}

	export function pTravelLinkStrip(passages: PassageReference[], caption = "Travel"): string {
		return pPassageLinkStrip(passages, caption, cssStyles.action.travel);
	}

	export function isTravelDestinationWithScene(d: Data.Travel.DestinationChoice): d is Data.Travel.SceneDestination {
		return d.hasOwnProperty('scene');
	}

	export function resolveDynamicText(player: Entity.Player): undefined;
	export function resolveDynamicText(player: Entity.Player, text: DynamicText): string;
	export function resolveDynamicText(player: Entity.Player, text?: DynamicText): string | undefined {
		if (!text) {
			return text;
		}
		return typeof text === "string" ? text : PR.tokenizeString(player, undefined, text(player));
	}

	export function wCustomMenuLink(text: string, action?: string | Entity.NPC | Job, passageName?: string): HTMLAnchorElement {
		const res = document.createElement("a");
		res.classList.add("link-internal");
		const targetPassage = passageName ?? text;
		res.append(text);
		res.addEventListener('click', () => {
			if (action) {
				State.variables.menuAction = action;
			}
			if (!tags().includes("custom-menu")) {
				State.variables.gameBookmark = passage();
			}
			Engine.play(targetPassage);
		})
		return res;
	}

	function executeAction(travel: Data.Travel.DestinationChoice, world: Entity.World) {
		return Choices.applyChoiceAction(travel, world.pc, world);
	}

	export function wTravels(world: Entity.World, destinations: Data.Travel.Destinations): HTMLDivElement {
		const res = document.createElement("div");
		const travels = appendNewElement("div", res, {classNames: [cssStyles.action.row]});
		const divLabel = appendNewElement("div", travels, {classNames: [cssStyles.action.caption]});
		appendNewElement("span", divLabel, {content: "Travel", classNames: [cssStyles.action.travel]});
		const listDiv = appendNewElement("div", travels, {classNames: [cssStyles.action.links]});
		const links = appendNewElement("ul", listDiv, {classNames: [cssStyles.choices.strip]});
		const travelSceneContainer = appendNewElement("div", res);
		for (const d of destinations) {
			const link = document.createElement("li");
			if (typeof d === "string") {
				link.append(passageLink(resolveDynamicText(world.pc, Data.Travel.passageDisplayNames[d]) ?? d, d));
				links.append(link);
			} else {
				const available = d.available;
				if (available) {
					const exp = new Conditions.Expression(available);
					const ctx = new Conditions.EvaluationContext({world: setup.world}, Conditions.EvaluationContext.REQUIREMENTS_CHECK_DEFAULTS);
					const evalExp = exp.compute(ctx);
					if (!evalExp.status) {
						continue;
					}
				}
				let enabled = true;
				if (d.enabled) {
					const exp = new Conditions.Expression(d.enabled);
					const ctx = new Conditions.EvaluationContext({world: setup.world}, Conditions.EvaluationContext.REQUIREMENTS_CHECK_DEFAULTS);
					enabled = exp.compute(ctx).status;
				}

				if (isTravelDestinationWithScene(d)) {
					const caption = resolveDynamicText(world.pc, d.caption);
					if (enabled) {
						const anchor = appendNewElement("a", link, {content: caption});
						if (d.hint) {
							tippy(anchor, {content: resolveDynamicText(world.pc, d.hint)});
						}
						anchor.addEventListener('click', () => {
							travelSceneContainer.textContent = '';
							void Text.playScene(Data.Travel.scenes[d.scene], world, travelSceneContainer);
						});
					} else {
						const span = appendNewElement("span", link, {content: caption, classNames: [cssStyles.state.disabled]});
						if (d.hint) {
							tippy(span, {content: resolveDynamicText(world.pc, d.hint)});
						}
					}
				} else {
					if (d.destination === null) {
						appendNewElement("span", link, {content: resolveDynamicText(world.pc, d.caption ?? ""), classNames: [cssStyles.state.disabled]});
					} else {
						const destinationPasage = typeof d.destination === "string" ? d.destination : d.destination(world.pc);
						const text = d.caption ? resolveDynamicText(world.pc, d.caption) :
							resolveDynamicText(world.pc, Data.Travel.passageDisplayNames[destinationPasage]) ?? destinationPasage;
						if (enabled) {
							link.append(passageLink(text, destinationPasage, () => executeAction(d, world)));
						} else {
							appendNewElement("span", link, {content: text, classNames: [cssStyles.state.disabled]});
						}
					}
				}
				links.append(link);
			}
		}

		// late night teleport
		if (setup.world.phase >= DayPhase.LateNight && !tags().includes("no-teleport")) {
			const lnt = appendNewElement("div", res, {content: " It's getting kind of late… "});
			lnt.append(passageLink("head back to the Mermaid", "Cabin"));
			lnt.style.marginLeft = "0.5em";
		}

		return res;
	}

	export function wNPC(player: Entity.Player, npcTag: string, custom?: PassageReference[]): HTMLDivElement {
		const npc = setup.world.npc(npcTag);
		const res = makeElement("div", {classNames: ["npc-interact"]});
		$(res).wiki(PR.tokenizeString(player, npc, npc.shortDesc));
		const npcMenu = appendNewElement("div", res)
		appendNewElement("span", npcMenu, {content: "NPC Menu", classNames: ["npc-menu"]});
		npcMenu.append(": ");

		const npcLinks = appendNewElement("ul", npcMenu, {classNames: cssStyles.choices.strip})
		appendNewElement("li", npcLinks).append(wCustomMenuLink("Examine", npcTag, "ExamineNPC"));

		if (Job.hasJobs(player, npc)) {
			const liJobs = appendNewElement("li", npcLinks);
			liJobs.append(wCustomMenuLink("Jobs", npcTag));
			if (Job.jobsAvailable(player, npc)) {
				appendNewElement("span", liJobs, {content: " (!)", classNames: "task-available"});
			}
		}

		if (Quest.list("any", player, npcTag).length > 0) {
			const liQuests = appendNewElement("li", npcLinks);
			liQuests.append(wCustomMenuLink("Quests", npcTag));
			if (Quest.list(QuestStatus.Available, player, npcTag).length > 0) {
				appendNewElement("span", liQuests, {content: " (!)", classNames: "task-available"});
			}
			if (Quest.list(QuestStatus.CanComplete, player, npcTag).length > 0) {
				appendNewElement("span", liQuests, {content: " (!)", classNames: "task-cancomplete"});
			}
		}

		if (storeEngine.hasStore(npc)) {
			const liShop = appendNewElement("li", npcLinks);
			if (storeEngine.isOpen(npc)) {
				liShop.append(wCustomMenuLink("Shop", npc));
			} else {
				appendNewElement("span", liShop, {content: "Shop", classNames: cssStyles.state.disabled});
			}
		}

		if (custom) {
			for (const pr of custom) {
				if (typeof pr === "string") {
					appendNewElement("li", npcLinks).append(wCustomMenuLink(pr, npcTag, pr));
				} else {
					appendNewElement("li", npcLinks).append(wCustomMenuLink(pr[0], npcTag, pr[1] ?? undefined));
				}
			}
		}
		return res;
	}

	export function wLocationJobs(player: Entity.Player, location: string, brief = false): HTMLDivElement | null {
		const jobList = Job.locationJobs(location).filter(job => job.available(player) || job.unavailable(player));
		if (jobList.length === 0) {
			return null;
		}
		const res = document.createElement("div");
		if (!brief) {
			appendNewElement("span", res, {content: "Action", classNames: cssStyles.action.general});
			res.append(": ");
		}
		const links = appendNewElement("ul", res, {classNames: cssStyles.choices.strip})
		for (const job of jobList) {
			const li = appendNewElement("li", links);
			if (job.ready(player)) {
				li.append(wCustomMenuLink(job.title(true), job, "SelfJobs"));
			} else {
				appendNewElement("span", li, {content: job.title(true), classNames: cssStyles.state.disabled});
			}
			$(li).wiki(job.ratingAndCosts());
		}
		return res;
	}

	export function rJobResults(world: Entity.World, job: Job): DocumentFragment {
		const ctx = job.makeActionContext(world);
		const start = job.printStart(ctx);
		const scenes = job.playScenes(ctx);
		const end = job.printEnd(ctx, scenes);

		const res = new DocumentFragment();
		const $res = $(res);
		if (start.length > 0) {
			$res.wiki(start);
			res.append(document.createTextNode(' '));
		}
		for (const s of scenes) {
			res.append(s.text);
			res.append(document.createTextNode(' '));
		}
		if (end.length > 0) {
			$res.wiki(end);
		}

		const allActions = scenes.reduce((actions: Actions.Base[], s) => {
			actions.push(...s.actions);
			return actions;
		}, []);

		const rewardsFrag = new DocumentFragment();
		const rewardRenderer = new Actions.TaskRewardRenderer(ctx, allActions, rewardsFrag, job);
		if (!rewardRenderer.isEmpty) {
			appendNewElement('br', res);
			appendFormattedFragment(res, {text: "Your receive some items:", style: 'job-results-items'});
			appendNewElement('br', res);
			res.append(rewardsFrag);
		}

		for (const a of allActions) {
			a.apply(ctx);
		}
		return res;
	}

	export function rBodyScore(player: Entity.Player): HTMLTableElement {
		const res = makeElement("table", {classNames: "body-stats"});
		res.style.width = "100%";
		res.style.textAlign = "left";

		const tbody = appendNewElement("tbody", res);

		const statsOrder: BodyStat[] = [
			BodyProperty.Height,
			BodyPart.Hair, BodyPart.Face, BodyPart.Lips,
			BodyPart.Bust, BodyProperty.Lactation,
			BodyPart.Waist, BodyPart.Hips, BodyPart.Ass, BodyPart.Penis, BodyPart.Balls,
		];
		const shownIfPresent: Set<BodyStatStr> = new Set([BodyPart.Bust, BodyProperty.Lactation, BodyPart.Penis, BodyPart.Balls]);

		for (const st of statsOrder) {
			if (player.bodyStats[st] > 0 || !shownIfPresent.has(st)) {
				const tr = appendNewElement("tr", tbody);
				appendNewElement("td", tr, {content: `${_.pascalCase(st)}:`});
				const meter = appendNewElement("td", tr);
				meter.id = st;
				meter.classList.add(cssStyles.meter.score);
				meter.append(rBodyMeter(st, player));
				const pString = PR.tokenizeString(player, undefined, 'p' + st.toUpperCase());
				if (pString) {
					tippy(meter, {content: Wikifier.wikifyEval(pString)})
				}
			}
		}
		return res;
	}

	const rGameScoreLinkHandler = () => {
		const passageObject = Story.get(passage());
		if (!passageObject.tags.contains("custom-menu")) {
			State.variables.gameBookmark = passage();
		}
	};

	export function rGameScore(): DocumentFragment {
		const res = new DocumentFragment();

		const player = setup.world.pc;

		appendNewElement("div", res, {content: player.slaveName, classNames: 'state-feminity'});

		const dayDiv = appendNewElement("div", res, {content: `Day ${setup.world.day}, `});
		dayDiv.append(Wikifier.wikifyEval(`${setup.world.phaseName(false)} ${PR.phaseIconStrip()}`));

		const coins = appendNewElement("div", res, {content: "Coins: ", classNames: 'item-money'});
		appendNewElement("span", coins, {content: `${player.money}`, id: "Money"});

		if (player.skills.courtesan) {
			const tokens = appendNewElement("div", res, {content: "Tokens: ", classNames: 'state-sexiness'});
			appendNewElement("span", tokens, {content: `${player.tokens}`, id: "Tokens"});
		}

		const linksDiv = appendNewElement("div", res);
		linksDiv.id = "game-info-links";
		linksDiv.style.textAlign = "center";
		linksDiv.append(linksStrip(
			passageLink("Journal", "Journal", rGameScoreLinkHandler),
			passageLink("Skills", "Skills", rGameScoreLinkHandler),
			passageLink("Inventory", "Inventory", rGameScoreLinkHandler)
		));

		const scoreTable = appendNewElement("table", res);
		scoreTable.style.width = "100%";
		scoreTable.style.marginTop = "0.5ex";
		const scoreTableBody = appendNewElement("tbody", scoreTable);

		const statsOrder: CoreStat[] = [
			CoreStat.Health, CoreStat.Energy, CoreStat.Willpower,
			CoreStat.Perversion, CoreStat.Nutrition, CoreStat.Femininity, CoreStat.Toxicity,
		];

		for (const st of statsOrder) {
			const tr = appendNewElement("tr", scoreTableBody);
			appendNewElement("td", tr, {content: `${_.pascalCase(st)}:`});
			const meterTd = appendNewElement("td", tr);
			meterTd.id = st;
			meterTd.append(rCoreStatMeter(st, setup.world.pc));
			meterTd.classList.add(cssStyles.meter.score);
		}
		// hormones meter needs a special symbol
		const tr = appendNewElement("tr", scoreTableBody);
		appendNewElement("td", tr, {content: "Hormones:"});
		const meterTd = appendNewElement("td", tr);
		meterTd.id = "hormones";
		meterTd.append(rCoreStatMeter(CoreStat.Hormones, setup.world.pc));
		meterTd.innerHTML += PR.pHormoneSymbol();
		meterTd.classList.add(cssStyles.meter.score);

		// update avatar if empty
		if (settings.displayAvatar && document.getElementById("avatarFace")?.children.length === 0) {
			setup.avatar.drawPortrait();
		}

		// update body score
		if (settings.displayBodyScore) {
			const container = $("#bodyScoreContainer");
			container.empty();
			container.append(rBodyScore(setup.world.pc));
		}

		return res;
	}
}
namespace App.UI.Widgets {

	function jobButton(job: Job, isAvailable: boolean, world: Entity.World) {
		const res = document.createDocumentFragment();
		const button = appendNewElement("button", res, {classNames: "job-button"});
		if (isAvailable) {
			button.textContent = "Accept";
			button.addEventListener('click', () => {
				UI.replace("#JobUI", UI.rJobResults(world, job));
			});
		} else {
			button.classList.add("job-button-disabled");
			button.textContent = job.onCoolDown(world.pc) ? "COOLDOWN" : "LOCKED";
			job.renderRequirements(world.pc, res);
		}
		return res;
	}

	const jobCompareByRating = (left: Job, right: Job): number => {
		return left.taskData.rating - right.taskData.rating;
	};

	export function rJobList(world: Entity.World, jobs: Job[]): HTMLSpanElement {
		const jobUI = document.createElement('span');
		jobUI.id = "JobUI";

		for (const job of jobs.filter(job => job.available(world.pc)).sort(jobCompareByRating)) {
			const jobDiv = appendNewElement("div", jobUI, {classNames: "job-intro"});
			$(jobDiv).wiki(job.title() + "<br/>" + job.intro(world.pc));
			appendNewElement("br", jobDiv);
			jobDiv.append(jobButton(job, true, world));
			appendNewElement("br", jobDiv);
		}

		for (const job of jobs.filter(job => job.unavailable(world.pc)).sort(jobCompareByRating)) {
			const jobDiv = appendNewElement("div", jobUI, {classNames: "job-intro"});
			$(jobDiv).wiki(job.title() + "<br/>");
			appendNewElement("br", jobDiv);
			jobDiv.append(jobButton(job, false, world));
			appendNewElement("br", jobDiv);
		}
		return jobUI;
	}
}
