/**
 * Created by Johnson on 2017-06-17.
 */

import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
/**
 * Extract the side component from a location string
 * @param partLoc Location string of a part
 * @returns Either the side string or null if no side indicated
 */
export function extractSideLocation(partLoc: string): string;

/**
 * Extract the base component from a location string
 * @param partLoc Location string of a part
 * @returns The base component
 */
export function extractBaseLocation(partLoc: string): string;

/**
 * Extract the underlying location (side + base) of a location after stripping it of
 * potential modifier characters, which are prepended (use + for non-restrictive, - for exclusive)
 * @param partLoc location name of part
 */
export function extractUnmodifiedLocation(partLoc: string): string;

/**
 * Extract modifier characters (+ for non-restrictive, - for exclusive)
 * @param partLoc Location name of part
 * @returns Modifier characters for this location
 */
export function extractLocationModifier(partLoc: string): string;

/**
 * List of all base locations (without side and modifiers)
 */
export enum Location {
	ARM = "arm",
	CHEST = "chest",
	TORSO = "torso",
	BUTT = "butt",
	FEET = "feet",
	GROIN = "groin",
	HAND = "hand",
	HEAD = "head",
	HAIR = "hair",
	LEG = "leg",
	NECK = "neck",
	PENIS = "penis",
	VAGINA = "vagina",
	TESTICLES = "testicles",
	GENITALS = "genitals",
	EAR = "ears",
	EYEBROW = "brow",
	EYELASH = "eyelash",
	EYELID = "eyelid",
	EYES = "eyes",
	IRIS = "iris",
	LIPS = "lips",
	MOUTH = "mouth",
	NOSE = "nose",
	PUPIL = "pupil",
}

export function locationIsSideless(loc: string): boolean;

/**
 * Get a location relative to a drawpoint
 */
export function locateRelativeToDrawpoint(ex: DrawingExports, opt: {drawpoint: string, dx: number, dy: number}): DrawPoint;
