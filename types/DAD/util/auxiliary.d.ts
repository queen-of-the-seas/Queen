import {DrawPoint} from "drawpoint/dist-esm";

/**
	takes the length in y axis and compare it with y positions of array of points
	getLimbPoints(
		the highest reference point (aka 0%),
		the lowest reference point (aka 100%),
		percentage of covered area (0-1),
		point1 (the highest one),
		point2,
		etc.
	);
	returns array of points in the covered area
	used for sleeves and pants with letiable length
*/
export function getLimbPoints(...points: DrawPoint[]): DrawPoint[];

/**
	similar to "getLimbPoints" but needs only the lowest point
	getLimbPointsAbovePoint(
		the lowest reference point
		revert (true/false) - returns reversed array (eg for inner points which needs to be drawn in order from the lowest to the highest)
		point1 (the highest one),
		point2,
		etc.
	);
	used  when you have the lowestmost point on the one side of limb and need to find all the point above it on the other side
*/
export function getLimbPointsAbovePoint(...points: DrawPoint[]): DrawPoint[];

/**
	the same as getLimbPoints but returns points that are outside the covered area
		getLimbPointsNegative(
		the highest reference point (aka 0%),
		the lowest reference point (aka 100%),
		percentage of covered area (0-1),
		point1 (the highest one),
		point2,
		etc.
	);
	used for socks
*/
export function getLimbPointsNegative(...points: DrawPoint[]): DrawPoint[];

/**
	similar to "getLimbPointsAbovePoint" but needs only the highest point
	getLimbPointsBellowPoint(
		the highest reference point
		revert (true/false) - returns reversed array (eg for inner points which needs to be drawn in order from the lowest to the highest)
		point1 (the highest one),
		point2,
		etc.
	);
	used  when you have the highest point on the one side of limb and need find all the point bellow it on the other side
*/
export function getLimbPointsBellowPoint(control_point: DrawPoint, revert: boolean, ...points: DrawPoint[]): DrawPoint[];

/**
	findBetween(first value,second value, percent)
	returns value with percent distance between first and second value
*/
export function findBetween(lower: DrawPoint, higher: DrawPoint, percent?: number): DrawPoint;

/**
	gradually straightens curve
	(by moving both control points closer to the centre of the curve)
*/

export function straightenCurve(previous_point: DrawPoint, point: DrawPoint, percent?: number): void;

/**
	finds point which is intersection of lines AB and CD
*/

export function lineLineIntersection(A: DrawPoint, B: DrawPoint, C: DrawPoint, D: DrawPoint): DrawPoint;

/**
	finds point which is intersection of bezier curve and line AB
	lineCubicIntersection(startpoint,endpoint,A,B){
*/

export function lineCubicIntersection(start: DrawPoint, end: DrawPoint, lineA: DrawPoint, lineB: DrawPoint): DrawPoint;

/**
	finds if point C is located on line defined by points A and B
	(returns 0 if it is)
*/
//TODO??
export function pointLineIntersection(A: DrawPoint, B: DrawPoint, C: DrawPoint): number;

/**
	returns points for drawing lacing between two curves
	getLacingPoints(
		startpoint of the inner curve
		endpoint of the inner curve
		startpoint of outer
		endpoint of outer
		number of crossings
		adjustment (to not have lace hole straight on the curve)
	);
*/
export function getLacingPoints(innerStart: DrawPoint, innerEnd: DrawPoint, outerStart: DrawPoint, outerEnd: DrawPoint, count: number, adjustment?: number): { inner: DrawPoint, outer: DrawPoint };

/**
	first finds a point T on the line between points "A" and "B" with distance from "A" "percent"*AB
	then returns point C perpendicular to line AB with distance from the point T "distance"

		C
		|
	A---T----B

	perpendicularPoint(
		A - fist point {x,y}
		B - second point {x,y}
		percent - percentage of distance AB (values 0-1)
		distance - perpendicular distance from a point T on line AB and the resulting point C
	)
	returns C
*/
export function perpendicularPoint(A: DrawPoint, B: DrawPoint, percent: number, distance: number): DrawPoint;

/**
	transfers point from Cartesian coordinates to Polar
*/
export function cartesian2polar(point: DrawPoint, origin?: DrawPoint): { r: number, theta: number };

/**
	transfers point from Polar coordinates to Cartesian
	theta is in radians
*/
export function polar2cartesian(r: number, theta: number, origin?: DrawPoint): DrawPoint;

/**
	copies control points from the first curve to the second curve,
	adjusted by size difference and rotation between both curves
	copyCurve(
		start of the original fist curve,
		end of the original curve (with control points),
		start of the second curve (or at this point only a line before cps are added)
		end of the second curve
	)
*/

//TODO NOT SURE IF WORKS PROPERLY
// FIXME types are wrong
export function copyCurve(START: DrawPoint, END: DrawPoint, start: DrawPoint, end: DrawPoint): void;

/**
	draws spiral
	drawSpiral(
		center around which the spiral spirals
		outermost startpoint
		number of loops around the center
		direction: -1 clockwise; 1 counterclockwise //TO DO - maybe it is the other way around
	)
*/

export function drawSpiral(center: DrawPoint, start: DrawPoint, loops: number, direction?: number): DrawPoint[];

/**
	drawStar(
		center {x,y}
		number of spikes
		outerRadius
		innerRadius
		up (boolean) - the first spike should be facing upwards (false - downwards)
		outer -  0 = nothing; 1 = there should be a circle drawn around the star; -1 = there should be a polygon drawn around the star;
		inner - the same thing as outer but inside the star
	)
	(the last three parameters are there to enable drawing nice pentagrams)
*/
export function drawStar(center: DrawPoint, spikes: number, outerRadius: number, innerRadius: number, up?: boolean, outer?: number, inner?: number): DrawPoint[];
