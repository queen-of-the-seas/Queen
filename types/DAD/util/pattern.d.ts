import {DrawPoint} from "drawpoint/dist-esm";
import {Tattoo} from "../decorative_parts/tattoo";
import {DrawingExports} from "../draw/draw";

/**
 * Created by Johnson on 2017-06-17.
 */
export const IMAGE_MAXSIZE = 100;

/**
 * Cache of loaded patterns, lazily created as needed by getPattern.
 * Each one is either a function that returns either a color, gradient, pattern (all are statically cached),
 * or a function that returns either a color, gradient, or pattern (dynamically cached since the parameters depend
 * on draw exports).
 */
export const cachedPatterns: {};

/**
 * Patterns to load as soon as we want to draw. Populated by getPattern as necessary.
 */
export const patternLoadingQueue: Promise<any>[];


/**
 * List names of available patterns to gettable by getPattern
 */
export function listAvailablePatterns(): string[];

export function getPatternFullName(patternName: string, patternSize: number|string): string;

export function getPatternBaseName(patternName: string): string;

/**
 * Intermediate storage of a pattern object (cached). Can be loaded to get a render-able object.
 */
export class CachedPattern {
	constructor(data:any);
	getLoaded(ctx: CanvasRenderingContext2D, ex: DrawingExports): any;
}

/**
 * Get a fill or stroke pattern. When used inside draw, guaranteed to supply a usable pattern.
 * Used outside of draw could result in unfulfilled promises.
 * @param patternName Name of the pattern
 * @param patternSize How large the image canvas should be for this pattern
 * @returns pattern method
 */
export function getPattern(patternName: string, patternSize?: number): CachedPattern;

export function getLoadedPattern(pattern: CachedPattern | Function, ctx:CanvasRenderingContext2D, ex: DrawingExports): any;

export function addPattern(patternName: string, patternSource: string|Function): void;

export function addDebugPattern(patternName: string, debugSrc: any, releaseSrc: any): void;

/**
 * Load pattern from some source method (either function or an image path) asynchronously
 * @param patternName
 * @param patternSource
 * @param patternSize How large the image canvas should be for this pattern
 * @returns Promise to produce this pattern (and cache it)
 */
export function loadPattern(patternName: string, patternSource: string | Function, patternSize: number): Promise<any>;

export function drawTattooPattern(tattoo: Tattoo, image: CanvasImageSource, ctx: CanvasRenderingContext2D, renderPosition:DrawPoint): void;

export function isPattern(colour: any): boolean;
