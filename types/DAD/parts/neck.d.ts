import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {BodyPart} from "./part";

declare class ChinShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class NeckShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Neck extends BodyPart {
	constructor(...data: object[]);
}


// neck will export neck.nape, neck.top, neck.cusp, trapezius.top, collarbone
export class NeckHuman extends Neck {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
