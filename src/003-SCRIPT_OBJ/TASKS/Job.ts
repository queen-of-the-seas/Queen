namespace App {
	interface DanceInfo {
		dance: Data.Fashion.Style,
		dateTime: DateTime;
	}

	enum JobStdAttribute { // property names begin with '_' to avoid clashes with task variables
		LastCompletedOn = "_lastCompleted",
		TimesCompleted = "_timesCompleted",
	}

	/**
	 * Represents a job.
	 */
	export class Job extends Task {
		//#region Job lists
		/** These are jobs whose template data provide all the required data,
		 * particularly with the giver npc defined
		 */
		static readonly #jobs: Map<string, Job> = new Map();
		/**
		 * This map contains job templates with the giver undefined
		 */
		static readonly #jobTemplates: Map<string, Data.Tasks.Job> = new Map();

		static #danceInfo: DanceInfo | null = null;

		static loadJobs(): void {
			const res = new Map<string, Job>();
			for (const [id, data] of Object.entries(Data.jobs)) {
				if (Job._isFullyDefined(data)) {
					Job.#jobs.set(id, new Job(id, data));
				} else {
					Job.#jobTemplates.set(id, data);
				}
			}
		}

		/**
		 * Hack for reporting the current 'desired dance' for dance halls.
		 */
		static getDance(): Data.Fashion.Style {
			if (!Job.#danceInfo || compareDateTime(Job.#danceInfo.dateTime, setup.world.now) < 0 ) {
				Job.#danceInfo = {
					dance: Object.values(Data.Fashion.Style).randomElement(),
					dateTime: setup.world.now,
				};
			}
			return Job.#danceInfo.dance;
		}

		/**
		 * Lists all jobs at person/location
		 */
		static byGiver(giver: Entity.NPC): Job[] {
			const res: Job[] = [];
			for (const job of this.#jobs.values()) {
				if (job.giverId === giver.id) {
					res.push(job);
				}
			}
			return res;
		}

		static byTag(tag: string, npc?: Entity.NPC): Job[] {
			const res: Job[] = [];
			for (const job of this.#jobs.values()) {
				if (job.tags.has(tag) && (npc && npc === job.giver)) {
					res.push(job);
				}
			}

			if (npc) {
				for (const jobTemplate of Job.#jobTemplates) {
					if (jobTemplate[1].tags?.includes(tag)) {
						res.push(new Job(jobTemplate[0], jobTemplate[1], npc.id));
					}
				}
			}
			return res;
		}

		/**
		 * Return a Job by it's id property.
		 */
		static byId(jobID: string, npc?: Entity.NPC): Job {
			const job = this.#jobs.get(jobID);
			if (job) {
				return job;
			}

			const jobTemplate = this.#jobTemplates.get(jobID);
			if (jobTemplate) {
				return new Job(jobID, jobTemplate, npc?.id ?? Entity.NpcId.Dummy);
			}

			throw new Error(`No job with id '${jobID}'`);
		}

		static available(player: Entity.Player, jobID: string): Job | undefined {
			const job = Job.byId(jobID);
			return job?.available(player) ? job : undefined;
		}

		/**
		 * Lists all AVAILABLE jobs at a person
		 */
		static availableJobs(player: Entity.Player, giver: Entity.NPC): Job[] {
			return Job.byGiver(giver)
				.filter(job => job.available(player))
		}

		/**
		 * Lists all UNAVAILABLE jobs on person
		 */
		static unavailableJobs(player: Entity.Player, giver: Entity.NPC): Job[] {
			return Job.byGiver(giver)
				.filter(job => job.unavailable(player))
		}

		static locationJobs(location: string): Job[] {
			return Job.byTag(`location:${location}`, setup.world.npc(Entity.NpcId.Dummy));
		}

		/**
		 * Lists available jobs at a location
		 */
		static availableLocationJobs(player: Entity.Player, location: string): Job[] {
			return Job.locationJobs(location).filter(job => job.available(player));
		}

		/**
		 * Lists unavailable jobs at a location
		 */
		static unavailableLocationJobs(player: Entity.Player, location: string): Job[] {
			return Job.locationJobs(location).filter(job => job.unavailable(player));
		}

		/**
		 * True if there are AVAILABLE jobs at this person
		 */
		static jobsAvailable(player: Entity.Player, giver: Entity.NPC): boolean {
			return Job.availableJobs(player, giver).length > 0;
		}

		/**
		 * True if there are ANY jobs at this person
		 */
		static hasJobs(player: Entity.Player, giver: Entity.NPC): boolean {
			return (Job.availableJobs(player, giver).length > 0) ||
				(Job.unavailableJobs(player, giver).length > 0);
		}

		//#endregion

		private static _isFullyDefined(data: Data.Tasks.Job): boolean {
			return isDefined(data.giver);
		}

		private constructor(id: string, data: Data.Tasks.Job, giver?: string) {
			super(id, data, giver);
		}

		override get taskData(): Data.Tasks.Job {
			return super.taskData as Data.Tasks.Job;
		}

		// eslint-disable-next-line class-methods-use-this
		protected override getState(player: Entity.Player): Partial<Record<string, GameState.TasksState>> {
			return player.taskState(TaskType.Job);
		}

		get pay(): number {return this.taskData.pay ?? 0;}
		get tokens(): number {return this.taskData.tokens ?? 0;}
		get phases(): NonEmptyArray<DayPhase> {return this.taskData.phases;}

		ratingAndCosts(): string {
			let res = " &#9733;".repeat(this.taskData.rating);
			res = PR.colorizeString(this.taskData.rating, res, 5);

			const time = this._getCost("time");
			const energy = this._getCost(Stat.Core, CoreStat.Energy);
			const money = this._getCost("money");
			const tokens = this._getCost("tokens");
			const strings: string[] = [];

			if (time !== 0) strings.push(`@@.item-time;Time ${time}@@`);
			if (energy !== 0) strings.push(`@@.item-energy;Energy ${energy}@@`);
			if (money !== 0) strings.push(`@@.item-money;Money ${money}@@`);
			if (tokens !== 0) strings.push(`@@.item-courtesan-token;Tokens ${tokens}@@`);

			if (strings.length > 0) res += ` [${strings.join("&nbsp;")}]`;
			return res;
		}

		/**
		 * Print the title of a job.
		 * @param brief print brief description
		 */
		override title(brief = false): string {
			const output = this.taskData.title;
			if (brief) return output;

			return `${output} ${this.ratingAndCosts()}`;
		}

		/**
		 * Check to see if the player meets the other requirements to take the job.
		 *  Usually skill, stat, body related or quest flags.
		 */
		requirements(): Conditions.ComputedExpression {
			return Task.evaluateRequirements(this._req(), this.definitionContext);
		}

		checkRequirements(): boolean {
			return Task.checkRequirements(this._req(), this.definitionContext);
		}

		/**
		 * Calculate cost of a job
		 */
		private _getCost(type: Data.Tasks.Costs.CostType, name?: string) {
			const check: Data.Tasks.Costs.Any[] = name !== undefined
				? this._cost().filter((c) => {
					return c.type === type && ((c.type === Stat.Core || c.type === Stat.Body || c.type === Stat.Skill || c.type === "item") && c.name === name);
				})
				: this._cost().filter(c => c.type === type);
			if (check !== undefined && check.length > 0) return check[0].value;
			return 0;
		}

		get energyCost(): number {
			return this._getCost(Stat.Core, CoreStat.Energy);
		}

		get timeCost(): number {
			return this._getCost("time");
		}

		/**
		 * Check to see if the player meets the "Cost" requirement for a job. Usually energy as this
		 * is subtracted from their status upon taking the job.
		 */
		cost(player: Entity.Player): boolean {
			if (this._cost() === undefined) return true;

			const checkFail = (c: Data.Tasks.Costs.Any) => {
				switch (c.type) {
					case Stat.Core:
					case Stat.Body:
					case Stat.Skill:
						return player.stat(c.type, c.name) < c.value;
					case "money":
						return player.money < c.value;
					case "tokens":
						return player.tokens < c.value;
					default:
						return false;
				}
			};
			return this._cost().some(checkFail) ? false : true;
		}

		/**
		 * Check to see if the time cost falls within the open/allowed phases of the activity and that there
		 * is enough time in the day to complete it.
		 */
		private _checkTime() {
			return (this.phases.includes(setup.world.phase) && (this._getCost("time") + setup.world.phase <= DayPhase.LateNight));
		}

		/**
		 * Check to see if the time cost falls within the open/allowed phases of the activity
		 */
		private _checkReady(player: Entity.Player): boolean;
		private _checkReady(player: Entity.Player, opt: false): boolean;
		private _checkReady(player: Entity.Player, opt: true): number;
		private _checkReady(player: Entity.Player, opt = false): boolean | number {
			const flagValue = this.variable(player, JobStdAttribute.LastCompletedOn, 0 as number);
			if (opt) return (flagValue + this.taskData.days - setup.world.day);
			return ((flagValue === 0) || (flagValue + this.taskData.days <= setup.world.day));
		}

		/**
		 * Set's the last completed flag for the job.
		 */
		private _setCompletedState(player: Entity.Player) {
			this.setVariable(player, JobStdAttribute.LastCompletedOn, setup.world.day);
			this.setVariable(player, JobStdAttribute.TimesCompleted,
				this.variable<number>(player, JobStdAttribute.TimesCompleted, 0) + 1);
		}

		/**
		 * Day when the quest was completed by player
		 */
		lastCompletedOn(player: Entity.Player, defaultValue = 0): number {
			return (this.state(player)?.[JobStdAttribute.LastCompletedOn] ?? defaultValue) as number;
		}

		timesCompleted(player: Entity.Player, defaultValue = 0): number {
			return (this.state(player)?.[JobStdAttribute.TimesCompleted] ?? defaultValue) as number;
		}

		/**
		 * Check to see if this job is available to be used.
		 */
		available(player: Entity.Player): boolean {
			const requirementsStatus = this.checkRequirements();
			const costStatus = this.cost(player);
			const timeStatus = this._checkTime();
			const readyStatus = this._checkReady(player);

			Job.debug("Job.Available:", this.id);
			Job.debug("Cost:", `${costStatus}`);
			Job.debug("Requirements:", `${requirementsStatus}`);
			Job.debug("_CheckTime:", `${timeStatus}`);
			Job.debug("_CheckReady:", `${readyStatus}`);

			return requirementsStatus && costStatus && timeStatus && readyStatus;
		}

		/**
		 * The job can not be taken right now, but is visible to the player
		 */
		unavailable(player: Entity.Player): boolean {
			const evaluatedRequirements = this.requirements();
			const hidden = Conditions.hidesTask(evaluatedRequirements.root, evaluatedRequirements.ctx);
			const isReady = this.ready(player);
			return hidden
				? evaluatedRequirements.status && !isReady
				: !evaluatedRequirements.status || !isReady;
		}

		/**
		 * Print out the requirements (missing) string for a job.
		 */
		renderRequirements(player: Entity.Player, parent: ParentNode): void {
			const div = UI.makeElement('div', {classNames: 'job-requirements'})
			const coolDown = this._checkReady(player, true);
			if (this.onCoolDown(player)) {
				UI.appendFormattedFragment(div,
					{text: `Available in ${coolDown} day${coolDown > 1 ? "s" : ""}`, style: 'state-neutral'});
			} else if (!this._checkTime()) {
				const timeSpan = div.appendNewElement('span');
				UI.appendFormattedFragment(timeSpan, {text: "Only Available ", style: 'state-neutral'});
				for (const phase of this.phases)
					$(timeSpan).wiki(PR.phaseIcon(phase));
			}
			const requirements = this.requirements();
			if (!requirements.status) {
				const reqs = UI.appendFormattedFragment(div,
					{text: "(Requirements not met)", style: ['state-negative', 'text-with-tooltip']});
				const requirementsTooltip = document.createElement('div');
				const renderer = new Conditions.RequirementRenderer(requirements.ctx, requirementsTooltip);
				requirements.root.acceptVisitor(renderer);
				tippy(reqs, {content: requirementsTooltip});
			}
			parent.append(div);
		}

		/**
		 * Check to see if a job is ready (if it's known about).
		 */
		ready(player: Entity.Player): boolean {
			return this.cost(player) && this._checkTime() && this._checkReady(player);
		}

		/**
		 * Return if the job is on cool down.
		 */
		onCoolDown(player: Entity.Player): boolean {
			Job.debug("OnCoolDown", JSON.stringify(player.taskState(TaskType.Job)[this.id]));
			return !this._checkReady(player);
		}

		override playScenes(ctx: Actions.EvaluationContext): CalculatedScene[] {
			const calculated = super.playScenes(ctx);
			calculated.push({
				text: new DocumentFragment(),
				actions: [
					new Actions.SilentFunction((ctx) => {
						ctx.player.earnMoney(this.pay, GameState.CommercialActivity.Jobs);
						ctx.player.adjustTokens(this.tokens);
						this._setCompletedState(ctx.player);
					}),
					...(this.taskData.cost ?? []).map(data => new Actions.CostSilentAction(data)),
				],
			})

			return calculated;
		}

		/**
		 * Print the intro to a job.
		 */
		intro(player: Entity.Player): string {
			return PR.tokenizeString(player, this.giver, this.taskData.intro);
		}

		/**
		 * Print the "start" scene of a job.
		 */
		printStart(ctx: Actions.EvaluationContext): string {
			const text = this.taskData.start;
			return text ? "<p>" + this.tokenize(ctx, [], text) + "</p>" : "";
		}

		/**
		 * Print the "end" scene of a job.
		 */
		printEnd(ctx: Actions.EvaluationContext, scenes: CalculatedScene[]): string {
			const text = this.taskData.end;
			return text ? "<p>" + this.tokenize(ctx, scenes, text) + "</p>" : "";
		}

		protected override availableScenes(player: Entity.Player): Scene[] {
			return this.taskData.scenes.map(x => new JobScene(this, player, x));
		}

		protected override createNewState(): Partial<Record<string, GameVariable>> {
			const res = super.createNewState();
			res[JobStdAttribute.TimesCompleted] = 0;
			return res;
		}

		/**
		 * Tokenize a string and return result.
		 */
		protected override tokenize(ctx: Actions.EvaluationContext, scenes: CalculatedScene[], str: string): string {
			if (str === undefined) return "";
			str = str.replaceAll('JOB_PAY', this.printPay(ctx, scenes));

			if (str.includes("JOB_RESULTS")) { // Kludge because replace evaluates function even if no pattern match. Dumb.
				str = str.replaceAll('JOB_RESULTS', this.printJobResults(ctx.conditionContext));
			}
			return super.tokenize(ctx, scenes, str);
		}

		private _cost() {return this.taskData.cost ?? [];}
		private _req() {return this.taskData.requirements ?? [];}

		/**
		 * Match a result to a string and return the colorized version.
		 */
		private _matchResult(tag: string, value: Conditions.ActivatorResult): string {
			const output = "";
			const percent = value.percent;

			const colorize = (a: string): string => a === undefined ? "" : PR.colorizeString(percent, a.slice(2, -2), 100);

			for (const jr of this.taskData.jobResults ?? [])
				if (percent <= jr[tag])
					return jr.text.replaceAll(/@@((?!color:).)+?@@/g, colorize);

			return output;
		}

		/**
		 * Print the "job results" at the end of a job.
		 */
		protected printJobResults(ctx: Conditions.EvaluationContext): string {
			const cKeys = ctx.allTags();
			let results: string[] = [];

			for (const t of cKeys)
				results.push(this._matchResult(t, ctx.tagged(t)));
			results = results.filter(r => r !== "");
			return results.length > 0 ? " " + results.join(" ") : results.join("");
		}

		/**
		 * Calculate the total pay from all scenes for the task.
		 */
		protected printPay(ctx: Actions.EvaluationContext, scenes: CalculatedScene[]): string {
			let pay = this.pay;
			const visitor = new Actions.PayCalculatorVisitor(ctx);
			for (const s of scenes) {
				for (const a of s.actions) {
					a.acceptVisitor(visitor);
				}
			}
			pay += visitor.money;
			return (pay > 0) ? `<span class='item-money'>${pay}</span>` : "";
		}

		private _printTokens(ctx: Actions.EvaluationContext, scenes: CalculatedScene[]): string {
			let tokens = this.tokens;
			const visitor = new Actions.PayCalculatorVisitor(ctx);
			for (const s of scenes) {
				for (const a of s.actions) {
					a.acceptVisitor(visitor);
				}
			}
			tokens += visitor.courtesanTokens;
			return (tokens > 0) ? `<span class='item-courtesan-token'>${tokens}</span>` : "";
		}

		/**
		 * Print all of the items earned in this task
		 */
		/*
		private _SummarizeTask(): string {
			var items: string[] = [];
			var Pay = this.TaskData.PAY;
			var Tokens = this.TaskData.TOKENS;

			if (Pay === undefined) Pay = 0;
			if (Tokens === undefined) Tokens = 0;

			for (const sb of this.Scenes) {
				Pay += sb.RewardItems().Pay;
				Tokens += sb.RewardItems().Tokens;

				for (const ri of sb.RewardItems().Items) {
					var n = Items.SplitId(ri["Name"]);
					var oItem = Items.Factory(n.Category, n.Tag);
					items.push(oItem.Description + " x " + ri["Value"]);
				}
			}

			if (Tokens > 0) {
				items.unshift(`<span class='item-courtesan-token'>${Tokens} courtesan tokens</span>\n`);
			}

			if (Pay > 0) {
				items.unshift(`<span class='item-money'>${Pay} coins</span>\n`);
			}

			if (items.length > 0 || Pay > 0 || Tokens > 0) {
				items.unshift("<span style='color:cyan'>Your receive some items:</span>");
			}

			if (items.length > 0) return items.join("\n") + "\n";
			return "";
		}
*/
	}
	/**
	 * Stores and plays a "scene" from a job.
	*/
	class JobScene extends Scene {
		constructor(job: Job, player: Entity.Player, sceneData: Data.Tasks.Scene) {
			super(job, player, sceneData);
		}

		/**
		 * Tokenize a string
		 */
		protected override tokenize(str: string) {
			// str = str.replace(/JOB_PAY/g, this.Pay());
			return super.tokenize(str);
		}
	}
}
