namespace App.Data {
	const stdXpPoints = {
		[Items.Rarity.Common]: {points: 1, suffixCount: 1},
		[Items.Rarity.Uncommon]: {points: 2, suffixCount: 2},
		[Items.Rarity.Rare]: {points: 4, suffixCount: 3},
		[Items.Rarity.Legendary]: {points: 8, suffixCount: 4},
	};

	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyProperty.Height, a), {
		SHRINK_XP: {knowledge: "Height Down", knowledgeSuffix: '-', pointsFactor: -50, valueFactor: 15, data: stdXpPoints},
		GROW_XP: {knowledge: "Height Up", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 75, data: stdXpPoints},
	}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Hair, a),
		{HAIR_XP: {knowledge: "Hair Growth", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 25, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Face, a),
		{FACE_XP: {knowledge: "Face Prettier", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Bust, a),
		{BUST_XP: {knowledge: "Bust Grow", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyProperty.BustFirmness, a),
		{BUST_FIRMNESS_XP: {knowledge: "Bust Firmness", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Ass, a),
		{ASS_XP: {knowledge: "Ass Grow", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Hips, a),
		{HIPS_XP: {knowledge: "Hips Wider", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Lips, a),
		{LIPS_XP: {knowledge: "Lips Plumper", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Penis, a),
		{PENIS_XP: {knowledge: "Penis Grow", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Balls, a),
		{BALLS_XP: {knowledge: "Balls Grow", knowledgeSuffix: '+', pointsFactor: 50, valueFactor: 50, data: stdXpPoints}}));
	Object.append(effectLibrary, Effects.makeEffects((p, a) => p.adjustBodyXP(BodyPart.Waist, a),
		{WAIST_XP: {knowledge: "Waist Narrower", knowledgeSuffix: '+', pointsFactor: -50, valueFactor: 50, data: stdXpPoints}}));
}
