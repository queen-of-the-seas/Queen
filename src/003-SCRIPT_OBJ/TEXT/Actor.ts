namespace App.Text {
	function make3rdPresentSimple(verb: string): string {
		if (verb === "have") return "has";
		if (verb.endsWith('o')) return verb + 'es';
		//  -s, -se, -ss, -sh, -ch, -x -> es
		if (verb.endsWith('s') ||
			verb.endsWith('se') || verb.endsWith('ss') || verb.endsWith('sh') ||
			verb.endsWith('ch') || verb.endsWith('x')) {
			return verb + 'es';
		}

		if (verb.endsWith('y') && verb.length > 1 && isConsonant(verb.charAt(-2))) {
			return verb.slice(0, -1) + 'ies';
		}
		return verb.length > 0 ? verb + 's' : verb;
	}

	function make2ndPresentSimpleplural(verb: string): string {
		if (verb === "is") return "are"
		return verb;
	}

	export abstract class Actor {
		protected constructor(pr: Pronouns) {
			this._pronouns = pr;
		}

		get name(): string {
			return `<span class=${this.cssClasses().join(' ')}>${this.naming.given}</span>`;
		}

		get pr(): Pronouns {
			return this._pronouns;
		}

		abstract nameFor(actor: Actor): string;

		verb(v: string): string {
			if (v.length === 0) return v;

			const converted = this.convertVerb(v.toLowerCase());
			const vUpper = v.toUpperCase();
			if (v === vUpper) return converted.toUpperCase();
			if (v.startsWith(vUpper[0])) return _.capitalize(converted);
			return converted;
		}

		abstract isSame(other: Actor): boolean;
		abstract get naming(): GameState.CharacterName;
		abstract cssClasses(): string[];
		abstract convertVerb(verb: string): string;

		private readonly _pronouns: Pronouns;
	}

	export class ActorHuman extends Actor {
		constructor(human: Entity.Human, pr?: Pronouns) {
			super(pr ?? Pronouns.get(human.gender, human.name));
			this._entity = human;
			this._coreDesc = new Human.CoreDescImpl(this);
			this._bodyDesc = new Human.BodyDesc(this);
			this._lookDesc = new Human.LookImpl(this);
		}

		override isSame(other: Actor): boolean {
			if (other instanceof ActorHuman) {
				return other.entity === this.entity;
			}
			return false;
		}

		override get naming(): GameState.CharacterName {
			return this._entity.naming;
		}

		override nameFor(actor: Actor): string {
			return actor instanceof ActorHuman && actor._entity === this._entity ? 'I' : this.name;
		}

		// eslint-disable-next-line class-methods-use-this
		override convertVerb(verb: string): string {
			return make3rdPresentSimple(verb);
		}

		// eslint-disable-next-line class-methods-use-this
		override cssClasses(): string[] {
			return this._entity.cssClasses;
		}

		get core(): Human.Core {
			return this._coreDesc;
		}

		get body(): Human.Body {
			return this._bodyDesc;
		}

		get look(): Human.Look {
			return this._lookDesc;
		}

		get entity(): Entity.Human {
			return this._entity;
		}

		private readonly _entity: Entity.Human;
		private readonly _coreDesc: Human.CoreDescImpl;
		private readonly _bodyDesc: Human.BodyDesc;
		private readonly _lookDesc: Human.LookImpl;
	}

	export class ActorPc extends ActorHuman {
		constructor(pc: Entity.Player) {
			super(pc, Pronouns.pc);
		}

		override get name(): string {
			return `<span class=${this.cssClasses().join(' ')}>${this.entity.name}</span>`;
		}

		override nameFor(actor: Actor): string {
			return actor instanceof ActorPc ? super.pr.He : this.name;
		}

		// eslint-disable-next-line class-methods-use-this
		override convertVerb(verb: string): string {
			return make2ndPresentSimpleplural(verb);
		}

		// eslint-disable-next-line class-methods-use-this
		override cssClasses(): string[] {
			return ['pc-text'];
		}
	}

	export class ActorNpc extends Actor {
		constructor(npc: Entity.NPC) {
			super(Pronouns.get(npc.gender));
			this._npc = npc;
		}

		override isSame(other: Actor): boolean {
			return other instanceof ActorNpc && other._npc.name === this._npc.name;
		}

		override get naming(): GameState.CharacterName {
			return {
				given: this._npc.pName,
				original: "",
				nick: "",
			}
		}

		override get name(): string {
			return this._npc.pName;
		}

		override nameFor(actor: Actor): string {
			return actor instanceof ActorNpc && actor._npc === this._npc ? 'I' : this.name;
		}

		// eslint-disable-next-line class-methods-use-this
		override convertVerb(verb: string): string {
			return make3rdPresentSimple(verb);
		}

		// eslint-disable-next-line class-methods-use-this
		override cssClasses(): string[] {
			return ['npc-text'];
		}

		private readonly _npc: Entity.NPC;
	}
}
