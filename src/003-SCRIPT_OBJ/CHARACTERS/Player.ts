namespace App.Entity {
	export class Player extends Human {
		#clothing: Entity.ClothingManager| null = null;
		#inventory: Entity.PlayerInventoryManager | null = null;

		constructor() {
			super();
		}

		// eslint-disable-next-line class-methods-use-this
		protected override get state(): GameState.Player {
			return setup.world.state.character.pc as GameState.Player;
		}

		/**
		 * Sort of a generic skill roll that just uses the base formulas and doesn't inherently
		 * grant xp. Used for arbitrary checks that are derived from meta statistics, etc.
		 */
		static genericRoll(skillValue: number, difficulty: number): number {
			const mu0 = skillValue
			const mu = Math.clamp(mu0, 0, 100);

			const diceRoll = roll(mu, difficulty, 0);
			const modifier = modifierFormula(diceRoll, difficulty);

			if (setup.world.state.debugMode)
				console.debug(
					`GenericRoll(${skillValue},${difficulty}): DiceRoll=${diceRoll}, Mod=${modifier}`);
			return modifier;
		}

		acknowledgeSkillUse(skill: Skills.Any, xpEarned: number): void {
			// Corrupt player for performing sex skill checks.
			switch (skill) {
				case Skills.Sexual.HandJobs:
					this.corruptWillPower(xpEarned, 40);
					break;
				case Skills.Sexual.TitFucking:
					this.corruptWillPower(xpEarned, 50);
					break;
				case Skills.Sexual.BlowJobs:
					this.corruptWillPower(xpEarned, 60);
					break;
				case Skills.Sexual.AssFucking:
					this.corruptWillPower(xpEarned, 70);
					break;
			}
		}

		/**
		 * Performs a skill roll.
		 * @param skillName - Skill to check.
		 * @param difficulty - Test difficulty.
		 * @returns result of check.
		 */

		// TODO: Is this called from anywhere other than SkillRoll? Should I just simplify and move to that method?
		private _skillRoll(skillName: Skills.Any, difficulty: number): number {
			const skillValue = this.statPercent(Stat.Skill, skillName);
			if (skillValue <= 0 && hiddenSkills.has(skillName)) {
				return 0;
			}

			const synergyBonus = this._synergyBonus(skillName);
			const rollBonus = this.rollBonus(Stat.Skill, skillName);

			const diceRoll = roll(skillValue, difficulty, synergyBonus + rollBonus);

			const baseXp = Math.clamp(difficulty - this.stat(Stat.Skill, skillName), 10, 50);
			const xpModifier = modifierFormula(diceRoll, difficulty);
			const xpEarned = Math.ceil(baseXp * xpModifier);

			this.adjustSkillXP(skillName, xpEarned);

			this.acknowledgeSkillUse(skillName, xpEarned);

			if (setup.world.state.debugMode) {
				console.debug("skillRoll(%s,%d): DiceRoll = %d XPMod = %f\n", skillName, difficulty, diceRoll, xpModifier);
			}

			if (this.state.gameStats.skills[skillName] === undefined) {
				this.state.gameStats.skills[skillName] = {success: 0, failure: 0};
			}

			if (xpModifier >= 1) {
				(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[skillName].success += 1;
			} else {
				(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[skillName].failure += 1;
			}

			return xpModifier;
		}

		corruptWillPower(xp: number, difficulty = 50): void {
			xp = Math.abs(xp); // make sure negatives become positives.

			console.debug("corruptWillPower(%d, %d) called", xp, difficulty);

			const perv = this.stat(Stat.Core, CoreStat.Perversion);
			const modifier = 1 - Math.max(0, Math.min(perv / difficulty, 1)); // 0 - 1
			let toWillPowerXP = Math.ceil(xp * modifier) * -1.0;
			const toPerversionXP = Math.ceil((xp * modifier) / 2);

			if (setup.world.state.difficultySetting === GameState.Difficulty.Landlubber) {
				toWillPowerXP = Math.ceil(Math.abs(toWillPowerXP) * 0.8) * -1.0;
			} else if (setup.world.state.difficultySetting === GameState.Difficulty.PatheticLoser) {
				toWillPowerXP = Math.ceil(Math.abs(toWillPowerXP) * 0.7) * -1.0;
			}

			this.adjustXP(Stat.Core, CoreStat.Willpower, toWillPowerXP, 0, true);
			this.adjustXP(Stat.Core, CoreStat.Perversion, toPerversionXP, 0, true);
		}

		/**
		 *
		 * @param type SKILL or STAT
		 * @param name Attribute to check
		 */
		rollBonus<T extends keyof StatTypeStrMap>(type: T, name: StatTypeStrMap[T]): number {
			let bonus = 0;

			if (type === Stat.Skill) {
				bonus += this.hexStrength(GameState.VoodooEffect.PiratesProwess) +
					setup.world.state.difficultySetting * 5;
			}
			bonus += this.getWornSkillBonus(name);;
			return bonus;
		}

		/**
		 * Performs a skill roll with a value amount which is modified when returned. Only works on skills
		 * and will auto generate and apply XP to characters.
		 */
		skillRoll(skillName: Skills.Any, difficulty: number): number;
		skillRoll(skillName: Skills.AnyStr, difficulty: number): number;
		skillRoll(skillName: Skills.Any, difficulty: number): number {
			const modifier = this._skillRoll(skillName, difficulty);
			if (setup.world.state.debugMode) console.debug(`skillRoll: Mod=${modifier},Ret=${modifier}`);
			return modifier;
		}

		/**
		 * Like a skill roll, but doesn't grant xp. Can roll against other stats as well.
		 */
		statRoll<T extends keyof StatTypeMap>(type: T, name: StatTypeMap[T], difficulty: number): number;
		statRoll<T extends keyof StatTypeStrMap>(type: T, name: StatTypeStrMap[T], difficulty: number): number;
		statRoll<T extends keyof StatTypeMap>(type: T, name: StatTypeMap[T], difficulty: number): number {
			const synergyBonus = type === Stat.Skill ? Math.clamp(this._synergyBonus(name as Skills.Any), 0, 100) : 0;
			const rollBonus = this.rollBonus(type, name);

			const diceRoll = roll(this.statPercent(type, name), difficulty, synergyBonus + rollBonus);
			const modifier = diceRoll / difficulty;

			if (setup.world.state.debugMode) {
				console.debug(`statRoll(${name},${difficulty}): diceRoll = ${diceRoll} mod=${modifier}\n`);
			}

			return modifier;
		}

		/**
		 * Checks the SkillSynergy dictionary and adds any skill bonuses to dice rolls on
		 * skill checks. For example, TitFucking gets a bonus for the size of the Players Bust score.
		 */
		private _synergyBonus(skillName: Skills.Any | CoreStat): number{
			const synergy = Data.Lists.skillSynergy[skillName];
			if (!synergy) {
				return 0;
			}
			let bonus = 0;
			for (const s of synergy) {
				bonus += Math.ceil(this.statPercent(s.type, s.name) * s.bonus);
			}
			return bonus;
		}

		/**
		 * Restyle hair.
		 */
		reStyle(): void {
			if (!this.canReStyle()) return;
			this.doStyling(this.equipmentInSlot(ClothingSlot.Wig)?.id ?? this.state.lastUsedHair, this.state.lastUsedMakeup);
			this.adjustCoreStat(CoreStat.Energy, -1);
		}

		/**
		 * Simple routine to check if the player can reapply their style.
		 */
		canReStyle(): boolean {
			if (this.state.core.value[CoreStat.Energy] < 1) return false;
			if (this.state.lastUsedMakeup === this.state.makeup.style) return false;
			const m1 = this.inventory.charges(Items.Category.Cosmetics, Data.CosmeticItem.BasicMakeup);
			const m2 = this.inventory.charges(Items.Category.Cosmetics, Data.CosmeticItem.ExpensiveMakeup);
			const makeup = Data.Lists.makeupStyles[this.state.lastUsedMakeup];
			if ((m1 < makeup.resource1) || (m2 < makeup.resource2)) return false;

			if (this.equipmentInSlot(ClothingSlot.Wig) !== null) return true;
			const lh = this.state.lastUsedHair;
			const hair = Data.Lists.hairStyles[lh as Data.Fashion.HairStyle];
			const h1 = this.inventory.charges(Items.Category.Cosmetics, Data.CosmeticItem.HairAccessories);
			const h2 = this.inventory.charges(Items.Category.Cosmetics, Data.CosmeticItem.HairProducts);
			return ((h1 >= hair.resource1) && (h2 >= hair.resource2));
		}

		/** TODO: THIS ENTIRE AREA IS GARBAGE. REFACTOR IT AND REDO MAKEUP AND HAIRSTYLE STUFF **/

		/**
		 * Style hair and makeup.
		 * @param hairID
		 * @param makeupID
		 */
		doStyling(hairID: Data.Fashion.HairStyle | Data.ItemNameTemplateClothing, makeupID: Data.Fashion.Makeup): void {
			const obj = this.getItemById(hairID as Data.ItemNameTemplateClothing);
			const wig = this.equipmentInSlot(ClothingSlot.Wig);
			if (obj) { // We passed an Item Id and found an item.
				if ((wig === null) || (wig.id !== hairID))
					this.wear(obj);
			} else {
				if (wig) this.remove(wig);

				const hair = Data.Lists.hairStyles[hairID as Data.Fashion.HairStyle];
				assertIsDefined(hair);

				if (this.itemChargesOfType(Data.CosmeticsType.HairTool) >= hair.resource1
					&& this.itemChargesOfType(Data.CosmeticsType.HairTreatment) >= hair.resource2) {
					this.state.hair.style = hairID as Data.Fashion.HairStyle;
					this.state.hair.bonus = this.skillRoll(Skills.Charisma.Styling, hair.difficulty) * hair.style;
					this.state.lastUsedHair = hairID as Data.Fashion.HairStyle;
					this.useItemCharges(Data.CosmeticsType.HairTool, hair.resource1);
					this.useItemCharges(Data.CosmeticsType.HairTreatment, hair.resource2);
				}
			}

			const makeup = Data.Lists.makeupStyles[makeupID];

			if (this.itemChargesOfType(Data.CosmeticsType.BasicMakeup) >= makeup.resource1
				&& this.itemChargesOfType(Data.CosmeticsType.ExpensiveMakeup) >= makeup.resource2) {
				this.state.makeup.style = makeupID;
				this.state.makeup.bonus = this.skillRoll(Skills.Charisma.Styling, makeup.difficulty) * makeup.style;
				this.state.lastUsedMakeup = makeupID;
				this.useItemCharges(Data.CosmeticsType.BasicMakeup, makeup.resource1);
				this.useItemCharges(Data.CosmeticsType.ExpensiveMakeup, makeup.resource2);
			}

			setup.avatar.drawPortrait();
		}

		getStyleSpecRating(spec: Data.Fashion.Style): number {
			return Object.values(this.clothing.equipment).reduce((accumulator, current) => {
				return current ? accumulator + current.categoryBonus(spec) : accumulator;
			}, 0);
		}

		earnMoney(m: number, cat: GameState.AnyIncome): void {
			const mi = Math.ceil(m);
			this.state.gameStats.moneyEarned[cat] += mi;
			this.state.money = Math.max(0, (this.state.money + mi));
		}

		spendMoney(m: number, cat: GameState.CommercialActivity | GameState.SpendingTarget.Unknown): void;
		spendMoney(m: number, cat: GameState.SpendingTarget.Shopping, subcat: Items.Category): void;
		spendMoney(m: number, cat: GameState.AnySpending, subCat?: Items.Category): void {
			const mi = Math.ceil(m);
			if (cat === GameState.SpendingTarget.Shopping) {
				this.state.gameStats.moneySpendings.shopping[subCat!] += mi;
			} else {
				this.state.gameStats.moneySpendings[cat] += mi;
			}
			this.state.money = Math.max(0, (this.state.money - mi));
		}

		adjustTokens(m: number): void {
			const mi = Math.ceil(m);
			console.debug(`AdjustTokens: ${mi}`);
			if (mi > 0) {this.state.gameStats.tokensEarned += mi;}
			console.debug(`state.Tokens=${this.state.tokens}`);
			this.state.tokens = Math.max(0, (this.state.tokens + mi));
			console.debug(`state.Tokens=${this.state.tokens}`);
		}

		randomAdjustBodyXP(amount: number): void {
			this.adjustBodyXP(Object.keys(this.state.body.xp).randomElement(), amount, 0);
		}

		get bodyEffects(): string[] {
			return this.state.bodyEffects ?
				[...Data.naturalBodyEffects, ...this.state.bodyEffects] :
				Data.naturalBodyEffects;
		}

		/**
		 * Add named effect to the list of active body effects
		 */
		addBodyEffect(effectName: string): void {
			if (!this.state.bodyEffects.includes(effectName)) {
				this.state.bodyEffects.push(effectName);
			}
		}

		/**
		 * Removes named effect from the list of active body effects
		 */
		removeBodyEffect(effectName: string): void {
			if (!this.state.bodyEffects.includes(effectName)) {
				this.state.bodyEffects = this.state.bodyEffects.filter(s => s !== effectName);
			}
		}

		// Resting and Sleeping functions.
		override nextDay(): void {
			// keep natural body effects before the clothing ones
			this.applyEffects(this.bodyEffects);

			// Gain 'Knowledge' about worn clothes, log days worn.
			// Apply passive effects on worn items.
			for (const eqItem of Object.values(this.clothing.equipment)) {
				if (!(eqItem instanceof Items.Clothing)) continue;

				if (State.random() > 0.8)
					this.addHistory('clothingKnowledge', eqItem.name, 1); // tracking effect knowledge
				this.addHistory("daysWorn", eqItem.name, 1); // tracking just days worn
				eqItem.applyEffects(this);
				const logMessage = eqItem.learnKnowledge(this);
				if (logMessage)  {
					setup.notifications.addMessage(Notifications.MessageCategory.Knowledge, setup.world.day + 1, logMessage);
				}
			}

			// Tarot card effects and dream.
			const tarotCard = this.flags.TAROT_CARD as string;
			if (tarotCard) {
				const tarot = Data.tarot[tarotCard];
				this.applyEffects(tarot.effects);
				setup.notifications.addMessage(Notifications.MessageCategory.Dreams, setup.world.day + 1, tarot.msg);
				delete this.flags.TAROT_CARD;
			}

			super.nextDay();

			this.state.hair.bonus = 0;
			this.state.makeup.bonus = 0;
			this.state.hair.style = Data.Fashion.HairStyle.BedHead;
			this.state.makeup.style = Data.Fashion.Makeup.Plain;

			// Decrease voodoo effects
			this.endHexDuration();

			// What day are we on our current voyage.
			this.state.sailDays = ((this.state.sailDays + 1) >= Data.Lists.shipRoute.length) ? 0 : (this.state.sailDays + 1);
		} // NextDay

		/**
		 * Move time counter to next phase of day.
		 * @param phases - Number of phases to increment.
		 */
		nextPhase(): void {
			this.adjustCoreStat(CoreStat.Nutrition, -5);
			this.levelStat(Stat.Core, CoreStat.Nutrition);
		}

		rest(): void {
			setup.world.nextPhase(1);
			this.applyEffects(["NATURAL_RESTING"]);
			this.levelStatGroup(Stat.Skill);
		}

		getShipLocation(): {x: number; y: number; passage: string; title: string;} {
			const routes = Data.Lists.shipRoute;
			if (this.state.sailDays >= routes.length) {
				this.state.sailDays = 0; // Shouldn't happen, but fix it if it does.
			}

			const dict = {
				x: routes[this.state.sailDays].left,
				y: routes[this.state.sailDays].top,
				passage: "",
				title: "",
			};

			const titles: {[x: string]: {passage: string, title: string}} = {
				/* eslint-disable @typescript-eslint/naming-convention */
				IslaHarbor: {passage: "IslaHarbor", title: "Isla Harbor"},
				GoldenIsle: {passage: "GoldenIsle", title: "Golden Isle"},
				Abamond: {passage: "Abamond", title: "Abamond"},
				PortRoyale: {passage: "PortRoyale", title: "Port Royale"},
				/* eslint-enable @typescript-eslint/naming-convention */
			};

			const pos = routes[this.state.sailDays].P;
			if (titles.hasOwnProperty(pos)) {
				dict.passage = titles[pos].passage;
				dict.title = titles[pos].title;
			}

			return dict;
		}

		/**
		 * @param n Number of days to look ahead.
		 * @returns
		 */
		isInPort(n = 0): boolean {
			const routes = Data.Lists.shipRoute;
			const days = (this.state.sailDays + n >= routes.length ? 0 : this.state.sailDays + n);
			return (routes[days].P !== "AtSea");
		}

		/**
		 *
		 * @param n Days to advance
		 */
		advanceSailDays(n = 1): boolean {
			if (this.isInPort(0) || this.isInPort(n)) return false;
			const routes = Data.Lists.shipRoute;
			this.state.sailDays = (this.state.sailDays >= routes.length ? n : this.state.sailDays + n);
			return true;
		}

		getWornSkillBonus(skill: Data.WearSkill): number {
			let bonus = 0;
			for (const eq of Object.values(this.clothing.equipment)) {
				if (!eq) {continue;}
				const tBonus = eq.getBonus(skill);
				if (tBonus > 0) {
					bonus += tBonus;
					if (setup.world.debugMode) console.log("Found skill bonus : " + skill + " on" + eq.name);
				}
			}
			return bonus;
		}

		/**
		 * Applies named effects on the player
		 */
		applyEffects(effectNames: string[]): void {
			for (const effectName of effectNames) {
				console.group(`Effect ${effectName}`);
				Data.effectLibrary[effectName].fun(this);
				console.groupEnd();
			}
		}

		private static _notifyBodyChange(bodyPart: BodyStat | CoreStat, step: number): void {
			const c = Data.Lists.bodyChanges[bodyPart];
			if (c) {
				setup.notifications.addMessage(Notifications.MessageCategory.StatChange, setup.world.day + 1, (step < 0 ? c.shrink : c.grow));
			}
		}

		getHistory(type: keyof GameState.PlayerHistory, flag: string): number {
			return this.state.history[type]?.[flag] ?? 0;
		}

		addHistory(type: keyof GameState.PlayerHistory, flag: string, amount: number): void {
			const t = this.getHistory(type, flag);
			this.state.history[type][flag] = (t + amount);
		}

		removeHistory(type: keyof GameState.PlayerHistory, flag: string): void {
			delete this.state.history[type]?.[flag];
		}

		override consumeItem(item: Items.Consumable): string {
			const messageBuffer = item.applyEffects(this);
			this.addHistory("items", item.tag, 1);

			// Knowledge.
			const usages = this.getHistory("items", item.tag);
			const itemKnowledge = item.getKnowledge();

			let output = item.data.message;
			if (usages <= itemKnowledge.length) {
				messageBuffer.push("\n\n@@.state-neutral;You learn something… this item has an effect!@@ " +
					PR.pEffectMeter(itemKnowledge[(usages - 1)]));
			}
			if (messageBuffer.length > 0) output += messageBuffer.join("\n");
			return output === undefined ? "" : PR.tokenizeString(this, undefined, output);
		}

		protected override makeActor() {
			return new Text.ActorPc(this);
		}

		// #region Voodoo
		hasHex(hex: GameState.VoodooEffect): boolean {
			return this.state.voodooEffects.hasOwnProperty(hex);
		}

		setHex(hex: GameState.VoodooEffect, strength: number, duration?: number): void {
			const newHex: GameState.VoodooEffectState = {strength: strength};
			if (duration !== undefined) {
				newHex.duration = duration;
			}
			this.state.voodooEffects[hex] = newHex;
		}

		removeHex(hex: GameState.VoodooEffect): void {
			delete this.state.voodooEffects[hex];
		}

		hexStrength(hex: GameState.VoodooEffect, defaultValue = 0): number {
			return this.state.voodooEffects[hex]?.strength ?? defaultValue;
		}

		endHexDuration(): void {
			for (const [hex, hexState] of Object.entries(this.state.voodooEffects)) {
				if (hexState.duration) {
					if (hexState.duration <= 1) {
						delete this.state.voodooEffects[hex];
						switch (hex) {
							case GameState.VoodooEffect.PiratesProwess:
								setup.notifications.addMessage(Notifications.MessageCategory.StatusChange, setup.world.day + 1, "You feel the effects of your pirates skill leave you…");
								break;
						}
					} else {
						--hexState.duration;
					}
				}
			}
		}
		//#endregion

		// Acquire everything for debug purposes
		acquireAllItems(): void {
			console.group("AcquireAllItems");
			for (const property of Object.keys(Data.clothes)) {
				if (this.ownsWardrobeItem(property)) {
					console.log("\"" + property + "\" (clothes) already owned");
				} else {
					console.log("Adding \"" + property + "\" (clothes)");
					this.addItem(Items.Category.Clothes, property, 1);
				}
			}
			console.groupEnd();
		}

		/**
		 * Returns number that represents how high-class the PC looks.
		 * 0 is obvious commoner and 100 is a rich noble. Can be higher or lower.
		 */
		get highClassPresentability(): number {
			// For now just consider "Slutty Lady" the proper attire,
			// while other clothes contribute only part of their style.
			// Do not count parts that are not visible in a "proper" situation.
			const getBonus = (slot: ClothingSlot, equipmentItem: Items.EquipmentItem) => {
				// The proper attire
				let bonus = equipmentItem.categoryBonus(Data.Fashion.Style.SluttyLady);
				if (bonus > 0) {
					console.log(`Slot: ${slot}, equipment name: "${equipmentItem.name}", bonus: ${bonus}`);
					return bonus;
				}

				// Not so proper, but closer than others
				bonus = equipmentItem.categoryBonus(Data.Fashion.Style.HighClassWhore) / 2;
				if (bonus > 0) {
					console.log(`Slot: ${slot}, equipment name: "${equipmentItem.name}", bonus: ${bonus}`);
					return bonus;
				}

				// Oh well
				bonus = equipmentItem.styleBonus / 4;
				console.log(`Slot: ${slot}, equipment name: "${equipmentItem.name}", bonus: ${bonus}`);
				return bonus;
			};

			console.group("HighClassPresentability");
			let result = 0;

			const underwearSlots = new Set([ClothingSlot.Bra, ClothingSlot.Nipples, ClothingSlot.Panty, ClothingSlot.Butt, ClothingSlot.Penis]);
			for (const [slot, equipmentItem] of Object.entries(this.clothing.equipment)) {
				if (equipmentItem && !underwearSlots.has(slot)) {
					result += getBonus(slot, equipmentItem);
				}
			}

			console.log(`Total for clothes: ${result}`);

			const forHair = this.hairRating * 0.25;
			const forMakeup = this.makeupRating * 0.25;
			console.log(`For hair: ${forHair}`);
			console.log(`For makeup: ${forMakeup}`);

			result += forHair + forMakeup;
			console.log(`Total: ${result}`);
			console.groupEnd();
			return result;
		}

		// TODO: include other factors and maybe calibrate.
		/**
		 * Returns number that represents how obvious it is that the PC is male.
		 *  100 means completely impassable and 0 means completely passable. Can be higher or lower.
		 */
		get obviousTrappiness(): number {
			console.group("ObviousTrappiness");
			// console.log(this.state_.body.value);
			// console.log(this.state_.core.value);

			// Huge penis makes trappiness very, very obvious
			const penisOversizing = Math.max(this.state.body.value.penis - 75, 0);
			const penisContribution = penisOversizing * penisOversizing;
			console.log(`penisContribution: ${penisContribution}`);

			// Let's say DD breasts give -50, and we have diminishing returns
			const bustContribution = -Math.sqrt(this.state.body.value.bust * 50 * 50 / 11);
			console.log(`bustContribution: ${bustContribution}`);

			// Style gives a small contribution, but synergizes with femininity
			const styleContribution = - this.clothesRating * .1;
			console.log(`styleContribution: ${styleContribution}`);

			// Femininity is important
			const femininityContribution = - this.state.core.value.femininity * .3;
			console.log(`femininityContribution: ${femininityContribution}`);

			// Full femininity and full style give -50 together
			const femininityStyleSynergy = - this.state.core.value.femininity * this.clothesRating * .005;
			console.log(`femininityStyleSynergy: ${femininityStyleSynergy}`);

			// Base value is full
			let result = 100 + penisContribution + bustContribution + styleContribution +
				femininityContribution + femininityStyleSynergy;
			// Let's make sure result is not too far from soft borders
			if (result > 100) result = 100 + Math.sqrt(result - 100);
			if (result < 0) result = -Math.sqrt(-result);

			console.log(`result: ${result}`);
			console.groupEnd();

			return result;
		}

		// region SLOT wheel stuff

		/**
		 * Unlock a slot.
		 */
		unlockSlot(): boolean {
			if (this.state.currentSlots + 1 > this.maxSlots) return false;
			this.state.currentSlots++;
			return true;
		}

		/**
		 * Fetch all reels in the players inventory.
		 */
		getReelsInInventory(): Items.Reel[] {
			return this.inventory.filter(o => o instanceof Items.Reel) as Items.Reel[];
		}

		/**
		 * Fetch a single reel by ID.
		 */
		getReelByID(filterID: string): Items.Reel | null {
			return this.getReelsInInventory().find(o => o.id === filterID) ?? null;
		}

		/**
		 * Attempt to pick a reel from inventory by Id and then equip it to a slot. It will remove any reel
		 * equipped in that slot and place it back in the inventory.
		 */
		equipReel(toEquipID: Data.ItemNameTemplate<Items.Category.Reel>, reelSlot: number): void {
			this.inventory.equipReel(toEquipID, reelSlot);
		}

		/**
		 * Remove an equipped reel and place it in the inventory.
		 */
		removeReel(slotID: number): void {
			this.inventory.removeReel(slotID);
		}

		/**
		 * Turn the equipped reels into an array to iterate/read.
		 */
		getReels(): Items.Reel[] {
			return this.inventory.equippedReelItems();
		}

		// endregion

		override get inventory(): PlayerInventoryManager {
			if (!this.#inventory) {
				this.#inventory = new Entity.PlayerInventoryManager();
			}
			return this.#inventory;
		}

		get clothing(): Entity.ClothingManager {
			if (!this.#clothing) {
				this.#clothing = new Entity.ClothingManager();
			}
			return this.#clothing;
		}

		override adjustCoreStat(statName: CoreStat, amount: number): void;
		override adjustCoreStat(statName: CoreStatStr, amount: number): void;
		override adjustCoreStat(statName: CoreStat, amount: number) {
			super.adjustCoreStat(statName, amount);
			Player._notifyBodyChange(statName, amount);
		}

		override adjustBody(statName: BodyStat, amount: number): void;
		override adjustBody(statName: BodyStatStr, amount: number): void;
		override adjustBody(statName: BodyStat, amount: number): void {
			super.adjustBody(statName, amount);
			Player._notifyBodyChange(statName, amount);
		}

		override adjustSkill(statName: Skills.Any, amount: number): void;
		override adjustSkill(statName: Skills.AnyStr, amount: number): void;
		override adjustSkill(statName: Skills.Any, amount: number): void {
			super.adjustSkill(statName, amount);
			const statTitle = PR.pSkillName(statName);
			const adjective = amount < 0 ? " skill decreases! How did this happen!?" : " skill improves!";
			setup.notifications.addMessage(Notifications.MessageCategory.Knowledge, setup.world.day + 1,
				"Your " + PR.colorizeString(this.statPercent(Stat.Skill, statName), statTitle) + adjective);
		}

		// redirections for the state properties
		get money(): number {return this.state.money;}
		/**
		 * @deprecated
		 * Use earnMoney() / spendMoney() instead
		 */
		set money(c: number) {this.state.money = c;}
		get tokens(): number {return this.state.tokens;}
		set tokens(c: number) {this.state.tokens = c;}
		get sailDays(): number {return this.state.sailDays;}
		get lastUsedMakeup(): Data.Fashion.Makeup {return this.state.lastUsedMakeup;}
		get lastUsedHair(): Data.Fashion.HairStyle | Data.ItemNameTemplateClothing {return this.state.lastUsedHair;}
		get lastQuickWardrobe(): string {return this.state.lastQuickWardrobe;}

		taskState(type: TaskType): Partial<Record<string, GameState.TasksState>> {
			switch (type) {
				case TaskType.Job:
					return this.state.tasks.job;
				case TaskType.Quest:
					return this.state.tasks.quest;
			}
		}

		increaseFlagValue(flag: string, delta = 1) {
			this.flags[flag] = GameVariables.add(delta, this.flags[flag]);
		}

		get history(): GameState.PlayerHistory {return this.state.history;}

		// Player Statistic Variables

		get slots(): Record<number, Items.Reel | null> {return this.inventory.reelSlots();}
		get currentSlots(): number {return this.state.currentSlots;} // Starting allocation of whoring

		// eslint-disable-next-line class-methods-use-this
		get maxSlots(): number {return 9;} // YOU SHALL NOT PASS

		saveLoaded(): void {
			this.#inventory = null;
			this.#clothing = null;
		}

		get gameStats(): GameState.GameStats {return this.state.gameStats;}
	}

	function statRollStddev(statOrSkillValue: number, difficulty: number): number {
		return difficulty < statOrSkillValue ? 2.5 : 5;
	}

	function roll(statOrSkillValue: number, difficulty: number, bonus: number): number {
		// idea: mu = skillValue
		const sigma = statRollStddev(statOrSkillValue, difficulty);
		return Utils.gaussianPair(statOrSkillValue + bonus, sigma).randomElement();
	}

	function modifierFormula(value: number, target: number): number {
		return Math.clamp(value / target, 0.1, 2);
	}

	/** These skills do not work and do not gain XP points until their value is set to non-zero */
	const hiddenSkills: Set<Skills.Any> = new Set([Skills.Charisma.Courtesan, Skills.Sexual.AssFu, Skills.Sexual.BoobJitsu]);
}
