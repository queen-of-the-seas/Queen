/* eslint-disable @typescript-eslint/naming-convention */
App.registerPassageDisplayNames({
	GirlfriendRoom: () => setup.world.character(App.Entity.CharacterId.Girlfriend).name + "'s Room",
	GovernorStudy: player => App.Quest.isCompleted(player, "DADDYSGIRL") ? "@@.state-girlinness;Daddy's room@@" : "Study",
	IslaFarm: "Farmlands",
	IslaHarbor: "The Docks",
	IslaMansion: "Governors Mansion",
	IslaStore: "General Store",
	IslaTavern: "Tavern",
	IslaTown: "Town Square",
	IslaVillageLane: "Village Lane",
});

App.registerTravelDestination({
	IslaHarbor: [
		{
			destination: "Deck",
			caption: "Back to The Mermaid",
		},
		"IslaTown", "IslaTavern",
	],
	IslaTavern: ["IslaHarbor"],
	IslaTown: [
		"IslaHarbor", "IslaVillageLane",
		{
			destination: "IslaStore",
			enabled: [{type: 'dayPhase', max: App.DayPhase.Evening}],
		},
		{
			destination: "IslaMansion",
			enabled: {
				op: "||",
				args: [
					{type: 'dayPhase', max: App.DayPhase.Evening},
					{type: 'quest', name: 'THEBACKDOOR', property: "status", value: App.QuestStatus.Completed},
				],
			},
		},
	],
	IslaVillageLane: ["IslaTown", "IslaFarm"],
	IslaFarm: [
		"IslaVillageLane",
		{
			destination: null,
			caption: "It's too embarrassing to go home",
		},
	],
	IslaStore: ["IslaTown"],
	IslaMansion: [
		"IslaTown",
		{
			// guarded until night
			available: [
				{type: 'dayPhase', max: App.DayPhase.Evening},
				{type: 'quest', name: "DADDYSGIRL", property: "status", value: App.QuestStatus.Completed, condition: '!='},
			],
			caption: () => setup.world.character(App.Entity.CharacterId.Girlfriend).naming.given + "'s Room",
			scene: "islaHarborGuardedGirlfriendRoom",
		},
		{
			// guarded until night
			available: [
				{type: 'dayPhase', max: App.DayPhase.Evening},
				{type: 'quest', name: "DADDYSGIRL", property: "status", value: App.QuestStatus.Completed, condition: '!='},
			],
			caption: "Study",
			scene: "islaHarborGuardedStudy",
		},
		{
			available: {
				op: '||',
				args: [
					{type: 'dayPhase', min: App.DayPhase.Night},
					{type: 'quest', name: "DADDYSGIRL", property: "status", value: App.QuestStatus.Completed, condition: '=='},
				],
			},
			destination: "GovernorStudy",
		},
		{
			available: {
				op: '||',
				args: [
					{type: 'dayPhase', min: App.DayPhase.Night},
					{type: 'quest', name: "DADDYSGIRL", property: "status", value: App.QuestStatus.Completed, condition: '=='},
				],
			},
			destination: "GirlfriendRoom",
		},
	],
	GovernorStudy: ["IslaMansion"],
	GirlfriendRoom: ["IslaMansion"],
});

/* eslint-enable @typescript-eslint/naming-convention */
