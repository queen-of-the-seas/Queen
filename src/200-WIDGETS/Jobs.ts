namespace App {
	Macro.add("TaggedJobs", {
		handler() {
			const npc = this.args.length > 1 ? setup.world.npc(this.args[1] as string) : undefined;

			this.output.append(App.UI.Widgets.rJobList(setup.world, Job.byTag(this.args[0] as string, npc)));
		},
	});
}
