namespace App.Utils {
	/**
	 * replaces special HTML characters with their '&xxx' forms
	 */
	export function escapeHtml(text: string): string {
		/* eslint-disable @typescript-eslint/naming-convention */
		const map: Record<string, string> = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#039;',
		};
		/* eslint-enable @typescript-eslint/naming-convention */
		return text.replaceAll(/["&'<>]/g, m => map[m]);
	}

	export function gaussianPair(mu: number, sigma: number): [number, number] {
		let u = 2 * State.random() - 1;
		let v = 2 * State.random() - 1;
		let s = u * u + v * v;
		while (s === 0 || s > 1) {
			u = 2 * State.random() - 1;
			v = 2 * State.random() - 1;
			s = u * u + v * v;
		}

		const f = Math.sqrt(-2 * Math.log(s) / s) * sigma;
		return [u * f + mu, v * f + mu];
	}
}

namespace App {
	export function isDefined<T>(v: T): v is NonNullable <T> {
		return v !== undefined && v !== null;
	}

	export function assertIsDefined<T>(value: T): asserts value is NonNullable<T> {
		if (!isDefined(value)) {
			throw `Expected 'val' to be defined, but received ${value}`; // eslint-disable-line @typescript-eslint/restrict-template-expressions
		}
	}

	export function ensureIsDefined<T>(value: T): NonNullable<T> {
		if (isDefined(value)) return value;
		throw new Error('ensureIsDefined() encountered nullish value');
	}

	export function assertIsFinalRating(value: Data.RatingValue): asserts value is Data.RatingFinalValue {
		if (!(typeof value === "string" || Array.isArray(value))) {
			throw new TypeError("rating value is not final");
		}
	}

	export function valueOrDefault<T>(v: T, defaultValue: NonNullable<T>): NonNullable<T> {
		return isDefined(v) ? v : defaultValue;
	}

	export function assertUnreachable(x: never): never {
		// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
		throw new Error(`Reached supposed to be unreachable code with argument value of ${x}`)
	}

	export function registerTravelDestination(destinations: Record<string, Data.Travel.Destinations>): Record<string, Data.Travel.Destinations> {
		return Object.append(App.Data.Travel.destinations, destinations);
	}

	export function registerPassageDisplayNames(names: Record<string, DynamicText>): Record<string, DynamicText> {
		return Object.append(App.Data.Travel.passageDisplayNames, names);
	}

	interface PassageLink {
		link: string;
		text?: string;
		setter?: string;
	}

	const sc2LinkSimple = /\[\[([^\|^(?:\->)]+)(?:\||(?:\->))?(?:([^\]]+))?\](?:\[(.+)\])?\]/;

	export function parseSCPassageLink(s: string): PassageLink {
		const m = sc2LinkSimple.exec(s);
		if (!m) {
			throw `Link parsing error: '${s} does not look like a passage link to me`;
		}
		return {link: m[1], text: m[2], setter: m[3]};
	}

	/**
	 * Returns random element from a weighted table
	 */
	export function randomProperty<T extends PropertyKey>(choices: Partial<Record<T, number>>): EnumerablePropertyKey<T> {
		const totalWeight = _.sum(Object.values(choices));
		let rnd = State.random() * totalWeight;
		const res = Object.entries(choices).find((entry) => {
			rnd -= entry[1];
			return rnd <= 0;
		})
		if (!res) {
			throw new Error("Could not find anything in the choices object! Perhaps it's empty?")
		}
		return res[0];
	}

	export function splitSlashed<T1 extends string, T2 extends string>(id: `${T1}/${T2}`): [T1, T2] {
		const parts = id.split('/');
		return [parts[0] as T1, parts[1] as T2];
	}

	export function splitUnderscored<T1 extends string, T2 extends string>(id: `${T1}_${T2}`): [T1, T2] {
		const parts = id.split('_');
		return [parts[0] as T1, parts[1] as T2];
	}

	export function makeCompoundName<C extends string, T extends string>(c: C, t: T): `${C}/${T}` {
		return `${c}/${t}`;
	}

	export function splitCompoundName<C extends string, T extends string>(n: `${C}/${T}`): [C, T]  {
		return n.split('/') as [C, T];
	}

	export function compareDateTime(left: DateTime, right: DateTime): number {
		if (left.day !== right.day) {return left.day - right.day;}
		return left.phase - right.phase;
	}

	export namespace GameVariables {
		export function add(a: GameVariable, b?: GameVariable): GameVariable {
			if (!isDefined(b)) {
				return a;
			}
			if (typeof a === typeof b) {
				switch (typeof a) {
					case 'number':
					case 'string':
						// @ts-expect-error TS can't see that types of a and b are equal
						// eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/restrict-plus-operands
						return a + b;
					default:
						throw new Error(`Could not add values of type ${typeof a}`);
				}
			} else {
				throw new TypeError(`Could not add ${typeof a} to ${typeof b}`);
			}
		}

		export function toString<T extends GameVariable>(v: T | readonly T[], converter?: (v: T) => string): string {
			if (Array.isArray(v)) { // TODO readonly T[]
				return v.map(av => toString(av, converter)).join(',');
			}
			return converter ? converter(v) : `${v}`;
		}
	}
}
